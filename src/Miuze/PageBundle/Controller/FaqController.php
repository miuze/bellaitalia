<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;


class FaqController extends BaseController
{
    /**
     * @Route(
     *     "/faq,{pageid},{slug}.html/{pager}",
     *      name="faq",
     *      defaults = {"pager" = 1},
     *      requirements={"page": "\d+"}
     * )
     */
    public function indexAction(Request $request)
    {
        $this->getPage($request);
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:FaqCategory')->getCategoriesFaqPage(array('page' => $this->page->getId()));
        return $this->view('@MiuzePage/Faq/index.html.twig', array('list' => $list));
    }
    
}
