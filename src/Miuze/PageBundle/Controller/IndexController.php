<?php

namespace Miuze\PageBundle\Controller;

use Miuze\AdminBundle\Manager\PageManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Miuze\AdminBundle\Entity\Page;

use Miuze\AdminBundle\Entity\Contact;
use Miuze\AdminBundle\Form\Contact\ContactType;

class IndexController extends Controller
{
    /**
     * @Route(
     *      "/{_locale}",
     *      name="index",
     *      requirements={"_locale": "[a-z]{2}"},
     *      defaults={"_locale"="pl"},
     * )
     */
    public function indexAction(Request $request){
        $homepageId = $this->container->getParameter('home_page');
        $page = $this->get(PageManager::class)->getFrontPage($homepageId);

        return $this->render('@MiuzePage/Index/index.html.twig', array(
            'page' => $page,
        ));
    }
    

    /**
     * @Route(
     *      "/lang/{_locale}",
     *      defaults={"_locale" = "pl"},
     *      requirements={"_locale": "[a-z]{2}"},
     *      name="index_lang"
     * )
     */
    public function langAction(Request $request)
    {
        $locale = $request->get('_locale');
        $session = $this->get('session');
        if($session->has('_locale')){
             $session->set('_locale', $locale);
        }else{
            $session->set('_locale', $locale);
        }
        return $this->redirect($this->generateUrl('index', array('_locale'=> $locale)));
    }
}
