<?php

declare(strict_types=1);

namespace Miuze\PageBundle\Controller;

use Miuze\AdminBundle\Entity\GalleryCategory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class GalleryController extends BaseController
{
    /**
     * @Route(
     *     "/gallery,{pageid},{slug}.html/{page}",
     *      name="gallery",
     *      defaults = {"page" = 1},
     *      requirements={"pageid": "\d+"}
     * )
     */
    public function indexAction(Request $request): Response
    {
        $this->getPage($request);
        $list = $this->getDoctrine()->getRepository(GalleryCategory::class)->getCategoriesGalleryPage(array('page' => $this->page->getId()));

        return $this->view('@MiuzePage/Gallery/index.html.twig', array('list' => $list));
    }
    
    /**
     * @Route(
     *      "/gallery,{pageid}/{id},{title}.html",
     *      name="gallery_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request): Response
    {
        $this->getPage($request);
        $id =  $request->attributes->getInt('id');
        $gallery = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Gallery')->getGalleryPhoto(array('gallery' => $id));

        return $this->view('@MiuzePage/Gallery/read.html.twig', array('gallery' => $gallery));
    }
}
