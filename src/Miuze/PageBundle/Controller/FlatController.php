<?php

namespace Miuze\PageBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;

class FlatController extends BaseController
{
    /**
     * @Route(
     *     "/{_locale}/flat,{pageid},{slug}.html/{page_number}",
     *      name="flat",
     *      defaults = {"_locale"="pl", "page_number" = 1},
     *      requirements={"pageid": "\d+", "page_number": "\d+"}
     * )
     */
    public function indexAction(Request $request)
    {
        $this->getPage($request);
        $repo = $this->getDoctrine()->getRepository("MiuzeAdminBundle:Flat");
        $query = $repo->getFlatPage(array(
            'page' => $this->page->getId(),
        ));
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->attributes->getInt('page_number'),
            12
        );
        return $this->view('@MiuzePage/Flat/index.html.twig', array('pagination' => $pagination));
        
    }
    
    
    /**
     * @Route(
     *      "/{_locale}/flat,{pageid}/{id},{title}.html",
     *      name="flat_read",
     *      requirements={"pageid": "\d+", "id": "\d+", "_locale"="[a-z]{2}"}
     * )
     */
    public function readAction(Request $request)
    {
        $this->getPage($request);
        $id = $request->attributes->getInt('id');
        $flat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Flat')->getFlatDetails(array('id' => $id));
        return $this->view('@MiuzePage/Flat/read.html.twig', array('flat' => $flat));
    }
    
}
