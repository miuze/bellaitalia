<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;


class SubpageListController extends BaseController
{
    /**
     * @Route(
     *     "/subpages,{pageid},{slug}.html/{page}",
     *      name="subpagelist",   
     *      defaults = {"pager" = 1},
     *      requirements={"page": "\d+"}
     * )
     */
    public function indexAction(Request $request)
    {
        
        $pages = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Page')->findSubpages($this->page->getId());
        $this->view('MiuzePage/Save/index.html.twig', array('pages' => $pages));
    }
    
}
