<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;

class PlaceController extends BaseController
{
    /**
     * @Route(
     *     "/place,{pageid},{slug}.html/{page}",
     *      name="place",
     *      defaults = {"page" = 1},
     *      requirements={"pageid": "\d+"}
     * )
     */
    public function indexAction(Request $request)
    {
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PlaceCategory')->getCategoriesPlacePage(array('page' => $pageid));
        $this->view('MiuzePage/Place/index.html.twig', array('list' => $list));
    }
    
    /**
     * @Route(
     *      "/place,{pageid}/{id}.html",
     *      name="place_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $place = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Place')->findOneBy(array('id' => $id));
        $this->view('MiuzePage/Place/read.html.twig', array('place' => $place));
    }
}
