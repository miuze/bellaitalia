<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;


class VideoController extends BaseController
{
    /**
     * @Route(
     *     "/video,{pageid},{slug}.html/{page}",
     *      name="video",
     *      defaults = {"page" = 1},
     *      requirements={"pageid": "\d+"}
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:VideoCategory')->getCategoriesVideoPage(array('page' => $this->page->getId()));
        $this->view('MiuzePage/Video/index.html.twig', array('list' => $list));
    }
    
    /**
     * @Route(
     *      "/video,{pageid}/{id},{title}.html",
     *      name="video_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $video = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Video')->findOneBy(array('id' => $id));
        $this->view('MiuzePage/Video/read.html.twig', array('video' => $video));
    }
}
