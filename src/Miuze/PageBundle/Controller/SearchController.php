<?php

namespace Miuze\PageBundle\Controller;

use Miuze\AdminBundle\Entity\Flat;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;
use Miuze\AdminBundle\Form\Flat\SearchType;



class SearchController extends BaseController
{
    /**
     * 
     * @param Request $request
     * @return type
     * @Route(
     *      "/{pageid},{slug}.html/page-{page_number}?search={params}",
     *      name="search",
     *      defaults = {"page_number"= 1, "params" = 1},
     *     requirements={"page_number": "\d+"}
     * )
     */
    public function indexAction(Request $request) {


        $this->getPage($request);
        $error = NULL;
        if ($request->getMethod() == 'GET') {
            $query = $request->get('search');

            $em = $this->getDoctrine()->getManager();
            $qb = $em->createQueryBuilder();
            $qb->select('f, b')
                ->from('MiuzeAdminBundle:Flat', 'f')
                ->leftJoin('f.build', 'b');
            if(!empty($query["address"])){
                $qb->andWhere('b.address LIKE :address')
                    ->setParameter('address', "%".$query["address"]."%");
            }
            if(!empty($query["room"])){
                $qb->andWhere('f.room = :room')
                    ->setParameter('room', $query["room"]);
            }
            if(!empty($query["minArea"])){
                $qb->andWhere('f.area >= :minArea')
                    ->setParameter('minArea', $query["minArea"]);
            }
            if(!empty($query["maxArea"])){
                $qb->andWhere('f.area <= :maxArea')
                    ->setParameter('maxArea', $query["maxArea"]);
            }
            if(!empty($query["bathroom"])){
                $qb->andWhere('f.bathroom = :bathroom')
                    ->setParameter('bathroom', $query["bathroom"]);
            }
            if(!empty($query["garage"])){
                $qb->andWhere('f.garage = :garage')
                    ->setParameter('garage', $query["garage"]);
            }
            if(!empty($query["minPrice"])){
                $minPrice = $query["minPrice"] * 1000;
                $qb->andWhere('f.price >= :minPrice')
                    ->setParameter('minPrice', $minPrice);
            }
            if(!empty($query["maxPrice"])){
                $maxPrice = $query["maxPrice"] * 1000;
                $qb->andWhere('f.price <= :maxPrice')
                    ->setParameter('maxPrice', $maxPrice);
            }

            $result = $qb->getQuery();

        }
        if(empty($result)){
            $error = 'Przykto nam ale nie znaleźliśmy mieszkania o podanych parametrach.';
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $result,
            $request->attributes->getInt('page_number'),
            12
        );
        
        return $this->view('@MiuzePage/Search/search.html.twig', array(
            'error' => $error,
            'result' => $pagination,
        ));
    }
}