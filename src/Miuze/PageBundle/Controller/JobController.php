<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;
use Miuze\AdminBundle\Form\Job\CvType;
use Miuze\AdminBundle\Entity\Cv;

class JobController extends BaseController
{
    /**
     * @Route(
     *      "/job,{pageid},{title}.html",
     *      name="job"
     * )
     */
    public function indexAction(Request $request){
        $this->view('MiuzePage/Job/read.html.twig');
    }
    
    /**
     * @Route(
     *      "/job,{pageid}/{id},{title}.html",
     *      name="job_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request){

        $this->view('MiuzePage/Job/read.html.twig');
    }
    
    
    /**
     * @Route(
     *      "/{_locale}/job-send,{pageid},{title}.html",
     *      name="job_send",
     *      defaults = {"_locale"="pl"},
     *      requirements={"page": "\d+"}
     * )
     */
    public function sendCvAction(Request $request){
        
        $cv = new Cv();
        $form = $this->createForm(CvType::class, $cv);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $session = $this->get('session');
                $cv->preUpload();
                $cv->upload();
                $mailer = $this->get('miuze_page_job_mailer');
                $htmlBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:cv.html.twig', array(
                    'cv' => $cv
                ));
                $htmlAdminBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:cvAdmin.html.twig', array(
                    'cv' => $cv
                ));
                $mailer->sendAdmin($cv, 'Prośba o wycenę', $htmlAdminBody);
                $mailer->send($cv, 'Dziękujemy za wiadomość', $htmlBody);
                $session->getFlashBag()->add('success', 'Wiadomość została wysłana, kopia została wysłana na podany adres e-mail');
            }
        }
        $this->view('MiuzePage/Job/read.html.twig', array('form' => $form->createView()));
    }
}
