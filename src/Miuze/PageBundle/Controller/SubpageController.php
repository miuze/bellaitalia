<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Miuze\AdminBundle\Entity\Page;
use Miuze\PageBundle\Controller\BaseController;
use Miuze\AdminBundle\Entity\Contact;
use Miuze\AdminBundle\Form\Contact\ContactType;

class SubpageController extends BaseController
{
    /**
     * @Route(
     *      "/{_locale}/{slug}/{page_number}",
     *      name="subpage",
     *      defaults={"_locale"="pl", "page_number"= 1},
     *      requirements={"pageid": "\d+", "_locale": "[a-z]{2}"}
     * )
     */
    public function indexAction(Request $request)
    {
        $this->getPage($request);

        if ($this->page->getType() !== 'subpage'){
            return $this->forward(
                'Miuze\PageBundle\Controller\\' . ucfirst($this->page->getType()) . 'Controller::indexAction',
                [
                    'pageid' => $this->page->getId(),
                    'slug' => $this->page->getSlug()->getSlug(),
                    'page_number' => $request->attributes->getInt('page_number', 1)
                ]
            );
        }

        return $this->view('@MiuzePage/Subpage/index.html.twig');
    }
}
