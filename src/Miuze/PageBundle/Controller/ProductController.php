<?php

namespace Miuze\PageBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;

class ProductController extends BaseController
{
    /**
     * @Route(
     *     "/{_locale}/product,{pageid},{title},page-{page}.html/{cat}",
     *      name="product",
     *      defaults = {"page" = 1, "cat" = 0, "_locale"="pl"},
     *      requirements={"pageid": "\d+"}
     * )
     */
    public function indexAction(Request $request)
    {
        $cat = $request->attributes->getInt('cat');
        $page = $request->attributes->getInt('page');

        $repo = $this->getDoctrine()->getRepository("MiuzeAdminBundle:ProductCategory");
        $category = $repo->findOneById($cat);

        if($cat == 0) {
            $breadcrumbs = array();
            $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product')->getProductsPage(array('page' => $this->page->etId()));
        }else{
            $breadcrumbs = $repo->getPath($category);
            $ids = [];
            $childrens = $repo->getChildren($category);
            foreach($childrens as $item) {
                array_push($ids, $item->getId());
            }
            if(empty($ids)){
                $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product')->getProductCat(array('cat' => array($cat)));
            }else{
                $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product')->getProductCat(array('cat' => $ids));
            }
        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $list,
            $page ,
            16
        );
        $this->view('MiuzePage/Product/index.html.twig', array(
            'breadcrumbs' => $breadcrumbs,
            'pagination' => $pagination,
            'thiscat' => (!isset($thiscat) ? null : $thiscat),
        ));
    }
    
    
    /**
     * @Route(
     *      "/{_locale}/product,{pageid}/{id},{title}.html",
     *      name="product_read",
     *      requirements={"pageid": "\d+", "id": "\d+", "_locale"="[a-z]{2}"}
     * )
     */
    public function readAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $product = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product')->getProductDetails(array('id' => $id));
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductCategory');
        $category = $product->getCategory();
        if($category == null) {
            $breadcrumbs = array();
        }else{
            $breadcrumbs = $repo->getPath($category);
        }
        $productService = $this->get('product');
        $productService->cntIncrement($product);

        $this->get('product')->addProductLastView($product);

        $this->view('MiuzePage/Product/read.html.twig', array(
            'breadcrumbs' => $breadcrumbs,
            'product' => $product,
            'catId' => $product->getCategory()->getId(),
        ));
    }
}
