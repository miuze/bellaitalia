<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;


class DownloadController extends BaseController
{
    /**
     * @Route(
     *     "/download,{pageid},{slug}.html/{page}",
     *      name="download",
     *      defaults = {"page" = 1},
     *      requirements={"pageid": "\d+"}
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadCategory')->getCategoriesDownloadPage(array('page' => $this->page->getId()));
        $this->view('MiuzePage/Download/index.html.twig', array('list' => $list));
    }
    
    /**
     * @Route(
     *      "/download,{pageid}/{id},{title}.html",
     *      name="download_read",
     *      requirements={"page": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadCategory')->getCategoriesDownloadPage(array('page' => $this->page->getId()));
        $item = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Download')->findOneBy(array('id' => $id));
        $this->view('MiuzePage/Download/read.html.twig', array('list' => $list));
    }
}
