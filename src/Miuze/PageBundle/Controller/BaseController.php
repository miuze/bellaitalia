<?php

declare(strict_types=1);

namespace Miuze\PageBundle\Controller;

use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Manager\PageManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller {

    public $page;

    /**
     * Gets a page from request parameter id.
     *
     *
     * @return mixed
     */
    public function getPage(Request $request): Page
    {
        $pageSlug = $request->attributes->get('slug');
        $page = $this->get(PageManager::class)->getFrontPage($pageSlug);
        $this->page = $page;

        return $page;
    }

    public function view($template, $params = array()): Response
    {
        $params['page'] = $this->page;
        $view = $this->renderView($template, $params);
        return new Response($view);
    }
}
