<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;


class ReferenceController extends BaseController
{
    /**
     * @Route(
     *     "/reference,{pageid},{slug}.html/{page}",
     *      name="reference",
     *      defaults = {"page" = 1},
     *      requirements={"pageid": "\d+"}
     * )
     */
    public function indexAction($pageid, $page)
    {
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ReferenceCategory')->getCategoriesReferencePage(array('page' => $pageid));
        $this->view('MiuzePage/Reference/index.html.twig', array('list' => $list));
    }
    
    /**
     * @Route(
     *      "/reference,{pageid}/{id},{title}.html",
     *      name="reference_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $reference = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Reference')->findOneBy(array('id' => $id));
        $this->view('MiuzePage/Reference/read.html.twig', array('reference' => $reference));
    }
}
