<?php

namespace Miuze\PageBundle\Controller;

use Miuze\AdminBundle\Entity\PersonCategory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;

class PersonController extends BaseController
{
    /**
     * @Route(
     *     "/person,{pageid},{slug}.html/{page}",
     *      name="person",
     *      defaults = {"page" = 1},
     *      requirements={"pageid": "\d+"}
     * )
     */
    public function indexAction(Request $request)
    {
        $this->getPage($request);
        $list = $this->getDoctrine()->getRepository(PersonCategory::class)->getCategoriesPersonPage(array('page' => $this->page->getId()));
        return $this->view('@MiuzePage/Person/index.html.twig', array('list' => $list));
    }
    
    /**
     * @Route(
     *      "/person,{pageid}/{id}.html",
     *      name="person_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request)
    {
        $this->getPage($request);
        $id = $request->attributes->getInt('id');
        $person = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Person')->findOneBy(array('id' => $id));
        return $this->view('MiuzePage/Person/read.html.twig', array('person' => $person));
    }
}
