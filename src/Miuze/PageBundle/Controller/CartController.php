<?php

namespace Miuze\PageBundle\Controller;

use Miuze\AdminBundle\Entity\Order;
use Miuze\AdminBundle\Entity\Travel;
use Miuze\AdminBundle\Form\Product\TravelType;
use Miuze\PageBundle\MiuzePageBundle;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Entity\Top;
use Miuze\AdminBundle\Entity\Cart;
use Miuze\UserBundle\Entity\User;
use Miuze\AdminBundle\Form\Product\ClientType;
use Miuze\AdminBundle\Service\Przelewy24;

/**
     * @Route(
     *      "/cart"
     * )
     */
class CartController extends Controller
{
    /**
     * @Route(
     *      "/product-{id}",
     *      name = "cart_add",
     *      requirements={"id"="\d+"}
     * )
     * @Template()
     */
    public function addAction(Request $request)
    {
        $productId = $request->attributes->getInt("id");
        $session = $this->get('session');
        if($request->isXmlHttpRequest()) {
            $product = $this->getDoctrine()->getRepository("MiuzeAdminBundle:Product")->getProductDetails(array(
                'id' => $productId
            ));
            if($product !== null) {
                $cart = new Cart();
                $cart->setProduct($product);
                $cart->setQuantity(1);
                if (!$session->has('cart')) {
                    $products = array();
                    array_push($products, $cart);
                    $session->set('cart', $products);
                }else{
                    $products = $session->get('cart');
                    array_push($products, $cart);
                    $session->set('cart', $products);
                }
                $res = array(
                    "msg" => "Dodano produkt do twojego koszyka",
                    "status" => "success"
                );
            }else{
                $res = array(
                    "msg" => "Nie znaleziono produktu",
                    "status" => "warning"
                );
            }
        }else{
            $res = array(
                "msg" => "Błąd przy dodawaniu produktu",
                "status" => "warning"
            );
        }
        $view = $this->renderView('MiuzePageBundle:common/widget:modal.html.twig', $res);
        return new JsonResponse($view);
    }


    /**
     * @Route(
     *      "/{pageid},{title}.html",
     *      name = "cart_index",
     *     requirements={"pageid" = "\d+"}
     * )
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $pageid = $request->attributes->getInt('pageid');
        $page = $this->getDoctrine()->getRepository("MiuzeAdminBundle:Page")->findOneById($pageid);
        $session = $this->get('session');
        $cart = new Cart();
        
        if(!$session->has('cart')){
            $products = array();
            $session->set('cart', $products);
        }
        $order = $session->get('cart');
        
        return array(
            'page' => $page,
            'cart' => $order,
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}/{pageid},{title}.html",
     *      name = "cart_delete_item",
     *      requirements={"pageid"="\d+"}
     * )
     * @Template()
     */
    public function deleteItemAction(Request $request)
    {
        $pageId = $request->attributes->getInt('pageid');
        $pageTitle = $request->attributes->get('title');
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        
        $order = $session->get('cart');
        
        foreach($order as $key => $val){
            if($key == $id){
                unset($order[$key]);
                $session->set('cart', $order);
                return $this->redirect($this->generateUrl('cart_index', array(
                    'pageid' => $request->attributes->getInt('pageid'),
                    'title' => $request->attributes->get('title')
                )));
            }
        }
        return $this->redirect($this->generateUrl('cart_index', array(
            'pageid' => $request->attributes->getInt('pageid'),
            'title' => $request->attributes->get('title')
        )));
    }
    
    /**
     * @Route(
     *      "/deleteall/{pageid},{title}.html",
     *      name = "cart_delete_all",
     *      requirements={"pageid"="\d+"}
     * )
     */
    public function deleteAllAction(Request $request)
    {
        $pageId = $request->attributes->getInt('pageid');
        $pageTitle = $request->attributes->get('title');
        $session = $this->get('session');
        if($session->has('order')){
            return $this->redirect($this->generateUrl('cart_delete_order', array(
                'pageid' => $request->attributes->getInt('pageid'),
                'title' => $request->attributes->get('title')
            )));
        }
        $order = $session->get('cart');
        $order  = array();
        $session->set('cart', $order);
        $session->invalidate();
        $session->start();
        return $this->redirect($this->generateUrl('cart_index', array(
            'pageid' => $request->attributes->getInt('pageid'),
            'title' => $request->attributes->get('title')
        )));
    }

    /**
     * @Route(
     *      "/delete-order/{pageid},{title}.html",
     *      name = "cart_delete_order",
     *      requirements={"pageid"="\d+"}
     * )
     */
    public function deleteOrderAction(Request $request)
    {
        $pageId = $request->attributes->getInt('pageid');
        $pageTitle = $request->attributes->get('title');
        $session = $this->get('session');
        $sesId = $session->getId();
        $entity = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order')->findOneBySession($sesId);
        if($entity !== null){
            $em = $this->getDoctrine()->getManager();
            $c = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Cart')->findBy(array('order' => $entity->getId()));
            if($c !== null){
                foreach ($c as $item){
                    $em->remove($item);
                }
            }
            $em->remove($entity);
            $em->flush();
        }
        $order = $session->get('cart');
        $order  = array();
        $session->set('cart', $order);
        $session->invalidate();
        $session->start();
        $session->getFlashBag()->add('success', 'Zamówienie usunięte');

        return $this->redirect($this->generateUrl('cart_index', array(
            'pageid' => $request->attributes->getInt('pageid'),
            'title' => $request->attributes->get('title')
        )));
    }


    /**
     * @Route(
     *      "/client-detail,{pageid},{title}.html",
     *      name = "cart_user",
     *      requirements={"pageid"="\d+"}
     * )
     * @Template()
     */
    public function userAction(Request $request)
    {
        if($this->getUser() !== null){
            $user = $this->getUser();
            if (in_array('ROLE_USER_REGISTER', $user->getRoles())){
                $entity = $user;
                $entity->setClientEmail($entity->getEmail());
            }elseif (in_array('ROLE_CONTRACTOR', $user->getRoles())){
                $this->redirect($this->generateUrl('cart_user'));
            }elseif(in_array('ROLE_SELLER', $user->getRoles())){
                $entity = new User();
            }elseif(in_array('ROLE_ADMIN', $user->getRoles())){
                $entity = new User();
            }

        }else{
            $entity = new User();
        }
        $pageId = $request->attributes->getInt('pageid');
        $pageTitle = $request->attributes->get('title');
        $page = $this->get('page_service')->getpage($pageId);

        $session = $this->get('session');
        
        if(!$session->has('cart')){
            return $this->redirect($this->generateUrl('cart_index'));
        }

        $form = $this->createForm(ClientType::class, $entity);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $productService = $this->get('product');
                $userManager = $this->get('user_manager');
                $carts = $session->get('cart');
                $em = $this->getDoctrine()->getManager();

                $user = $userManager->addClient($entity);
                $order = new Order();
                $price = 0;
                foreach($carts as $item){
                    $price += $item->getQuantity() * $item->getProduct()->calcVat($this->getUser());
                }
                $order->setBuyer($user);
                $order->setPrice($price);
                $order->setSession($session->getId());
                $em->persist($order);
                $em->flush();


                foreach($carts as $item){
                    $item->setOrder($order);
                    $em->merge($item);
                    $em->flush();
                }


                $session->set('order', $order->getId());

                return $this->redirect($this->generateUrl('cart_travel', array(
                    'pageid' => $pageId,
                    'title' => $pageTitle
                )));
            }else{
                $session->getFlashBag()->add('warning', 'Sprawdź formularz.');
            }
        }

        return array(
            'form' => $form->createView(),
            'page' => $page,
        );
    }

    /**
     * @Route(
     *      "/travel,{pageid},{title}.html",
     *      name = "cart_travel",
     *      requirements={"pageid"="\d+"}
     * )
     * @Template()
     */
    public function travelAction(Request $request)
    {
        $pageId = $request->attributes->getInt('pageid');
        $pageTitle = $request->attributes->get('title');
        $page = $this->get('page_service')->getpage($pageId);

        $session = $this->get('session');

        if(!$session->has('order')){
            $session->getFlashBag()->add('warning', 'Błąd "cart-travel-01" w przekazaniu parametru. Dokończ zamówienie telefonicznie i poinformuj nas o błędzie.');
            return $this->redirect($this->generateUrl('cart_index',array(
                'pageid' => $pageId,
                'title' => $pageTitle
            )));
        }
        $orderId = $session->get('order');
        $order = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order')->findOneById($orderId);
        if($order == null){
            $session->getFlashBag()->add('warning', 'Błąd "cart-travel-02" w przekazaniu parametru. Spróbuj ponownie lub złóż zamówienie telefonicznie i poinformuj nas o błędzie');
            return $this->redirect($this->generateUrl('cart_index',array(
                'pageid' => $pageId,
                'title' => $pageTitle
            )));
        }
        $form = $this->createForm(TravelType::class, $order);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $price = $order->getPrice() + $order->getTravel()->getPrice();
                $order->setPrice($price);
                $em->persist($order);
                $em->flush();
                return $this->redirect($this->generateUrl('cart_summary', array(
                    'pageid' => $pageId,
                    'title' => $pageTitle
                )));
            }
        }

        return array(
            'page' => $page,
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/summary,{pageid},{title}.html",
     *      name = "cart_summary",
     *      requirements={"pageid"="\d+"}
     * )
     * @Template()
     */
    public function summaryAction(Request $request)
    {
        $pageId = $request->attributes->getInt('pageid');
        $pageTitle = $request->attributes->getInt('title');
        $page = $this->get('page_service')->getpage($pageId);

        $session = $this->get('session');
        
        if(!$session->has('order')){
            $session->getFlashBag()->add('warning', 'Błąd "cart-summary-01" w przekazaniu parametru. Dokończ zamówienie telefonicznie i poinformuj nas o błędzie.');
            return $this->redirect($this->generateUrl('cart_index',array(
                'pageid' => $pageId,
                'title' => $pageTitle
            )));
        }
        $orderId = $session->get('order');
        $order = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order')->getDetails(array(
            'id' => $orderId
        ));

        if($order == null){
            $session->getFlashBag()->add('warning', 'Błąd "cart-summary-02" w przekazaniu parametru. Spróbuj ponownie lub złóż zamówienie telefonicznie i poinformuj nas o błędzie');
            return $this->redirect($this->generateUrl('cart_index',array(
                'pageid' => $pageId,
                'title' => $pageTitle
            )));
        }

        return array(
            'page' => $page,
            'order' => $order,
        );
    }
//    todo
    /**
     * @Route(
     *      "/pay",
     *      name = "cart_pay"
     * )
     */
    public function payAction(Request $request)
    {
        $crc = '3c740337dc6b7d9e';
        $payId = '57908';
        $currency = 'PLN';
        $session = $this->get('session');
        $pay = new Przelewy24($payId, $payId, $crc, true);
        
        $buyerId = $session->get('buyerId');
        $buyer = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Buyer')->findOneById($buyerId);
        
        $RET = $pay->testConnection();
        if( isset($RET["error"]) and $RET["error"]==='0' and $buyer != null) {
            $amont = $buyer->getPrice() * 100;
            $signIn = md5($buyer->getSes().'|'.$buyer->getId().'|'.$amont.'|'. $currency.'|'.$crc);
            $signOut = md5($buyer->getSes().'|'.$payId.'|'.$amont.'|'. $currency.'|'.$crc);        

            $pay->addValue('p24_session_id', $buyer->getSes()); //sesja
            $pay->addValue('p24_amount', $amont); //cena
            $pay->addValue('p24_currency', $currency); //waluta
            $pay->addValue('p24_description', 'Zakup w sklepie'.$this->container->getParameter('company_name')); //opis tranzakcji
            $pay->addValue('p24_email', $buyer->getEmail()); //email klienta
            $pay->addValue('p24_country', 'PL'); //kod kraju
            $pay->addValue('p24_url_return', $this->container->getParameter('domain').$this->generateUrl('cart_pay-success')); //adres powrotny
            $pay->addValue('p24_url_status', $this->container->getParameter('domain').$this->generateUrl('cart_pay-status')); //adres powrotny
            $pay->addValue('p24_sign', $signIn); //suma kontrolna
            $pay->addValue('p24_name_1', 'Koszyk'); //nazwa produktu
            $pay->addValue('p24_quantity_1', 1); //ilość produktu
            $pay->addValue('p24_price_1', $buyer->getPrice()); //ilość produktu
             
            $ver = $pay->trnRegister(true);

            if($ver["error"] !== '0') {
                $session->getFlashBag()->add('warning', 'Problem z rejestracją płatności.');
                return $this->redirect($this->generateUrl('cart_summary'));
            }
            
        }else {
            $session->getFlashBag()->add('warning', 'Problem z połączeniem do systemu płatności.');
            return $this->redirect($this->generateUrl('cart_summary'));
        }
    }
    
    
    /**
     * @Route(
     *      "/pay-status",
     *      name = "cart_pay-status"
     * )
     */
    public function statusAction(Request $request)
    {
        $date = new \DateTime('now');
        $data = PHP_EOL.$date->format('d-m-Y H:s')
                .PHP_EOL.' Session: '.$request->get('p24_session_id')
                .PHP_EOL.' Kwota:'.$request->get('p24_amount')
                .PHP_EOL.' Id zamówienia:'.$request->get('p24_order_id')
                .PHP_EOL.' Metoda płatności:'.$request->get('p24_method')
                .PHP_EOL.' Tytuł:'.$request->get('p24_statement')
                .PHP_EOL.'_____________________________________';
        file_put_contents(__DIR__.'/../../../../web/log/paystatus.txt', $data,FILE_APPEND);
        if($request->isMethod('POST')){
            $orderId = $request->get('p24_order_id');
            $sessionId = $request->get('p24_session_id');
            $buyer = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order')->findOneBy(array('session' => $sessionId));
            
            $crc = '3c740337dc6b7d9e';
            $payId = '57908';
            $currency = 'PLN';
            
            $amont = $buyer->getPrice() * 100;

            $pay = new Przelewy24($payId, $payId, $crc, true);

            $pay->addValue('p24_session_id', $sessionId); //sesja
            $pay->addValue('p24_amount', $amont); //cena
            $pay->addValue('p24_currency', $currency); //waluta
            $pay->addValue('p24_order_id', $orderId); //waluta
            
            $verify = $pay->trnVerify();
            if(isset($verify['error']) and $verify['error']==='0'){
                $buyer->setStatus(1);
                $em = $this->getDoctrine()->getManager();
                $em->persist($buyer);
                $em->flush();
            }else{
                file_put_contents('../../../../web/log/paysatus.txt', $request);
            }
        }else{
            return $this->redirect($this->generateUrl('cart_pay-success'));
        }
    }
    
    /**
     * @Route(
     *      "/pay-success",
     *      name = "cart_pay-success"
     * )
     * @Template()
     */
    public function successAction(Request $request)
    {

            $session = $this->get('session');
            $orderId = $session->get('order');
            $order = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order')->findOneById($orderId);
            if($order->getStatus() == 1){
                if($order->getSendEmail() == false){
                    $mailer = $this->get('miuze_buy_confirm');
                    $htmlBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:buyConfirm.html.twig', array(
                        'order' => $order
                    ));
                    try{ 
                        $mailer->send($order->getUser(), 'Dziękujemy za wybarnie naszego sklepu', $htmlBody);
                        $em = $this->getDoctrine()->getManager();
                        $buyer->setSendEmail(1);
                        $em->persist($buyer);
                        $em->flush();
                    } catch (Exception $e){
                        $session->getFlashBag()->add('danger', 'Wystapił błąd podczas wysyłania email.');
                    }
                }
                $session = $this->get('session');
                $order = $session->get('cart');
                $session->invalidate();
                $session->set('cart', []);
                $session->set('order', []);
            }
            return array(
                'page' => $page,
                'buyer' => $buyer
            );
        
    }

    /**
     * @Route(
     *      "/cart-quantity-plus/{key}",
     *      name = "cart_quantity_plus",
     *     requirements={"key"="\d+"}
     * )
     */
    public function quantityPlusAction(Request $request)
    {
        $key = $request->attributes->getInt('key');
        $session = $this->get('session');
        $cart = $session->get('cart');
        $quantity = $cart[$key]->getQuantity();
        $quantity += 1;
        $cart[$key]->setQuantity($quantity);
        $session->set('cart', $cart);
        $res = array(
            'status' => 'success',
            'msg' => $quantity
        );
        return new JsonResponse($res);
    }


    /**
     * @Route(
     *      "/cart-quantity-minus/{key}",
     *      name = "cart_quantity_minus",
     *     requirements={"key"="\d+"}
     * )
     */
    public function quantityMinusAction(Request $request)
    {
        $key = $request->attributes->getInt('key');
        $session = $this->get('session');
        $cart = $session->get('cart');
        $quantity = $cart[$key]->getQuantity();
        $quantity -= 1;
        if($quantity <=0){
            unset($cart[$key]);
            $res = array(
                'status' => 'delete',
                'msg' => $key
            );
        }else {
            $cart[$key]->setQuantity($quantity);
            $session->set('cart', $cart);
            $res = array(
                'status' => 'success',
                'msg' => $quantity
            );
        }

        return new JsonResponse($res);
    }
}
