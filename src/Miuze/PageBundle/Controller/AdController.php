<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;


class AdController extends BaseController
{
    /**
     * @Route(
     *     "/ad,{pageid},{slug}.html/{pager}",
     *      name="ad",
     *      defaults = {"pager" = 1},
     *      requirements={"pageid": "\d+"}
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:AdCategory')->getCategoriesAdPage(array('page' => $this->page->getId()));
        $this->view('MiuzePage/Ad/index.html.twig', array('list' => $list));
    }
    
    /**
     * @Route(
     *      "/ad,{pageid}/{id},{title}.html",
     *      name="ad_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $ad = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Ad')->getOffer(array('id' => $id));
        $this->view('MiuzePage/Ad/read.html.twig', array('ad' => $ad));
    }
}
