<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Request;
use Miuze\PageBundle\Controller\BaseController;


class NewsController extends BaseController
{
    /**
     * @Route(
     *     "/news,{pageid},{slug}.html/{page}",
     *      name="news",
     *      defaults = {"page" = 1},
     *      requirements={"pageid": "\d+"}
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:NewsCategory')->getCategoriesNewsPage(array('page' => $this->page->getId()));
        $this->view('MiuzePage/News/read.html.twig', array('list' => $list));
    }
    
    /**
     * @Route(
     *      "/news,{pageid}/{id},{title}.html",
     *      name="news_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request)
    {
        $id = $request->attrigutes->getInt('id');
        $news = $this->getDoctrine()->getRepository('MiuzeAdminBundle:News')->findOneBy(array('id' => $id));
        $this->view('MiuzePage/News/read.html.twig', array('news' => $news));
    }
}
