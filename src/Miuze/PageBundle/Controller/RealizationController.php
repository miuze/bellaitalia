<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;


class RealizationController extends BaseController
{
    /**
     * @Route(
     *     "/realization,{pageid},{slug}.html/{page}",
     *      name="realization",
     *      defaults = {"pager" = 1},
     *      requirements={"page": "\d+"}
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:RealizationCategory')->getCategoriesRealizationPage(array('page' => $this->page->getId()));
        $this->view('MiuzePage/Realization/index.html.twig', array('list' => $list));
    }
    
    /**
     * @Route(
     *      "/realization,{pageid}/{id},{title}.html",
     *      name="realization_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $realization = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Realization')->findWithPhoto(array('id' => $id));
        $this->view('MiuzePage/Realization/read.html.twig', array('realization' => $realization));
    }
}
