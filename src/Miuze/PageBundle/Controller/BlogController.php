<?php

namespace Miuze\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Entity\Blog;
use Miuze\AdminBundle\Entity\BlogCategory;
use Symfony\Component\HttpFoundation\Request;


class BlogController extends BaseController
{
    /**
     * @Route(
     *     "/blog,{pageid},{slug}.html/{page_number}",
     *      name="blog",   
     *      defaults = {"page_number" = 1},
     *      requirements={"pageid": "\d+", "page_number": "\d+"}
     * )
     */
    public function indexAction(Request $request)
    {
        $this->getPage($request);
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:BlogCategory')->getBlogPagePaginator(array('page' => $this->page->getId()));
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $list,
            $request->attributes->getInt('page_number'),
            12
        );
        return $this->view('@MiuzePage/Blog/index.html.twig', array('pagination' => $pagination));
    }
    
    /**
     * @Route(
     *      "/blog,{pageid}/{id},{title}.html",
     *      name="blog_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request)
    {
        $this->getPage($request);
        $id = $request->attributes->getInt('id');
        $blog = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Blog')->findOneBy(array('id' => $id));
        return $this->view('@MiuzePage/Blog/read.html.twig', array('blog' => $blog));
    }
}
