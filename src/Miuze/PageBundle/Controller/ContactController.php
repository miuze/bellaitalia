<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;
use Miuze\AdminBundle\Entity\Contact;
use Miuze\AdminBundle\Form\Contact\ContactType;
use Miuze\AdminBundle\Form\Contact\FastContactType;


class ContactController extends BaseController
{

    /**
     * @Route(
     *      "/contact,{pageid},{slug}.html/product-{id}",
     *      name="contact",
     *      defaults={"id"= 0}
     * )
     */
    public function indexAction(Request $request)
    {
        $this->getPage($request);
        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            $session = $this->get('session');
            if($form->isValid()){
                
                $mailer = $this->get('miuze_page_contact_mailer');
                $htmlBody = $this->renderView('@MiuzeAdmin/EmailTemplate/contact.html.twig', array(
                    'contact' => $contact
                ));
                $htmlAdminBody = $this->renderView('@MiuzeAdmin/EmailTemplate/contact.html.twig', array(
                    'contact' => $contact
                ));
                
                try{ 
                    $mailer->sendAdmin($contact, 'Wiadomość ze strony', $htmlAdminBody);
                    $mailer->send($contact, 'Dziękujemy za wiadomość', $htmlBody);
                    $session->getFlashBag()->add('success', 'Gratulacje, wiadomość została wysłana prawidłowo!');

                } catch (Exception $e){
                    $session->getFlashBag()->add('danger', 'Wystapił błąd podczas wysyłania.');
                    return $this->redirect($this->generateUrl('contact', array('page' => $this->page->getId(), 'title' => $this->page->getSeoUrl )));
                }
                
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
            }
        }        
       
        return $this->view('@MiuzePage/Contact/index.html.twig', array('contactform' => $form->createView()));
    }
    
    

    
    
    
    
    /**
     * @Route(
     *      "/contact-ajax",
     *      name="contact-ajax"
     * )
     */
    public function contactAjaxAction(Request $request)
    {
        $contact = new Contact();    
                
        $form = $this->createForm(FastContactType::class, $contact);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            $session = $this->get('session');
            if($form->isValid()){ 
                
                $mailer = $this->get('miuze_page_contact_mailer');
                $htmlBody = $this->renderView('@MiuzeAdmin/EmailTemplate/contact.html.twig', array(
                    'contact' => $contact
                ));
                $htmlAdminBody = $this->renderView('@MiuzeAdmin/EmailTemplate/contact.html.twig', array(
                    'contact' => $contact
                ));
                
                try{ 
                    $mailer->sendAdmin($contact, 'Wiadomość ze strony', $htmlAdminBody);
                    $mailer->send($contact, 'Dziękujemy za wiadomość', $htmlBody);
                    $resBody = array(
                        'status' => 'success',
                        'msg' => 'Dziękujemy za wiadomość.',
                    );
                    
                } catch (Exception $e){
                    $resBody = array(
                        'status' => 'error',
                        'msg' => 'Wystapił błąd podczas wysyłania.',
                    );
                }
            }else{
                $resBody = array(
                    'status' => 'error',
                    'msg' => 'Wystapił problem sprawdź formularz. Pole twoja wiadomość powinna zawierać więcej niż 30 znaków, oraz e-mail powinien być poprawny',
                );
            }
        }   
        return new JsonResponse($resBody);
    }    
}
