<?php

namespace Miuze\PageBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Miuze\AdminBundle\Entity\Newsletter;
use Miuze\AdminBundle\Form\Type\NewsletterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Miuze\AdminBundle\Mailer\NewsletterMailer;
use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Entity\Top;
/**
     * @Route(
     *      "/newsletter"
     * )
     */
class NewsletterController extends Controller
{
    protected $miuzeMailer;
    
    
    
    /**
     * @Route(
     *      "/",
     *      name = "newsletter_index"
     * )
     * @Template("@MiuzePage/Newsletter/index.html.twig")
     */
    public function indexAction(Request $Request)
    {
        $page = new Page;
        $top = new Top;
        $top->setImage('58874b2bdd3c5.jpeg');
        $top->setPath('/uploads/top/');
        $page->setClass('');
        $page->setTop($top);
        $page->setTitle('Newsletter');
        $page->setSeoDescription('Newsletter');
        $page->setLayout('layout-subpage.html.twig');
        
        return array(
            'page' => $page,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "add_subscriber"
     * )
     * @Template()
     */
    public function addSubscriberAction(Request $Request)
    {
        $session = $this->get('session');
        $newsletter = new Newsletter();  
       
        if($Request->isMethod('POST')){
            $email = $Request->get('newsletter')['email'];
            if($this->checkEmail($email) == true){ 
                $subscriberexist = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Newsletter')->findBy(array('email' => $email, 'active' => 1));
                if($subscriberexist != null){
                    $session->getFlashBag()->add('warning', 'Błąd , subskrybent z takim adresem już istnieje!');
                    return $this->redirect($this->generateUrl('newsletter_index'));
                }
                $newsletter->setEmail($email);
                $newsletter->setActionToken($this->generateActionTken());
                $em = $this->getDoctrine()->getManager();
                $em->persist($newsletter);
                $em->flush();
                $urlParams = array($newsletter->getActionToken());
                
                $urlactivation = $this->generateUrl('subscriber_active', $urlParams, UrlGeneratorInterface::ABSOLUTE_URL);
                
                $htmlBody = $this->renderView('@MiuzeAdmin/EmailTemplate/activeSubscriber.html.twig', array(
                    'urlactivation' => $urlactivation,
                ));
                
                $mailer = $this->get('miuze_newsletter_mailer');
                try{
                    $mailer->send($newsletter, 'Potwierdzenie newslettera', $htmlBody);
                    $session->getFlashBag()->add('success', 'Gratulacje, dodano Cię do listy newslettera na twój adres email został wysłany link aktywacyjny!');
                    return $this->redirect($this->generateUrl('newsletter_index'));
                } catch (Exception $e){
                    $session->getFlashBag()->add('warning', 'Błąd , przy wysyłaniu maila z linkiem aktywacyjnym!');
                    return $this->redirect($this->generateUrl('newsletter_index'));
                }
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
                return $this->redirect($this->generateUrl('newsletter_index'));
            }
        }else{
            $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
            return $this->redirect($this->generateUrl('newsletter_index'));
        }  
        
        return array(
           
        );
    }
    
    
    
    /**
     * @Route(
     *      "/active",
     *      name="subscriber_active"
     * )
     */
    public function ActiveSubscriberAction(Request $request)
    {
        $session = $this->get('session');
        $token = $request->get(0);
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Newsletter');
        $subscriber = $repo->findOneBy(array('actionToken' => $token));
        if(NULL == $subscriber ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $subscriber->setActive(1);
        $em->persist($subscriber);
        $em->flush();
        $session->getFlashBag()->add('success', 'Gratulacje od teraz będziesz otrzymywał od nas informacje o naszych promocjach');
        return $this->redirect($this->generateUrl('newsletter_index'));
    }
    
    /**
     * @Route(
     *      "/deactive",
     *      name="admin_subscriber_deactive"
     * )
     */
    public function deactiveAction(Request $request)
    {
        $session = $this->get('session');
        $token = $request->get('token');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Newsletter');
        $subscriber = $repo->findOneBy(array('actionToken' => $token));
        if(NULL == $subscriber ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $subscriber->setActive(0);
        $em->persist($subscriber);
        $em->flush();
        $session->getFlashBag()->add('success', 'Zostałeś usunięty z listy subskrybentów');
        return $this->redirect($this->generateUrl('newsletter_index'));
    }
    
    function  checkEmail($email) {
    if (!preg_match("/^( [a-zA-Z0-9] )+( [a-zA-Z0-9\._-] )*@( [a-zA-Z0-9_-] )+( [a-zA-Z0-9\._-] +)+$/" , $email)) {
        return true;
    }
        return false;
    }
    public function generateActionTken(){
        return substr(md5(uniqid(NULL, TRUE)), 0, 20);
    }
}
