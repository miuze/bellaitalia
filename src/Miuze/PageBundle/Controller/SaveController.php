<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PageBundle\Controller\BaseController;
use Miuze\AdminBundle\Entity\Save;
use Miuze\AdminBundle\Form\Save\SaveType;



class SaveController extends BaseController
{
    /**
     * @Route(
     *      "/save,{pageid},{title}.html",
     *      name="save"
     * )
     */
    public function indexAction(Request $request)
    {
        $this->getPage($request);
        $save = new Save();
        $form = $this->createForm(SaveType::class, $save);
        if($request->isMethod('POST')){
            $session = $this->get('session');
            $form->handleRequest($request);            
            if($form->isValid()){ 
                $mailer = $this->get('miuze_page_save_mailer');
                $htmlBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:save.html.twig', array(
                    'save' => $save
                ));
                $htmlAdminBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:save.html.twig', array(
                    'save' => $save
                ));
                
                try{ 
                    $mailer->sendAdmin($save, 'Rejestracja on-line', $htmlAdminBody);
                    $mailer->send($save, 'Dziękujemy za zgłoszenie', $htmlBody);
                    $session->getFlashBag()->add('success', 'Dziękujemy za zarejestrowanie się. Potwierdzimy termin telefonicznie lub przez e-mail.');
                    unset($save);
                    $save = new Save();   
                    $form = $this->createForm(SaveType::class, $save); 
                    
                } catch (Exception $e){
                    $session->getFlashBag()->add('danger', 'Wystapił błąd podczas wysyłania.');
                    return $this->redirect($this->generateUrl('save', array('page' => $page, 'title' => $title )));
                }
                
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
            }
        }


        return $this->view('@MiuzePage/Save/index.html.twig', array('form' => $form->createView()));
    }    
    
    
    
    
    /**
     * @Route(
     *      "/save-ajax",
     *      name = "save-ajax"
     * )
     */
    public function saveAjaxAction(Request $request)
    {
        $save = new Save();
        $form = $this->createForm(SaveType::class, $save);
        if($request->isMethod('POST')){
            $session = $this->get('session');
            $form->handleRequest($request);
            if($form->isValid()){
                $mailer = $this->get('miuze_page_save_mailer');
                
                $htmlBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:save.html.twig', array(
                    'save' => $save
                ));
                $htmlAdminBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:save.html.twig', array(
                    'save' => $save
                ));
                
                try{ 
                    $mailer->sendAdmin($save, 'Rezerwacja on-line', $htmlAdminBody);
                    $mailer->send($save, 'Dziękujemy za zgłoszenie', $htmlBody);
                    $res = array( 
                        'msg' => 'Dziękujemy za kontakt. Potwierdzimy termin e-mailem.',
                        'status' => 'success',
                        'myajax' => 1
                    );
                } catch (Exception $e){
                    $res = array( 
                        'msg' => 'Wystapił błąd podczas wysyłania.',
                        'status' => 'danger',
                        'myajax' => 1
                    );
                }
            }else{
                $res = array( 
                    'msg' => 'Wystapił problem sprawdź formularz.',
                    'status' => 'danger',
                    'myajax' => 1
                );
            }
        }else{
            $res = array( 
                'msg' => 'Wystapił problem sprawdź formularz.',
                'status' => 'danger',
                'myajax' => 1
            );
        }  
        return new JsonResponse($res);
        
        
    }
}
