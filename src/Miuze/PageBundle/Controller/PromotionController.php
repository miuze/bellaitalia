<?php

namespace Miuze\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Miuze\PageBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;


class PromotionController extends BaseController
{
    /**
     * @Route(
     *     "/promotion,{pageid},{slug}.html/{page}",
     *      name="promotion",
     *      defaults = {"page" = 1},
     *      requirements={"pageid": "\d+"}
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PromotionCategory')->getCategoriesPromotionPage(array('page' => $this->page->getId()));
        $this->view('MiuzePage/Promotion/index.html.twig', array(
            'list' => $list,
        ));
    }
    
    /**
     * @Route(
     *      "/promotion,{pageid}/{id},{title}.html",
     *      name="promotion_read",
     *      requirements={"pageid": "\d+", "id": "\d+"}
     * )
     */
    public function readAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $promotion = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Promotion')->findOneBy(array('id' => $id));
        $this->view('MiuzePage/Promotion/read.html.twig', array('promotion' => $promotion));
    }
}
