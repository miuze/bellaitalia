<?php

namespace Miuze\PageBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

class Repair {
    
    
    private $id;
    
    private $zlecenie;
    
    private $rok;
    
    private $klient;
    
    function getId() {
        return $this->id;
    }

    function getZlecenie() {
        return $this->zlecenie;
    }

    function getRok() {
        return $this->rok;
    }

    function getKlient() {
        return $this->klient;
    }

    function setZlecenie($zlecenie) {
        $this->zlecenie = $zlecenie;
    }

    function setRok($rok) {
        $this->rok = $rok;
    }

    function setKlient($klient) {
        $this->klient = $klient;
    }
}

