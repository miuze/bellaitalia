<?php

namespace Miuze\UserBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface as Templating;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Miuze\UserBundle\Mailer\UserMailer;
use Miuze\UserBundle\Entity\User;
use Miuze\UserBundle\Exception\UserException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Miuze\AdminBundle\Entity\Newsletter;

class UserManager {
    
    /**
     * @var Doctrine
     */
    protected $doctrine;
        
    /**
     * @var Router
     */
    protected $router;
    
    /**
     * @var Container
     */
    protected $container;
    
    /**
     * @var EncoderFactory
     */
    protected $encoderFactory;
    
    /**
     * @var UserMailer
     */
    protected $userMailer;
    
    
    function __construct(Doctrine $doctrine, Router $router, Container $container, EncoderFactory $encoderFactory, UserMailer $userMailer) {
        $this->doctrine = $doctrine;
        $this->router = $router;
        $this->container = $container;
        $this->encoderFactory = $encoderFactory;
        $this->userMailer = $userMailer;
    }
    

    

    public function addUser(User $user) {

        if(null !== $user->getId()){
            throw new UserException('Taki użytkownik już istnieje');
        }

        $encoder = $this->encoderFactory->getEncoder($user);
        $encodedPasswd = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());

        $user->setPassword($encodedPasswd);
        $user->setEnabled(true);

        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();

        return true;
    }

    public function addClient(User $user) {

        $em = $this->doctrine->getManager();
        $result = $this->doctrine->getRepository('MiuzeUserBundle:User')->findOneByEmail($user->getClientEmail());
        if(null == $result){
            $encoder = $this->encoderFactory->getEncoder($user);
            $encodedPasswd = $encoder->encodePassword($this->getRandomPassword(), $user->getSalt());
            $user->setEmail($user->getClientEmail());
            $user->setPassword($encodedPasswd);
            $user->setEnabled(false);
            $user->setAccountLocked(true);
            $em->persist($user);
            $em->flush();
        }else{
            $result->setCity($user->getCity());
            $result->setFirstName($user->getFirstName());
            $result->setLastName($user->getLastName());
            $result->setStreet($user->getStreet());
            $result->setHomeNumber($user->getHomeNumber());
            $result->setPostCode($user->getPostCode());
            $result->setPhone($user->getPhone());
            $em->persist($result);
            $em->flush();
            $user = $result;
        }
        return $user;
    }

    protected function addSubscriber(User $user){

        $newsletter = new Newsletter();
        $newsletter->setEmail($user->getEmail());
        $newsletter->setActive(TRUE);
        $date = new \DateTime('now');
        $newsletter->setDate($date);
        $newsletter->setActionToken($this->generateActionToken());

        $em = $this->doctrine->getManager();
        $em->persist($newsletter);
        $em->flush();

    }

    public function registerUser(User $user) {

        $exUser = $this->doctrine->getRepository('MiuzeUserBundle:User')->findOneByEmail($user->getClientEmail());
        if($exUser !== null){
            if($exUser->getAccountNonLocked() == true && $exUser->getEnabled() == true ){
                return false;
            }else{
                $exUser->setFirstName($user->getFirstName());
                $exUser->setLastName($user->getLastName());
                $exUser->setStreet($user->getStreet());
                $exUser->setPostCode($user->getPostCode());
                $exUser->setPhone($user->getPhone());
                $exUser->setHomeNumber($user->getHomeNumber());
                $exUser->setCity($user->getCity());
                $exUser->setCompanyName($user->getCompanyName());
                $exUser->setNip($user->getNip());
                $persistUser = $exUser;
            }
        }else{
            $persistUser = $user;
            $persistUser->setEmail($user->getClientEmail());
        }
        $passwd = $this->getRandomPassword();
        $encoder = $this->encoderFactory->getEncoder($persistUser);
//        $encodedPasswd = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        $encodedPasswd = $encoder->encodePassword($passwd, $persistUser->getSalt());

        $persistUser->setPassword($encodedPasswd);
        $persistUser->setActionToken($this->generateActionToken());
        $persistUser->setEnabled(false);

        $em = $this->doctrine->getManager();
        $em->persist($persistUser);
        $em->flush();

        $urlParams = array(
            'actionToken' => $persistUser->getActionToken()
        );
        $activationUrl = $this->router->generate('account_profile_activate', $urlParams, UrlGeneratorInterface::ABSOLUTE_URL);

        $emaiBody = $this->container->render('MiuzeUserBundle:Email:accountActivation.html.twig', array(
            'activationUrl' => $activationUrl,
            'pwd' => $passwd
        ));
        
        $this->userMailer->send($persistUser, 'Aktywacja konta', $emaiBody);
        
        return true;
    }
    
    public function activateAccount($actionToken){
        $user = $this->doctrine->getRepository('MiuzeUserBundle:User')
                        ->findOneByActionToken($actionToken);
        
        if(null === $user){
            return false;
        }

        if($user->getAddSubscriber() !== false){
            $this->addSubscriber($user);
        }
        $roles = array('ROLE_USER_REGISTER');
        $user->setRoles($roles);
        $user->setAccountNonLocked(true);
        $user->setEnabled(true);
        $user->setActionToken(null);
        
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
        
        return true;
    }
    
    public function changePassword(User $user){
        
        if(null == $user->getPlainPassword()){
            throw new UserException('Nie ustawiono nowego hasła!');
        }

        $encoder = $this->encoderFactory->getEncoder($user);
        $encoderPassword = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        $user->setPassword($encoderPassword);

        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();

        return true;
    }

    public function sendResetPasswordLink($userEmail) {

        $user = $this->doctrine->getRepository('MiuzeUserBundle:User')
            ->findOneByEmail($userEmail);

        if(null === $user){
            return false;
        }

        $user->setActionToken($this->generateActionToken());

        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();

        $urlParams = array(
            'actionToken' => $user->getActionToken()
        );

        $resetUrl = $this->router->generate('account_profile_remind-pass', $urlParams, UrlGeneratorInterface::ABSOLUTE_URL);

        $emailBody = $this->container->get('template')->render('MiuzeUserBundle:Email:passwdResetLink.html.twig', array(
            'resetUrl' => $resetUrl
        ));

        $this->userMailer->send($user, 'Link resetujący hasło', $emailBody);

        return true;
    }

    public function resetPassword($actionToken) {

        $user = $this->doctrine->getRepository('MiuzeUserBundle:User')
            ->findOneByActionToken($actionToken);

        if(null === $user){
            return false;
        }

        $plainPasswd = $this->getRandomPassword();

        $encoder = $this->encoderFactory->getEncoder($user);
        $encodedPasswd = $encoder->encodePassword($plainPasswd, $user->getSalt());

        $user->setPassword($encodedPasswd);
        $user->setActionToken(null);

        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();

        $emailBody = $this->container->get('template')->render('MiuzeUserBundle:Email:newPassword.html.twig', array(
            'plainPasswd' => $plainPasswd
        ));

        $this->userMailer->send($user, 'Nowe hasło do konta', $emailBody);

        return true;
    }



    protected function generateActionToken() {
        return substr(md5(uniqid(NULL, TRUE)), 0, 20);
    }

    protected function getRandomPassword($length = 8){
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}
