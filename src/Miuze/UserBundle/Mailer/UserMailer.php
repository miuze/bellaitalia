<?php

namespace Miuze\UserBundle\Mailer;

use Miuze\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;


class UserMailer {
    
    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;

    private $container;
    
    
    function __construct(\Swift_Mailer $swiftMailer, ContainerInterface $container) {
        $this->swiftMailer = $swiftMailer;
        $this->container = $container;
    }
    
    public function send(User $user, $subject, $htmlBody) {
        $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom($this->container->getParameter('mailmaster'), $this->container->getParameter('company_name'))
                        ->setTo($user->getEmail(), $user->getUsername())
                        ->setBody($htmlBody, 'text/html');
        
        $this->swiftMailer->send($message);
    }

}
