<?php

namespace Miuze\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Miuze\UserBundle\Exception\UserException;
use Miuze\UserBundle\Entity\User;
use Miuze\UserBundle\Form\Type\LoginType;
use Miuze\UserBundle\Form\Type\RememberPasswordType;
use Miuze\UserBundle\Form\Type\RegisterUserType;


class LoginController extends Controller
{
    /**
     * @Route(
     *      "/admin/login",
     *      name = "cms_login"
     * )
     * 
     * @Template("@MiuzeUser/Login/login.html.twig")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
//        dump($authenticationUtils->getLastUsername());die;
        $loginForm = $this->createForm( LoginType::class, array(
            'email' => $authenticationUtils->getLastUsername(),
        ));
//        $user = new \Miuze\UserBundle\Entity\User();
//        $plainPassword = 'Rowermtb91';
//        $encoder = $this->container->get('security.password_encoder');
//        $encoded = $encoder->encodePassword($user, $plainPassword);
////        dump($encoded);die;
//        $us = $this->getDoctrine()->getRepository('MiuzeUserBundle:User')->findOneById(2);
//        $us->setPassword($encoded);
//        $em = $this->getDoctrine()->getManager();
//        $em->persist($us);
//        $em->flush();
        return array(
            'loginForm' => $loginForm->createView(),
            'error'         => $error,
        );
    }
}
