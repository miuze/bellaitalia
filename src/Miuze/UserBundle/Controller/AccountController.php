<?php

namespace Miuze\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Miuze\UserBundle\Exception\UserException;
use Miuze\UserBundle\Entity\User;
use Miuze\UserBundle\Form\Type\LoginType;
use Miuze\UserBundle\Form\Type\RememberPasswordType;
use Miuze\UserBundle\Form\Type\RegisterUserType;


/**
 * @Route(
 *      "/account",
 * )
 */

class AccountController extends Controller
{
    /**
     * @Route(
     *      "/login",
     *      name = "account_account_login",
     * )
     * @Template("MiuzeAccountBundle:Login:index.html.twig")
     */
    public function loginAction(Request $request)
    {
        if($this->get('security.token_storage')->getToken()->getUser() != "anon."){
            return $this->redirect($this->generateUrl('account_default_index'));
        }else{
            $pageid = 6;
            $page = $this->get('page_service')->getpage($pageid);

            $authenticationUtils = $this->get('security.authentication_utils');
            $error = $authenticationUtils->getLastAuthenticationError();
            $lastUsername = $authenticationUtils->getLastUsername();
            $loginForm = $this->createForm( LoginType::class, array(
                'username' => $authenticationUtils->getLastUsername(),
            ));
        }
        
        if($page->getView() == NULL){
            return $this->render('MiuzeZoneBundle:Login:index.html.twig', array(
                'page' => $page,
                'loginForm' => $loginForm->createView(),
                'error'         => $error,
            ));
        }else{
            $view = 'MiuzePageBundle:Custom:'.$page->getView();
            return $this->render( $view, array(
                'page' => $page,
                'loginForm' => $loginForm->createView(),
                'error'         => $error,
            ));
        }
    }


    /**
     * @Route(
     *     "/register",
     *      name="account_register_register"
     * )
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createFormBuilder(RegisterUserType::class, $user);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $userManager = $this->get('user_manager');
                $userManager->registerUser($user);
                $this->get('session')->getFlashBag()->add('success', 'Twoje konto zostało dodane, na twój email został wysłany link aktywacyjny');
                return $this->redirect($this->generateUrl('account_account_login'));
            }
        }

        return array(
            'form' => $form->createView(),
        );

    }
}
