<?php

namespace Miuze\UserBundle\Controller;

use Miuze\UserBundle\Form\Type\AddUserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use Miuze\UserBundle\Form\Type\RegisterUserType;
use Miuze\UserBundle\Form\Type\ChangePasswordType;
use Miuze\UserBundle\Form\Type\ManageUserType;
use Miuze\UserBundle\Entity\User;
use Miuze\UserBundle\Exception\UserException;


class UserController extends Controller
{
    /**
     * @Route(
     *      "/",
     *      name = "user_user_index"
     * )
     * @Template("@MiuzeUser/User/index.html.twig")
     */
    public function indexAction(Request $Request)
    {        
        $list = $this->getDoctrine()->getManager()->getRepository('MiuzeUserBundle:User')->findAll();
        
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/change-password/{id}",
     *      name = "user_user_change-password",
     *      defaults={"id": "0"},
     * )
     * @Template("@MiuzeUser/User/changepass.html.twig")
     */
    public function changepassAction(Request $Request, $id)
    {        
        
        $User = $this->getDoctrine()->getManager()->getRepository('MiuzeUserBundle:User')->findOneBy(array('id' => $id));
        
        // Change Password
        $changePasswdForm = $this->createForm( ChangePasswordType::class, $User);
        if($Request->isMethod('POST')){
        
            $changePasswdForm->handleRequest($Request);
            
            if($changePasswdForm->isValid()){
                try {
                    $userManager = $this->get('user_manager');
                    $userManager->changePassword($User);
                    $this->get('session')->getFlashBag()->add('success', 'Twoje hasło zostało zmienione!');
                    return $this->redirect($this->generateUrl('user_user_index'));
                    
                } catch (UserException $ex) {
                    $this->get('session')->getFlashBag()->add('error', $ex->getMessage());
                }
                
            }else{
                $this->get('session')->getFlashBag()->add('error', 'Popraw błędy formularza2!');
            }
        }
        
        
        return array(
            'user' => $User,
            'changePasswdForm' => $changePasswdForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/add/{id}",
     *      name = "user_user_add",
     *      defaults={"id": "0"},
     * )
     * @Template("@MiuzeUser/User/addUser.html.twig")
     */
    public function addUser(Request $Request, $id){
        
        $User = new User();
        
        $form = $this->createForm( AddUserType::class, $User);
        
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);
            if($form->isValid()){
                try{
                    $userManager = $this->get('user_manager');
                    $userManager->addUser($User);
                    
                    $this->get('session')->getFlashBag()->add('success', 'Konto zostało utworzone.');
                    
                    return $this->redirect($this->generateUrl('user_user_index'));
                    
                } catch (UserException $ex) {
                    $this->get('session')->getFlashBag()->add('error', $ex->getMessage());
                }
                
            }
        }
        
        return array(
            'form' => $form->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/edit/{id}", 
     *      name="user_user_manage",
     *      requirements={"id"="\d+"}
     * )
     * @Template("@MiuzeUser/User/manage.html.twig")
     */
    public function manageAction(Request $Request, User $User) {
                
        $form = $this->createForm( ManageUserType::class, $User);
        
        $form->handleRequest($Request);
        
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $User->preUpload();
            $em->persist($User);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Zaktualizowano konto użytkownika');

            return $this->redirect($this->generateUrl('user_user_index', array(
                'id' => $User->getId()
            )));
        }
        
        
        return array(
            'form' => $form->createView(),
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}", 
     *      name="user_user_delete",
     *      requirements={"id"="\d+"}
     * )
     */
    public function deleteAction(Request $Request) {
                
        
        $id = $Request->get('id');
        $user = $this->getDoctrine()->getRepository('MiuzeUserBundle:User')->findOneById($id);
        if($user != null){
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Usunięto konto użytkownika');
        }else{
            $this->get('session')->getFlashBag()->add('warning', 'Nie zanleziono użytkownika z takim id');
        }
        
        return $this->redirect($this->generateUrl('user_user_index'));
    }
    
}
