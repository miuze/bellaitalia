<?php

namespace Miuze\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * @ORM\Entity(repositoryClass="Miuze\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
* @ORM\HasLifecycleCallbacks
 * 
 * @UniqueEntity(fields={"username"})
 * @UniqueEntity(fields={"email"})
 */
class User implements AdvancedUserInterface, \Serializable {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length = 20, unique = true, nullable=true)
     * 
     * @Assert\NotBlank(
     *      groups = {"ChangeDetails"}
     * )
     * 
     * @Assert\Length(
     *      min=5,
     *      max=20,
     *      groups = {"ChangeDetails"}
     * )
     */
    private $username;
    

    /**
     * @ORM\Column(type="string", nullable= true)
     * 
     */
    private $lead;


    /**
     * @ORM\Column(type="string", length = 200, unique = true)
     * 
     * @Assert\NotBlank(
     *      groups = {"Registration"}
     * )
     * 
     * @Assert\Email(
     *      groups = {"Registration"}
     * )
     * 
     * @Assert\Length(
     *      max = 200,
     *      groups = {"Registration"}
     * )
     */
    private $email;
    
    /**
     * @ORM\Column(type="string", length = 64)
     */
    private $password;
    
    /**
     * @Assert\NotBlank(
     *      groups = {"Registration", "ChangePassword"}
     * )
     * 
     * @Assert\Length(
     *      min = 8,
     *      groups = {"Registration", "ChangePassword"}
     * )
     */
    private $plainPassword;
    
    /**
     * @ORM\Column(name="account_non_expired", type="boolean")
     */
    private $accountNonExpired = true;
    
    /**
     * @ORM\Column(name="account_non_locked", type="boolean")
     */
    private $accountNonLocked = true;
    
    /**
     * @ORM\Column(name="credentials_non_expired", type="boolean")
     */
    private $credentialsNonExpired = true;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled = false;
    
    /**
     * @ORM\Column(type="array")
     */
    private $roles;
    
    /**
     * @ORM\Column(name="action_token", type="string", length = 20, nullable = true)
     */
    private $actionToken;
    
    /**
     * @ORM\Column(name="register_date", type="datetime")
     */
    private $registerDate;
    
    /**
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length = 100, nullable = true)
     */
    private $avatar;
    
    /**
     * @ORM\Column(type="string", length = 100, nullable = true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updateDate;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\AdminBundle\Entity\Blog",
     *      mappedBy = "user"
     * )
     */
    protected $blog;

//  buyer data

    /**
     * @ORM\Column(type="text", length=120, nullable=true)
     * @Assert\Length(
     *      max = 120,
     *      minMessage = "Nazwa nie może zawierać więcej nież 120 znaków",
     *      groups = {"Buyer", "Registration"}
     * )
     */
    private $companyName;

    /**
     * @ORM\Column(type="decimal", length=10, nullable=true)
     * @Assert\Length(
     *      min = 10,
     *      max = 10,
     *      minMessage = "Nip musi zawierać 10 cyfr",
     *      groups = {"Buyer", "Registration"}
     * )
     * @Assert\Type(
     *     type="numeric",
     *     message="Ta wartość {{ value }} nie jest typu {{ type }}."
     * )
     */
    private $nip;

    /**
     * @ORM\Column(type="text", length=50, nullable=true)
     * @Assert\NotBlank(
     *      groups = {"Buyer", "Registration"}
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "Imię musi mieć conajmniej 3 znaki",
     *      minMessage = "Imię nie może zawierać więcej nież 50 znaków",
     *      groups = {"Buyer"}
     * )
     */
    private $firstName;

    /**
     * @ORM\Column(type="text", length=100, nullable=true)
     * @Assert\NotBlank(
     *     groups = {"Buyer", "Registration"}
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 100,
     *      minMessage = "Nazwisko musi mieć conajmniej 3 znaki",
     *      minMessage = "Nazwisko nie może zawierać więcej nież 100 znaków",
     *      groups = {"Buyer"}
     * )
     */
    private $lastName;

    /**
     * @ORM\Column(type="text", length=100, nullable=true)
     * @Assert\NotBlank(
     *     groups = {"Buyer", "Registration"}
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 100,
     *      minMessage = "Miasto musi mieć conajmniej 3 znaki",
     *      minMessage = "Miasto nie może zawierać więcej nież 100 znaków",
     *      groups = {"Buyer"}
     * )
     */
    private $city;

    /**
     * @ORM\Column(type="text", length=5, nullable=true)
     * @Assert\NotBlank(
     *     groups = {"Buyer", "Registration"}
     * )
     * @Assert\Regex(
     *      pattern     = "/^([0-9]){2}-([0-9]){3}/",
     *      message = "Format musi składać się z XX-XXX",
     *     groups = {"Buyer", "Registration"}
     * )
     */
    private $postCode;

    /**
     * @ORM\Column(type="text", length=100, nullable=true)
     * @Assert\NotBlank(
     *     groups = {"Buyer", "Registration"}
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 100,
     *      minMessage = "Ulica musi mieć conajmniej 3 znaki",
     *      minMessage = "Ulica nie może zawierać więcej nież 100 znaków",
     *      groups = {"Buyer", "Registration"}
     * )
     */
    private $street;

    /**
     * @ORM\Column(type="text", length=10, nullable=true)
     * @Assert\NotBlank(
     *     groups = {"Buyer", "Registration"}
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "Numer musi mieć conajmniej 1 znak",
     *      minMessage = "Numer nie może zawierać więcej nież 10 znaków",
     *      groups = {"Buyer", "Registration"}
     * )
     */
    private $homeNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      max = 150,
     *      minMessage = "Numer nie może zawierać więcej nież 150 znaków",
     *      groups = {"Buyer", "Registration"}
     * )
     */
    private $phone;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\AdminBundle\Entity\Order",
     *      mappedBy = "buyer"
     * )
     */
    protected $order;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\AdminBundle\Entity\Order",
     *      mappedBy = "seller"
     * )
     */
    protected $orderSeller;

    /**
     * @Assert\NotBlank(
     *      groups = {"Buyer"}
     * )
     *
     * @Assert\Email(
     *      groups = {"Buyer"}
     * )
     *
     * @Assert\Length(
     *      max = 200,
     *      groups = {"Buyer"}
     * )
     */
    private $clientEmail;

    private $addSubscriber = true;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\IsTrue(
     *      message = "Regulamin musi być zaakceptowany",
     *      groups = {"Buyer"}
     * )
     */
    private $acceptRegulations = true;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\AdminBundle\Entity\Rabate",
     *      mappedBy = "user"
     * )
     */
    protected $rabate;

//    end of buyer
    function getFile() {
        return $this->file;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function getBlog() {
        return $this->blog;
    }

    function setBlog($blog) {
        $this->blog = $blog;
    }

    function getName() {
        return $this->name;
    }

    function getLead() {
        return $this->lead;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setLead($lead) {
        $this->lead = $lead;
    }
    
    public function eraseCredentials() {
        $this->plainPassword = null;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getRoles() {
        if(empty($this->roles)){
            return array('ROLE_USER');
        }
        
        return $this->roles;
    }

    public function getSalt() {
        return null;
    }

    public function getUsername() {
        return $this->username;
    }

    public function isAccountNonExpired() {
        return $this->accountNonExpired;
    }

    public function isAccountNonLocked() {
        return $this->accountNonLocked;
    }

    public function isCredentialsNonExpired() {
        return $this->credentialsNonExpired;
    }

    public function isEnabled() {
        return $this->enabled;
    }

    /**
     * @return mixed
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * @param mixed $clientEmail
     */
    public function setClientEmail($clientEmail)
    {
        $this->clientEmail = $clientEmail;
    }



    function __construct() {
        $this->registerDate = new \DateTime();
        $this->roles = array('ROLE_USER');
    }

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set accountNonExpired
     *
     * @param boolean $accountNonExpired
     * @return User
     */
    public function setAccountNonExpired($accountNonExpired)
    {
        $this->accountNonExpired = $accountNonExpired;

        return $this;
    }

    /**
     * Get accountNonExpired
     *
     * @return boolean 
     */
    public function getAccountNonExpired()
    {
        return $this->accountNonExpired;
    }

    /**
     * Set accountNonLocked
     *
     * @param boolean $accountNonLocked
     * @return User
     */
    public function setAccountNonLocked($accountNonLocked)
    {
        $this->accountNonLocked = $accountNonLocked;

        return $this;
    }

    /**
     * Get accountNonLocked
     *
     * @return boolean 
     */
    public function getAccountNonLocked()
    {
        return $this->accountNonLocked;
    }

    /**
     * Set credentialsNonExpired
     *
     * @param boolean $credentialsNonExpired
     * @return User
     */
    public function setCredentialsNonExpired($credentialsNonExpired)
    {
        $this->credentialsNonExpired = $credentialsNonExpired;

        return $this;
    }

    /**
     * Get credentialsNonExpired
     *
     * @return boolean 
     */
    public function getCredentialsNonExpired()
    {
        return $this->credentialsNonExpired;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return User
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Set actionToken
     *
     * @param string $actionToken
     * @return User
     */
    public function setActionToken($actionToken)
    {
        $this->actionToken = $actionToken;

        return $this;
    }

    /**
     * Get actionToken
     *
     * @return string 
     */
    public function getActionToken()
    {
        return $this->actionToken;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime 
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password
        ));
    }

    public function unserialize($serialized) {
        list(
            $this->id,
            $this->username,
            $this->password
        ) = unserialize($serialized);
    }

    
    public function getPlainPassword() {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword) {
        $this->plainPassword = $plainPassword;
    }
    
    public function setAccountExpired($accountNonExpired)
    {
        $this->accountNonExpired = !$accountNonExpired;

        return $this;
    }

    public function getAccountExpired()
    {
        return !$this->accountNonExpired;
    }

    public function setAccountLocked($accountNonLocked)
    {
        $this->accountNonLocked = !$accountNonLocked;

        return $this;
    }

    public function getAccountLocked()
    {
        return !$this->accountNonLocked;
    }

    public function setCredentialsExpired($credentialsNonExpired)
    {
        $this->credentialsNonExpired = !$credentialsNonExpired;

        return $this;
    }

    public function getCredentialsExpired()
    {
        return !$this->credentialsNonExpired;
    }
    
    public function getAbsolutePath()
    {
        return null === $this->avatar ? null : $this->getUploadRootDir().'/'.$this->avatar;
    }

    public function getWebPath()
    {
        return null === $this->avatar ? null : $this->getUploadDir().'/'.$this->avatar;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/avatars/';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {   
            $this->setPath('/uploads/avatars/');
            // zrób cokolwiek chcesz aby wygenerować unikalną nazwę
            $this->setAvatar(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // musisz wyrzucać tutaj wyjątek jeśli plik nie może zostać przeniesiony
        // w tym przypadku encja nie zostanie zapisana do bazy
        // metoda move() obiektu UploadedFile robi to automatycznie
        //dump($this->getUploadRootDir()); die; 
        $this->file->move($this->getUploadRootDir(), $this->avatar);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
    


    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return User
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return User
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return User
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set nip
     *
     * @param string $nip
     *
     * @return User
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postCode
     *
     * @param string $postCode
     *
     * @return User
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get postCode
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return User
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set homeNumber
     *
     * @param string $homeNumber
     *
     * @return User
     */
    public function setHomeNumber($homeNumber)
    {
        $this->homeNumber = $homeNumber;

        return $this;
    }

    /**
     * Get homeNumber
     *
     * @return string
     */
    public function getHomeNumber()
    {
        return $this->homeNumber;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set acceptRegulations
     *
     * @param boolean $acceptRegulations
     *
     * @return User
     */
    public function setAcceptRegulations($acceptRegulations)
    {
        $this->acceptRegulations = $acceptRegulations;

        return $this;
    }

    /**
     * Get acceptRegulations
     *
     * @return boolean
     */
    public function getAcceptRegulations()
    {
        return $this->acceptRegulations;
    }

    /**
     * Add blog
     *
     * @param \Miuze\AdminBundle\Entity\Blog $blog
     *
     * @return User
     */
    public function addBlog(\Miuze\AdminBundle\Entity\Blog $blog)
    {
        $this->blog[] = $blog;

        return $this;
    }

    /**
     * Remove blog
     *
     * @param \Miuze\AdminBundle\Entity\Blog $blog
     */
    public function removeBlog(\Miuze\AdminBundle\Entity\Blog $blog)
    {
        $this->blog->removeElement($blog);
    }

    
    /**
     * Add orderSeller
     *
     * @param \Miuze\AdminBundle\Entity\Order $orderSeller
     *
     * @return User
     */
    public function addOrderSeller(\Miuze\AdminBundle\Entity\Order $orderSeller)
    {
        $this->orderSeller[] = $orderSeller;

        return $this;
    }

    /**
     * Remove orderSeller
     *
     * @param \Miuze\AdminBundle\Entity\Order $orderSeller
     */
    public function removeOrderSeller(\Miuze\AdminBundle\Entity\Order $orderSeller)
    {
        $this->orderSeller->removeElement($orderSeller);
    }

    /**
     * Get orderSeller
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderSeller()
    {
        return $this->orderSeller;
    }

    /**
     * Add order
     *
     * @param \Miuze\AdminBundle\Entity\Order $order
     *
     * @return User
     */
    public function addOrder(\Miuze\AdminBundle\Entity\Order $order)
    {
        $this->order[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \Miuze\AdminBundle\Entity\Order $order
     */
    public function removeOrder(\Miuze\AdminBundle\Entity\Order $order)
    {
        $this->order->removeElement($order);
    }

    /**
     * Get order
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Add rabate
     *
     * @param \Miuze\AdminBundle\Entity\Rabate $rabate
     *
     * @return User
     */
    public function addRabate(\Miuze\AdminBundle\Entity\Rabate $rabate)
    {
        $this->rabate[] = $rabate;

        return $this;
    }

    /**
     * Remove rabate
     *
     * @param \Miuze\AdminBundle\Entity\Rabate $rabate
     */
    public function removeRabate(\Miuze\AdminBundle\Entity\Rabate $rabate)
    {
        $this->rabate->removeElement($rabate);
    }

    /**
     * Get rabate
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRabate()
    {
        return $this->rabate;
    }
}
