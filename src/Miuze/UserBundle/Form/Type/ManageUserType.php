<?php

namespace Miuze\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints as Assert;

class ManageUserType extends AbstractType
{  
    
    public function getName()
    {
        return 'manageUser';
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array(
                'label' => 'Imię',
                'attr' => array(
                    'placeholder' => 'Imię'
                )
            ))
            ->add('lastName', TextType::class, array(
                'label' => 'Nazwisko',
                'attr' => array(
                    'placeholder' => 'Nazwisko'
                )
            ))
            ->add('city', TextType::class, array(
                'label' => 'Miejscowość',
                'attr' => array(
                    'placeholder' => 'Miejscowość'
                )
            ))
            ->add('street', TextType::class, array(
                'label' => 'Ulica',
                'attr' => array(
                    'placeholder' => 'Ulica'
                )
            ))
            ->add('homeNumber', TextType::class, array(
                'label' => 'Numer domu',
                'attr' => array(
                    'placeholder' => 'Numer domu'
                )
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Telefon',
                'attr' => array(
                    'placeholder' => 'Telefon'
                )
            ))
            ->add('postCode', TextType::class, array(
                'label' => 'Kod pocztowy',
                'attr' => array(
                    'placeholder' => 'Kod pocztowy'
                )
            ))
            ->add('lead', TextareaType::class, array(
                'label' => 'Opis',
            ))

            ->add('email', EmailType::class, array(
                'label' => 'E-mail',
                'attr' => array(
                    'placeholder' => 'E-mail'
                )
            ))
            ->add('accountExpired', CheckboxType::class, array(
                'label' => 'Konto wygasło',
                'required' => FALSE
            ))
            ->add('accountLocked', CheckboxType::class, array(
                'label' => 'Konto zablokowane',
                'required' => FALSE
            ))
            ->add('credentialsExpired', CheckboxType::class, array(
                'label' => 'Dane uwierzytelniające wygasły',
                'required' => FALSE
            ))
            ->add('enabled', CheckboxType::class, array(
                'label' => 'Konto aktywowane',
                'required' => FALSE
            ))
            ->add('roles', ChoiceType::class, array(
                'label' => 'Role',
                'multiple' => true,
                'choices' => array(
                    'Klient niezarejestrowany' => 'ROLE_USER',
                    'Klient zarejestrowany' => 'ROLE_USER_REGISTER',
                    'Kontrahent' => 'ROLE_CONTRACTOR',
                    'Sprzedawca' => 'ROLE_SELLER',
                    'Administrator' => 'ROLE_ADMIN',
                )
            ))
            ->add('file', FileType::class, array(
                'label' => 'Avatar',
                'data_class' => NULL
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz'
            ))
            ;
    }
    
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\UserBundle\Entity\User'
        ));
    }
    
}