<?php

namespace Miuze\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;


class ChangePasswordType extends AbstractType {
    
    public function getName() {
        return 'changePassword';
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('currentPassword', PasswordType::class, array(
                'label' => 'Aktualne hasło',
                'mapped' => false,
                'constraints' => array(
                    new UserPassword(array(
                        'message' => 'Podano błędne aktualne hasło użytkownika'
                    ))
                )
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array(
                    'label' => 'Nowe hasło'
                ),
                'second_options' => array(
                    'label' => 'Powtórz hasło'
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zmień hasło'
            ));
    }
    
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\UserBundle\Entity\User',
            'validation_groups' => array('Default', 'ChangePassword')
        ));
    }
}