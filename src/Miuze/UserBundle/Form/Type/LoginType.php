<?php

namespace Miuze\UserBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class LoginType extends AbstractType{
    
    public function getName() {
        return 'login';
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('email', TextType::class, array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'E-mail',
                    )
                ))
                ->add('password', PasswordType::class, array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'Hasło',
                    )
                ))
                ->add('save', SubmitType::class, array(
                    'label' => 'Zaloguj'
                ));
    }
    
}