<?php

namespace Miuze\UserBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use Symfony\Component\Validator\Constraints as Assert;


class AccountSettingsType  extends AbstractType{
    
    public function getName() {
        return 'accountSettings';
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('email', EmailType::class, array(
                    'label' => 'E-mail',
                    'required' => FALSE
                ))
                ->add('firstName', TextType::class, array(
                    'label' => 'Imię',
                    'attr' => array(
                        'placeholder' => 'Imię'
                    )
                ))
                ->add('lastName', TextType::class, array(
                    'label' => 'Nazwisko',
                    'attr' => array(
                        'placeholder' => 'Nazwisko'
                    )
                ))
                ->add('city', TextType::class, array(
                    'label' => 'Miejscowość',
                    'attr' => array(
                        'placeholder' => 'Miejscowość'
                    )
                ))
                ->add('street', TextType::class, array(
                    'label' => 'Ulica',
                    'attr' => array(
                        'placeholder' => 'Ulica'
                    )
                ))
                ->add('homeNumber', TextType::class, array(
                    'label' => 'Numer domu',
                    'attr' => array(
                        'placeholder' => 'Numer domu'
                    )
                ))
                ->add('phone', TextType::class, array(
                    'label' => 'Telefon',
                    'attr' => array(
                        'placeholder' => 'Telefon'
                    )
                ))
                ->add('postCode', TextType::class, array(
                    'label' => 'Kod pocztowy',
                    'attr' => array(
                        'placeholder' => 'Kod pocztowy'
                    )
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Zapisz zmiany'
                ));
    }
    
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\UserBundle\Entity\User',
            'validation_groups' => array('Buyer')
        ));
    }
    
}