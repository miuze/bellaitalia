<?php
namespace Miuze\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;


class AddUserType extends AbstractType{
    
    public function getName() {
        return 'userRegister';
    }    
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder
            ->add('companyName', TextType::class, array(
                'label' => 'Nazwa firmy (opcjonalne).',
                'attr' => array(
                    'placeholder' => 'Wpisz nazwę firmy (opcjonalne).',
                ),
            ))
            ->add('nip', TextType::class, array(
                'label' => 'NIP (opcjonalne).',
                'attr' => array(
                    'placeholder' => 'Wpisz NIP (opcjonalne).',
                ),
            ))
            ->add('firstName', TextType::class, array(
                'label' => 'Imię',
                'attr' => array(
                    'placeholder' => 'Imię',
                ),
            ))
            ->add('lastName', TextType::class, array(
                'label' => 'Nazwisko',
                'attr' => array(
                    'id' => 'blindHeight',
                    'placeholder' => 'Nazwisko',
                ),
            ))
            ->add('city', TextType::class, array(
                'label' => 'Miejscowość',
                'attr' => array(
                    'placeholder' => 'Miejscowość',
                ),
            ))
            ->add('postCode', TextType::class, array(
                'label' => 'Kod pocztowy',
                'attr' => array(
                    'placeholder' => 'Kod pocztowy',
                ),
            ))
            ->add('street', TextType::class, array(
                'label' => 'Ulica',
                'attr' => array(
                    'placeholder' => 'Ulica',
                ),
            ))
            ->add('homeNumber', TextType::class, array(
                'label' => 'Numer domu',
                'attr' => array(
                    'placeholder' => 'Numer domu',
                ),
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'placeholder' => 'Email',
                ),
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Telefon',
                'attr' => array(
                    'placeholder' => 'Telefon',
                ),
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'label' => 'Hasło',
                'first_options' => array(
                    'label' => 'Nowe hasło'
                ),
                'second_options' => array(
                    'label' => 'Powtórz hasło'
                )
            ))
            ->add('accountExpired', CheckboxType::class, array(
                'label' => 'Konto wygasło',
                'required' => FALSE
            ))
            ->add('accountLocked', CheckboxType::class, array(
                'label' => 'Konto zablokowane',
                'required' => FALSE
            ))
            ->add('credentialsExpired', CheckboxType::class, array(
                'label' => 'Dane uwierzytelniające wygasły',
                'required' => FALSE
            ))
            ->add('enabled', CheckboxType::class, array(
                'label' => 'Konto aktywowane',
                'required' => FALSE
            ))
            ->add('roles', ChoiceType::class, array(
                'label' => 'Uprawnienia',
                'multiple' => true,
                'choices' => array(
                    'Klient niezarejestrowany' => 'ROLE_USER',
                    'Klient zarejestrowany' => 'ROLE_USER_REGISTER',
                    'Kontrahent' => 'ROLE_CONTRACTOR',
                    'Sprzedawca' => 'ROLE_SELLER',
                    'Administrator' => 'ROLE_ADMIN',
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj',
                'attr' => array(
                    'class'=> 'btn-0'
                )
            ));
    }
    
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\UserBundle\Entity\User',
            'validation_groups' => array('Registration')
        ));
    }
}
