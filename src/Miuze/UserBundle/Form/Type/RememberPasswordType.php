<?php

namespace Miuze\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;

class RememberPasswordType extends AbstractType {
    
    public function getName() {
        return 'rememberPassword';
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('clientEmail', EmailType::class, array(
                    'label' => 'Twój email',
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Email()
                    )
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Przypomnij hasło'
                ));
    }

}