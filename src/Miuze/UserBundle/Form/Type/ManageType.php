<?php

namespace Miuze\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints as Assert;

class ManageType extends AbstractType
{  
    
    public function getName()
    {
        return 'manage';
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Imię i nazwisko',
                'attr' => array(
                    'placeholder' => 'Imię i nazwisko'
                )
            ))
            ->add('lead', TextareaType::class, array(
                'label' => 'Opis',
            ))
            ->add('email', EmailType::class, array(
                'label' => 'E-mail',
                'attr' => array(
                    'placeholder' => 'E-mail'
                )
            ))
            ->add('file', FileType::class, array(
                'label' => 'Avatar',
                'data_class' => NULL
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz'
            ))
            ;
    }
    
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\UserBundle\Entity\User'
        ));
    }
    
}