<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 02.12.17
 * Time: 14:37
 */

namespace Miuze\AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Miuze\AdminBundle\Entity\Product;

class ProductService {

    protected $em;

    private $router;

    private $session;

    public function __construct(EntityManager $entityManager, $router, Session $session)
    {
        $this->em = $entityManager;
        $this->router = $router;
        $this->session = $session;
    }

    /**
     * @param Product $product
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function cntIncrement(Product $product){
        $cnt = $product->getCnt();
        $cnt++;
        $product->setCnt($cnt);
        $this->em->persist($product);
        $this->em->flush();
        return true;
    }

    /**
     * @param Product $product
     * @return float Price of product
     */
    public function getPrice(Product $product){
        if($product->getPromoPrice() != null){
            $price = $product->getPromoPrice();
        }else{
            $price = $product->getPrice();
        }
        return $price;
    }

    public function addProductLastView(Product $product){
        if($this->session->has('lastViewProduct')){
            $products = $this->session->get('lastViewProduct');
            if($this->productExist($product) == false) {
                array_unshift($products, $product);
                $this->session->set('lastViewProduct', $products);
            }
        }else{
            $products =[];
            array_unshift($products, $product);
            $this->session->set('lastViewProduct', $products);
        }
        return true;
    }

    protected function productExist(Product $product){
        $ex = false;
        $products = $this->session->get('lastViewProduct');
        foreach ($products as $item){
            if($product->getId() == $item->getId()){
                $ex = true;
            }
        }
        return $ex;
    }
}