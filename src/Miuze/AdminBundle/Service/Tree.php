<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 02.12.17
 * Time: 14:37
 */

namespace Miuze\AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Tree {

    protected $em;

    private $router;

    protected $childrenIndex = '__children';


    // We need to inject this variables later.
    public function __construct(EntityManager $entityManager, $router)
    {
        $this->em = $entityManager;
        $this->router = $router;
    }

    public function buildTreeHtmlAdmin(array $nodes, array $options = array())
    {
        $childrenIndex = $this->childrenIndex;
        $build = function ($tree) use (&$build, &$options, &$childrenIndex) {
            foreach ($tree as $key => $node) {
                if($key == 0) {
                    if ($node['parent'] == null) {
                        $output = is_string($options['level0Open']) ? $options['level0Open'] : $options['level0Open'];
                    } else {
                        $output = is_string($options['rootOpen']) ? $options['rootOpen'] : $options['rootOpen']($node);
                    }
                }
                $output .= is_string($options['childOpen']) ? $options['childOpen'] : $options['childOpen']($node);
                $output .= $options['nodeDecorator']($node);

                if (count($node[$childrenIndex]) > 0) {
                    $output .= $build($node[$childrenIndex]);
                }
                $output .= is_string($options['childClose']) ? $options['childClose'] : $options['childClose']($node);
            }

            return $output.(is_string($options['rootClose']) ? $options['rootClose'] : $options['rootClose']($tree));
        };

        return $build($nodes);
    }

    public function buildTreeHtml(array $nodes, array $options = array())
    {
        $childrenIndex = $this->childrenIndex;
        $build = function ($tree) use (&$build, &$options, &$childrenIndex) {
            foreach ($tree as $key => $node) {
                if($key == 0) {
                    if ($node['lvl'] === 1) {
                        $output = is_string($options['level0Open']) ? $options['level0Open'] : $options['level0Open'];
                    } else {
                        $output = is_string($options['rootOpen']) ? $options['rootOpen'] : $options['rootOpen']($node);
                    }
                }
                $output .= is_string($options['childOpen']) ? $options['childOpen'] : $options['childOpen']($node);
                $output .= $options['nodeDecorator']($node);

                if (count($node[$childrenIndex]) > 0) {
                    $output .= $build($node[$childrenIndex]);
                }
                $output .= is_string($options['childClose']) ? $options['childClose'] : $options['childClose']($node);
            }

            return $output.(is_string($options['rootClose']) ? $options['rootClose'] : $options['rootClose']($tree));
        };

        return $build($nodes);
    }

    public function buildProductCategoryTreeHtmlWithOpen(array $nodes, array $options = array())
    {
        $childrenIndex = $this->childrenIndex;
        $build = function ($tree) use (&$build, &$options, &$childrenIndex) {
            foreach ($tree as $key => $node) {
                $result = $this->getNodeOpen($node, $options['path']);
                if($key == 0) {
                    if ($result == true) {
                        $output = $options['levelOpen'];
                    } else {
                        $output = $options['rootOpen'];
                    }
                }
                $output .= $options['childOpen'];
                $output .= $options['nodeDecorator']($node, $options['pageid'], $options['title']);

                if (count($node[$childrenIndex]) > 0) {
                    $output .= $build($node[$childrenIndex]);
                }
                $output .= $options['childClose'];
            }
            return $output .= $options['rootClose'];
        };

        return $build($nodes);

    }

    private function getNodeOpen($node, $cats){
        foreach ($cats as $item){
            if($item->getId() == $node['id']) {
                return true;
            }
        }
        return false;
    }
    public function buildProductCategoryTreeHtml(array $nodes, array $options = array())
    {
        $childrenIndex = $this->childrenIndex;
        $build = function ($tree) use (&$build, &$options, &$childrenIndex) {
            $output = is_string($options['rootOpen']) ? $options['rootOpen'] : $options['rootOpen'];
            foreach ($tree as $key => $node) {
                $output .= is_string($options['childOpen']) ? $options['childOpen'] : $options['childOpen']($node);
                $output .= $options['nodeDecorator']($node, $options['pageid'], $options['title']);

                if (count($node[$childrenIndex]) > 0) {
                    $output .= $build($node[$childrenIndex]);
                }
                $output .= is_string($options['childClose']) ? $options['childClose'] : $options['childClose']($node);
            }

            return $output.(is_string($options['rootClose']) ? $options['rootClose'] : $options['rootClose']($tree));
        };

        return $build($nodes);
    }
}