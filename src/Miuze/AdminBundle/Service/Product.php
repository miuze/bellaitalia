<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 02.12.17
 * Time: 14:37
 */

namespace Miuze\AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Miuze\AdminBundle\Entity\Product;

class ProductService {

    protected $em;

    private $router;

    public function __construct(EntityManager $entityManager, $router)
    {
        $this->em = $entityManager;
        $this->router = $router;
    }

    public function cntIncrement(Product $product){
        $cnt = $product->getCnt();
        $cnt++;
        $product->setCnt($cnt);
        $this->em->persist($product);
        $this->em->flush();
    }
}