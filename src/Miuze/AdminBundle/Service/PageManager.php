<?php


namespace Miuze\AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Miuze\AdminBundle\Entity\Page;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;



/**
 * Description of PageManager
 *
 * @author Kamil
 */
class PageManager {
    
    protected $em;
    
    private $router;
    
    // We need to inject this variables later.
    public function __construct(EntityManager $entityManager, $router)
    {
        $this->em = $entityManager;
        $this->router = $router;
    }
    
    public function getPages($menu){
        
        $repository = $this->em->getRepository(Page::class);
        
        $options = array(
            'decorate' => true,
            'rootOpen' => '<ul>',
            'rootClose' => '</ul>',
            'childOpen' => '<li>',
            'childClose' => '</li>',
            'nodeDecorator' => function($node) {
                return '<a href="/page/'.$node['id'].'"></a>';
        });
        $query = $repository
            ->createQueryBuilder('p')
            ->select('p')
            ->andwhere('p.active = 1')
            ->andwhere('p.removed = 0')
            ->andWhere('p.category = :menu')
            ->setParameter('menu', $menu)
            ->getQuery();
        
        return $pagelist = $repository->buildTree($query->getArrayResult(), $options = array());
    }   
    
    
    public function getpage($page){
        $repo = $this->em->getRepository('MiuzeAdminBundle:Page');
        $pageList = $repo->findBy(array('visable' => 1, 'active' => 1 ));
        
        foreach($pageList as $pages){    
            if($pages->getId() == $page){
                return $page = $pages;
            }
        }
    
    }
    

    
}
