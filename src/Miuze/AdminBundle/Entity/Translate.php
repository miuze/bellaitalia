<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

class Translate {
           
    private $content;
    
    function getContent() {
        return $this->content;
    }

    function setContent($content) {
        $this->content = $content;
    }
}
