<?php

namespace Miuze\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="download_file")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\DownloadFileRepository")
 */
class DownloadFile {
    
    public function __construct() {
        $this->path = '/uploads/download/';
    }

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Download",
     *      inversedBy = "file"
     * )
     * @ORM\JoinTable(
     *      name = "download_file_to_download",
     * )
     */
    private $download;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "DownloadCategory",
     *      inversedBy = "file"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $categoryFile;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(
     *      maxSize="10M",
     *      mimeTypes = {
     *          "image/jpg", 
     *          "image/jpeg", 
     *          "image/gif", 
     *          "image/png",
     *          "application/pdf",
     *          "application/x-pdf",
     *      },
     *      mimeTypesMessage = "Plik musi być w formacie JPG, GIF ou PNG. oraz nie większy niż 10MB"
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    
    function getId() {
        return $this->id;
    }

    function getPath() {
        return $this->path;
    }

    function getFile() {
        return $this->file;
    }
    
    function setPath($path) {
        $this->path = $path;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function getImage() {
        return $this->image;
    }

    function setImage($image) {
        $this->image = $image;
    }  
    
    function getDownload() {
        return $this->download;
    }

    function setDownload($download) {
        $this->download = $download;
    }
    
    function getTitle() {
        return $this->title;
    }

    function setTitle($title) {
        $this->title = $title;
    }
    
    function getCategoryFile() {
        return $this->categoryFile;
    }

    function setCategoryFile($categoryFile) {
        $this->categoryFile = $categoryFile;
    }

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web'.$this->path;
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/download/'. $this->getCategoryFile()->getId().'/';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {        
        if (null !== $this->file) {
            // zrób cokolwiek chcesz aby wygenerować unikalną nazwę
            $name = strstr($this->file->getClientOriginalName(), '.', true);
            $name = str_replace(' ', '', $name);
            $this->setImage($name.'_'.uniqid().'.'.$this->file->guessExtension());
            $this->setPath('/'.$this->getUploadDir());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        
        if (null === $this->file) {
            return;
        }
        // musisz wyrzucać tutaj wyjątek jeśli plik nie może zostać przeniesiony
        // w tym przypadku encja nie zostanie zapisana do bazy
        // metoda move() obiektu UploadedFile robi to automatycznie
        //dump($this->getUploadRootDir()); die; 
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
    


    /**
     * Add download
     *
     * @param \Miuze\AdminBundle\Entity\Download $download
     *
     * @return DownloadFile
     */
    public function addDownload(\Miuze\AdminBundle\Entity\Download $download)
    {
        $this->download[] = $download;

        return $this;
    }

    /**
     * Remove download
     *
     * @param \Miuze\AdminBundle\Entity\Download $download
     */
    public function removeDownload(\Miuze\AdminBundle\Entity\Download $download)
    {
        $this->download->removeElement($download);
    }
}
