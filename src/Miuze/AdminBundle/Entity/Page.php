<?php

namespace Miuze\AdminBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Miuze\AdminBundle\Enum\SlugEnum;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ClassMetadata;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @Gedmo\Tree(type="nested")
* @ORM\Entity
* @ORM\Table(name="page")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\PageRepository")
* 
*/

class Page {
    public function __construct() {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**     
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\AdminBundle\Entity\Section",
     *      mappedBy = "page",
     *     cascade={"all"}
     * )
     */
    protected $sections;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "PageCategory",
     *      inversedBy = "pages"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "\Miuze\AdminBundle\Entity\Slug",
     *      inversedBy = "page",
     *      cascade={"all"}
     * )
     * @ORM\JoinColumn(
     *      name = "slug_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $subtitle;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lead;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $seoDescription;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $class;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $pageid;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $visable = 1;
     
    /**
     * @ORM\Column(type="boolean")
     */
    private $showInPage = 1;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $active = 1;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $removed = 0;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $link;
    
    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $type;
    
    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $layout;
    
    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $view;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "PageGallery",
     *      inversedBy = "gallerypage"
     * )
     * @ORM\JoinColumn(
     *      name = "gallery_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $gallerypage;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "NewsCategory",
     *      mappedBy = "page"
     * )
     */
    private $newsCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "VideoCategory",
     *      mappedBy = "page"
     * )
     */
    private $videoCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "AdCategory",
     *      mappedBy = "page"
     * )
     */
    private $adCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "AwardCategory",
     *      mappedBy = "page"
     * )
     */
    private $awardCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Build",
     *      mappedBy = "page"
     * )
     */
    private $build;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "DownloadCategory",
     *      mappedBy = "page"
     * )
     */
    private $downloadCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "ClientCategory",
     *      mappedBy = "page"
     * )
     */
    private $clientCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "BlogCategory",
     *      mappedBy = "page"
     * )
     */
    private $blogCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "FaqCategory",
     *      mappedBy = "page"
     * )
     */
    private $faqCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "GalleryCategory",
     *      mappedBy = "page"
     * )
     */
    private $galleryCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "PersonCategory",
     *      mappedBy = "page"
     * )
     */
    private $personCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "PlaceCategory",
     *      mappedBy = "page"
     * )
     */
    private $placeCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "ProductCategory",
     *      mappedBy = "page"
     * )
     */
    private $productCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "RealizationCategory",
     *      mappedBy = "page"
     * )
     */
    private $realizationCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "PromotionCategory",
     *      mappedBy = "page"
     * )
     */
    private $promotionCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "ReferenceCategory",
     *      mappedBy = "page"
     * )
     */
    private $referenceCategory;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "BannerCategory",
     *      mappedBy = "page"
     * )
     */
    private $bannerCategory;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Top",
     *      inversedBy = "top"
     * )
     * @ORM\JoinColumn(
     *      name = "top_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $top;
    
    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="Page")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;
    
    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;
    
     /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     * )
     */
    private $sort = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return Page
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set lead
     *
     * @param string $lead
     *
     * @return Page
     */
    public function setLead($lead)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return string
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     *
     * @return Page
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set class
     *
     * @param string $class
     *
     * @return Page
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set pageid
     *
     * @param string $pageid
     *
     * @return Page
     */
    public function setPageid($pageid)
    {
        $this->pageid = $pageid;

        return $this;
    }

    /**
     * Get pageid
     *
     * @return string
     */
    public function getPageid()
    {
        return $this->pageid;
    }

    /**
     * Set visable
     *
     * @param boolean $visable
     *
     * @return Page
     */
    public function setVisable($visable)
    {
        $this->visable = $visable;

        return $this;
    }

    /**
     * Get visable
     *
     * @return boolean
     */
    public function getVisable()
    {
        return $this->visable;
    }

    /**
     * Set showInPage
     *
     * @param boolean $showInPage
     *
     * @return Page
     */
    public function setShowInPage($showInPage)
    {
        $this->showInPage = $showInPage;

        return $this;
    }

    /**
     * Get showInPage
     *
     * @return boolean
     */
    public function getShowInPage()
    {
        return $this->showInPage;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Page
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set removed
     *
     * @param boolean $removed
     *
     * @return Page
     */
    public function setRemoved($removed)
    {
        $this->removed = $removed;

        return $this;
    }

    /**
     * Get removed
     *
     * @return boolean
     */
    public function getRemoved()
    {
        return $this->removed;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Page
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Page
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set layout
     *
     * @param string $layout
     *
     * @return Page
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * Get layout
     *
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Set view
     *
     * @param string $view
     *
     * @return Page
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return Page
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Page
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return Page
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return Page
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set category
     *
     * @param \Miuze\AdminBundle\Entity\PageCategory $category
     *
     * @return Page
     */
    public function setCategory(\Miuze\AdminBundle\Entity\PageCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Miuze\AdminBundle\Entity\PageCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set slug
     *
     * @param \Miuze\AdminBundle\Entity\Slug $slug
     *
     * @return Page
     */
    public function setSlug(\Miuze\AdminBundle\Entity\Slug $slug = null)
    {
        $slug->setType(SlugEnum::PAGE);
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return \Miuze\AdminBundle\Entity\Slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set lang
     *
     * @param \Miuze\AdminBundle\Entity\Lang $lang
     *
     * @return Page
     */
    public function setLang(\Miuze\AdminBundle\Entity\Lang $lang = null)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return \Miuze\AdminBundle\Entity\Lang
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set gallerypage
     *
     * @param \Miuze\AdminBundle\Entity\PageGallery $gallerypage
     *
     * @return Page
     */
    public function setGallerypage(\Miuze\AdminBundle\Entity\PageGallery $gallerypage = null)
    {
        $this->gallerypage = $gallerypage;

        return $this;
    }

    /**
     * Get gallerypage
     *
     * @return \Miuze\AdminBundle\Entity\PageGallery
     */
    public function getGallerypage()
    {
        return $this->gallerypage;
    }

    /**
     * Add newsCategory
     *
     * @param \Miuze\AdminBundle\Entity\NewsCategory $newsCategory
     *
     * @return Page
     */
    public function addNewsCategory(\Miuze\AdminBundle\Entity\NewsCategory $newsCategory)
    {
        $this->newsCategory[] = $newsCategory;

        return $this;
    }

    /**
     * Remove newsCategory
     *
     * @param \Miuze\AdminBundle\Entity\NewsCategory $newsCategory
     */
    public function removeNewsCategory(\Miuze\AdminBundle\Entity\NewsCategory $newsCategory)
    {
        $this->newsCategory->removeElement($newsCategory);
    }

    /**
     * Get newsCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNewsCategory()
    {
        return $this->newsCategory;
    }

    /**
     * Add videoCategory
     *
     * @param \Miuze\AdminBundle\Entity\VideoCategory $videoCategory
     *
     * @return Page
     */
    public function addVideoCategory(\Miuze\AdminBundle\Entity\VideoCategory $videoCategory)
    {
        $this->videoCategory[] = $videoCategory;

        return $this;
    }

    /**
     * Remove videoCategory
     *
     * @param \Miuze\AdminBundle\Entity\VideoCategory $videoCategory
     */
    public function removeVideoCategory(\Miuze\AdminBundle\Entity\VideoCategory $videoCategory)
    {
        $this->videoCategory->removeElement($videoCategory);
    }

    /**
     * Get videoCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideoCategory()
    {
        return $this->videoCategory;
    }

    /**
     * Add adCategory
     *
     * @param \Miuze\AdminBundle\Entity\AdCategory $adCategory
     *
     * @return Page
     */
    public function addAdCategory(\Miuze\AdminBundle\Entity\AdCategory $adCategory)
    {
        $this->adCategory[] = $adCategory;

        return $this;
    }

    /**
     * Remove adCategory
     *
     * @param \Miuze\AdminBundle\Entity\AdCategory $adCategory
     */
    public function removeAdCategory(\Miuze\AdminBundle\Entity\AdCategory $adCategory)
    {
        $this->adCategory->removeElement($adCategory);
    }

    /**
     * Get adCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdCategory()
    {
        return $this->adCategory;
    }

    /**
     * Add awardCategory
     *
     * @param \Miuze\AdminBundle\Entity\AwardCategory $awardCategory
     *
     * @return Page
     */
    public function addAwardCategory(\Miuze\AdminBundle\Entity\AwardCategory $awardCategory)
    {
        $this->awardCategory[] = $awardCategory;

        return $this;
    }

    /**
     * Remove awardCategory
     *
     * @param \Miuze\AdminBundle\Entity\AwardCategory $awardCategory
     */
    public function removeAwardCategory(\Miuze\AdminBundle\Entity\AwardCategory $awardCategory)
    {
        $this->awardCategory->removeElement($awardCategory);
    }

    /**
     * Get awardCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAwardCategory()
    {
        return $this->awardCategory;
    }

    /**
     * Add build
     *
     * @param \Miuze\AdminBundle\Entity\Build $build
     *
     * @return Page
     */
    public function addBuild(\Miuze\AdminBundle\Entity\Build $build)
    {
        $this->build[] = $build;

        return $this;
    }

    /**
     * Remove build
     *
     * @param \Miuze\AdminBundle\Entity\Build $build
     */
    public function removeBuild(\Miuze\AdminBundle\Entity\Build $build)
    {
        $this->build->removeElement($build);
    }

    /**
     * Get build
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBuild()
    {
        return $this->build;
    }

    /**
     * Add downloadCategory
     *
     * @param \Miuze\AdminBundle\Entity\DownloadCategory $downloadCategory
     *
     * @return Page
     */
    public function addDownloadCategory(\Miuze\AdminBundle\Entity\DownloadCategory $downloadCategory)
    {
        $this->downloadCategory[] = $downloadCategory;

        return $this;
    }

    /**
     * Remove downloadCategory
     *
     * @param \Miuze\AdminBundle\Entity\DownloadCategory $downloadCategory
     */
    public function removeDownloadCategory(\Miuze\AdminBundle\Entity\DownloadCategory $downloadCategory)
    {
        $this->downloadCategory->removeElement($downloadCategory);
    }

    /**
     * Get downloadCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDownloadCategory()
    {
        return $this->downloadCategory;
    }

    /**
     * Add clientCategory
     *
     * @param \Miuze\AdminBundle\Entity\ClientCategory $clientCategory
     *
     * @return Page
     */
    public function addClientCategory(\Miuze\AdminBundle\Entity\ClientCategory $clientCategory)
    {
        $this->clientCategory[] = $clientCategory;

        return $this;
    }

    /**
     * Remove clientCategory
     *
     * @param \Miuze\AdminBundle\Entity\ClientCategory $clientCategory
     */
    public function removeClientCategory(\Miuze\AdminBundle\Entity\ClientCategory $clientCategory)
    {
        $this->clientCategory->removeElement($clientCategory);
    }

    /**
     * Get clientCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientCategory()
    {
        return $this->clientCategory;
    }

    /**
     * Add blogCategory
     *
     * @param \Miuze\AdminBundle\Entity\BlogCategory $blogCategory
     *
     * @return Page
     */
    public function addBlogCategory(\Miuze\AdminBundle\Entity\BlogCategory $blogCategory)
    {
        $this->blogCategory[] = $blogCategory;

        return $this;
    }

    /**
     * Remove blogCategory
     *
     * @param \Miuze\AdminBundle\Entity\BlogCategory $blogCategory
     */
    public function removeBlogCategory(\Miuze\AdminBundle\Entity\BlogCategory $blogCategory)
    {
        $this->blogCategory->removeElement($blogCategory);
    }

    /**
     * Get blogCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlogCategory()
    {
        return $this->blogCategory;
    }

    /**
     * Add faqCategory
     *
     * @param \Miuze\AdminBundle\Entity\FaqCategory $faqCategory
     *
     * @return Page
     */
    public function addFaqCategory(\Miuze\AdminBundle\Entity\FaqCategory $faqCategory)
    {
        $this->faqCategory[] = $faqCategory;

        return $this;
    }

    /**
     * Remove faqCategory
     *
     * @param \Miuze\AdminBundle\Entity\FaqCategory $faqCategory
     */
    public function removeFaqCategory(\Miuze\AdminBundle\Entity\FaqCategory $faqCategory)
    {
        $this->faqCategory->removeElement($faqCategory);
    }

    /**
     * Get faqCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFaqCategory()
    {
        return $this->faqCategory;
    }

    /**
     * Add galleryCategory
     *
     * @param \Miuze\AdminBundle\Entity\GalleryCategory $galleryCategory
     *
     * @return Page
     */
    public function addGalleryCategory(\Miuze\AdminBundle\Entity\GalleryCategory $galleryCategory)
    {
        $this->galleryCategory[] = $galleryCategory;

        return $this;
    }

    /**
     * Remove galleryCategory
     *
     * @param \Miuze\AdminBundle\Entity\GalleryCategory $galleryCategory
     */
    public function removeGalleryCategory(\Miuze\AdminBundle\Entity\GalleryCategory $galleryCategory)
    {
        $this->galleryCategory->removeElement($galleryCategory);
    }

    /**
     * Get galleryCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGalleryCategory()
    {
        return $this->galleryCategory;
    }

    /**
     * Add personCategory
     *
     * @param \Miuze\AdminBundle\Entity\PersonCategory $personCategory
     *
     * @return Page
     */
    public function addPersonCategory(\Miuze\AdminBundle\Entity\PersonCategory $personCategory)
    {
        $this->personCategory[] = $personCategory;

        return $this;
    }

    /**
     * Remove personCategory
     *
     * @param \Miuze\AdminBundle\Entity\PersonCategory $personCategory
     */
    public function removePersonCategory(\Miuze\AdminBundle\Entity\PersonCategory $personCategory)
    {
        $this->personCategory->removeElement($personCategory);
    }

    /**
     * Get personCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonCategory()
    {
        return $this->personCategory;
    }

    /**
     * Add placeCategory
     *
     * @param \Miuze\AdminBundle\Entity\PlaceCategory $placeCategory
     *
     * @return Page
     */
    public function addPlaceCategory(\Miuze\AdminBundle\Entity\PlaceCategory $placeCategory)
    {
        $this->placeCategory[] = $placeCategory;

        return $this;
    }

    /**
     * Remove placeCategory
     *
     * @param \Miuze\AdminBundle\Entity\PlaceCategory $placeCategory
     */
    public function removePlaceCategory(\Miuze\AdminBundle\Entity\PlaceCategory $placeCategory)
    {
        $this->placeCategory->removeElement($placeCategory);
    }

    /**
     * Get placeCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlaceCategory()
    {
        return $this->placeCategory;
    }

    /**
     * Add productCategory
     *
     * @param \Miuze\AdminBundle\Entity\ProductCategory $productCategory
     *
     * @return Page
     */
    public function addProductCategory(\Miuze\AdminBundle\Entity\ProductCategory $productCategory)
    {
        $this->productCategory[] = $productCategory;

        return $this;
    }

    /**
     * Remove productCategory
     *
     * @param \Miuze\AdminBundle\Entity\ProductCategory $productCategory
     */
    public function removeProductCategory(\Miuze\AdminBundle\Entity\ProductCategory $productCategory)
    {
        $this->productCategory->removeElement($productCategory);
    }

    /**
     * Get productCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductCategory()
    {
        return $this->productCategory;
    }

    /**
     * Add realizationCategory
     *
     * @param \Miuze\AdminBundle\Entity\RealizationCategory $realizationCategory
     *
     * @return Page
     */
    public function addRealizationCategory(\Miuze\AdminBundle\Entity\RealizationCategory $realizationCategory)
    {
        $this->realizationCategory[] = $realizationCategory;

        return $this;
    }

    /**
     * Remove realizationCategory
     *
     * @param \Miuze\AdminBundle\Entity\RealizationCategory $realizationCategory
     */
    public function removeRealizationCategory(\Miuze\AdminBundle\Entity\RealizationCategory $realizationCategory)
    {
        $this->realizationCategory->removeElement($realizationCategory);
    }

    /**
     * Get realizationCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRealizationCategory()
    {
        return $this->realizationCategory;
    }

    /**
     * Add promotionCategory
     *
     * @param \Miuze\AdminBundle\Entity\PromotionCategory $promotionCategory
     *
     * @return Page
     */
    public function addPromotionCategory(\Miuze\AdminBundle\Entity\PromotionCategory $promotionCategory)
    {
        $this->promotionCategory[] = $promotionCategory;

        return $this;
    }

    /**
     * Remove promotionCategory
     *
     * @param \Miuze\AdminBundle\Entity\PromotionCategory $promotionCategory
     */
    public function removePromotionCategory(\Miuze\AdminBundle\Entity\PromotionCategory $promotionCategory)
    {
        $this->promotionCategory->removeElement($promotionCategory);
    }

    /**
     * Get promotionCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPromotionCategory()
    {
        return $this->promotionCategory;
    }

    /**
     * Add referenceCategory
     *
     * @param \Miuze\AdminBundle\Entity\ReferenceCategory $referenceCategory
     *
     * @return Page
     */
    public function addReferenceCategory(\Miuze\AdminBundle\Entity\ReferenceCategory $referenceCategory)
    {
        $this->referenceCategory[] = $referenceCategory;

        return $this;
    }

    /**
     * Remove referenceCategory
     *
     * @param \Miuze\AdminBundle\Entity\ReferenceCategory $referenceCategory
     */
    public function removeReferenceCategory(\Miuze\AdminBundle\Entity\ReferenceCategory $referenceCategory)
    {
        $this->referenceCategory->removeElement($referenceCategory);
    }

    /**
     * Get referenceCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferenceCategory()
    {
        return $this->referenceCategory;
    }

    /**
     * Add bannerCategory
     *
     * @param \Miuze\AdminBundle\Entity\BannerCategory $bannerCategory
     *
     * @return Page
     */
    public function addBannerCategory(\Miuze\AdminBundle\Entity\BannerCategory $bannerCategory)
    {
        $this->bannerCategory[] = $bannerCategory;

        return $this;
    }

    /**
     * Remove bannerCategory
     *
     * @param \Miuze\AdminBundle\Entity\BannerCategory $bannerCategory
     */
    public function removeBannerCategory(\Miuze\AdminBundle\Entity\BannerCategory $bannerCategory)
    {
        $this->bannerCategory->removeElement($bannerCategory);
    }

    /**
     * Get bannerCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBannerCategory()
    {
        return $this->bannerCategory;
    }

    /**
     * Set top
     *
     * @param \Miuze\AdminBundle\Entity\Top $top
     *
     * @return Page
     */
    public function setTop(\Miuze\AdminBundle\Entity\Top $top = null)
    {
        $this->top = $top;

        return $this;
    }

    /**
     * Get top
     *
     * @return \Miuze\AdminBundle\Entity\Top
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * Set root
     *
     * @param \Miuze\AdminBundle\Entity\Page $root
     *
     * @return Page
     */
    public function setRoot(\Miuze\AdminBundle\Entity\Page $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return \Miuze\AdminBundle\Entity\Page
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set parent
     *
     * @param \Miuze\AdminBundle\Entity\Page $parent
     *
     * @return Page
     */
    public function setParent(\Miuze\AdminBundle\Entity\Page $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Miuze\AdminBundle\Entity\Page
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param \Miuze\AdminBundle\Entity\Page $child
     *
     * @return Page
     */
    public function addChild(\Miuze\AdminBundle\Entity\Page $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Miuze\AdminBundle\Entity\Page $child
     */
    public function removeChild(\Miuze\AdminBundle\Entity\Page $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }
    

    /**
     * Add section.
     *
     * @param \Miuze\AdminBundle\Entity\Section $section
     *
     * @return Page
     */
    public function addSection(\Miuze\AdminBundle\Entity\Section $section)
    {
        $section->setPage($this);
        $this->sections[] = $section;

        return $this;
    }

    /**
     * Remove section.
     *
     * @param \Miuze\AdminBundle\Entity\Section $section
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSection(\Miuze\AdminBundle\Entity\Section $section)
    {
        return $this->sections->removeElement($section);
    }

    /**
     * Get sections.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSections()
    {
        return $this->sections;
    }
}
