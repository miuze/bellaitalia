<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="newsletter")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\NewsletterRepository")
*/
class Newsletter{
    
    public function __construct()
    {
        $this->date = new \DateTime('now');
    }
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;
    
    /**
     * @ORM\Column(name="action_token", type="string", length = 20, nullable = true)
     */
    private $actionToken;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $active = 0;
    
    /**
     * @ORM\Column(name="submitdate", type="datetime")
     * @var type 
     */
    private  $date;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "NewsletterCategory",
     *      inversedBy = "newsletter"
     * )
     * @ORM\JoinTable(
     *      name = "newslleter_category_to_newsletter",
     * )
     */
    private $category;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Offer",
     *      mappedBy = "subscriber"
     * )
     */
    protected $offer;

    function getId() {
        return $this->id;
    }

    function getEmail() {
        return $this->email;
    }

    function getActionToken() {
        return $this->actionToken;
    }

    function getActive() {
        return $this->active;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setActionToken($actionToken) {
        $this->actionToken = $actionToken;
    }

    function setActive($active) {
        $this->active = $active;
    }
    
    function getCategory() {
        return $this->category;
    }

    function setCategory($category) {
        $this->category = $category;
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Newsletter
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Add category
     *
     * @param \Miuze\AdminBundle\Entity\NewsletterCategory $category
     *
     * @return Newsletter
     */
    public function addCategory(\Miuze\AdminBundle\Entity\NewsletterCategory $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \Miuze\AdminBundle\Entity\NewsletterCategory $category
     */
    public function removeCategory(\Miuze\AdminBundle\Entity\NewsletterCategory $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Add offer
     *
     * @param \Miuze\AdminBundle\Entity\Offer $offer
     *
     * @return Newsletter
     */
    public function addOffer(\Miuze\AdminBundle\Entity\Offer $offer)
    {
        $this->offer[] = $offer;

        return $this;
    }

    /**
     * Remove offer
     *
     * @param \Miuze\AdminBundle\Entity\Offer $offer
     */
    public function removeOffer(\Miuze\AdminBundle\Entity\Offer $offer)
    {
        $this->offer->removeElement($offer);
    }

    /**
     * Get offer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffer()
    {
        return $this->offer;
    }
}
