<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="reference_category")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\ReferenceRepository")
*/

class ReferenceCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "referenceCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "referenceCategory"
     * )
     * @ORM\JoinTable(
     *      name = "reference_category_to_page",
     * )
     */
    private $page;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Reference",
     *      mappedBy = "category"
     * )
     */
    protected $reference;

    function getId() {
        return $this->id;
    }

    function getPage() {
        return $this->page;
    }

    function getTitle() {
        return $this->title;
    }

    function getReference() {
        return $this->reference;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPage($page) {
        $this->page = $page;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setReference($reference) {
        $this->reference = $reference;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reference = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return ReferenceCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add reference
     *
     * @param \Miuze\AdminBundle\Entity\Reference $reference
     *
     * @return ReferenceCategory
     */
    public function addReference(\Miuze\AdminBundle\Entity\Reference $reference)
    {
        $this->reference[] = $reference;

        return $this;
    }

    /**
     * Remove reference
     *
     * @param \Miuze\AdminBundle\Entity\Reference $reference
     */
    public function removeReference(\Miuze\AdminBundle\Entity\Reference $reference)
    {
        $this->reference->removeElement($reference);
    }
    
    function getLang() {
        return $this->lang;
    }

    function setLang($lang) {
        $this->lang = $lang;
    }
}
