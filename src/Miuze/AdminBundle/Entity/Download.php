<?php

namespace Miuze\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity
     * @ORM\Table(name="download")
     * @ORM\HasLifecycleCallbacks
     */
class Download {
    
    public function __construct()
    {
        $this->updateDate = new \DateTime('now');
        $this->startDate = new \DateTime('now');
        $this->createDate = new \DateTime('now');
        $this->startDate = new \DateTime('now');
        $this->endDate = NULL;
    }

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "DownloadCategory",
     *      inversedBy = "download"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "DownloadFile",
     *      mappedBy = "download"
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $startDate;
    
    /**
     * @ORM\Column(type="date", nullable = true)
     * @Assert\Date()
     */
    private $endDate;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $updateDate;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $createDate;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $title;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $urlTitle;
    
    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     * )
     */
    private $sort = 0;
    
    
    
    function getTitle() {
        return $this->title;
    }

    function getUrlTitle() {
        return $this->urlTitle;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setUrlTitle($urlTitle) {
        $this->urlTitle = $urlTitle;
    }

    function getFile() {
        return $this->file;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function getId() {
        return $this->id;
    }

    function getCategory() {
        return $this->category;
    }

    function getStartDate() {
        return $this->startDate;
    }

    function getEndDate() {
        return $this->endDate;
    }

    function getUpdateDate() {
        return $this->updateDate;
    }

    function getCreateDate() {
        return $this->createDate;
    }

    function getContent() {
        return $this->content;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    function setUpdateDate($updateDate) {
        $this->updateDate = $updateDate;
    }

    function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }

    function setContent($content) {
        $this->content = $content;
    }
    function getSort() {
        return $this->sort;
    }

    function setSort($sort) {
        $this->sort = $sort;
    }

    /**
     * Add file
     *
     * @param \Miuze\AdminBundle\Entity\DownloadFile $file
     *
     * @return Download
     */
    public function addFile(\Miuze\AdminBundle\Entity\DownloadFile $file)
    {
        $this->file[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \Miuze\AdminBundle\Entity\DownloadFile $file
     */
    public function removeFile(\Miuze\AdminBundle\Entity\DownloadFile $file)
    {
        $this->file->removeElement($file);
    }
}
