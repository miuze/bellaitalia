<?php

namespace Miuze\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity
     * @ORM\Table(name="top")
     * @ORM\HasLifecycleCallbacks
     */
class Top {
    
    public function __construct()
    {
        $this->path = '/uploads/top/';
    }
    
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
        
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Page",
     *      mappedBy = "top"
     * )
     */
    protected $top;
    
    function getTop() {
        return $this->top;
    }

    function setTop($top) {
        $this->top = $top;
    }

        
    function getPage() {
        return $this->page;
    }

    function setPage($page) {
        $this->page = $page;
    }

        
    function getId() {
        return $this->id;
    }


    function getPath() {
        return $this->path;
    }

    function getFile() {
        return $this->file;
    }

    function setPath($path) {
        $this->path = $path;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function getImage() {
        return $this->image;
    }

    function setImage($image) {
        $this->image = $image;
    }

        
    
        
    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/top/';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            // zrób cokolwiek chcesz aby wygenerować unikalną nazwę
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // musisz wyrzucać tutaj wyjątek jeśli plik nie może zostać przeniesiony
        // w tym przypadku encja nie zostanie zapisana do bazy
        // metoda move() obiektu UploadedFile robi to automatycznie
        //dump($this->getUploadRootDir()); die; 
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
    


    /**
     * Add top
     *
     * @param \Miuze\AdminBundle\Entity\Page $top
     *
     * @return Top
     */
    public function addTop(\Miuze\AdminBundle\Entity\Page $top)
    {
        $this->top[] = $top;

        return $this;
    }

    /**
     * Remove top
     *
     * @param \Miuze\AdminBundle\Entity\Page $top
     */
    public function removeTop(\Miuze\AdminBundle\Entity\Page $top)
    {
        $this->top->removeElement($top);
    }
}
