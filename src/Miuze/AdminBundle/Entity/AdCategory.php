<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="ad_category")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\AdRepository")
*/

class AdCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "adCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "adCategory"
     * )
     * @ORM\JoinTable(
     *      name = "ad_category_to_page",
     * )
     */
    private $page;
    
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Ad",
     *      mappedBy = "category"
     * )
     */
    protected $ad;
    
    function getId() {
        return $this->id;
    }

    function getPage() {
        return $this->page;
    }

    function getTitle() {
        return $this->title;
    }

    function getAd() {
        return $this->ad;
    }

    function setPage($page) {
        $this->page = $page;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setAd($ad) {
        $this->ad = $ad;
    }
    
    function getLang() {
        return $this->lang;
    }

    function setLang($lang) {
        $this->lang = $lang;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ad = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return AdCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add ad
     *
     * @param \Miuze\AdminBundle\Entity\Ad $ad
     *
     * @return AdCategory
     */
    public function addAd(\Miuze\AdminBundle\Entity\Ad $ad)
    {
        $this->ad[] = $ad;

        return $this;
    }

    /**
     * Remove ad
     *
     * @param \Miuze\AdminBundle\Entity\Ad $ad
     */
    public function removeAd(\Miuze\AdminBundle\Entity\Ad $ad)
    {
        $this->ad->removeElement($ad);
    }
}
