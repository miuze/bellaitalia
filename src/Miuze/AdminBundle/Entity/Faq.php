<?php

namespace Miuze\AdminBundle\Entity;

use Miuze\AdminBundle\MiuzeAdminBundle;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="faq") 
* @ORM\HasLifecycleCallbacks
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\FaqRepository")
*/
class Faq {
           
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "FaqCategory",
     *      inversedBy = "faq"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lead;
    
    /**
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $sort = 0;
    
    function getId() {
        return $this->id;
    }

    function getCategory() {
        return $this->category;
    }

    function getTitle() {
        return $this->title;
    }

    function getContent() {
        return $this->content;
    }

    function getFile() {
        return $this->file;
    }

    function getPath() {
        return $this->path;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function setPath($path) {
        $this->path = $path;
    }
    function getImage() {
        return $this->image;
    }

    function setImage($image) {
        $this->image = $image;
    }
    
    function getSort() {
        return $this->sort;
    }

    function setSort($sort) {
        $this->sort = $sort;
    }

    /**
     * @return mixed
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * @param mixed $lead
     */
    public function setLead($lead)
    {
        $this->lead = $lead;
    }

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/faq/'. $this->getCategory()->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {   
            $this->setPath('/uploads/faq/'.$this->getCategory()->getId().'/');
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
        if($this->lead == null && $this->content !== null){
            $content = strip_tags($this->content);
            $content = \Miuze\AdminBundle\Utils\Functions::WordLimiter($content);
            $this->lead = $content;
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}
