<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="offer_product")
*/
class ProductOffer
{


    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Offer",
     *      inversedBy = "productItems"
     * )
     * @ORM\JoinColumn(
     *      name = "offer_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $offer;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Product",
     *      inversedBy = "productOffer"
     * )
     * @ORM\JoinColumn(
     *      name = "product_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $product;

    /**
     * @ORM\Column(type="integer", length=4)
     * @Assert\NotBlank
     * @Assert\Regex(
     *      pattern="/\d+/",
     *      message="Wartość powinna być liczbą załkowitą"
     * )
     */
    private $quantity;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ProductOffer
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return ProductOffer
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set offer
     *
     * @param \Miuze\AdminBundle\Entity\Offer $offer
     *
     * @return ProductOffer
     */
    public function setOffer(\Miuze\AdminBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return \Miuze\AdminBundle\Entity\Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Set product
     *
     * @param \Miuze\AdminBundle\Entity\Product $product
     *
     * @return ProductOffer
     */
    public function setProduct(\Miuze\AdminBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Miuze\AdminBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
