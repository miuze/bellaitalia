<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Description of Page
 *
 * @author Kamil
 */

class Contact {
   
    /**
    * @Assert\NotBlank(
     *     groups = {"contact"}
     * )
    * @Assert\Length(
    *      max=255,
     *     groups = {"contact"}
    * )
    */
    private $title;
    
    /**
    * @Assert\NotBlank(
     *     groups = {"contact", "fastcontact"}
     * )
    * @Assert\Length(
    *      min=30,
     *     groups = {"contact", "fastcontact"}
    * )
    */
    private $content;
    
    /**
    * @Assert\Email
    * @Assert\NotBlank(
     *     groups = {"contact", "fastcontact"}
     * )
    */
    private $email;
    
    function getTitle() {
        return $this->title;
    }

    function getContent() {
        return $this->content;
    }

    function getEmail() {
        return $this->email;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setEmail($email) {
        $this->email = $email;
    }


}
