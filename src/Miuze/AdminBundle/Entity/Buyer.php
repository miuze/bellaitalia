<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="buyer") 
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\BuyerRepository")
*/
class Buyer {
           
    public function __construct()
    {
        $this->createDate = new \DateTime('now');
    }
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
    * @ORM\Column(type="text", length=50)
    * @Assert\NotBlank()
    * @Assert\Length(
    *      min = 3,
    *      max = 50,
    *      minMessage = "Imię musi mieć conajmniej 3 znaki",
    *      minMessage = "Imię nie może zawierać więcej nież 50 znaków"
    * )
    */
    private $firstName;
    
    /**
    * @ORM\Column(type="text", length=100)
    * @Assert\NotBlank()
    * @Assert\Length(
    *      min = 3,
    *      max = 100,
    *      minMessage = "Nazwisko musi mieć conajmniej 3 znaki",
    *      minMessage = "Nazwisko nie może zawierać więcej nież 100 znaków"
    * )
    */
    private $lastName;
    
    /**
    * @ORM\Column(type="text", length=100)
    * @Assert\NotBlank()
    * @Assert\Length(
    *      min = 3,
    *      max = 100,
    *      minMessage = "Miasto musi mieć conajmniej 3 znaki",
    *      minMessage = "Miasto nie może zawierać więcej nież 100 znaków"
    * )
    */
    private $city;
    
    /**
    * @ORM\Column(type="text", length=5)
    * @Assert\NotBlank()
    * @Assert\Regex(
    *      pattern     = "/^([0-9]){2}-([0-9]){3}/",
    *      message = "Format musi składać się z XX-XXX"
    * )
    */
    private $postCode;
    
    /**
    * @ORM\Column(type="text", length=100)
    * @Assert\NotBlank()
    * @Assert\Length(
    *      min = 3,
    *      max = 100,
    *      minMessage = "Ulica musi mieć conajmniej 3 znaki",
    *      minMessage = "Ulica nie może zawierać więcej nież 100 znaków"
    * )
    */
    private $street;
    
    /**
    * @ORM\Column(type="text", length=10)
    * @Assert\NotBlank()
    * @Assert\Length(
    *      min = 1,
    *      max = 10,
    *      minMessage = "Numer musi mieć conajmniej 1 znak",
    *      minMessage = "Numer nie może zawierać więcej nież 10 znaków"
    * )
    */
    private $homeNumber;
    
    /**
    * @ORM\Column(type="text", length=150)
    * @Assert\NotBlank()
    * @Assert\Email()
    * @Assert\Length(
    *      max = 150,
    *      minMessage = "Numer nie może zawierać więcej nież 150 znaków"
    * )
    */
    private $email;
    
    /**
    * @ORM\Column(type="text", nullable=true)
    * @Assert\Length(
    *      max = 150,
    *      minMessage = "Numer nie może zawierać więcej nież 150 znaków"
    * )
    */
    private $phone;
    
    /**
    * @ORM\Column(type="date", nullable = true)
    * @Assert\Date()
    */
    private $createDate;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Cart",
     *      mappedBy = "buyer"
     * )
     */
    protected $cart;
    
    /**
    * @ORM\Column(type="text", length=100, nullable=true)
    */
    private $status;
    
    /**
    * @ORM\Column(type="text", nullable=true)
    */
    private $ses;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Travel",
     *      inversedBy = "buyer"
     * )
     * @ORM\JoinColumn(
     *      name = "travel_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $travel;
    
    /**
    * @ORM\Column(type="float", nullable=true)
    */
    private $price;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $sendEmail = 0;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $close = 0;
    
    function getId() {
        return $this->id;
    }

    function getFirstName() {
        return $this->firstName;
    }

    function getLastName() {
        return $this->lastName;
    }

    function getCity() {
        return $this->city;
    }

    function getPostCode() {
        return $this->postCode;
    }

    function getStreet() {
        return $this->street;
    }

    function getHomeNumber() {
        return $this->homeNumber;
    }

    function getEmail() {
        return $this->email;
    }

    function getPhone() {
        return $this->phone;
    }

    function getCreateDate() {
        return $this->createDate;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    function setCity($city) {
        $this->city = $city;
    }

    function setPostCode($postCode) {
        $this->postCode = $postCode;
    }

    function setStreet($street) {
        $this->street = $street;
    }

    function setHomeNumber($homeNumber) {
        $this->homeNumber = $homeNumber;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }
    function getCart() {
        return $this->cart;
    }

    function setCart($cart) {
        $this->cart = $cart;
    }
    
    function getStatus() {
        return $this->status;
    }

    function setStatus($status) {
        $this->status = $status;
    }
    
    function getTravel() {
        return $this->travel;
    }

    function setTravel($travel) {
        $this->travel = $travel;
    }
    
    function getPrice() {
        return $this->price;
    }

    function setPrice($price) {
        $this->price = $price;
    }
    function getSes() {
        return $this->ses;
    }

    function setSes($ses) {
        $this->ses = $ses;
    }

    function getSendEmail() {
        return $this->sendEmail;
    }

    function setSendEmail($sendEmail) {
        $this->sendEmail = $sendEmail;
    }

    function getClose() {
        return $this->close;
    }

    function setClose($close) {
        $this->close = $close;
    }

}