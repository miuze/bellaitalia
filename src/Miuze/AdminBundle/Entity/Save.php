<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Description of Page
 *
 * @author Kamil
 */

class Save {
   
    /**
    * @Assert\NotBlank
    */
    private $startDate;
    
    /**
    * @Assert\NotBlank
    */
    private $endDate;
    
    /**
    * @Assert\Email
    */
    private $email;
    
    function getStartDate() {
        return $this->startDate;
    }

    function getEndDate() {
        return $this->endDate;
    }

    function getEmail() {
        return $this->email;
    }

    function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    function setEmail($email) {
        $this->email = $email;
    }
}
