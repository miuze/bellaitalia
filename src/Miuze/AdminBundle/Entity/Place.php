<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="place") 
* @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\PlaceRepository")
*/
class Place {

    public function __construct()
    {
        $this->path = '/uploads/place/';
    }
        
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "PlaceCategory",
     *      inversedBy = "place"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $lat;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $lng;
    
    
    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $address;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $phone;
    
    /**
    * @ORM\Column(type="text", nullable=true)
    * @Assert\Email(
    *      message = "Ten adres email '{{ value }}' nie jest poprawny.",
    *      checkMX = true
    * )
    */
    private $email;
     
    /**
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $image;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $sort = 0;
    
    function getSort() {
        return $this->sort;
    }

    function setSort($sort) {
        $this->sort = $sort;
    }

    function getPath() {
        return $this->path;
    }

    function setPath($path) {
        $this->path = $path;       
    }

        
    function getId() {
        return $this->id;
    }

    function getDescription() {
        return $this->description;
    }

    function getFile() {
        return $this->file;
    }

    function getImage() {
        return $this->image;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function setImage($image) {
        $this->image = $image;
    }
    
    function getCategory() {
        return $this->category;
    }

    function setCategory($category) {
        $this->category = $category;
    }
    
    function getPhone() {
        return $this->phone;
    }

    function getEmail() {
        return $this->email;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/place/'. $this->category->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {   
            $this->setPath('/uploads/place/'.$this->category->getId().'/');
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
        if(null !== $this->address){
            $latlong = \Miuze\AdminBundle\Utils\Functions::getGeo($this->address);
            $this->setLat($latlong['lat']);
            $this->setLng($latlong['long']);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);
        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
    
    function getName() {
        return $this->name;
    }

    function getLat() {
        return $this->lat;
    }

    function getLng() {
        return $this->lng;
    }

    function getAddress() {
        return $this->address;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setLat($lat) {
        $this->lat = $lat;
    }

    function setLng($lng) {
        $this->lng = $lng;
    }

    function setAddress($address) {
        $this->address = $address;
    }
}
