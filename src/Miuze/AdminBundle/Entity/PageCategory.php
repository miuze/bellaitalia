<?php

namespace Miuze\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="page_category")
*/

class PageCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "menu"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;

    /**
     * @ORM\Column(type="boolean")
     */
    private $removed;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Page",
     *      mappedBy = "category",
     *     cascade={"all"}
     * )
     */
    protected $pages;

    function getId() {
        return $this->id;
    }

    
    function getTitle() {
        return $this->title;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    
    function getPage() {
        return $this->page;
    }

    function setPage($page) {
        $this->page = $page;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set removed
     *
     * @param boolean $removed
     *
     * @return PageCategory
     */
    public function setRemoved($removed)
    {
        $this->removed = $removed;

        return $this;
    }

    /**
     * Get removed
     *
     * @return boolean
     */
    public function getRemoved()
    {
        return $this->removed;
    }

    /**
     * Add category
     *
     * @param \Miuze\AdminBundle\Entity\Page $category
     *
     * @return PageCategory
     */
    public function addCategory(\Miuze\AdminBundle\Entity\Page $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \Miuze\AdminBundle\Entity\Page $category
     */
    public function removeCategory(\Miuze\AdminBundle\Entity\Page $category)
    {
        $this->category->removeElement($category);
    }

    

    /**
     * Set lang
     *
     * @param \Miuze\AdminBundle\Entity\Lang $lang
     *
     * @return PageCategory
     */
    public function setLang(\Miuze\AdminBundle\Entity\Lang $lang = null)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return \Miuze\AdminBundle\Entity\Lang
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return PageCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }
}
