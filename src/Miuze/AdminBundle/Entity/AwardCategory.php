<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="award_category")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\AwardRepository")
*/

class AwardCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "awardCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "awardCategory"
     * )
     * @ORM\JoinTable(
     *      name = "award_category_to_page",
     * )
     */
    private $page;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Award",
     *      mappedBy = "category"
     * )
     */
    protected $award;

    function getId() {
        return $this->id;
    }

    function getPage() {
        return $this->page;
    }

    function getTitle() {
        return $this->title;
    }

    function getAward() {
        return $this->award;
    }

    function setPage($page) {
        $this->page = $page;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setAward($award) {
        $this->award = $award;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->award = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return AwardCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add award
     *
     * @param \Miuze\AdminBundle\Entity\Award $award
     *
     * @return AwardCategory
     */
    public function addAward(\Miuze\AdminBundle\Entity\Award $award)
    {
        $this->award[] = $award;

        return $this;
    }

    /**
     * Remove award
     *
     * @param \Miuze\AdminBundle\Entity\Award $award
     */
    public function removeAward(\Miuze\AdminBundle\Entity\Award $award)
    {
        $this->award->removeElement($award);
    }
    
    function getLang() {
        return $this->lang;
    }

    function setLang($lang) {
        $this->lang = $lang;
    }
}
