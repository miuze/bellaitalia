<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="place_category")
 * @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\PlaceRepository")
*/

class PlaceCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "placeCategory"
     * )
     * @ORM\JoinTable(
     *      name = "place_category_to_page",
     * )
     */
    private $page;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Place",
     *      mappedBy = "category"
     * )
     */
    protected $place;
    
    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     * )
     */
    private $sort = 0;
    
    function getSort() {
        return $this->sort;
    }

    function setSort($sort) {
        $this->sort = $sort;
    }

    function getId() {
        return $this->id;
    }

    function getPage() {
        return $this->page;
    }

    function getTitle() {
        return $this->title;
    }

    function getPlace() {
        return $this->place;
    }

    function setPage($page) {
        $this->page = $page;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setPlace($place) {
        $this->place = $place;
    }  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->place = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return PlaceCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add place
     *
     * @param \Miuze\AdminBundle\Entity\Place $place
     *
     * @return PlaceCategory
     */
    public function addPlace(\Miuze\AdminBundle\Entity\Place $place)
    {
        $this->place[] = $place;

        return $this;
    }

    /**
     * Remove place
     *
     * @param \Miuze\AdminBundle\Entity\Place $place
     */
    public function removePlace(\Miuze\AdminBundle\Entity\Place $place)
    {
        $this->place->removeElement($place);
    }
    
}
