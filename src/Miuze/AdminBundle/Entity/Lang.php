<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="lang")
*/

class Lang {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=2)
     * @Assert\NotBlank
     * @Assert\Regex("/^([a-z]{2})/")
     */
    private $lang;    
   
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\AdminBundle\Entity\PageCategory",
     *      mappedBy = "lang"
     * )
     */
    protected $menu;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->menu = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lang
     *
     * @param string $lang
     *
     * @return Lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Add menu
     *
     * @param \Miuze\AdminBundle\Entity\PageCategory $menu
     *
     * @return Lang
     */
    public function addMenu(\Miuze\AdminBundle\Entity\PageCategory $menu)
    {
        $this->menu[] = $menu;

        return $this;
    }

    /**
     * Remove menu
     *
     * @param \Miuze\AdminBundle\Entity\PageCategory $menu
     */
    public function removeMenu(\Miuze\AdminBundle\Entity\PageCategory $menu)
    {
        $this->menu->removeElement($menu);
    }

    /**
     * Get menu
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMenu()
    {
        return $this->menu;
    }
}
