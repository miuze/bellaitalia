<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="download_category")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\DownloadCategoryRepository")
*/
class DownloadCategory {
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "downloadCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "downloadCategory"
     * )
     * @ORM\JoinTable(
     *      name = "download_category_to_page",
     * )
     */
    private $page;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Download",
     *      mappedBy = "category"
     * )
     */
    protected $download;
    
    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     * )
     */
    private $sort = 0;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "DownloadFile",
     *      mappedBy = "categoryFile"
     * )
     */
    private $file;
    
    function getId() {
        return $this->id;
    }

    function getPage() {
        return $this->page;
    }

    function getCategory() {
        return $this->category;
    }

    function setPage($page) {
        $this->page = $page;
    }
    
    function setCategory($category) {
        $this->category = $category;
    }
    
    function getTitle() {
        return $this->title;
    }

    function getDownload() {
        return $this->download;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setDownload($download) {
        $this->download = $download;
    }
    function getSort() {
        return $this->sort;
    }

    function setSort($sort) {
        $this->sort = $sort;
    }
    function getFile() {
        return $this->file;
    }

    function setFile($file) {
        $this->file = $file;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->download = new \Doctrine\Common\Collections\ArrayCollection();
        $this->file = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return DownloadCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add download
     *
     * @param \Miuze\AdminBundle\Entity\Download $download
     *
     * @return DownloadCategory
     */
    public function addDownload(\Miuze\AdminBundle\Entity\Download $download)
    {
        $this->download[] = $download;

        return $this;
    }

    /**
     * Remove download
     *
     * @param \Miuze\AdminBundle\Entity\Download $download
     */
    public function removeDownload(\Miuze\AdminBundle\Entity\Download $download)
    {
        $this->download->removeElement($download);
    }

    /**
     * Add file
     *
     * @param \Miuze\AdminBundle\Entity\DownloadFile $file
     *
     * @return DownloadCategory
     */
    public function addFile(\Miuze\AdminBundle\Entity\DownloadFile $file)
    {
        $this->file[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \Miuze\AdminBundle\Entity\DownloadFile $file
     */
    public function removeFile(\Miuze\AdminBundle\Entity\DownloadFile $file)
    {
        $this->file->removeElement($file);
    }
    
    function getLang() {
        return $this->lang;
    }

    function setLang($lang) {
        $this->lang = $lang;
    }
}
