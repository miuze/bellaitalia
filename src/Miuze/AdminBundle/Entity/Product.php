<?php

namespace Miuze\AdminBundle\Entity;

use Miuze\AdminBundle\MiuzeAdminBundle;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Miuze\AdminBundle\Utils\Functions as Uttils;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="product")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\ProductRepository")
* @ORM\HasLifecycleCallbacks
*/
class Product {
     
    public function __construct() {
        $this->createDate = new \DateTime('now');
        $this->startDate = new \ DateTime('now');
    }
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "ProductPhoto",
     *      mappedBy = "product"
     * )
     */
    protected $photo;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "ProductCategory",
     *      inversedBy = "product"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;
    
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Cart",
     *      mappedBy = "product"
     * )
     */
    private $cart;

    /**
     * @ORM\Column(type="integer")
     */
    private $cnt = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isStock = 1;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $showMain = 0;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $priceOnLogin = 0;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank(groups={"add"})
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;
    
    /**
     * @ORM\Column(type="text", nullable=true ,length=50)
     * @Assert\Length(
     *      max=50
     * )
     */
    private $code;
    
    /**
     * @ORM\Column(type="text", nullable=true, length=13)
     * @Assert\Length(
     *      max=13
     * )
     */
    private $ean;
    
    /**
     * @ORM\Column(type="text", nullable=true, length=255)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $tags;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    
     /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lead;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $details;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $createDate;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    private $startDate;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    private $endDate;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $promoPrice;
    
    /**
     * @ORM\Column(type="text", length=255)
     */
    private $urlTitle;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    
    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    private $oldDate;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Provider",
     *      inversedBy = "product"
     * )
     * @ORM\JoinColumn(
     *      name = "provider_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $provider;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Vat",
     *      inversedBy = "product"
     * )
     * @ORM\JoinColumn(
     *      name = "vat_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $vat;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "ProductOffer",
     *      mappedBy = "product"
     * )
     */
    protected $productOffer;

    function getId() {
        return $this->id;
    }

    function getPhoto() {
        return $this->photo;
    }

    function getCategory() {
        return $this->category;
    }

    function getCart() {
        return $this->cart;
    }

    function getIsStock() {
        return $this->isStock;
    }

    function getName() {
        return $this->name;
    }

    function getCode() {
        return $this->code;
    }

    function getEan() {
        return $this->ean;
    }

    function getTags() {
        return $this->tags;
    }

    function getContent() {
        return $this->content;
    }

    function getLead() {
        return $this->lead;
    }

    function getCreateDate() {
        return $this->createDate;
    }

    function getStartDate() {
        return $this->startDate;
    }

    function getEndDate() {
        return $this->endDate;
    }

    function getPrice() {
        return $this->price;
    }

    function getUrlTitle() {
        return $this->urlTitle;
    }

    function getPath() {
        return $this->path;
    }

    function getImage() {
        return $this->image;
    }

    function getOldDate() {
        return $this->oldDate;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPhoto($photo) {
        $this->photo = $photo;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setCart($cart) {
        $this->cart = $cart;
    }

    function setIsStock($isStock) {
        $this->isStock = $isStock;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setCode($code) {
        $this->code = $code;
    }

    function setEan($ean) {
        $this->ean = $ean;
    }

    function setTags($tags) {
        $this->tags = $tags;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setLead($lead) {
        $this->lead = $lead;
    }

    public function getDetails()
    {
        return $this->details;
    }

    public function setDetails($details)
    {
        $this->details = $details;
    }



    function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }

    function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setUrlTitle($urlTitle = '') {
        $this->urlTitle = Uttils::slugify($this->name);
    }

    function setPath($path) {
        $this->path = $path;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setOldDate($oldDate) {
        $this->oldDate = $oldDate;
    }    
    
    function getShowMain() {
        return $this->showMain;
    }

    function setShowMain($showMain) {
        $this->showMain = $showMain;
    }

    /**
     * @return mixed
     */
    public function getPromoPrice()
    {
        return $this->promoPrice;
    }

    /**
     * @param mixed $promoPrice
     */
    public function setPromoPrice($promoPrice)
    {
        $this->promoPrice = $promoPrice;
    }

    /**
     * @return mixed
     */
    public function getCnt()
    {
        return $this->cnt;
    }

    /**
     * @param mixed $cnt
     */
    public function setCnt($cnt)
    {
        $this->cnt = $cnt;
    }




    /**
     * Add photo
     *
     * @param \Miuze\AdminBundle\Entity\ProductPhoto $photo
     *
     * @return Product
     */
    public function addPhoto(\Miuze\AdminBundle\Entity\ProductPhoto $photo)
    {
        $this->photo[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \Miuze\AdminBundle\Entity\ProductPhoto $photo
     */
    public function removePhoto(\Miuze\AdminBundle\Entity\ProductPhoto $photo)
    {
        $this->photo->removeElement($photo);
    }

    /**
     * Add cart
     *
     * @param \Miuze\AdminBundle\Entity\Cart $cart
     *
     * @return Product
     */
    public function addCart(\Miuze\AdminBundle\Entity\Cart $cart)
    {
        $this->cart[] = $cart;

        return $this;
    }

    /**
     * Remove cart
     *
     * @param \Miuze\AdminBundle\Entity\Cart $cart
     */
    public function removeCart(\Miuze\AdminBundle\Entity\Cart $cart)
    {
        $this->cart->removeElement($cart);
    }

    /**
     * Set provider
     *
     * @param \Miuze\AdminBundle\Entity\Provider $provider
     *
     * @return Product
     */
    public function setProvider(\Miuze\AdminBundle\Entity\Provider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \Miuze\AdminBundle\Entity\Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set vat
     *
     * @param \Miuze\AdminBundle\Entity\Vat $vat
     *
     * @return Product
     */
    public function setVat(\Miuze\AdminBundle\Entity\Vat $vat = null)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Get vat
     *
     * @return \Miuze\AdminBundle\Entity\Vat
     */
    public function getVat()
    {
        return $this->vat;
    }


    /**
     * Calculate netto price with rabate
     * @param User|null $user
     * @return mixed
     */
    public function calcNettoNoRabate($user = null){

        $priceNetto = $this->promoPrice !== null ? $this->promoPrice : $this->price;

        return $priceNetto;
    }

    /**
     * Calculate netto price with rabate
     * @param User|null $user
     * @return mixed
     */
    public function calcNetto($user = null){

        $rabate = $this->calcRabate($user);
        $priceNetto = $this->promoPrice !== null ? $this->promoPrice : $this->price;
        $price = $priceNetto - $rabate;

        return $price;
    }

    /**
     * Calculate Price with Vat and Rabate
     * @param User|null $user
     * @return float|int|mixed
     */
    public function calcVat($user = null){
        $priceNetto = $this->calcNetto($user);
        $price = $priceNetto + ($priceNetto * ($this->vat->getRate() / 100));
        return $price;
    }


    /**
     * Calculate rabate to price
     * @param $user
     * @return float|int
     */
    protected function calcRabate($user){
        $priceNetto = $this->promoPrice !== null ? $this->promoPrice : $this->price;
        if($user !== null){
            $rabates = $this->category->getRabate();
            if($rabates !== null && !$rabates->isEmpty()){
                $userRabate =[];
                foreach ($rabates as $item){
                    if($item->getUser()->getId() == $user->getId()){
                        array_push($userRabate, $item);
                    }
                }
                if(!empty($userRabate)){
                    $rabate = $priceNetto * ($userRabate[0]->getRate() / 100);
                }else{
                    $rabate = $priceNetto * ($this->category->getDefaultRabate() / 100);
                }
            }else {
                $rabate = $priceNetto * ($this->category->getDefaultRabate() / 100);
            }
        }else{
            $rabate = 0;
        }

        return $rabate;
    }


    /**
     * Add productOffer
     *
     * @param \Miuze\AdminBundle\Entity\ProductOffer $productOffer
     *
     * @return Product
     */
    public function addProductOffer(\Miuze\AdminBundle\Entity\ProductOffer $productOffer)
    {
        $this->productOffer[] = $productOffer;

        return $this;
    }

    /**
     * Remove productOffer
     *
     * @param \Miuze\AdminBundle\Entity\ProductOffer $productOffer
     */
    public function removeProductOffer(\Miuze\AdminBundle\Entity\ProductOffer $productOffer)
    {
        $this->productOffer->removeElement($productOffer);
    }

    /**
     * Get productOffer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductOffer()
    {
        return $this->productOffer;
    }
    
    function getPriceOnLogin() {
        return $this->priceOnLogin;
    }

    function setPriceOnLogin($priceOnLogin) {
        $this->priceOnLogin = $priceOnLogin;
    }
}
