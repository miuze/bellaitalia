<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Description of Page
 *
 * @author Kamil
 */

class Cv {
   
    /**
    * @Assert\NotBlank
    * @Assert\Length(
    *      max=255
    * )
    */
    private $firstName;
    
    /**
    * @Assert\NotBlank
    * @Assert\Length(
    *      max=255
    * )
    */
    private $lastName;
    
    /**
    * @Assert\Length(
    *      max=1000      
    * )
    */
    private $content;
    
    /**
    * @Assert\Email
    * @Assert\NotBlank
    */
    private $email;
    
    /**
    * @Assert\NotBlank
    * @Assert\Length(
    *      max=9
    * )
    * @Assert\Regex(
    *       pattern = "/^([0-9]){9}/",
    *       message="Numer musi składać się z 9 cyfr np 555555555"
    * ) 
    */
    private $phone;
    
    /**
     * @Assert\NotBlank
     * @Assert\File(
     *     maxSize = "1M",
     *     mimeTypes = {"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "Plik musi być w formacie PDF",
     *      maxSizeMessage = "Plik jest zbyt duży ({{ size }} {{ suffix }}). Maksymalna dopuszczalna wielkość {{ limit }} {{ suffix }}."
     * )
     */
    private $file;
    
    private $pdf;
            
    function getFirstName() {
        return $this->firstName;
    }

    function getLastName() {
        return $this->lastName;
    }

    function getContent() {
        return $this->content;
    }

    function getEmail() {
        return $this->email;
    }

    function getPhone() {
        return $this->phone;
    }

    function getFile() {
        return $this->file;
    }

    function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setFile($file) {
        $this->file = $file;
    }
    
    function getPdf() {
        return $this->pdf;
    }

    function setPdf($pdf) {
        $this->pdf = $pdf;
    }

    public function getAbsolutePath()
    {
        return null === $this->file ? null : __DIR__.'/../../../../web/'. $this->getUploadDir().$this->pdf;
    }
    public function getRootFile(){
        return __DIR__.'/../../../../web/'. $this->getUploadDir().$this->pdf;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/cv/';
    }
    
    public function preUpload()
    {
        if (null !== $this->file) {  
            $name = $this->file->getClientOriginalName();
            $this->pdf = $name;
        }
    }

    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->pdf);

        unset($this->file);
    }

    public function removeUpload()
    {
        $file =  __DIR__.'/../../../../web/'. $this->getUploadDir().$this->pdf;
        if(file_exists($file) ){
            unlink($file);
        }
    }
}
