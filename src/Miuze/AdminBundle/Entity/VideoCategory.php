<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="video_category")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\VideoRepository")
*/

class VideoCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "videoCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "videoCategory"
     * )
     * @ORM\JoinTable(
     *      name = "video_category_to_page",
     * )
     */
    private $page;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Video",
     *      mappedBy = "category"
     * )
     */
    protected $category;

    function getId() {
        return $this->id;
    }

    function getLang() {
        return $this->lang;
    }

    function getTitle() {
        return $this->title;
    }

    function getCategory() {
        return $this->category;
    }

    function setLang($lang) {
        $this->lang = $lang;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setCategory($category) {
        $this->category = $category;
    }
    
    function getNews() {
        return $this->news;
    }

    function setNews($news) {
        $this->news = $news;
    }  
    function getPage() {
        return $this->page;
    }

    function setPage($page) {
        $this->page = $page;
    }
    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return NewsCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add category
     *
     * @param \Miuze\AdminBundle\Entity\News $category
     *
     * @return NewsCategory
     */
    public function addCategory(\Miuze\AdminBundle\Entity\News $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \Miuze\AdminBundle\Entity\News $category
     */
    public function removeCategory(\Miuze\AdminBundle\Entity\News $category)
    {
        $this->category->removeElement($category);
    }
    
    
}
