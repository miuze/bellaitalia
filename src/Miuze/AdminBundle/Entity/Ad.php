<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="ad") 
* @ORM\HasLifecycleCallbacks
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\AdRepository")
*/
class Ad {
           
    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->startDate = new \DateTime('now');
        $this->endDate = NULL;
        $this->path = '/uploads/ad/';
    }
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "AdPhoto",
     *      mappedBy = "ad"
     * )
     */
    protected $photo;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "AdCategory",
     *      inversedBy = "ad"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
     /**
     * @ORM\Column(type="text", length=255)
     */
    private $urlTitle;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    
     /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lead;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $times;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $organizer = 'KriSport, Oś. Kościuszkowskie 10/18, 31-858 Kraków';
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $attributes;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $place;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $old;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $price;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $priceHave;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $priceNotHave;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $startDate;
    
    /**
     * @ORM\Column(type="date", nullable = true)
     * @Assert\Date()
     */
    private $endDate;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $createDate;
    
    /**
     * @Assert\File(
     *      maxSize="3M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png", "application/pdf"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG a plik w PDF"
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disable = 0;
    
    function getId() {
        return $this->id;
    }

    function getCategory() {
        return $this->category;
    }

    function getTitle() {
        return $this->title;
    }

    function getUrlTitle() {
        return $this->urlTitle;
    }

    function getContent() {
        return $this->content;
    }

    function getLead() {
        return $this->lead;
    }

    function getStartDate() {
        return $this->startDate;
    }

    function getEndDate() {
        return $this->endDate;
    }

    function getCreateDate() {
        return $this->createDate;
    }

    function getFile() {
        return $this->file;
    }

    function getPath() {
        return $this->path;
    }

    function getImage() {
        return $this->image;
    }

    function getDisable() {
        return $this->disable;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setUrlTitle($urlTitle) {
        $this->urlTitle = $urlTitle;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setLead($lead) {
        $this->lead = $lead;
    }

    function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function setPath($path) {
        $this->path = $path;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setDisable($disable) {
        $this->disable = $disable;
    }
    
    function getPhoto() {
        return $this->photo;
    }

    function setPhoto($photo) {
        $this->photo = $photo;
    }
    
    function getTimes() {
        return $this->times;
    }

    function getOrganizer() {
        return $this->organizer;
    }

    function getAttributes() {
        return $this->attributes;
    }

    function getPlace() {
        return $this->place;
    }

    function getOld() {
        return $this->old;
    }

    function getPrice() {
        return $this->price;
    }

    function getPriceHave() {
        return $this->priceHave;
    }

    function getPriceNotHave() {
        return $this->priceNotHave;
    }

    function setTimes($times) {
        $this->times = $times;
    }

    function setOrganizer($organizer) {
        $this->organizer = $organizer;
    }

    function setAttributes($attributes) {
        $this->attributes = $attributes;
    }

    function setPlace($place) {
        $this->place = $place;
    }

    function setOld($old) {
        $this->old = $old;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setPriceHave($priceHave) {
        $this->priceHave = $priceHave;
    }

    function setPriceNotHave($priceNotHave) {
        $this->priceNotHave = $priceNotHave;
    }

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebPath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/ad/'. $this->category->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {   
            $this->setPath('/uploads/ad/'.$this->category->getId().'/');
            // zrób cokolwiek chcesz aby wygenerować unikalną nazwę
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // musisz wyrzucać tutaj wyjątek jeśli plik nie może zostać przeniesiony
        // w tym przypadku encja nie zostanie zapisana do bazy
        // metoda move() obiektu UploadedFile robi to automatycznie
        //dump($this->getUploadRootDir()); die; 
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * Add photo
     *
     * @param \Miuze\AdminBundle\Entity\AdPhoto $photo
     *
     * @return Ad
     */
    public function addPhoto(\Miuze\AdminBundle\Entity\AdPhoto $photo)
    {
        $this->photo[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \Miuze\AdminBundle\Entity\AdPhoto $photo
     */
    public function removePhoto(\Miuze\AdminBundle\Entity\AdPhoto $photo)
    {
        $this->photo->removeElement($photo);
    }
}
