<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="realization_category")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\RealizationRepository")
*/

class RealizationCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "realizationCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "realizationCategory"
     * )
     * @ORM\JoinTable(
     *      name = "realization_category_to_page",
     * )
     */
    private $page;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Realization",
     *      mappedBy = "category"
     * )
     */
    protected $realization;

    function getId() {
        return $this->id;
    }

    function getLang() {
        return $this->lang;
    }

    function getTitle() {
        return $this->title;
    }

    function getCategory() {
        return $this->category;
    }

    function setLang($lang) {
        $this->lang = $lang;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setCategory($category) {
        $this->category = $category;
    }
    
    function getNews() {
        return $this->news;
    }

    function setNews($news) {
        $this->news = $news;
    }  
    function getPage() {
        return $this->page;
    }

    function setPage($page) {
        $this->page = $page;
    }
    function getRealization() {
        return $this->realization;
    }

    function setRealization($realization) {
        $this->realization = $realization;
    }



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->realization = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return RealizationCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add realization
     *
     * @param \Miuze\AdminBundle\Entity\Realization $realization
     *
     * @return RealizationCategory
     */
    public function addRealization(\Miuze\AdminBundle\Entity\Realization $realization)
    {
        $this->realization[] = $realization;

        return $this;
    }

    /**
     * Remove realization
     *
     * @param \Miuze\AdminBundle\Entity\Realization $realization
     */
    public function removeRealization(\Miuze\AdminBundle\Entity\Realization $realization)
    {
        $this->realization->removeElement($realization);
    }
    
    
}
