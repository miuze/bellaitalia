<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="gallery")
* @ORM\HasLifecycleCallbacks
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\GalleryRepository")
*/

class Gallery {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;
    
    /**
     * @ORM\Column(type="text", length=255)
     */
    private $urlTitle;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "GalleryPhoto",
     *      mappedBy = "gallery"
     * )
     */
    protected $photo;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "GalleryCategory",
     *      inversedBy = "gallery"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;
    
    /**
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getPhoto() {
        return $this->photo;
    }

    function getCategory() {
        return $this->category;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPhoto($photo) {
        $this->photo = $photo;
    }

    function setCategory($category) {
        $this->category = $category;
    }
    function getFile() {
        return $this->file;
    }

    function getPath() {
        return $this->path;
    }

    function getImage() {
        return $this->image;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function setPath($path) {
        $this->path = $path;
    }

    function setImage($image) {
        $this->image = $image;
    }
    function getUrlTitle() {
        return $this->urlTitle;
    }

    function setUrlTitle($urlTitle) {
        $this->urlTitle = $urlTitle;
    }

    public function getAbsolutePath(){
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir(){
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir(){
        return 'uploads/fotograf/'. $this->category->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload(){
        if (null !== $this->file) { 
            $this->setPath('/uploads/fotograf/'.$this->category->getId().'/');
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload(){
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);
        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload(){
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photo = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add photo
     *
     * @param \Miuze\AdminBundle\Entity\GalleryPhoto $photo
     *
     * @return Gallery
     */
    public function addPhoto(\Miuze\AdminBundle\Entity\GalleryPhoto $photo)
    {
        $this->photo[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \Miuze\AdminBundle\Entity\GalleryPhoto $photo
     */
    public function removePhoto(\Miuze\AdminBundle\Entity\GalleryPhoto $photo)
    {
        $this->photo->removeElement($photo);
    }
}
