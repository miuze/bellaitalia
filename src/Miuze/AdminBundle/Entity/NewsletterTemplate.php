<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */


class NewsletterTemplate {
    
    
    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title = 'Newsletter';
    
    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=500
     * )
     */
    private $content = '<p style="text-align: center; font-size: 16px;">Zobacz nowe aktualności</p>';
    
    private $news;
    
    private $userCategory;
 
    function getTitle() {
        return $this->title;
    }

    function getContent() {
        return $this->content;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function getNews() {
        return $this->news;
    }

    function setNews($news) {
        $this->news = $news;
    }

    function getUserCategory() {
        return $this->userCategory;
    }

    function setUserCategory($userCategory) {
        $this->userCategory = $userCategory;
    }
}

