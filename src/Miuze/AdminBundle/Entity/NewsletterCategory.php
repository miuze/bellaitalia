<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="newsletter_category")
*/

class NewsletterCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
       
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Newsletter",
     *      mappedBy = "category"
     * )
     */
    private $newsletter;

    function getId() {
        return $this->id;
    }

    function getTitle() {
        return $this->title;
    }

    function setTitle($title) {
        $this->title = $title;
    }
    
    function getNewsletter() {
        return $this->newsletter;
    }

    function setNewsletter($newsletter) {
        $this->newsletter = $newsletter;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->newsletter = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add newsletter
     *
     * @param \Miuze\AdminBundle\Entity\Newsletter $newsletter
     *
     * @return NewsletterCategory
     */
    public function addNewsletter(\Miuze\AdminBundle\Entity\Newsletter $newsletter)
    {
        $this->newsletter[] = $newsletter;

        return $this;
    }

    /**
     * Remove newsletter
     *
     * @param \Miuze\AdminBundle\Entity\Newsletter $newsletter
     */
    public function removeNewsletter(\Miuze\AdminBundle\Entity\Newsletter $newsletter)
    {
        $this->newsletter->removeElement($newsletter);
    }
}
