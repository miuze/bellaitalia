<?php

namespace Miuze\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Miuze\AdminBundle\Utils\Functions as Utils;

    /**
     * @ORM\Entity
     * @ORM\Table(name="product_photo")
     * @ORM\HasLifecycleCallbacks
     */
class ProductPhoto {
    
    public function __construct()
    {
        $this->path = '/uploads/product/';
    }
    
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    

    private $link;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Product",
     *      inversedBy = "photo"
     * )
     * @ORM\JoinColumn(
     *      name = "product_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $product;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    function getImage() {
        return $this->image;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function getId() {
        return $this->id;
    }

    function getPath() {
        return $this->path;
    }

    function getFile() {
        return $this->file;
    }

    function setPath($path) {
        $this->path = $path;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function getProduct() {
        return $this->product;
    }

    function setProduct($product) {
        $this->product = $product;
    }

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/product/'. $this->getProduct()->getCategory()->getId().'/'.$this->getProduct()->getId();
    }

    public function importPhoto($imgName)
    {
        if (null !== $this->link) {
            $upl = __DIR__.'/../../../../web/uploads/product/';
            $uplEnd = __DIR__.'/../../../../web';
            if(!is_dir($upl.$this->getProduct()->getCategory()->getId())){
                mkdir($upl.$this->getProduct()->getCategory()->getId());
            }
            if(!is_dir($upl.$this->getProduct()->getCategory()->getId().'/'.$this->getProduct()->getId())){
                mkdir($upl.$this->getProduct()->getCategory()->getId().'/'.$this->getProduct()->getId());
            }

            $this->setPath('/uploads/product/'. $this->getProduct()->getCategory()->getId().'/'.$this->getProduct()->getId().'/');
            $this->setImage(uniqid().'-'.Utils::slugifyFile($imgName));
            if($this->rfile_exists($this->link)){
                $img = file_get_contents($this->link);
                file_put_contents($uplEnd . $this->path . $this->image, file_get_contents($this->link));
                return true;
            }else{
                return false;
            }

        }
    }

    function rfile_exists($address)
    {
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$address);
        curl_setopt($ch,CURLOPT_NOBODY,1);
        curl_setopt($ch,CURLOPT_FAILONERROR,1);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

        if(curl_exec($ch) !== FALSE)
        {
            return true;
        }

        return false;

        curl_close($ch);
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->setPath('/uploads/product/'. $this->getProduct()->getCategory()->getId().'/'.$this->getProduct()->getId().'/');
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }


    

}
