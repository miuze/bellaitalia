<?php

namespace Miuze\AdminBundle\Entity;

use Miuze\AdminBundle\MiuzeAdminBundle;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Miuze\AdminBundle\Utils\Functions as Uttils;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="flat")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\FlatRepository")
* @ORM\HasLifecycleCallbacks
*/
class Flat {
     
    public function __construct() {
        $this->createDate = new \DateTime('now');
    }
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "FlatPhoto",
     *      mappedBy = "flat"
     * )
     */
    protected $photo;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Build",
     *      inversedBy = "flat"
     * )
     * @ORM\JoinColumn(
     *      name = "build_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $build;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $cnt = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isStock = 1;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank(
     *     groups = {"add"}
     * )
     * @Assert\Length(
     *      max=255,
     *     groups = {"add"}
     * )
     */
    private $name;
    
       
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    
     /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lead;

    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $createDate;
    
    /**
    * @ORM\Column(type="float", nullable=true)
    */
    private $price;

    /**
    * @ORM\Column(type="float", nullable=true)
    * @Assert\NotBlank(
     *     groups = {"add"}
     * )
    */
    private $area;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(
     *     groups = {"add"}
     * )
     */
    private $bathroom;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(
     *     groups = {"add"}
     * )
     */
    private $room;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(
     *     groups = {"add"}
     * )
     */
    private $garage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cords;
    
    /**
     * @ORM\Column(type="text", length=255)
     */
    private $urlTitle;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    private $minPrice;
    private $maxPrice;
    private $minArea;
    private $maxArea;
    private $address;

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }


    /**
     * @return mixed
     */
    public function getMinPrice()
    {
        return $this->minPrice;
    }

    /**
     * @param mixed $minPrice
     */
    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;
    }

    /**
     * @return mixed
     */
    public function getMaxPrice()
    {
        return $this->maxPrice;
    }

    /**
     * @param mixed $maxPrice
     */
    public function setMaxPrice($maxPrice)
    {
        $this->maxPrice = $maxPrice;
    }

    /**
     * @return mixed
     */
    public function getMinArea()
    {
        return $this->minArea;
    }

    /**
     * @param mixed $minArea
     */
    public function setMinArea($minArea)
    {
        $this->minArea = $minArea;
    }

    /**
     * @return mixed
     */
    public function getMaxArea()
    {
        return $this->maxArea;
    }

    /**
     * @param mixed $maxArea
     */
    public function setMaxArea($maxArea)
    {
        $this->maxArea = $maxArea;
    }



    function setUrlTitle($urlTitle = '') {
        $this->urlTitle = Uttils::slugify($this->name);
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cnt
     *
     * @param integer $cnt
     *
     * @return Flat
     */
    public function setCnt($cnt)
    {
        $this->cnt = $cnt;

        return $this;
    }

    /**
     * Get cnt
     *
     * @return integer
     */
    public function getCnt()
    {
        return $this->cnt;
    }

    /**
     * Set isStock
     *
     * @param boolean $isStock
     *
     * @return Flat
     */
    public function setIsStock($isStock)
    {
        $this->isStock = $isStock;

        return $this;
    }

    /**
     * Get isStock
     *
     * @return boolean
     */
    public function getIsStock()
    {
        return $this->isStock;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Flat
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->setUrlTitle($name);

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Flat
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set lead
     *
     * @param string $lead
     *
     * @return Flat
     */
    public function setLead($lead)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return string
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return Flat
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Flat
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get urlTitle
     *
     * @return string
     */
    public function getUrlTitle()
    {
        return $this->urlTitle;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Flat
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Flat
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add photo
     *
     * @param \Miuze\AdminBundle\Entity\FlatPhoto $photo
     *
     * @return Flat
     */
    public function addPhoto(\Miuze\AdminBundle\Entity\FlatPhoto $photo)
    {
        $this->photo[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \Miuze\AdminBundle\Entity\FlatPhoto $photo
     */
    public function removePhoto(\Miuze\AdminBundle\Entity\FlatPhoto $photo)
    {
        $this->photo->removeElement($photo);
    }

    /**
     * Get photo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set build
     *
     * @param \Miuze\AdminBundle\Entity\Build $build
     *
     * @return Flat
     */
    public function setBuild(\Miuze\AdminBundle\Entity\Build $build = null)
    {
        $this->build = $build;

        return $this;
    }

    /**
     * Get build
     *
     * @return \Miuze\AdminBundle\Entity\Build
     */
    public function getBuild()
    {
        return $this->build;
    }

    /**
     * Set area
     *
     * @param float $area
     *
     * @return Flat
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return float
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set bathroom
     *
     * @param integer $bathroom
     *
     * @return Flat
     */
    public function setBathroom($bathroom)
    {
        $this->bathroom = $bathroom;

        return $this;
    }

    /**
     * Get bathroom
     *
     * @return integer
     */
    public function getBathroom()
    {
        return $this->bathroom;
    }

    /**
     * Set room
     *
     * @param integer $room
     *
     * @return Flat
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return integer
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set cords
     *
     * @param string $cords
     *
     * @return Flat
     */
    public function setCords($cords)
    {
        $this->cords = $cords;

        return $this;
    }

    /**
     * Get cords
     *
     * @return string
     */
    public function getCords()
    {
        return $this->cords;
    }

    /**
     * Set garage
     *
     * @param integer $garage
     *
     * @return Flat
     */
    public function setGarage($garage)
    {
        $this->garage = $garage;

        return $this;
    }

    /**
     * Get garage
     *
     * @return integer
     */
    public function getGarage()
    {
        return $this->garage;
    }
}
