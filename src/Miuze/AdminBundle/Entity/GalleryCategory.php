<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="gallery_category")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\GalleryRepository")
*/

class GalleryCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "galleryCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "galleryCategory"
     * )
     * @ORM\JoinTable(
     *      name = "gallery_category_to_page",
     * )
     */
    private $page;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Gallery",
     *      mappedBy = "category"
     * )
     */
    protected $gallery;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;
    
    function getId() {
        return $this->id;
    }

    function getPage() {
        return $this->page;
    }

    function getGallery() {
        return $this->gallery;
    }

    function getName() {
        return $this->name;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPage($page) {
        $this->page = $page;
    }

    function setGallery($gallery) {
        $this->gallery = $gallery;
    }

    function setName($name) {
        $this->name = $name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->gallery = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return GalleryCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add gallery
     *
     * @param \Miuze\AdminBundle\Entity\Gallery $gallery
     *
     * @return GalleryCategory
     */
    public function addGallery(\Miuze\AdminBundle\Entity\Gallery $gallery)
    {
        $this->gallery[] = $gallery;

        return $this;
    }

    /**
     * Remove gallery
     *
     * @param \Miuze\AdminBundle\Entity\Gallery $gallery
     */
    public function removeGallery(\Miuze\AdminBundle\Entity\Gallery $gallery)
    {
        $this->gallery->removeElement($gallery);
    }
    
    function getLang() {
        return $this->lang;
    }

    function setLang($lang) {
        $this->lang = $lang;
    }


}
