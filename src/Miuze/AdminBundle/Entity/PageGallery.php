<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
     * @ORM\Entity
     * @ORM\Table(name="page_gallery")
     */
class PageGallery {
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "PagePhoto",
     *      mappedBy = "gallery"
     * )
     */
    protected $gallery;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Page",
     *      mappedBy = "gallerypage"
     * )
     */
    protected $gallerypage;
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getGallery() {
        return $this->gallery;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setGallery($gallery) {
        $this->gallery = $gallery;
    }
 
    function getGallerypage() {
        return $this->gallerypage;
    }

    function setGallerypage($gallerypage) {
        $this->gallerypage = $gallerypage;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gallery = new \Doctrine\Common\Collections\ArrayCollection();
        $this->gallerypage = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add gallery
     *
     * @param \Miuze\AdminBundle\Entity\PagePhoto $gallery
     *
     * @return PageGallery
     */
    public function addGallery(\Miuze\AdminBundle\Entity\PagePhoto $gallery)
    {
        $this->gallery[] = $gallery;

        return $this;
    }

    /**
     * Remove gallery
     *
     * @param \Miuze\AdminBundle\Entity\PagePhoto $gallery
     */
    public function removeGallery(\Miuze\AdminBundle\Entity\PagePhoto $gallery)
    {
        $this->gallery->removeElement($gallery);
    }

    /**
     * Add gallerypage
     *
     * @param \Miuze\AdminBundle\Entity\Page $gallerypage
     *
     * @return PageGallery
     */
    public function addGallerypage(\Miuze\AdminBundle\Entity\Page $gallerypage)
    {
        $this->gallerypage[] = $gallerypage;

        return $this;
    }

    /**
     * Remove gallerypage
     *
     * @param \Miuze\AdminBundle\Entity\Page $gallerypage
     */
    public function removeGallerypage(\Miuze\AdminBundle\Entity\Page $gallerypage)
    {
        $this->gallerypage->removeElement($gallerypage);
    }
}
