<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="cart")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\CartRepository")
*/
class Cart {
           
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Product",
     *      inversedBy = "cart",
     *      cascade={"persist", "remove"}
     * )
     * @ORM\JoinColumn(
     *      name = "product_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    protected $product;
    
    /**
    * @ORM\Column(type="integer", length=4)
    * @Assert\NotBlank
    * @Assert\Regex(
    *      pattern="/\d+/",
    *      message="Wartość powinna być liczbą załkowitą"
    * )
    */
    private $quantity;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Order",
     *      inversedBy = "carts",
     *      cascade={"persist", "remove", "merge"}
     * )
     * @ORM\JoinColumn(
     *      name = "order_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $order;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Cart
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set product
     *
     * @param \Miuze\AdminBundle\Entity\Product $product
     *
     * @return Cart
     */
    public function setProduct(\Miuze\AdminBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Miuze\AdminBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set order
     *
     * @param \Miuze\AdminBundle\Entity\Order $order
     *
     * @return Cart
     */
    public function setOrder(\Miuze\AdminBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Miuze\AdminBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }
}
