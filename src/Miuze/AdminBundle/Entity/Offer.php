<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="offer")
* @ORM\HasLifecycleCallbacks
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\OfferRepository")
*/
class Offer{
    
    public function __construct()
    {
        $this->date = new \DateTime('now');
    }
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     * @var type
     */
    private  $date;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Newsletter",
     *      inversedBy = "offer"
     * )
     * @ORM\JoinColumn(
     *      name = "subscriber_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $subscriber;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "ProductOffer",
     *      mappedBy = "offer",
     *      cascade={"persist", "remove"}
     * )
     */
    protected $productItems;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Offer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Offer
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Offer
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set subscriber
     *
     * @param \Miuze\AdminBundle\Entity\Newsletter $subscriber
     *
     * @return Offer
     */
    public function setSubscriber(\Miuze\AdminBundle\Entity\Newsletter $subscriber = null)
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    /**
     * Get subscriber
     *
     * @return \Miuze\AdminBundle\Entity\Newsletter
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }

    /**
     * Add productItem
     *
     * @param \Miuze\AdminBundle\Entity\ProductOffer $productItem
     *
     * @return Offer
     */
    public function addProductItem(\Miuze\AdminBundle\Entity\ProductOffer $productItem)
    {
        $productItem->setOffer($this);
        $this->productItems[] = $productItem;

        return $this;
    }

    /**
     * Remove productItem
     *
     * @param \Miuze\AdminBundle\Entity\ProductOffer $productItem
     */
    public function removeProductItem(\Miuze\AdminBundle\Entity\ProductOffer $productItem)
    {
        $this->productItems->removeElement($productItem);
    }

    /**
     * Get productItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductItems()
    {
        return $this->productItems;
    }
}
