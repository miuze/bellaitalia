<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="realization") 
* @ORM\HasLifecycleCallbacks
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\RealizationRepository")
*/
class Realization {
           
    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->startDate = new \DateTime('now');
    }
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "RealizationCategory",
     *      inversedBy = "realization"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "RealizationPhoto",
     *      mappedBy = "realization"
     * )
     */
    protected $gallery;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
     /**
     * @ORM\Column(type="text", length=255)
     */
    private $seoTitle;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    
     /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lead;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $startDate;
        
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $createDate;
    /**
     * @Assert\File(
     *      maxSize="1M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    function getId() {
        return $this->id;
    }

    function getCategory() {
        return $this->category;
    }

    function getTitle() {
        return $this->title;
    }

    function getSeoTitle() {
        return $this->seoTitle;
    }

    function getContent() {
        return $this->content;
    }

    function getLead() {
        return $this->lead;
    }

    function getStartDate() {
        return $this->startDate;
    }

    function getCreateDate() {
        return $this->createDate;
    }

    function getFile() {
        return $this->file;
    }

    function getPath() {
        return $this->path;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setSeoTitle($seoTitle) {
        $this->seoTitle = $seoTitle;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setLead($lead) {
        $this->lead = $lead;
    }

    function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function setPath($path) {
        $this->path = $path;
    }
    function getGallery() {
        return $this->gallery;
    }

    function setGallery($gallery) {
        $this->gallery = $gallery;
    }
    
    function getImage() {
        return $this->image;
    }

    function setImage($image) {
        $this->image = $image;
    }

    
    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/realization/'. $this->category->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {   
            $this->setPath('/uploads/realization/'.$this->category->getId().'/');
            // zrób cokolwiek chcesz aby wygenerować unikalną nazwę
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);
        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * Add gallery
     *
     * @param \Miuze\AdminBundle\Entity\RealizationPhoto $gallery
     *
     * @return Realization
     */
    public function addGallery(\Miuze\AdminBundle\Entity\RealizationPhoto $gallery)
    {
        $this->gallery[] = $gallery;

        return $this;
    }

    /**
     * Remove gallery
     *
     * @param \Miuze\AdminBundle\Entity\RealizationPhoto $gallery
     */
    public function removeGallery(\Miuze\AdminBundle\Entity\RealizationPhoto $gallery)
    {
        $this->gallery->removeElement($gallery);
    }
}
