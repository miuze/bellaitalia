<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

class Orders {
           
    
    /**
    * @Assert\NotBlank()
    * @Assert\Length(
    *      min = 3,
    *      max = 50,
    *      minMessage = "Imię i nazwsko musi mieć conajmniej 3 znaki",
    *      minMessage = "Imię i nazwisko nie może zawierać więcej nież 50 znaków"
    * )
    */
    private $name;
    
    /**
    * @Assert\NotBlank()
    * @Assert\Length(
    *      min = 3,
    *      max = 50,
    *      minMessage = "Adres musi mieć conajmniej 3 znaki",
    *      minMessage = "Adres nie może zawierać więcej nież 150 znaków"
    * )
    */
    private $address;
    
    /**
    * @Assert\NotBlank()
    * @Assert\Length(
    *      min = 8,
    *      max = 12,
    *      minMessage = "Adres musi mieć conajmniej 8 znaki",
    *      minMessage = "Adres nie może zawierać więcej nież 12 znaków"
    * )
    */
    private $phone;
    
    private $products;
    
    /**
    * @Assert\Length(
    *      max = 500,
    *      minMessage = "Wiadomość nie może zawierać więcej nież 500 znaków"
    * )
    */
    private $message;
    
    function getName() {
        return $this->name;
    }

    function getAddress() {
        return $this->address;
    }

    function getPhone() {
        return $this->phone;
    }

    function getProducts() {
        return $this->products;
    }

    function getMessage() {
        return $this->message;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setAddress($address) {
        $this->address = $address;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setProducts($products) {
        $this->products = $products;
    }

    function setMessage($message) {
        $this->message = $message;
    }
}