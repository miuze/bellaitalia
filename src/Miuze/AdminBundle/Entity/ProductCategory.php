<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Description of ProductCategory
 *
 * @author Kamil
 */

/**
* @Gedmo\Tree(type="nested")
* @ORM\Entity
* @ORM\Table(name="product_category")
* @ORM\HasLifecycleCallbacks
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\ProductCategoryRepository")
*/

class ProductCategory{

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "productCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;

    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "productCategory"
     * )
     * @ORM\JoinTable(
     *      name = "product_category_to_page",
     * )
     */
    private $page;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Product",
     *      mappedBy = "category"
     * )
     */
    private $product;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Rabate",
     *      mappedBy = "productCategory"
     * )
     */
    protected $rabate;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    protected $defaultRabate = 0;

    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;


     /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     * )
     */
    private $sort = 0;

    /**
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagePath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="Miuze\AdminBundle\Entity\ProductCategory")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Miuze\AdminBundle\Entity\ProductCategory", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Miuze\AdminBundle\Entity\ProductCategory", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    function getFile() {
        return $this->file;
    }

    function getImagePath() {
        return $this->imagePath;
    }

    function getImage() {
        return $this->image;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function setImagePath($imagePath) {
        $this->imagePath = $imagePath;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function getId() {
        return $this->id;
    }

    function getPage() {
        return $this->page;
    }

    function getProduct() {
        return $this->product;
    }

    function getName() {
        return $this->name;
    }

    function getSort() {
        return $this->sort;
    }

    function setPage($page) {
        $this->page = $page;
    }

    function setProduct($product) {
        $this->product = $product;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setSort($sort) {
        $this->sort = $sort;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->product = new \Doctrine\Common\Collections\ArrayCollection();
        $this->imagePath = '/uploads/product/';
    }


    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/product';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->setImagePath('/uploads/product/');
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }


    /**
     * Set root
     *
     * @param \Miuze\AdminBundle\Entity\ProductCategory $root
     *
     * @return Page
     */
    public function setRoot(\Miuze\AdminBundle\Entity\ProductCategory $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Add child
     *
     * @param \Miuze\AdminBundle\Entity\ProductCategory $child
     *
     * @return Page
     */
    public function addChild(\Miuze\AdminBundle\Entity\ProductCategory $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Miuze\AdminBundle\Entity\ProductCategory $child
     */
    public function removeChild(\Miuze\AdminBundle\Entity\ProductCategory $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param mixed $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    /**
     * @return mixed
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @param mixed $lft
     */
    public function setLft($lft)
    {
        $this->lft = $lft;
    }

    /**
     * @return mixed
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * @param mixed $lvl
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;
    }

    /**
     * @return mixed
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * @param mixed $rgt
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }





    /**
     * Set defaultRabate
     *
     * @param integer $defaultRabate
     *
     * @return ProductCategory
     */
    public function setDefaultRabate($defaultRabate)
    {
        $this->defaultRabate = $defaultRabate;

        return $this;
    }

    /**
     * Get defaultRabate
     *
     * @return integer
     */
    public function getDefaultRabate()
    {
        return $this->defaultRabate;
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return ProductCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add product
     *
     * @param \Miuze\AdminBundle\Entity\Product $product
     *
     * @return ProductCategory
     */
    public function addProduct(\Miuze\AdminBundle\Entity\Product $product)
    {
        $this->product[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \Miuze\AdminBundle\Entity\Product $product
     */
    public function removeProduct(\Miuze\AdminBundle\Entity\Product $product)
    {
        $this->product->removeElement($product);
    }

    /**
     * Add rabate
     *
     * @param \Miuze\AdminBundle\Entity\Rabate $rabate
     *
     * @return ProductCategory
     */
    public function addRabate(\Miuze\AdminBundle\Entity\Rabate $rabate)
    {
        $this->rabate[] = $rabate;

        return $this;
    }

    /**
     * Remove rabate
     *
     * @param \Miuze\AdminBundle\Entity\Rabate $rabate
     */
    public function removeRabate(\Miuze\AdminBundle\Entity\Rabate $rabate)
    {
        $this->rabate->removeElement($rabate);
    }

    /**
     * Get rabate
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRabate()
    {
        return $this->rabate;
    }

    /**
     * Get root
     *
     * @return \Miuze\AdminBundle\Entity\ProductCategory
     */
    public function getRoot()
    {
        return $this->root;
    }
}
