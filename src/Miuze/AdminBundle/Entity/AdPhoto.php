<?php

namespace Miuze\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity
     * @ORM\Table(name="ad_photo")
     * @ORM\HasLifecycleCallbacks
     * @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\AdRepository")
     */
class AdPhoto {
    
    public function __construct()
    {
        $this->path = '/uploads/ad/';
    }
    
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Ad",
     *      inversedBy = "photo"
     * )
     * @ORM\JoinColumn(
     *      name = "ad_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $ad;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png", "application/pdf"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ext;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;
    
    function getImage() {
        return $this->image;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function getId() {
        return $this->id;
    }

    function getPath() {
        return $this->path;
    }

    function getFile() {
        return $this->file;
    }

    function setPath($path) {
        $this->path = $path;
    }

    function setFile($file) {
        $this->file = $file;
    }
    function getAd() {
        return $this->ad;
    }

    function setAd($ad) {
        $this->ad = $ad;
    }

    function getExt() {
        return $this->ext;
    }

    function setExt($ext) {
        $this->ext = $ext;
    }
    function getName() {
        return $this->name;
    }

    function setName($name) {
        $this->name = $name;
    }

    
    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/ad/'. $this->ad->getCategory()->getId().'/'.$this->ad->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {  
            $this->setPath('/uploads/ad/'.$this->ad->getCategory()->getId().'/'.$this->ad->getId().'/');
            $name = $this->file->getClientOriginalName();
            $name = str_replace(' ', '', $name);
            $this->setExt($this->file->guessExtension());
            $this->setImage(uniqid().'_'.$name);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
    

}
