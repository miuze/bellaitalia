<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="promotion_category")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\PromotionRepository")
*/

class PromotionCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "promotionCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "promotionCategory"
     * )
     * @ORM\JoinTable(
     *      name = "promotion_category_to_page",
     * )
     */
    private $page;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Promotion",
     *      mappedBy = "category"
     * )
     */
    protected $promotion;

    function getId() {
        return $this->id;
    }

    function getPage() {
        return $this->page;
    }

    function getTitle() {
        return $this->title;
    }

    function getPromotion() {
        return $this->promotion;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPage($page) {
        $this->page = $page;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setPromotion($promotion) {
        $this->promotion = $promotion;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->promotion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return PromotionCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add promotion
     *
     * @param \Miuze\AdminBundle\Entity\Promotion $promotion
     *
     * @return PromotionCategory
     */
    public function addPromotion(\Miuze\AdminBundle\Entity\Promotion $promotion)
    {
        $this->promotion[] = $promotion;

        return $this;
    }

    /**
     * Remove promotion
     *
     * @param \Miuze\AdminBundle\Entity\Promotion $promotion
     */
    public function removePromotion(\Miuze\AdminBundle\Entity\Promotion $promotion)
    {
        $this->promotion->removeElement($promotion);
    }
    
    function getLang() {
        return $this->lang;
    }

    function setLang($lang) {
        $this->lang = $lang;
    }
}
