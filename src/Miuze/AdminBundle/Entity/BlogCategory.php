<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="blog_category")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\BlogRepository")
*/

class BlogCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "blogCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "blogCategory"
     * )
     * @ORM\JoinTable(
     *      name = "blog_category_to_page",
     * )
     */
    private $page;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Blog",
     *      mappedBy = "category"
     * )
     */
    protected $category;

    function getPage() {
        return $this->page;
    }

    function setPage($page) {
        $this->page = $page;
    }
    
    function getId() {
        return $this->id;
    }

    function getTitle() {
        return $this->title;
    }

    function getCategory() {
        return $this->category;
    }
    function setLang($lang) {
        $this->lang = $lang;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function getBlog() {
        return $this->blog;
    }

    function setBlog($blog) {
        $this->blog = $blog;
    }  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return BlogCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add category
     *
     * @param \Miuze\AdminBundle\Entity\Blog $category
     *
     * @return BlogCategory
     */
    public function addCategory(\Miuze\AdminBundle\Entity\Blog $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \Miuze\AdminBundle\Entity\Blog $category
     */
    public function removeCategory(\Miuze\AdminBundle\Entity\Blog $category)
    {
        $this->category->removeElement($category);
    }

    function getLang() {
        return $this->lang;
    }
    
}
