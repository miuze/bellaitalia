<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="product_order")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\OrderRepository")
 */
class Order {


    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Date()
     */
    private $createDate;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Cart",
     *      mappedBy = "order",
     *      cascade={"persist", "remove", "merge"}
     * )
     */
    private $carts;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Travel",
     *      inversedBy = "order"
     * )
     * @ORM\JoinColumn(
     *      name = "travel_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $travel;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Payment",
     *      inversedBy = "order"
     * )
     * @ORM\JoinColumn(
     *      name = "payment_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $payment;

    /**
     * @ORM\Column(type="text", length=255)
     */
    private $session;

    /**
     * @ORM\Column(type="boolean")
     */
    private $close = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sendEmail = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pay = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;


    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\UserBundle\Entity\User",
     *      inversedBy = "order"
     * )
     * @ORM\JoinColumn(
     *      name = "buyer_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $buyer;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\UserBundle\Entity\User",
     *      inversedBy = "orderSeller"
     * )
     * @ORM\JoinColumn(
     *      name = "seller_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $seller;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->carts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createDate = new \DateTime('now');
    }

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return Order
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Order
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set session
     *
     * @param string $session
     *
     * @return Order
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return string
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set pay
     *
     * @param boolean $pay
     *
     * @return Order
     */
    public function setPay($pay)
    {
        $this->pay = $pay;

        return $this;
    }

    /**
     * Get pay
     *
     * @return boolean
     */
    public function getPay()
    {
        return $this->pay;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Order
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add cart
     *
     * @param \Miuze\AdminBundle\Entity\Cart $cart
     *
     * @return Order
     */
    public function addCart(\Miuze\AdminBundle\Entity\Cart $cart)
    {
        $this->carts[] = $cart;

        return $this;
    }

    /**
     * Remove cart
     *
     * @param \Miuze\AdminBundle\Entity\Cart $cart
     */
    public function removeCart(\Miuze\AdminBundle\Entity\Cart $cart)
    {
        $this->carts->removeElement($cart);
    }

    /**
     * Get carts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarts()
    {
        return $this->carts;
    }

    /**
     * Set travel
     *
     * @param \Miuze\AdminBundle\Entity\Travel $travel
     *
     * @return Order
     */
    public function setTravel(\Miuze\AdminBundle\Entity\Travel $travel = null)
    {
        $this->travel = $travel;

        return $this;
    }

    /**
     * Get travel
     *
     * @return \Miuze\AdminBundle\Entity\Travel
     */
    public function getTravel()
    {
        return $this->travel;
    }
    

    /**
     * Set seller
     *
     * @param \Miuze\UserBundle\Entity\User $seller
     *
     * @return Order
     */
    public function setSeller(\Miuze\UserBundle\Entity\User $seller = null)
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * Get seller
     *
     * @return \Miuze\UserBundle\Entity\User
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * Set buyer
     *
     * @param \Miuze\UserBundle\Entity\User $buyer
     *
     * @return Order
     */
    public function setBuyer(\Miuze\UserBundle\Entity\User $buyer = null)
    {
        $this->buyer = $buyer;

        return $this;
    }

    /**
     * Get buyer
     *
     * @return \Miuze\UserBundle\Entity\User
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * Set sendEmail
     *
     * @param boolean $sendEmail
     *
     * @return Order
     */
    public function setSendEmail($sendEmail)
    {
        $this->sendEmail = $sendEmail;

        return $this;
    }

    /**
     * Get sendEmail
     *
     * @return boolean
     */
    public function getSendEmail()
    {
        return $this->sendEmail;
    }

    /**
     * @return boolean
     */
    public function getClose()
    {
        return $this->close;
    }

    /**
     * @param boolean $close
     */
    public function setClose($close)
    {
        $this->close = $close;
    }



    /**
     * Set payment
     *
     * @param \Miuze\AdminBundle\Entity\Payment $payment
     *
     * @return Order
     */
    public function setPayment(\Miuze\AdminBundle\Entity\Payment $payment = null)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return \Miuze\AdminBundle\Entity\Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }
}
