<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="product_travel") 
*/
class Travel {
           
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
    * @ORM\Column(type="text", length=50)
    * @Assert\NotBlank()
    * @Assert\Length(
    *      min = 3,
    *      max = 50,
    *      minMessage = "Nazwa musi mieć conajmniej 3 znaki",
    *      minMessage = "Nazwa nie może zawierać więcej nież 50 znaków"
    * )
    */
    private $name;
    
    /**
    * @ORM\Column(type="float", length=100)
    * @Assert\NotBlank()
    */
    private $price;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Order",
     *      mappedBy = "travel"
     * )
     */
    protected $order;
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->order = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Travel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Travel
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add order
     *
     * @param \Miuze\AdminBundle\Entity\Order $order
     *
     * @return Travel
     */
    public function addOrder(\Miuze\AdminBundle\Entity\Order $order)
    {
        $this->order[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \Miuze\AdminBundle\Entity\Order $order
     */
    public function removeOrder(\Miuze\AdminBundle\Entity\Order $order)
    {
        $this->order->removeElement($order);
    }

    /**
     * Get order
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrder()
    {
        return $this->order;
    }
}
