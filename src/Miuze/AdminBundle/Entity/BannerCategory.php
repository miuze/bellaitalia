<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Page
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="banner_category")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\BannerRepository")
*/

class BannerCategory {
   
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "bannerCategory"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;
    
    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "bannerCategory"
     * )
     * @ORM\JoinTable(
     *      name = "banner_category_to_page",
     * )
     */
    private $page;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "Banner",
     *      mappedBy = "category"
     * )
     */
    protected $banner;
    
    function getId() {
        return $this->id;
    }

    function getPage() {
        return $this->page;
    }

    function getTitle() {
        return $this->title;
    }

    function getBanner() {
        return $this->banner;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPage($page) {
        $this->page = $page;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setBanner($banner) {
        $this->banner = $banner;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->banner = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return BannerCategory
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Add banner
     *
     * @param \Miuze\AdminBundle\Entity\Banner $banner
     *
     * @return BannerCategory
     */
    public function addBanner(\Miuze\AdminBundle\Entity\Banner $banner)
    {
        $this->banner[] = $banner;

        return $this;
    }

    /**
     * Remove banner
     *
     * @param \Miuze\AdminBundle\Entity\Banner $banner
     */
    public function removeBanner(\Miuze\AdminBundle\Entity\Banner $banner)
    {
        $this->banner->removeElement($banner);
    }
    
    function getLang() {
        return $this->lang;
    }

    function setLang($lang) {
        $this->lang = $lang;
    }
}
