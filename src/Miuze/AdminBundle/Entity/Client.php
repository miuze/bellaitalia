<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="client") 
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\ClientRepository")
* @ORM\HasLifecycleCallbacks
*/
class Client {
           
    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->path = '/uploads/client/';
    }
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "ClientCategory",
     *      inversedBy = "client"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;
    
    
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;
    
    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $person;
        
    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $address;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    
     /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lead;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $createDate;
    
    /**
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     * @ORM\Column(type="text", length=255)
     */
    private $urlTitle;
    
    function getId() {
        return $this->id;
    }

    function getCategory() {
        return $this->category;
    }

    function getName() {
        return $this->name;
    }

    function getContent() {
        return $this->content;
    }

    function getLead() {
        return $this->lead;
    }

    function getCreateDate() {
        return $this->createDate;
    }

    function getFile() {
        return $this->file;
    }

    function getPath() {
        return $this->path;
    }

    function getImage() {
        return $this->image;
    }

    function getUrlTitle() {
        return $this->urlTitle;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setLead($lead) {
        $this->lead = $lead;
    }

    function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function setPath($path) {
        $this->path = $path;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setUrlTitle($urlTitle) {
        $this->urlTitle = $urlTitle;
    }
    function getPerson() {
        return $this->person;
    }

    function getAddress() {
        return $this->address;
    }

    function setPerson($person) {
        $this->person = $person;
    }

    function setAddress($address) {
        $this->address = $address;
    }

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/client/'. $this->category->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {   
            $this->setPath('/uploads/client/'.$this->category->getId().'/');
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}
