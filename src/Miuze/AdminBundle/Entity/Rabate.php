<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="rabate")
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\RabateRepository")
*/
class Rabate {
           
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $rate;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "ProductCategory",
     *      inversedBy = "rabate"
     * )
     * @ORM\JoinColumn(
     *      name = "product_category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    protected $productCategory;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\UserBundle\Entity\User",
     *      inversedBy = "rabate"
     * )
     * @ORM\JoinColumn(
     *      name = "user_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    protected $user;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     *
     * @return Rabate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set productCategory
     *
     * @param \Miuze\AdminBundle\Entity\ProductCategory $productCategory
     *
     * @return Rabate
     */
    public function setProductCategory(\Miuze\AdminBundle\Entity\ProductCategory $productCategory = null)
    {
        $this->productCategory = $productCategory;

        return $this;
    }

    /**
     * Get productCategory
     *
     * @return \Miuze\AdminBundle\Entity\ProductCategory
     */
    public function getProductCategory()
    {
        return $this->productCategory;
    }

    /**
     * Set user
     *
     * @param \Miuze\UserBundle\Entity\User $user
     *
     * @return Rabate
     */
    public function setUser(\Miuze\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Miuze\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
