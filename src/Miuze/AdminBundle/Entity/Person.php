<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="person") 
* @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\PersonRepository")
*/
class Person {
    public function __construct()
    {
        $this->path = '/uploads/person/';
    }
        
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "PersonCategory",
     *      inversedBy = "person"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $firstname;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $lastname;
    
     /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $position;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $place;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $phone;
    
    /**
    * @ORM\Column(type="text", nullable=true)
    * @Assert\Email(
    *      message = "Ten adres email '{{ value }}' nie jest poprawny.",
    *      checkMX = true
    * )
    */
    private $email;
     
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     * @ORM\Column(type="string")
     */
    private $path;
    
    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     * )
     */
    private $sort = 0;
    
    function getSort() {
        return $this->sort;
    }

    function setSort($sort) {
        $this->sort = $sort;
    }

    function getPath() {
        return $this->path;
    }

    function setPath($path) {
        $this->path = $path;       
    }

        
    function getId() {
        return $this->id;
    }

    function getFirstname() {
        return $this->firstname;
    }

    function getLastname() {
        return $this->lastname;
    }

    function getPosition() {
        return $this->position;
    }

    function getPlace() {
        return $this->place;
    }

    function getDescription() {
        return $this->description;
    }

    function getFile() {
        return $this->file;
    }

    function getImage() {
        return $this->image;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    function setPosition($position) {
        $this->position = $position;
    }

    function setPlace($place) {
        $this->place = $place;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function setImage($image) {
        $this->image = $image;
    }
    
    function getCategory() {
        return $this->category;
    }

    function setCategory($category) {
        $this->category = $category;
    }
    
    function getPhone() {
        return $this->phone;
    }

    function getEmail() {
        return $this->email;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/person/'. $this->category->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            if (!is_null($this->image)) {
                $this->removeUpload();
            }
            $this->setPath('/uploads/person/'.$this->category->getId().'/');
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);
        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            try {
                unlink($file);
            } catch (\Exception $e){

            }
        }
    }
}
