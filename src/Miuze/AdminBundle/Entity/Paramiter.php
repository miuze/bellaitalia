<?php
/**
 * Created by PhpStorm.
 * User: Kamil Zawalski
 * Date: 2018-02-05
 * Time: 19:38
 */

namespace Miuze\AdminBundle\Entity;


class Paramiter{
    public $name;
    public $value;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }


}