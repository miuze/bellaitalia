<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * Description of Subpage
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="award") 
* @ORM\HasLifecycleCallbacks
*/
class Award {
           
    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->startDate = new \DateTime('now');
        $this->endDate = NULL;
        $this->path = '/uploads/award/';
    }
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "AwardCategory",
     *      inversedBy = "award"
     * )
     * @ORM\JoinColumn(
     *      name = "category_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $category;
    
    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $title;
    
     /**
     * @ORM\Column(type="text", length=255)
     */
    private $urlTitle;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    
     /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lead;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $startDate;
    
    /**
     * @ORM\Column(type="date", nullable = true)
     * @Assert\Date()
     */
    private $endDate;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    private $createDate;
    /**
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;
    
    function getId() {
        return $this->id;
    }

    function getCategory() {
        return $this->category;
    }

    function getTitle() {
        return $this->title;
    }

    function getUrlTitle() {
        return $this->urlTitle;
    }

    function getContent() {
        return $this->content;
    }

    function getLead() {
        return $this->lead;
    }

    function getStartDate() {
        return $this->startDate;
    }

    function getEndDate() {
        return $this->endDate;
    }

    function getCreateDate() {
        return $this->createDate;
    }

    function getFile() {
        return $this->file;
    }

    function getPath() {
        return $this->path;
    }

    function getImage() {
        return $this->image;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setUrlTitle($urlTitle) {
        $this->urlTitle = $urlTitle;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setLead($lead) {
        $this->lead = $lead;
    }

    function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }

    function setFile($file) {
        $this->file = $file;
    }

    function setPath($path) {
        $this->path = $path;
    }

    function setImage($image) {
        $this->image = $image;
    }

    
    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebPath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/award/'. $this->category->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {   
            $this->setPath('/uploads/award/'.$this->category->getId().'/');
            // zrób cokolwiek chcesz aby wygenerować unikalną nazwę
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // musisz wyrzucać tutaj wyjątek jeśli plik nie może zostać przeniesiony
        // w tym przypadku encja nie zostanie zapisana do bazy
        // metoda move() obiektu UploadedFile robi to automatycznie
        //dump($this->getUploadRootDir()); die; 
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}
