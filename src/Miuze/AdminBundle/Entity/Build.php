<?php

namespace Miuze\AdminBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Description of ProductCategory
 *
 * @author Kamil
 */

/**
* @ORM\Entity
* @ORM\Table(name="build")
* @ORM\HasLifecycleCallbacks
* @ORM\Entity(repositoryClass="Miuze\AdminBundle\Repository\BuildRepository")
*/

class Build{

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Lang",
     *      inversedBy = "build"
     * )
     * @ORM\JoinColumn(
     *      name = "lang_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $lang;

    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Page",
     *      inversedBy = "build"
     * )
     * @ORM\JoinTable(
     *      name = "build_to_page",
     * )
     */
    private $page;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Flat",
     *      mappedBy = "build"
     * )
     */
    private $flat;

    

    /**
     * @ORM\Column(type="text", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;


    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $lat;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Length(
     *      max=255
     * )
     */
    private $lng;

    private $autoGeo = false;

    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     * )
     */
    private $sort = 0;

    /**
     * @Assert\File(
     *      maxSize="2M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG."
     * )
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagePath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;



    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
        $this->flat = new \Doctrine\Common\Collections\ArrayCollection();
        $this->imagePath = '/uploads/build/';
    }


    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/flat';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->setImagePath('/uploads/flat/');
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
        if(null !== $this->address && $this->autoGeo == true){
            $latlong = \Miuze\AdminBundle\Utils\Functions::getGeo($this->address);
            $this->setLat($latlong['lat']);
            $this->setLng($latlong['long']);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);

    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Build
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return Build
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return Build
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Build
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set lang
     *
     * @param \Miuze\AdminBundle\Entity\Lang $lang
     *
     * @return Build
     */
    public function setLang(\Miuze\AdminBundle\Entity\Lang $lang = null)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return \Miuze\AdminBundle\Entity\Lang
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Add page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     *
     * @return Build
     */
    public function addPage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Miuze\AdminBundle\Entity\Page $page
     */
    public function removePage(\Miuze\AdminBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Get page
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Add flat
     *
     * @param \Miuze\AdminBundle\Entity\Flat $flat
     *
     * @return Build
     */
    public function addFlat(\Miuze\AdminBundle\Entity\Flat $flat)
    {
        $this->flat[] = $flat;

        return $this;
    }

    /**
     * Remove flat
     *
     * @param \Miuze\AdminBundle\Entity\Flat $flat
     */
    public function removeFlat(\Miuze\AdminBundle\Entity\Flat $flat)
    {
        $this->flat->removeElement($flat);
    }

    /**
     * Get flat
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFlat()
    {
        return $this->flat;
    }
    
    function getFile() {
        return $this->file;
    }

    function setFile($file) {
        $this->file = $file;
    }


    /**
     * Set address
     *
     * @param string $address
     *
     * @return Build
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set lat
     *
     * @param float $lat
     *
     * @return Build
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param float $lng
     *
     * @return Build
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @return bool
     */
    public function isAutoGeo()
    {
        return $this->autoGeo;
    }

    /**
     * @param bool $autoGeo
     */
    public function setAutoGeo($autoGeo)
    {
        $this->autoGeo = $autoGeo;
    }
}
