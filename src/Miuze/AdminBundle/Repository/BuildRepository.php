<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class BuildRepository extends EntityRepository {

    public function getAdminAllCategories($params = array()){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('b, l')
            ->from('MiuzeAdminBundle:Build', 'b')
            ->leftJoin('b.lang', 'l')
            ->where('l.lang = :lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('b.sort', 'ASC');

        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getPageBuilds($params = array()){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('b, f, photo, p')
            ->from('MiuzeAdminBundle:Build', 'b')
            ->leftJoin('b.page', 'p')
            ->where('p.id = :page')
            ->setParameter('page', $params['page'])
            ->leftJoin('b.flat', 'f')
            ->leftJoin('f.photo', 'photo')
            ->orderBy('b.sort', 'ASC');
        $r = $r->getQuery()->getResult();
        return $r;
    }
}