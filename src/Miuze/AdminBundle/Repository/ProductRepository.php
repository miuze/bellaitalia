<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class ProductRepository extends EntityRepository {

    public function getAdminProducts($params = array()){

        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('p, c')
            ->from('MiuzeAdminBundle:Product', 'p')
            ->leftJoin('p.category', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang']);
        if(isset($params['category'])){
            $r->andWhere('c.id =:category')
                ->setParameter('category', $params['category']);
        }

        $r = $r->getQuery();
        return $r;
    }

    public function getProductsPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('p')
            ->from('MiuzeAdminBundle:Product', 'p')
            ->leftJoin('p.category', 'c')
            ->leftJoin('c.page', 'pa')
            ->where('pa.id =:page AND p.isStock = 1')
            ->setParameter('page', $params['page'])
            ->orderBy('c.sort', 'ASC');
            
        $r = $r->getQuery();
        return $r;
    }
    
    
    public function getProductCat($params = array()){

        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('p')
            ->from('MiuzeAdminBundle:Product', 'p')
            ->where('p.isStock = 1');
        if($params['cat'] != null && $params['cat'] != 0){
            $r->andWhere('p.category IN (:cat)')
            ->setParameter('cat', $params['cat']);
        }
        $r = $r->getQuery();
        return $r;
    }
    
    public function getProduct($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('n')
            ->from('MiuzeAdminBundle:Product', 'n')
            ->leftJoin('n.category', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang  AND n.isStock = 1')
            ->setParameter('lang', $params['lang'])
            ->setMaxResults($params['limit']);
        if(isset($params['cat'])){
            $r->andWhere('c.id = :cat')
            ->setParameter('cat', $params['cat']);
        }
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getProductDetails($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('p, c, g, v, page')
            ->from('MiuzeAdminBundle:Product', 'p')
            ->leftJoin('p.photo', 'g')
            ->leftJoin('p.category', 'c')
            ->leftJoin('p.vat', 'v')
            ->leftJoin('c.rabate' , 'r')
            ->leftJoin('c.page', 'page')
            ->where('p.id =:id  AND p.isStock = 1')
            ->setParameter('id', $params['id']);
        $r = $r->getQuery()->getOneOrNullResult();
        return $r;
    }
    

    public function getMainProduct($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('n, c')
            ->from('MiuzeAdminBundle:Product', 'n')
            ->leftJoin('n.category', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang and n.showMain = 1  AND n.isStock = 1')
            ->setParameter('lang', $params['lang'])
            ->setMaxResults($params['limit']);
            
        $r = $r->getQuery()->getResult();
        return $r;
    }

    /**
     * @param array $params $params[name] as Product name $params[content] as Product content
     * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
     */
    public function getSearch($params = array()){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('p, c, page, l')
            ->from('MiuzeAdminBundle:Product', 'p')
            ->leftJoin('p.category', 'c')
            ->leftJoin('c.page', 'page')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang  AND p.isStock = 1')
            ->setParameter('lang', $params['lang']);
        if(isset($params['name']) && !empty($params['name'])){
            $r->andWhere('p.name LIKE :name')
                ->setParameter('name', '%'.$params['name'].'%');
        }
        if(isset($params['content']) && !empty($params['content'])){
            $r->andWhere('p.content LIKE :content')
                ->setParameter('content', '%'.$params['content'].'%');
        }

        $r = $r->getQuery();
        return $r;
    }

    /**
     * @param $lang 2 char of lang
     * @return array|\Doctrine\ORM\QueryBuilder
     */
    public function getLastAddProduct($lang){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('p, c, l')
            ->from('MiuzeAdminBundle:Product', 'p')
            ->leftJoin('p.category', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang  AND p.isStock = 1')
            ->setParameter('lang', $lang)
            ->orderBy('p.createDate', 'ASC')
            ->setMaxResults(8);

        $r = $r->getQuery()->getResult();
        return $r;
    }

    /**
     * @param $lang 2 char of lang
     * @return array|\Doctrine\ORM\QueryBuilder
     */
    public function getMorePopularProduct($lang){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('p, c, l')
            ->from('MiuzeAdminBundle:Product', 'p')
            ->leftJoin('p.category', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang  AND p.isStock = 1')
            ->setParameter('lang', $lang)
            ->orderBy('p.cnt', 'ASC')
            ->setMaxResults(8);

        $r = $r->getQuery()->getResult();
        return $r;
    }

    /**
     * @param $lang 2 char of lang
     * @return array|\Doctrine\ORM\QueryBuilder
     */
    public function getBestSellerProduct($lang){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('product, cart')
            ->from('MiuzeAdminBundle:Product', 'product')
            ->where('product.isStock = 1')
            ->innerJoin('product.cart', 'cart')
            ->groupBy('cart.product')
            ->orderBy('cart.quantity')
            ->setMaxResults(8);

        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getNoPhoto(){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('product, photo')
            ->from('MiuzeAdminBundle:Product', 'product')
            ->where('product.image IS NULL')
            ->leftJoin('product.photo', 'photo');

        $r = $r->getQuery()->getResult();
        return $r;
    }

    
}