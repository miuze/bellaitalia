<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class OfferRepository extends EntityRepository {

   
    public function getAllQuery($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('o','n')
            ->from('MiuzeAdminBundle:Offer', 'o')
            ->leftJoin('o.subscriber', 'n');
            
        $r = $r->getQuery();
        return $r;
    }
    
    public function getDetails($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('o, n, product, p')
            ->from('MiuzeAdminBundle:Offer', 'o')
            ->leftJoin('o.subscriber', 'n')
            ->leftJoin('o.productItems', 'p')
            ->leftJoin('p.product', 'product')
            ->where('o.id = :id')
            ->setParameter('id', $params['id']);
            
        $r = $r->getQuery()->getOneOrNullResult();
        return $r;
    }

}