<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class BannerRepository extends EntityRepository {

    public function getCategoriesBannerPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        
        $r = $qb->select('c','b')
            ->from('MiuzeAdminBundle:BannerCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.category', 'b')
            ->andWhere('b.startDate < :date')
            ->andWhere('b.endDate > :date OR b.endDate IS NULL')
            ->setParameter('date', $currentDate)
            ->orderBy('b.startDate', 'DESC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getBanners($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        
        $r = $qb->select('b')
            ->from('MiuzeAdminBundle:Banner', 'b')
            ->where('b.category =:cat')
            ->setParameter('cat', $params['cat'])
            ->orderBy('b.sort', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }

    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c','l')
            ->from('MiuzeAdminBundle:BannerCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
}