<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class FaqRepository extends EntityRepository {

    public function getCategoriesFaqPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('c','f')
            ->from('MiuzeAdminBundle:FaqCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.faq', 'f')
            ->orderBy('f.sort', 'ASC')
            ->getQuery()
            ->getResult();
        
        return $r;
    }
    
    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c','l')
            ->from('MiuzeAdminBundle:FaqCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getFaq($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('f','c')
            ->from('MiuzeAdminBundle:Faq', 'f')
            ->leftJoin('f.category', 'c')
            ->where('c.id = :cat')
            ->setParameter('cat', $params['cat'])
            ->orderBy('c.id', 'ASC');
            if(isset($params['limit'])){
                $r->setMaxResults($params['limit']);
            }
        $r = $r->getQuery()->getResult();
        return $r;
    }

}