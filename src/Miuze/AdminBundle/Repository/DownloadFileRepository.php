<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class DownloadFileRepository extends EntityRepository {

    public function getFilesDownload($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        
        $r = $qb->select('f')
            ->from('MiuzeAdminBundle:DownloadFile', 'f')
            ->innerJoin('f.download', 'd')
            ->where('d.id =:download')
            ->setParameter('download', $params['download'])
            ->getQuery()
            ->getResult();
        
        return $r;
    }
    

}