<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class BuyerRepository extends EntityRepository {

    public function getSuccess($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('b')
            ->from('MiuzeAdminBundle:Buyer', 'b')
            ->where('b.status =:status')
            ->setParameter('status', 'success')
            ->orderBy('b.id', 'DESC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getWaiting($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('b')
            ->from('MiuzeAdminBundle:Buyer', 'b')
            ->where('b.status =:status')
            ->setParameter('status', 'waiting')
            ->orderBy('b.id', 'DESC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getDetails($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('b, c, p, cat, t')
            ->from('MiuzeAdminBundle:Buyer', 'b')
            ->leftJoin('b.cart', 'c')
            ->leftJoin('b.travel', 't')
            ->leftJoin('c.product', 'p')
            ->leftJoin('p.category', 'cat')
            ->where('b.id =:id')
            ->setParameter('id', $params['id']);
        
        $r = $r->getQuery()->getOneOrNullResult();
        return $r;
    }
    
}