<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class OrderRepository extends EntityRepository {

    public function getAllOrder($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('o')
            ->from('MiuzeAdminBundle:Order', 'o')
            ->where('o.user =:user')
            ->setParameter('user', $params['user'])
            ->orderBy('o.id', 'DESC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getDetails($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('o, c, p, t, u')
            ->from('MiuzeAdminBundle:Order', 'o')
            ->where('o.id =:id')
            ->leftJoin('o.carts', 'c')
            ->leftJoin('c.product', 'p')
            ->leftJoin('o.travel', 't')
            ->leftJoin('o.buyer', 'u')
            ->setParameter('id', $params['id']);
            
        $r = $r->getQuery()->getOneOrNullResult();
        return $r;
    }

//    account
    public function getAccountOrdersFinished($params = array()){

        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('o, u')
            ->from('MiuzeAdminBundle:Order', 'o')
            ->where('u.id =:user AND o.status = 1')
            ->leftJoin('o.user', 'u')
            ->setParameter('user', $params['user']);

        $r = $r->getQuery();
        return $r;
    }

    public function getAccountOrdersWaiting($params = array()){

        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('o, u')
            ->from('MiuzeAdminBundle:Order', '  o')
            ->leftJoin('o.user', 'u', 'WITH', 'u.id = :user')
            ->where('o.status = 0 AND o.pay = 0')
            ->setParameter('user', $params['user']);

        $r = $r->getQuery();
        return $r;
    }

    public function getAccountOrdersInrealization($params = array()){

        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('o, u')
            ->from('MiuzeAdminBundle:Order', 'o')
            ->where('u.id =:user AND o.status = 0 AND o.pay = 1')
            ->leftJoin('o.user', 'u')
            ->setParameter('user', $params['user']);

        $r = $r->getQuery();
        return $r;
    }

    public function getAccountOrderDetails($params = array()){

        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('o, c, p, t, u')
            ->from('MiuzeAdminBundle:Order', 'o')
            ->where('o.id =:id AND u.id =:user')
            ->leftJoin('o.carts', 'c')
            ->leftJoin('c.product', 'p')
            ->leftJoin('o.travel', 't')
            ->leftJoin('o.user', 'u')
            ->setParameter('id', $params['id'])
        ->setParameter('user', $params['user']);

        $r = $r->getQuery()->getOneOrNullResult();
        return $r;
    }

    public function getShop(){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('c')
            ->from('MiuzeAdminBundle:Order', 'c')
            ->leftJoin('c.buyer', 'buyer')
            ->leftJoin('c.seller', 'seller')
            ->where('c.seller IS NOT NULL')
            ->getQuery();

        return $r;
    }

    public function getOnline(){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('c')
            ->from('MiuzeAdminBundle:Order', 'c')
            ->leftJoin('c.buyer', 'buyer')
            ->leftJoin('c.seller', 'seller')
            ->where('c.seller IS NULL AND ( buyer.roles LIKE :user OR buyer.roles LIKE :register)')
            ->setParameter('user', '%ROLE_USER%')
            ->setParameter('register', '%ROLE_USER_REGISTER%')
            ->getQuery();

        return $r;
    }

    public function getPartners(){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('c')
            ->from('MiuzeAdminBundle:Order', 'c')
            ->leftJoin('c.buyer', 'buyer')
            ->leftJoin('c.seller', 'seller')
            ->where('c.seller IS NULL AND buyer.roles LIKE :roles')
            ->setParameter('roles', '%ROLE_CONTRACTOR%')
            ->getQuery();

        return $r;
    }



}