<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class VideoRepository extends EntityRepository {

    public function getCategoriesVideoPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        
        $r = $qb->select('c','n')
            ->from('MiuzeAdminBundle:VideoCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.category', 'n')
            ->andWhere('n.startDate < :date')
            ->andWhere('n.endDate > :date OR n.endDate IS NULL')
            ->setParameter('date', $currentDate)
            ->orderBy('n.startDate', 'DESC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    
    
    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c','l')
            ->from('MiuzeAdminBundle:VideoCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getVideo($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('v','c')
            ->from('MiuzeAdminBundle:Video', 'v')
            ->leftJoin('v.category', 'c')
//            ->where('c.id = :cat')
//            ->setParameter('cat', $params['cat'])
            ->orderBy('c.id', 'ASC');
            if(isset($params['limit'])){
                $r->setMaxResults($params['limit']);
            }
        $r = $r->getQuery()->getResult();
        return $r;
    }

}