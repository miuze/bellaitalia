<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class NewsRepository extends EntityRepository {

    public function getCategoriesNewsPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        
        $r = $qb->select('c','n')
            ->from('MiuzeAdminBundle:NewsCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.category', 'n')
            ->andWhere('n.startDate < :date')
            ->andWhere('n.endDate > :date OR n.endDate IS NULL')
            ->setParameter('date', $currentDate)
            ->orderBy('n.startDate', 'DESC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getNews($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        
        $r = $qb->select('n')
            ->from('MiuzeAdminBundle:News', 'n')
            ->andWhere('n.startDate < :date')
            ->andWhere('n.endDate > :date OR n.endDate IS NULL')
            ->setParameter('date', $currentDate)
            ->orderBy('n.startDate', 'DESC')
            ->setMaxResults($params['limit']);
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    
    public function getSlider($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        
        $r = $qb->select('n')
            ->from('MiuzeAdminBundle:News', 'n')
            ->where('n.slider = true')
            ->andWhere('n.startDate < :date')
            ->andWhere('n.endDate > :date OR n.endDate IS NULL')
            ->setParameter('date', $currentDate)
            ->orderBy('n.startDate', 'DESC')
            ->setMaxResults($params['limit']);
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    
    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c','l')
            ->from('MiuzeAdminBundle:NewsCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }

}