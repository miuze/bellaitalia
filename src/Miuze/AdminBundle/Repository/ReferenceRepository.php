<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

//use Doctrine\Common\Cache\RedisCache;
use Snc\RedisBundle\Client\Phpredis;

/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class ReferenceRepository extends EntityRepository {

    public function getCategoriesReferencePage($params = array()){

        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('c','r')
            ->from('MiuzeAdminBundle:ReferenceCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.reference', 'r')
            ->getQuery()
            ->getResult();
        
        return $r;
    }
    
    
    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c','l')
            ->from('MiuzeAdminBundle:ReferenceCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }

    public function getLastReferences($params = array()){

        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('r')
            ->from('MiuzeAdminBundle:Reference', 'r')
            ->setMaxResults($params['limit'])
            ->orderBy('r.id', 'DESC');

        $r = $r->getQuery()->getResult();
        return $r;
    }


}