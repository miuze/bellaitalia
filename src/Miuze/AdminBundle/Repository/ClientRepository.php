<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

//use Doctrine\Common\Cache\RedisCache;
use Snc\RedisBundle\Client\Phpredis;

/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class ClientRepository extends EntityRepository {

    public function getCategoriesClientPage($params = array()){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('c','r')
            ->from('MiuzeAdminBundle:ClientCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.client', 'cl')
            ->getQuery()
            ->getResult();
        
        return $r;
    }
    
    public function getToPage($page){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('c')
            ->from('MiuzeAdminBundle:Client', 'c')
            ->innerJoin('c.category', 'cat')
            ->innerJoin('cat.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $page)
            ->getQuery()
            ->getResult();
        
        return $r;
    }
    
    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c','l')
            ->from('MiuzeAdminBundle:ClientCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }

}