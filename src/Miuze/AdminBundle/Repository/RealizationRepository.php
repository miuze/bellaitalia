<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class RealizationRepository extends EntityRepository {

    public function getCategoriesRealizationPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('c','r')
            ->from('MiuzeAdminBundle:RealizationCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.realization', 'r')
            ->getQuery()
            ->getResult();
        
        return $r;
    }
    
    public function findWithPhoto($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('p','r')
            ->from('MiuzeAdminBundle:Realization', 'r')
            ->where('r.id =:id')
            ->setParameter('id', $params['id'])
            ->leftJoin('r.gallery', 'p')
            ->getQuery()
            ->getOneOrNullResult();
        
        return $r;
    }
    
    public function getRealization($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('r')
            ->from('MiuzeAdminBundle:Realization', 'r')
            ->orderBy('r.id', 'DESC')
            ->setMaxResults($params['limit']);
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c','l')
            ->from('MiuzeAdminBundle:RealizationCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }

}