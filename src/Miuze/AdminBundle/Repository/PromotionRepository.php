<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class PromotionRepository extends EntityRepository {

    public function getCategoriesPromotionPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        
        $r = $qb->select('c','pr')
            ->from('MiuzeAdminBundle:PromotionCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.promotion', 'pr')
            ->andWhere('pr.startDate < :date')
            ->andWhere('pr.endDate > :date OR pr.endDate IS NULL')            
            ->setParameter('date', $currentDate)
            ->orderBy('pr.startDate', 'DESC')
            ->getQuery()
            ->getResult();
        
        return $r;
    }
    
    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c','l')
            ->from('MiuzeAdminBundle:PromotionCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }

}