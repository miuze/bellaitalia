<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class RabateRepository extends EntityRepository {

    public function getRabate($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('r, c, u')
            ->from('MiuzeAdminBundle:Rabate', 'r')
            ->leftJoin('r.user', 'u')
            ->leftJoin('r.productCategory', 'c');
            
        $r = $r->getQuery();
        return $r;
    }

}