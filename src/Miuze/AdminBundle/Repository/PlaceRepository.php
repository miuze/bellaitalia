<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PlaceRepository extends EntityRepository {

    public function getCategoriesPlacePage($params = []){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        
        $r = $qb->select('c','pe')
            ->from('MiuzeAdminBundle:PlaceCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.place', 'pe')
            ->orderBy('pe.sort', 'DESC')
            ->getQuery()
            ->getResult();
        
        return $r;
    }
    
    public function getAllCategories($params = []){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c')
            ->from('MiuzeAdminBundle:PlaceCategory', 'c')
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    
    public function getPlace($params = []){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('p','c')
            ->from('MiuzeAdminBundle:Place', 'p')
            ->leftJoin('p.category', 'c')
            ->where('c.id = :cat')
            ->setParameter('cat', $params['cat'])
            ->orderBy('c.id', 'ASC');
            if(isset($params['limit'])){
                $r->setMaxResults($params['limit']);
            }
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    
    public function getLatLng($params = []){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('p')
            ->from('MiuzeAdminBundle:Place', 'p')
            ->leftJoin('p.category', 'c')
            ->where('c.id = :cat')
            ->setParameter('cat', $params['cat'])
            ->orderBy('c.id', 'ASC');
            if(isset($params['limit'])){
                $r->setMaxResults($params['limit']);
            }
        $r = $r->getQuery()->getResult();
        return $r;
    }

}