<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class NewsletterRepository extends EntityRepository {
    
    public function getNewsletter($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('n')
            ->from('MiuzeAdminBundle:Newsletter', 'n')
            ->leftJoin('n.category', 'c')
            ->where('c.id =:cat')
            ->setParameter('cat', $params['cat']);
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getCatNewsletter(){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c')
            ->from('MiuzeAdminBundle:NewsletterCategory', 'c')
            ->leftJoin('c.newsletter', 'n');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
}