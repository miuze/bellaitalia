<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class ProductCategoryRepository extends NestedTreeRepository {

//    use NestedTreeRepositoryTrait;
//
//    public function __construct(EntityManager $em, ClassMetadata $class)
//    {
//        parent::__construct($em, $class);
//
//        $this->initializeTreeRepository($em, $class);
//    }

    public function getCategoryMenu($params = array()){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('c, parent, page')
            ->from('MiuzeAdminBundle:ProductCategory', 'c')
            ->leftJoin('c.page', 'page')
            ->leftJoin('c.lang', 'l')
            ->leftJoin('c.parent', 'parent')
            ->where('l.lang = :lang AND page.id = :page')
            ->setParameter('page', $params['page'])
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.root, c.sort, c.lft', 'ASC');
        if(isset($params['cat'])){
            $r->andWhere('parent.id =: category')
                ->setParameter('category', $params['cat']);
        }

        $r = $r->getQuery()->getArrayResult();
        return $r;
    }

    public function getLeftCategoryMenu($params = array()){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('c, parent')
            ->from('MiuzeAdminBundle:ProductCategory', 'c')
            ->leftJoin('c.page', 'page')
            ->leftJoin('c.lang', 'l')
            ->leftJoin('c.parent', 'parent')
            ->where('l.lang = :lang AND page.id = :page AND c.lvl = 0')
            ->setParameter('page', $params['page'])
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.root, c.sort, c.lft', 'ASC');

        $r = $r->getQuery()->getArrayResult();
        return $r;
    }

    public function getOnlyChildren($params = array()){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('c, parent')
            ->from('MiuzeAdminBundle:ProductCategory', 'c')
            ->leftJoin('c.parent', 'parent')
            ->orderBy('c.root, c.sort, c.lft', 'ASC')
            ->where('parent.id =:parentId')
            ->setParameter('parentId', $params['parentId']);

        $r = $r->getQuery()->getResult();
        return $r;
    }

    public function getAdminAllCategoriesToTree($params = array()){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('c, parent')
            ->from('MiuzeAdminBundle:ProductCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->leftJoin('c.parent', 'parent')
            ->where('l.lang = :lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.root, c.lft, c.lvl, c.sort', 'ASC');

        $r = $r->getQuery()->getArrayResult();
        return $r;
    }

    public function getCategoriesProductPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('c','product')
            ->from('MiuzeAdminBundle:ProductCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->leftJoin('c.product', 'product')
            ->orderBy('c.sort', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    


    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c','l')
            ->from('MiuzeAdminBundle:ProductCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.sort', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    

     public function getMainCategory($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c')
            ->from('MiuzeAdminBundle:ProductCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->setMaxResults($params['limit']);
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
     public function getOneCategory($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c')
            ->from('MiuzeAdminBundle:ProductCategory', 'c')
            ->where('c.id = :id')
            ->leftJoin('c.page', 'page')
            ->setParameter('id', $params['id']);
            
        $r = $r->getQuery()->getOneOrNullResult();
        return $r;
    }
}