<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class GalleryRepository extends EntityRepository {

    public function getCategoriesGalleryPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c, g, ph')
            ->from('MiuzeAdminBundle:GalleryCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->leftJoin('c.gallery', 'g')
            ->leftJoin('g.photo', 'ph')
            ->orderBy('g.id', 'DESC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getGalleryPhoto($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('p','g')
            ->from('MiuzeAdminBundle:Gallery', 'g')
            ->where('g.id =:gallery')
            ->setParameter('gallery', $params['gallery'])
            ->leftJoin('g.photo', 'p');
            
        $r = $r->getQuery()->getOneOrNullResult();
        return $r;
    }
    
    public function getGallery($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('g')
            ->from('MiuzeAdminBundle:Gallery', 'g')
            ->orderBy('g.id', 'DESC')
            ->setMaxResults($params['limit']);
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getPhoto($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('p')
            ->from('MiuzeAdminBundle:GalleryPhoto', 'p')
            ->orderBy('p.id', 'DESC')
            ->setMaxResults($params['limit']);
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c','l')
            ->from('MiuzeAdminBundle:GalleryCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }

}