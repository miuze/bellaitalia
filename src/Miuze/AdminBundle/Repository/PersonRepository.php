<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class PersonRepository extends EntityRepository {

    public function getCategoriesPersonPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        
        $r = $qb->select('c','pe')
            ->from('MiuzeAdminBundle:PersonCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.person', 'pe')
            ->orderBy('pe.sort', 'DESC')
            ->getQuery()
            ->getResult();
        
        return $r;
    }
    
    public function getAllCategories($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('c')
            ->from('MiuzeAdminBundle:PersonCategory', 'c')
            ->orderBy('c.id', 'ASC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }
    
    public function getPerson($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('p','c')
            ->from('MiuzeAdminBundle:Person', 'p')
            ->leftJoin('p.category', 'c')
            ->where('c.id = :cat')
            ->setParameter('cat', $params['cat'])
            ->orderBy('c.id', 'ASC');
            if(isset($params['limit'])){
                $r->setMaxResults($params['limit']);
            }
        $r = $r->getQuery()->getResult();
        return $r;
    }

}