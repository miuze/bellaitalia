<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class FlatRepository extends EntityRepository {

    public function getAdminFlats($params = array()){

        $qb = $this->getEntityManager()->createQueryBuilder();

        $r = $qb->select('p, c')
            ->from('MiuzeAdminBundle:Flat', 'p')
            ->leftJoin('p.build', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang']);
        if(isset($params['build'])){
            $r->andWhere('c.id =:build')
                ->setParameter('build', $params['build']);
        }

        $r = $r->getQuery();
        return $r;
    }

    public function getFlatPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('f')
            ->from('MiuzeAdminBundle:Flat', 'f')
            ->leftJoin('f.build', 'c')
            ->leftJoin('c.page', 'pa')
            ->where('pa.id =:page AND f.isStock = 1')
            ->setParameter('page', $params['page'])
            ->orderBy('c.sort', 'ASC');
            
        $r = $r->getQuery();
        return $r;
    }
    
    

    public function getLastFlats($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('f, p, b')
            ->from('MiuzeAdminBundle:Flat', 'f')
            ->leftJoin('f.build', 'b')
            ->leftJoin('b.page', 'p')
            ->where('p.id = :page AND f.isStock = 1')
            ->setParameter('page', $params['page'])
            ->setMaxResults($params['limit'])
            ->orderBy('f.createDate', 'DESC');
            
        $r = $r->getQuery()->getResult();
        return $r;
    }

    public function getFlatDetails($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $r = $qb->select('f, p, b')
            ->from('MiuzeAdminBundle:Flat', 'f')
            ->leftJoin('f.photo', 'p')
            ->leftJoin('f.build', 'b')
            ->where('f.id =:id  AND f.isStock = 1')
            ->setParameter('id', $params['id']);
        $r = $r->getQuery()->getOneOrNullResult();
        return $r;
    }
}