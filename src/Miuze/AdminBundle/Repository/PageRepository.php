<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\Query;
use Miuze\AdminBundle\Entity\Page;

/**
 * Description of PageRepository
 *
 * @author Kamil
 */
class PageRepository extends NestedTreeRepository{
    
    
    public function __construct(\Doctrine\ORM\EntityManager $em, \Doctrine\ORM\Mapping\ClassMetadata $class) {
        parent::__construct($em, $class);
    }

    public function getMainPageByMenu(int $menuId): ?Page
    {
        $roots = $this->getRootNodes();
        $ids = [];
        foreach($roots as $root){
            array_push($ids, $root->getId());
        }
        if(empty($ids)){
            return null;
        }

        return $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from(Page::class, 'p')
            ->leftJoin('p.category', 'c')
            ->where('c.id = :menu AND p.id IN(:ids)')
            ->setParameter('menu', $menuId)
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getOneOrNullResult();

    }
    /**
     * Get children, ordered by the sort property.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSortedChildren($mainPage)
    {
        return $this->childrenQueryBuilder($mainPage)
            ->select('p, parent, m, s')
            ->from('MiuzeAdminBundle:Page', 'p')
            ->orderBy('p.root, p.lft', 'ASC')
            ->leftJoin('p.slug', 's')
            ->leftJoin('p.category', 'm')
            ->leftJoin('p.parent', 'parent')
            ->getQuery()
            ->getArrayResult();
    }

    public function getAdminAllCategoriesToTree($menuId){

        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('p, parent, m, s')
            ->from('MiuzeAdminBundle:Page', 'p')
            ->orderBy('p.root, p.lft', 'ASC')
            ->leftJoin('p.slug', 's')
            ->leftJoin('p.category', 'm')
            ->leftJoin('p.parent', 'parent')
            ->where('m.id = :menuId')
            ->setParameter('menuId', $menuId);
        $r = $r->getQuery()->getArrayResult();
        return $r;
    }


    public function getPageToTree($params = array()){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->select('p, parent, m, s')
            ->from('MiuzeAdminBundle:Page', 'p')
            ->leftJoin('p.slug', 's')
            ->leftJoin('p.category', 'm')
            ->leftJoin('m.lang', 'lang')
            ->leftJoin('p.parent', 'parent')
            ->where('lang.lang = :lang AND p.visable = 1 AND p.active = 1')
            ->setParameter('lang', $params['lang'])
            ->orderBy('p.root, p.lft', 'ASC');

        $r = $r->getQuery()->getArrayResult();
        return $r;
    }


    public function getPageBySlug($slug){
        $qb = $this->getEntityManager()->createQueryBuilder('p, s');
        $r = $qb->select('p, sections, s')
            ->from('MiuzeAdminBundle:Page', 'p')
            ->where('p.active = 1 AND s.slug = :slug')
            ->leftJoin('p.slug', 's')
            ->setParameter('slug', $slug)
            ->leftJoin('p.sections', 'sections');
        return $menu = $r->getQuery()->getOneOrNullResult();
    }


    public function getAllEnablePages(){
        $qb = $this->getEntityManager()->createQueryBuilder('p');
        $r = $qb->select('p')
            ->from('MiuzeAdminBundle:Page', 'p')
            ->where('p.removed != 1 AND p.active = 1');
        return $menu = $r->getQuery()->getResult();
    }

//$this->getEntityManager()->getConfiguration()->addCustomHydrationMode(
//'tree',
//'Gedmo\Tree\Hydrator\ORM\TreeObjectHydrator'
//);
//>getQuery();
//            ->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
//            ->getResult('tree');
}