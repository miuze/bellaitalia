<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Miuze\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Description of BlogRepository
 *
 * @author Kamil
 */
class BlogRepository extends EntityRepository {

    public function getCategoriesBlogPage($params = array()){
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        $r = $qb->select('c','b')
            ->from('MiuzeAdminBundle:BlogCategory', 'c')
            ->innerJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->innerJoin('c.category', 'b')
            ->andWhere('b.startDate < :date')
            ->andWhere('b.endDate > :date OR b.endDate IS NULL')
            ->setParameter('date', $currentDate)
            ->orderBy('b.startDate', 'DESC');
            if (!empty($params['limit']) && isset($params['limit']) && $params['limit'] != NULL){
                $qb->setMaxResults($params['limit']);
            }
        
        $r = $r->getQuery()->getResult();
        
        return $r;
    }
    
    public function getAllCategories($params = array()){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query = $qb->select('c','l')
            ->from('MiuzeAdminBundle:BlogCategory', 'c')
            ->leftJoin('c.lang', 'l')
            ->where('l.lang =:lang')
            ->setParameter('lang', $params['lang'])
            ->orderBy('c.id', 'ASC')
            ->getQuery();
        return $query->getResult();
    }

    public function getBlogPagePaginator($params = array()){
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query = $qb->select('b, c')
            ->from('MiuzeAdminBundle:Blog', 'b')
            ->leftJoin('b.category', 'c')
            ->leftJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->orderBy('b.startDate', 'DESC')
            ->andWhere('b.startDate < :date')
            ->andWhere('b.endDate > :date OR b.endDate IS NULL')
            ->setParameter('date', $currentDate)
            ->getQuery();
        return $query;
    }

    public function getLastPost($params = array()){
        $currentDate = new \DateTime('now');
        $currentDate->format('dd-mm-yyyy');
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query = $qb->select('b, p, c')
            ->from('MiuzeAdminBundle:Blog', 'b')
            ->leftJoin('b.category', 'c')
            ->leftJoin('c.page', 'p')
            ->where('p.id =:page')
            ->setParameter('page', $params['page'])
            ->orderBy('b.startDate', 'DESC')
            ->setMaxResults($params['limit'])
            ->andWhere('(b.endDate > :date OR b.endDate IS NULL) AND b.startDate < :date')
            ->setParameter('date', $currentDate)
            ->getQuery();
        return $query->getResult();
    }




}