<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Creator;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Manager\LinkManager;

class SitemapCreator
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var LinkManager
     */
    private $linkManager;

    /**
     * @var string
     */
    private $sitemapDir;

    public function __construct(
        Registry $doctrine,
        LinkManager $linkManager,
        string $sitemapDir
    ) {
        $this->doctrine = $doctrine;
        $this->linkManager = $linkManager;
        $this->sitemapDir = $sitemapDir;
    }

    public function create($page){
        $xml = new \SimpleXMLElement('<xml/>');
        $pages = $this->doctrine->getRepository(Page::class)->getAllEnablePages();
        $itemsXml = new \SimpleXMLElement('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"></urlset>');

        foreach ($pages as $item){
            $itemXml = $itemsXml->addChild('url');
//            $itemXml->addAttribute('id', $item->getId());
//            $itemXml->addAttribute('controller', $item->getType());
            $url = $this->linkManager->generateUri($item);
            $mobileXml = $itemXml->addChild('loc', $url);
        }
        $doc = new \DOMDocument('1.0');
        $doc->formatOutput = true;
        $domnode = dom_import_simplexml($itemsXml);
        $domnode->preserveWhiteSpace = false;
        $domnode = $doc->importNode($domnode, true);
        $domnode = $doc->appendChild($domnode);
        $saveXml = $doc->saveXML();
        $path = $this->sitemapDir . 'sitemap.xml';
        file_put_contents($path, $saveXml);
    }

}