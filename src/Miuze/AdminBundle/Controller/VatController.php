<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Vat;
use Miuze\AdminBundle\Form\Vat\VatType;


/**
     * @Route(
     *      "/vat"
     * )
     */
class VatController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_vat_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction($page)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Vat');
        $list = $repo->findAll();
        
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_vat_add",
     * )
     * @Template()
     */
    public function addAction(Request $request)
    {
        $session = $this->get('session');
        $entity = new Vat();

        $form = $this->createForm(VatType::class, $entity); 
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){      
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, Vat dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_vat_index'));
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_vat_edit"
     * )
     * @Template("MiuzeAdminBundle:Vat:add.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Vat');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( VatType::class, $entity);        
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){  
                
                $em = $this->getDoctrine()->getManager();
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, Vat zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_vat_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_vat_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Vat');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Dostawca został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_vat_index'));
    }
}
