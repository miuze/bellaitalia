<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Provider;
use Miuze\AdminBundle\Form\Provider\ProviderType;


/**
     * @Route(
     *      "/provider"
     * )
     */
class ProviderController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_provider_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction($page)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Provider');
        $list = $repo->findAll();
        
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_provider_add",
     * )
     * @Template()
     */
    public function addAction(Request $request)
    {
        $session = $this->get('session');
        $entity = new Provider();

        $form = $this->createForm(ProviderType::class, $entity); 
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){      
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, Dostawcę dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_provider_index'));
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_provider_edit"
     * )
     * @Template("MiuzeAdminBundle:Provider:add.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Provider');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( ProviderType::class, $entity);        
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){  
                
                $em = $this->getDoctrine()->getManager();
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, dostawcę zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_provider_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_provider_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Provider');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Dostawca został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_provider_index'));
    }
}
