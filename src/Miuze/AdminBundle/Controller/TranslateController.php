<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Translate;
use Miuze\AdminBundle\Form\Translate\TranslateType;
use Miuze\AdminBundle\Entity\Lang;
use Miuze\AdminBundle\Form\Translate\TranslateAddType;

/**
     * @Route(
     *      "/translate"
     * )
     */
class TranslateController extends Controller
{
           
    /**
     * @Route(
     *      "/",
     *      name = "admin_translate_index"
     * )
     * @Template("@MiuzeAdmin/Translate/index.html.twig")
     */
    public function indexAction(Request $request){        

        $dirviews = __DIR__.'/../../PageBundle/Resources/translations/';
        $view = array();
        foreach(scandir($dirviews) as $key => $file){
            if($file != '.' && $file != '..'){
                $name = explode('.', $file);
                array_push($view, $name[1]);
            }
        }
        return array(
            'list' => $view,
        );
    }
    
    /**
     * @Route(
     *      "/edit/{lang}",
     *      name = "admin_translate_edit"
     * )
     * @Template("@MiuzeAdmin/Translate/edit.html.twig")
     */
    public function editAction(Request $request){        

        $session = $this->get('session');
        $lang = $request->get('lang');
        $dirviews = __DIR__.'/../../PageBundle/Resources/translations/messages.'.$lang.'.yml';
        $file = file_get_contents($dirviews);
        
        $entity = new Translate();
        $entity->setContent($file);
        
        $form = $this->createForm( TranslateType::class, $entity, array(
            'attr'=> array(
                'class' => 'no-wysiwyg'
            )
        ));
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                file_put_contents($dirviews, $entity->getContent());
                $session->getFlashBag()->add('success', 'Gratulacje, plik zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_translate_index'));
            }
        }
        
        return array(
            'lang' => $lang,
            'form' => $form->createView(),
        );
    }
    
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_translate_add"
     * )
     * @Template("@MiuzeAdmin/Translate/add.html.twig")
     */
    public function addAction(Request $request){        

        $session = $this->get('session');
        $entity = new Lang();
        
        $form = $this->createForm( TranslateAddType::class, $entity);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $dirviews = __DIR__.'/../../PageBundle/Resources/translations/messages.'.$entity->getLang().'.yml';
                file_put_contents($dirviews, '');
                $session->getFlashBag()->add('success', 'Gratulacje, dodano nowy język prawidłowo');
                return $this->redirect($this->generateUrl('admin_translate_index'));
            }
        }
        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/delete/{lang}",
     *      name = "admin_translate_delete"
     * )
     */
    public function deleteAction(Request $request){        

        $lang = $request->get('lang');
        $session = $this->get('session');
        $entity = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
        $msg = '';
        if($entity != NULL){
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
            $msg = $msg. 'Usunięto z bazy danych';
        }else{
            $msg = $msg. 'Nie usunięto z bazy danych, taki wpis nie istniał.';
        }
        
        $dirviews = __DIR__.'/../../PageBundle/Resources/translations/messages.'.$lang.'.yml';
        if(file_exists($dirviews)){
            unlink($dirviews);
            $msg = $msg.' Usunięto z systemu plików';
        }else{
            $msg = $msg.' Nie usunięto z systemu plików, taki plik nieistnieje.';
        }
        $session->getFlashBag()->add('success', $msg);
        return $this->redirect($this->generateUrl('admin_translate_index'));
    }
    
}
