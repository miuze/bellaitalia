<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Miuze\AdminBundle\Entity\Product;
use Miuze\AdminBundle\Entity\ProductPhoto;
use Miuze\AdminBundle\Form\Product\ProductCategoryType;
use Miuze\AdminBundle\Form\Product\ProductPhotoType;
use Miuze\AdminBundle\Form\Product\ProductType;

/**
     * @Route(
     *      "/product-photo",
     * )
     */
class ProductPhotoController extends Controller
{
    
    /**
     * @Route(
     *      "/upload/{id}",
     *      name="admin_product-photo_upload",
     * )
     * @Template()
     */
    public function uploadAction(Request $Request, $id)
    {    
        $session = $this->get('session');
        //list zdjęć
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductPhoto');
        $photoList = $repo->findBy(array('product' => $id));
        
        //pobieranie kategorii 
        $product = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product')->findOneById($id);
        //tworzenie obiektu i ustawienie id
        $photo = new ProductPhoto();
        if ($product) {
           $photo->setProduct($product); 
        }
        
        $form = $this->createForm( ProductPhotoType::class, $photo, array(
            'attr' => array(
                'id' => 'upload'
            )
        ));
        if ($Request->isMethod('POST')) {
            $form->handleRequest($Request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($photo);
                $em->flush();
                $res = array(
                    "status" => "success",
                    "path" => $photo->getPath().$photo->getImage(),
                    "links" => array( 
                        "delete" => $this->generateUrl('admin_product_photo_delete', array(
                            "category" =>  $id,
                            "id" => $photo->getId()
                        )),
                        "setImage" => $this->generateUrl('admin_product-photo_set', array(
                            "id" => $photo->getId()
                        ))
                                
                    )
                );
                return new JsonResponse($res);
            }else{
                $err='';
                foreach ($form->getErrors(true) as $e){
                    $err .= $e->getMessage().', ';
                }
                $res = array(
                    "status" => "error",
                    "msg" => $err
                );
                return new JsonResponse($res);
            }
        }

        return array(
            'form' => $form->createView(),
            'photoList' => $photoList,
        );        
    }
    
    /**
     * @Route(
     *      "/delete/{id}/category/{category}",
     *      name="admin_product_photo_delete",
     * )
     */
    public function deleteAction(Request $request, $id, $category)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductPhoto');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono zdjęcia');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Gratulacje, zdjęcie usunieto prawidłowo.');
        return $this->redirect($this->generateUrl('admin_product-photo_upload', array('id' => $category)));
    }
    
    
    
    /**
     * @Route(
     *      "/setimage/{id}",
     *      name="admin_product-photo_set",
     * )
     * @Template()
     */
    public function setAction(Request $Request, $id)
    {    
        $session = $this->get('session');
        //list zdjęć
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductPhoto');
        $photo = $repo->findOneById($id);
        $repo1 = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product');
        $product = $repo1->findOneById($photo->getProduct()->getId());

        $product->setImage($photo->getImage());
        $product->setPath($photo->getPath());
        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();
        
        $session->getFlashBag()->add('success', 'Gratulacje, zdjęcie wyróżniające przypisano prawidłowo.');
        return $this->redirect($this->generateUrl('admin_product-photo_upload', array('id' => $product->getId())));
    }
}