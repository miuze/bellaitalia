<?php

namespace Miuze\AdminBundle\Controller;

use Miuze\AdminBundle\Entity\ProductPhoto;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Miuze\AdminBundle\Entity\Product;
use Miuze\AdminBundle\Form\Product\ProductType;

/**
     * @Route(
     *      "/product-import",
     * )
     */
class ProductImportController extends Controller
{

    /**
     * @Route(
     *      "/satel",
     *      name="admin_product-import_satel",
     * )
     * @Template()
     */
    public function satelAction()
    {
        $uOld= 'https://www.satel.pl/pl/products#cat67';
        $page = file_get_contents('https://www.satel.pl/pl/cat/53/systemy_sygnalizacji_wlamania_i_napadu/sterowniki_radiowe/piloty');
        $paternPage = "/<div class='photo'\><a href='[\/a-zA-z1-9\s,-‑\(\)]*'/";
        preg_match_all($paternPage, $page, $contentPage);

        $p1 = "/href='(.+?)'/";

        $links = [];
        foreach ($contentPage[0] as $divs){
            preg_match($p1,$divs, $link);
            $arLink = 'https://www.satel.pl'. $link[1];
            array_push($links, $arLink);
        }
        dump($links);die;
        $em = $this->getDoctrine()->getManager();
        foreach ($links as $link) {
            $product = file_get_contents($link);
            $paternTitle = "/<h1 class='product_symbol'>(.+?)<\/h1>/";
            $paternImage = "/<a rel='type:jpg' class='multibox' href='(.+?)'/";
            $paternContent = "/<div id='leftpane'><p>(.+?)<\/p>/";
            $paternDetails = "/<div id='leftpane'><p>(.+?)<\/p>\n<ul>(\n<li>(.?)*)*\n<\/ul>/";
            $oDetails = "/<ul>(.*\s)*<\/ul>/";
            preg_match($paternTitle, $product, $title);
            $title = isset($title[1]) ? $title[1] : 'Do uzupełnienia';
            preg_match($paternImage, $product, $image);
            preg_match('/[a-zA-Z 0-9]+.jpg/', $image[1], $i);
            $img = $i[0];
            $path = str_replace($img , '' , $image[1]);
            $imageLink = 'https://www.satel.pl' . $path . $img;
            preg_match($paternContent, $product, $content);
            if (isset($content[1])) {
                $content = $content[1];
            } else {
                $content = 'Brak opisu';
            }
            preg_match($paternDetails, $product, $details);
            if (isset($details[0])) {
                preg_match($oDetails, $details[0], $ul);
                $ul = $ul[0];
            } else {
                $ul = 'Brak danych';
            }

            $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductCategory')->findOneById(41);
            $provider = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Provider')->findOneById(2);
            $vat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Vat')->findOneById(1);

            $pr = new Product();
            $pr->setProvider($provider);
            $pr->setName($title);
            $pr->setCategory($cat);
            $pr->setCode($title);
            $pr->setContent($content);
            $pr->setDetails($ul);
            $pr->setUrlTitle();
            $pr->setVat($vat);
            $em->persist($pr);
            $em->flush();

            $photo = new ProductPhoto();
            $photo->setProduct($pr);
            $photo->setLink($imageLink);
            $test = $photo->importPhoto($img);
            if ($test == true) {
                $em->persist($photo);
                $em->flush();
            }
        }
        dump('Udało się');
        die;
        return array(
            'productList' => $productList
        );        
    }

    /**
     * @Route(
     *      "/satel-photo",
     *      name="admin_product-import-photo_satel",
     * )
     * @Template()
     */
    public function satelPhotoAction()
    {
        $products = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product')->getNoPhoto();
        $em = $this->getDoctrine()->getManager();
        foreach ($products as $product){
            $photo = $product->getPhoto();
            if(isset($photo[0])){
                $product->setPath($photo[0]->getPath());
                $product->setImage($photo[0]->getImage());
                $em->persist($product);
                $em->flush();
            }
            
        }
        dump('Success');die;
        return array();
    }
    
    
    /**
     * @Route(
     *      "/hikvision/{sys}/{cat}",
     *      name="admin_product-import_hikvision",
     * )
     * @Template()
     */
    public function hikvisionAction(Request $request)
    {
        dump();die;
        $em = $this->getDoctrine()->getManager();
        $vat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Vat')->findOneById(1);
        $provider = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Provider')->findOneById(1);
        $file = __DIR__.'/../../../../web/imports/hikvision.txt';
        $domain= 'http://www.hikvision.com/pl/';
        $domainPhoto= 'http://www.hikvision.com/';
        $cat = $request->attributes->get('cat');
        $link = $domain.$cat;
        $sys = $request->attributes->getInt('sys');
        $enCategory = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductCategory')->findOneById($sys); 
        $page = file_get_contents($link);
        $paternLinkProduct = '/<dt><a\ href="([a-zA-Z\_0-9])+.html"/';
        preg_match_all($paternLinkProduct, $page, $contentLink);
        
        $paternLinkLast = '/"([a-zA-Z\_0-9])+.html/';
        $it=0;
        foreach($contentLink[0] as $prtoLink){
            preg_match_all($paternLinkLast, $prtoLink, $linkProd);
            $clearLink= str_replace('"', '', $linkProd[0]);
            
//            image
            $productLink = $domain.$clearLink[0];
            $pageProduct = file_get_contents($productLink);
            $patternPhoto = '/<div class="left"><a href="([a-z0-9\:\/\.])*/';
            preg_match_all($patternPhoto, $pageProduct, $photoLink);
            $patternHref = '/href="([a-zA-Z0-9\:\/\.])*/';
            preg_match($patternHref, $photoLink[0][1], $clearLinkPhoto);
            $clearLinkPhoto = str_replace('href="http://www.hikvision.com//', '', $clearLinkPhoto[0]);
//          image convert
            preg_match('/[a-zA-Z0-9]+.(jpg|png|jpeg|JPG|PNG|JPEG)/', $clearLinkPhoto, $i);
            if(empty($i)){
                $clearLinkPhoto.='JPG';
                preg_match('/[a-zA-Z0-9]+.(jpg|png|jpeg|JPG|PNG|JPEG)/', $clearLinkPhoto, $i);
            }
            
            $img = $i[0];
            $path = str_replace($img , '' , $clearLinkPhoto);
            $path = '/'.$path;
            
//          title            
            $patternRight = '/<div class="right">([\s])*<dl>([\s])*<dt><span>([a-zA-Z\-0-9])*/';
            preg_match($patternRight, $pageProduct, $contentRight);
            preg_match('/<dt><span>([a-zA-Z\-0-9])*/', $pageProduct, $title);
            $title = str_replace('<dt><span>', '', $title[0]);
            
//          content
            $rigPanelPattern ='/<dd>([\n\s])*<ul class=\"ul\">([^`]*?)<\/ul>/';
            preg_match($rigPanelPattern, $pageProduct, $res);
            $res = str_replace("\n", '', $res[0]);
            $res = str_replace("\t", '', $res);
            $res = str_replace("\r", '', $res);
            $res = str_replace("<dd>", '', $res);
            
//            details
            $detailsPattern = "/<div class=\"tab3_main\" id=\"([a-zA-Z0-9])*\">([^`]*?)<\/table>/";
            preg_match($detailsPattern, $pageProduct, $details);
            $details = str_replace('<div class="tab3_main" id="cs1">', '', $details[0]);
            
            $entity = new Product();
            $entity->setCode($title);
            $entity->setCategory($enCategory);
            $entity->setContent($res);
            $entity->setDetails($details);
            $entity->setName($title);
            $entity->setProvider($provider);
            $entity->setIsStock(true);
            $entity->setUrlTitle();
            $entity->setVat($vat);
            
            $em->persist($entity);
            $em->flush();
            
            $photo = new ProductPhoto();
            $photo->setProduct($entity);
            $photo->setLink($domainPhoto.$clearLinkPhoto);
            $test = $photo->importPhoto($img);
            if ($test == true) {
                $em->persist($photo);
                $em->flush();
            }
        }
        dump('Udało się');
        die;
        return array();        
    }
    
    /**
     * @Route(
     *      "/eltrox/{sys}",
     *      name="admin_product-import_eltrox",
     * )
     * @Template()
     */
    public function eltroxAction(Request $request)
    {
        die;
        $params= 'automatyka-domowa/wideodomofony/panele-zewnetrzne.html?v_100907=1074';
//        dump($request->attributes->get('cat'));die;
        $em = $this->getDoctrine()->getManager();
        $vat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Vat')->findOneById(1);
        $provider = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Provider')->findOneById(1);
        $file = __DIR__.'/../../../../web/imports/hikvision.txt';
        $domain= 'https://www.eltrox.pl/'.$params;
        $domainPhoto= 'https://www.eltrox.pl/';
        $cat = $request->attributes->get('cat');
        $link = $domain.$params;
        $sys = $request->attributes->getInt('sys');
        $enCategory = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductCategory')->findOneById($sys); 
        $page = file_get_contents($link);
        
        $paternLinkProduct = '/<h2 class="product-name">[\s\n]*<a\ href="([a-zA-Z\/\:\.\-0-9])*./';
        preg_match_all($paternLinkProduct, $page, $contentLink);
        $paternLinkLast = '/"([a-zA-Z\_0-9\/\-\:\.])*\.html/';
        $it=0;
        foreach($contentLink[0] as $prtoLink){
            preg_match_all($paternLinkLast, $prtoLink, $linkProd);
            $clearLink= str_replace('"', '', $linkProd[0]);

//            image
            $productLink = $clearLink[0];
            $pageProduct = file_get_contents($productLink);
            $patternPhoto = '/<a href="#" onclick=\"jQuery\(\'a.cloud-zoom-gallery\'\)\.eq\(0\)\.click\(\)\;return false\;\">[\n\s]+<img src=\"([^`]*?)\"/';
            preg_match($patternPhoto, $pageProduct, $photoLink);
            $clearLinkPhoto = str_replace('href="https://www.eltrox.pl', '', $photoLink[1]);
////          image convert
            preg_match('/[a-zA-Z0-9\_]+.(jpg|png|jpeg|JPG|PNG|JPEG)/', $clearLinkPhoto, $i);
            
            $img = $i[0];

//          title            
            $patternRight = '/<h2\ class\=\"product-name\"\>(\s)*[a-zA-Z\ \-\(\)\.0-9]+/';
            preg_match($patternRight, $pageProduct, $contentRight);
            preg_match('/(\ ){3}[a-zA-Z\ \-\(\)\.0-9]+/', $contentRight[0], $title);
            $title = trim($title[0]);
            

            
//          content
            $rigPanelPattern ='/<div class="desc std ul-short-description col-md-6 col-lg-6">([^`]*?)<\/div>/';
            preg_match($rigPanelPattern, $pageProduct, $res);
            $res = $res[0];
            
//            details
            $detailsPattern = "/<table class=\"data-table\" id=\"product-attribute-specs-table\"\>([^`]*?)<\/table>/";
            preg_match($detailsPattern, $pageProduct, $details);
            $details = str_replace('<div class="tab3_main" id="cs1">', '', $details[0]);
//            dump($details);die;
            
//          Price
            $pricePattern = '/<p class="l-v2-price">([^`]*?)<\/p>/' ;
            preg_match($pricePattern, $pageProduct, $price);
            if(isset($price[1])){
            $price = str_replace(' zł', '', $price[1]);
            $price = str_replace(',', '.', $price);
            $price = floatval($price);
            $nettoPrice = $price / 1.23;
            }else{
                $nettoPrice = null;
            }
            $entity = new Product();
            $entity->setCode($title);
            $entity->setCategory($enCategory);
            $entity->setContent($res);
            $entity->setDetails($details);
            $entity->setName($title);
            $entity->setProvider($provider);
            $entity->setIsStock(true);
            $entity->setUrlTitle();
            $entity->setVat($vat);
            $entity->setPrice($nettoPrice);

            $em->persist($entity);
            $em->flush();
            
            $photo = new ProductPhoto();
            $photo->setProduct($entity);
            $photo->setLink($clearLinkPhoto);
            $test = $photo->importPhoto($img);
            if ($test == true) {
                $em->persist($photo);
                $em->flush();
            }
        }
        dump('Udało się');
        die;
        return array();        
    }

}
