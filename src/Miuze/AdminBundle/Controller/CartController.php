<?php

namespace Miuze\AdminBundle\Controller;

use Miuze\AdminBundle\Entity\Payment;
use Miuze\AdminBundle\Entity\Travel;
use Miuze\AdminBundle\Form\Cart\TravelType as TrafelAdminType;
use Miuze\AdminBundle\Form\Product\ClientType;
use Miuze\UserBundle\Entity\User;
use Miuze\UserBundle\Form\Type\AddUserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Miuze\AdminBundle\Entity\Order;
use Miuze\AdminBundle\Form\Order\OrderType;
use Miuze\AdminBundle\Form\Cart\PaymentType;


/**
     * @Route(
     *      "/cart"
     * )
     */
class CartController extends Controller
{
        
    /**
     * @Route(
     *      "/list/{page}",
     *      name = "admin_cart_index",
     *      defaults={"page" = 1, "status" = 0},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction(Request $request, $page)
    {        

        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order');
        $query = $repo->getShop();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $page/*page number*/,
            30/*limit per page*/
        );

        return array(
            'pagination' => $pagination,
        );
    }
    /**
     * @Route(
     *      "/online/{page}",
     *      name = "admin_cart_online",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function onlineAction($page)
    {
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order');
        $query = $repo->getOnline();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $page/*page number*/,
            30/*limit per page*/
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * @Route(
     *      "/parners/{page}",
     *      name = "admin_cart_partners",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function partnersAction($page)
    {
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order');
        $query = $repo->getPartners();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $page/*page number*/,
            30/*limit per page*/
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * @Route(
     *      "/my/{page}",
     *      name = "admin_cart_my",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function myAction($page)
    {
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order');
        $list = $repo->findBy(array(), array('id' => 'DESC'));

        return array(
            'list' => $list,
        );
    }
    
//        Szczegóły
    /**
     * @Route(
     *      "/details/{id}",
     *      name = "admin_cart_details",
     *      defaults={"id" = 0},
     *      requirements={"id": "\d+"}
     * )
     * @Template()
     */
    public function detailsAction($id)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order');
        $item = $repo->getDetails(array('id' => $id ));
        return array(
            'item' => $item,
        );
    }

//    Zakupy stacjonarne
    /**
     * @Route(
     *      "/shop",
     *      name = "admin_cart_shop",
     * )
     * @Template()
     */
    public function shopAction(Request $request)
    {
        $session = $this->get('session');
        $entity = new Order();
        $form = $this->createForm(OrderType::class, $entity);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $entity->setSeller($this->getUser());
                $entity->setSession($session->getId());
                $price = 0;
                foreach($entity->getCarts() as $item){
                    $price += $item->getQuantity() * $item->getProduct()->calcVat($entity->getBuyer());
                }
                $price += $entity->getTravel()->getPrice();
                $entity->setPrice($price);
                $em = $this->getDoctrine()->getManager();
                $res = $em->merge($entity);
                $em->flush();

                $session->getFlashBag()->add('success', 'Gratulacje, Zgłoszenie dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_cart_details', array(
                    'id' => $res->getId()
                )));
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route(
     *      "/order-delete/{id}",
     *      name = "admin_cart_order-delete",
     *      defaults={"id" = 0},
     *      requirements={"id": "\d+"}
     * )
     */
    public function orderDeleteAction(Request $request)
    {
        $session = $this->get('session');
        $id = $request->attributes->getInt('id');
        $entity = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order')->findOneById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $session->getFlashBag()->add('success', 'Gratulacje, Zgłoszenie zostało usunięte prawidłowo');
        return $this->redirect($this->generateUrl('admin_cart_index'));
    }

    /**
     * @Route(
     *      "/buyer-add",
     *      name = "admin_cart_buyer-add",
     * )
     * @Template()
     */
    public function buyerAddAction(Request $request)
    {
        $session = $this->get('session');
        $entity = new User();
        $form = $this->createForm(AddUserType::class, $entity, array(
            'attr' => array(
                'id' => 'form-add-buyer',
            ),
            'action' => $this->generateUrl('admin_cart_buyer-add')
        ));
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $userManager = $this->get('user_manager');
                $userManager->addUser($entity);
                $res = array(
                    'status' => 'success',
                    'id' => $entity->getId(),
                    'name' =>$entity->getFirstName(). ' '. $entity->getLastName(). ' ' .$entity->getEmail(),
                    'msg' =>'Gratulacje, Osoba dodana prawidłowo'
                );
                return new JsonResponse($res);
            }else{
                $err='';
                foreach ($form->getErrors() as $e){
                    $err .= $e->getMessage().', ';
                }
                $msg = 'Błędy: '.$err;
            }
        }
        $res = $this->renderView('MiuzeAdminBundle:Cart:buyer.html.twig', array(
            'form' => $form->createView(),
            'msg' => isset($msg) ? $msg : null
        ));
//        dump($res);die;
        return new JsonResponse($res);
    }


//    Statusy
    /**
     * @Route(
     *      "/ready/{id}/{status}",
     *      name = "admin_cart_ready",
     *      defaults={"id" = 0},
     *      requirements={"id": "\d+"}
     * )
     */
    public function readyAction(Request $request)
    {
        $session = $this->get('session');
        $status = $request->attributes->getInt('status');
        $id = $request->attributes->getInt('id');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order');
        $item = $repo->findOneById($id);
        $item->setStatus($status);
        $em = $this->getDoctrine()->getManager();
        $em->persist($item);
        $em->flush();
        if($status == true){
            $session->getFlashBag()->add('warning', 'Zamówienie gotowe do wysyłki');
        }else{
            $session->getFlashBag()->add('warning', 'Zamówienie w czasie kompletowania');
        }
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route(
     *      "/closed/{id}/{status}",
     *      name = "admin_cart_close",
     *      defaults={"id" = 0},
     *      requirements={"id": "\d+"}
     * )
     */
    public function closeAction(Request $request)
    {
        $session = $this->get('session');
        $status = $request->attributes->getInt('status');
        $id = $request->attributes->getInt('id');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order');
        $item = $repo->findOneById($id);
        if($item->getStatus() == 0){
            $session->getFlashBag()->add('warning', 'Zmień status na gotowe do wysyłki i ponów zakończenie');
        }else {
            $item->setClose($status);
            $em = $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();
            if($status == true){
                $session->getFlashBag()->add('warning', 'Zamówienie zakończone');
            }else{
                $session->getFlashBag()->add('warning', 'Zamówienie w realizacji');
            }
        }
        return $this->redirect($request->headers->get('referer'));
    }


//    Wysyłka
    /**
     * @Route(
     *      "/travel-list/{id}",
     *      name = "admin_cart_travel-list",
     *      defaults={"id" = 0},
     *      requirements={"id": "\d+"}
     * )
     * @Template()
     */
    public function travelListAction(Request $request, $id)
    {        
        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Travel');
        $list = $repo->findAll();
        
        if($id != 0){
            $travel = $repo->findOneById($id);
        }else{
            $travel = new Travel();
        }
        
        $form = $this->createForm( TrafelAdminType::class , $travel);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($travel);
                $em->flush();
                return $this->redirect($this->generateUrl('admin_cart_travel-list'));
            }
        }
        
        return array(
            'form' => $form->createView(),
            'list' => $list,
        );
    }
    
    
    /**
     * @Route(
     *      "/travel-delete/{id}",
     *      name = "admin_cart_travel-delete",
     *      defaults={"id" = 0},
     *      requirements={"id": "\d+"}
     * )
     */
    public function deleteTravelAction($id)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Travel');
        $item = $repo->findOneBy(array('id' => $id ));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_cart_travel-list'));
    }

    /**
     * @Route(
     *      "/payment-list/{id}",
     *      name = "admin_cart_payment-list",
     *      defaults={"id" = 0},
     *      requirements={"id": "\d+"}
     * )
     * @Template()
     */
    public function paymentListAction(Request $request)
    {

        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Payment');
        $list = $repo->findAll();
        $id = $request->attributes->getInt('id');
        if($id != 0){
            $payment = $repo->findOneById($id);
        }else{
            $payment = new Payment();
        }

        $form = $this->createForm( PaymentType::class , $payment);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($payment);
                $em->flush();
                return $this->redirect($this->generateUrl('admin_cart_payment-list'));
            }
        }

        return array(
            'form' => $form->createView(),
            'list' => $list,
        );
    }


    /**
     * @Route(
     *      "/payment-delete/{id}",
     *      name = "admin_cart_payment-delete",
     *      defaults={"id" = 0},
     *      requirements={"id": "\d+"}
     * )
     */
    public function deletePaymentAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Payment');
        $item = $repo->findOneBy(array('id' => $id ));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        return $this->redirect($this->generateUrl('admin_cart_payment-list'));
    }
}
