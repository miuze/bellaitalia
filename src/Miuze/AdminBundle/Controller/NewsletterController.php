<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Miuze\AdminBundle\Entity\Newsletter;
use Miuze\AdminBundle\Form\Newsletter\NewsletterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Miuze\AdminBundle\Mailer\NewsletterMailer;
use Miuze\AdminBundle\Entity\NewsletterTemplate;
use Miuze\AdminBundle\Form\Newsletter\NewsletterTemplateType;
use Miuze\AdminBundle\Form\Newsletter\NewsletterEditType;

/**
     * @Route(
     *      "/newsletter"
     * )
     */
class NewsletterController extends Controller
{
    protected $miuzeMailer;

    /**
     * @Route(
     *      "/",
     *      name = "admin_newsletter_index"
     * )
     * @Template()
     */
    public function indexAction()
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Newsletter');
        $subscribers = $repo->findAll();
        return array(
            'subscribers' => $subscribers,
        );
    }
    
    /**
     * @Route(
     *      "/newslettercategorylist/{categoryId}/",
     *      name="admin_newsletter_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     * )
     * @Template("MiuzeAdminBundle:Newsletter:index.html.twig")
     */
    public function newsletterListCategoryAction($categoryId, $page)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Newsletter');
            $subscribers = $repo->getNewsletter(array('cat' => $categoryId ));
            return array(
                'subscribers' => $subscribers,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    /**
     * @Route(
     *      "/add",
     *      name = "admin_newsletter_add"
     * )
     * @Template()
     */
    public function addAction(Request $request)
    {
        $session = $this->get('session');
        $entity = new Newsletter();
        $form = $this->createForm( NewsletterEditType::class, $entity);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                return $this->redirect($this->generateUrl('admin_newsletter_index'));

            }else{
                $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
            }
        }
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_newsletter_edit"
     * )
     * @Template("MiuzeAdminBundle:Newsletter:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Newsletter');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }        
                
        $form = $this->createForm( NewsletterEditType::class, $entity); 
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);            
            if($form->isValid()){ 
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                return $this->redirect($this->generateUrl('admin_newsletter_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/create/{send}",
     *      name = "admin_newsletter_create",
     *      defaults = {"send" = 0}
     * )
     * @Template()
     */
    public function createAction(Request $request, $send)
    {
        $session = $this->get('session');
        $newsletter = new NewsletterTemplate();    
        
        $form = $this->createForm( NewsletterTemplateType::class, $newsletter); 
        if($request->isMethod('POST')){
            $form->handleRequest($request);            
            if($form->isValid()){
                $promarray = array();
                $it = 0;
                // subscriber category
                $users = array();
                foreach($newsletter->getUserCategory() as $cat){
                    foreach($cat->getNewsletter() as $item){    
                        array_push($users, $item);
                    }
                }
                // news list
                foreach ($newsletter->getNews() as $key => $promo){
                    if($key %2 == 0){
                        $promarray[$it][$key] = $promo;
                    }else{
                        $promarray[$it][$key] = $promo;   
                        $it++; 
                    }
                }
                
                $htmlBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:newsletter.html.twig', array(
                    'promotions' => $promarray,
                    'title' => $newsletter->getTitle(),
                    'content' => $newsletter->getContent()
                ));
                $session->set('paramsemail', array(
                    'promotions' => $promarray, 
                    'title' => $newsletter->getTitle(),
                    'content' => $newsletter->getContent(),
                    'users' => $users
                ));
                
                return array(
                    'show' => $htmlBody
                );
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
            }
        }
        if($send != 0){
            $param = $session->get('paramsemail');
            $emailList = $param['users'];
            $mailer = $this->get('admin_newsletter_sender');
            
            foreach($emailList as $email){
                $link = $this->container->getParameter('domain').$this->generateUrl('admin_subscriber_deactive').'?token='.$email->getActionToken();
                $htmlBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:newsletter.html.twig', array(
                    'promotions' => $param['promotions'],
                    'title' => $param['title'],
                    'content' => $param['content'],
                    'link' => $link
                ));
                
                try{
                    $mailer->send($email, $param['title'], $htmlBody);                    
                } catch (Exception $e){
                    $session->getFlashBag()->add('warning', 'Błąd , mailing nie został dodany do kolejki!');
                    return $this->redirect($this->generateUrl('admin_newsletter_index'));
                }
                $session->getFlashBag()->add('success', 'Gratulacje, mailing został dodany do kolejki prawidłowo!');
                return $this->redirect($this->generateUrl('admin_newsletter_index'));
            }
        }
        return array(
            'form' => $form->createView(),
        );
    }
    
    
    
    /**
     * @Route(
     *      "/active",
     *      name="admin_newsletter_active"
     * )
     */
    public function activeAction(Request $request)
    {
        $session = $this->get('session');
        $token = $request->get(0);
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Newsletter');
        $subscriber = $repo->findOneBy(array('actionToken' => $token));
        if(NULL == $subscriber ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $subscriber->setActive(1);
        $em->persist($subscriber);
        $em->flush();
        $session->getFlashBag()->add('success', 'Subskrybent został aktywowany');
        return $this->redirect($this->generateUrl('admin_newsletter_index'));
    }    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_newsletter_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Newsletter');
        $entity = $repo->findOneBy(array('id' => $id));
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $session->getFlashBag()->add('success', 'Subskrybent został usunięty!');
        return $this->redirect($this->generateUrl('admin_newsletter_index'));
    }
    
    
    /**
     * @Route(
     *      "/export",
     *      name = "admin_newsletter_export"
     * )
     * @Template()
     */
    public function exportAction(Request $request)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Newsletter');
        $subscribers = $repo->findBy(array('active' => 1));
        $import = array();
        $email = array();
        
        foreach($subscribers as $subscriber){
            array_push($email, $subscriber->getEmail());
        }
        array_push($import, $email);
        $path = $request->server->get('DOCUMENT_ROOT').'/csv/';
        $importfile = fopen($path.'newsletter.csv', 'w');
        foreach ($import as $fields) {
            fputcsv($importfile, $fields);
        }
        fclose($importfile);
        
        foreach(scandir($path) as $file){
            if($file != '.' && $file != '..'){
                $name = strstr($file, '.', true);
                $views[$name] = $file;
            }
        }
        return array(
            'files' => $views,
        );
    }
    
    /**
     * @Route(
     *      "/import",
     *      name = "admin_newsletter_import"
     * )
     */
    public function importAction(Request $Request){
        
        $session = $this->get('session');
        
        $path = $this->get('kernel')->getRootDir() . '/../web/csv/';
        $importFile = fopen ($path."data.csv","r");
        
        if($importFile == FALSE){
            $error['file'] = 'Błąd pliku';
        }
        while (($data = fgetcsv($importFile, 1000, ";", "!")) !== FALSE)  {
            $email = $data[0];
            $newsletter = new Newsletter(); 
            $subscriberexist = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Newsletter')->findBy(array('email' => $email));
            if($subscriberexist != null){
//                $session->getFlashBag()->add('warning', 'Błąd , subskrybent z takim adresem już istnieje!'.$subscriberexist->getEmail());
//                return $this->redirect($this->generateUrl('admin_newsletter_index'));
            }else{
                $newsletter->setEmail($email);
                $newsletter->setActive(1);
                $newsletter->setActionToken($this->generateActionTken());
                $em = $this->getDoctrine()->getManager();
                $em->persist($newsletter);
                $em->flush();
            }
        }
        
        $session->getFlashBag()->add('success', 'Import ok');
        return $this->redirect($this->generateUrl('admin_newsletter_index'));
        
    }
    
    
    
    public function generateActionTken(){
        return substr(md5(uniqid(NULL, TRUE)), 0, 20);
    }
}
