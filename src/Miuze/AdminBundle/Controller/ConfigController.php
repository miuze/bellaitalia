<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;
use Miuze\AdminBundle\Entity\Paramiter;
use Miuze\AdminBundle\Form\Paramiter\ParamiterType;

/**
 * Class ConfigController
 * @package Miuze\CmsBundle\Controller
 * @Route("/yml")
 */
class ConfigController extends Controller{

    /**
     * @Route(
     *     "/",
     *      name="admin_yml_index"
     * )
     * @Template("@MiuzeAdmin/Config/index.html.twig")
     */
    public function indexAction(Request $request){

        $file = $this->get('kernel')->getRootDir() . '/../app/config/user_parameters.yml';
        $values = Yaml::parseFile($file);
        $arrParams = [];
        foreach($values['parameters'] as $key => $value){
            $param = new Paramiter();
            $param->setName($key);
            $param->setValue($value);
            array_push($arrParams, $param);
        }

        return array(
            'list' => $arrParams
        );
    }

    /**
     * @Route(
     *     "/add",
     *      name="admin_yml_add"
     * )
     * @Template("@MiuzeAdmin/Config/add.html.twig")
     */
    public function addAction(Request $request){

        $session = $this->get('session');

        $param = new Paramiter();

        $form = $this->createForm(ParamiterType::class, $param);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $file = $this->get('kernel')->getRootDir() . '/../app/config/user_parameters.yml';
                $values = Yaml::parseFile($file);
                $values['parameters'][$param->getName()] = $param->getValue();
                $yaml = Yaml::dump($values);
                file_put_contents($file, $yaml);

                $session->getFlashBag()->add('success', 'Parametr dodany prawidłowo');
                return $this->redirect($this->generateUrl('admin_yml_index'));
            }else{
                $session->getFlashBag()->add('danger', 'Sprawdź formularz');
            }
        }

        return array(
            'form' => $form->createView()
        );
    }


    /**
     * @Route(
     *     "/edit-{param_name}",
     *      name="admin_yml_edit"
     * )
     * @Template("@MiuzeAdmin/Config/edit.html.twig")
     */
    public function editAction(Request $request){

        $session = $this->get('session');
        $paramName = $request->attributes->get('param_name');
        $file = $this->get('kernel')->getRootDir() . '/../app/config/user_parameters.yml';
        $values = Yaml::parseFile($file);
        $arrParams = [];
        foreach($values['parameters'] as $key => $value){
            if($paramName == $key){
                $param = new Paramiter();
                $param->setName($key);
                $param->setValue($value);
            }
        }
        if(!isset($param)){
            $session->getFlashBag()->add('danger', 'Nie znaleziono parametru');
            return $this->redirect($this->generateUrl('admin_yml_index'));
        }
        $form = $this->createForm(ParamiterType::class, $param, array('read' => true));
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $values['parameters'][$param->getName()] = $param->getValue();
                $yaml = Yaml::dump($values);
                file_put_contents($file, $yaml);

                $session->getFlashBag()->add('success', 'Parametr zapisany prawidłowo');
                return $this->redirect($this->generateUrl('admin_yml_index'));
            }else{
                $session->getFlashBag()->add('danger', 'Sprawdź formularz');
            }
        }

        return array(
            'form' => $form->createView()
        );
    }


}
