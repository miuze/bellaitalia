<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Rabate;
use Miuze\AdminBundle\Entity\Registry;
use Miuze\AdminBundle\Form\Rabate\RabateType;


/**
     * @Route(
     *      "/rabate"
     * )
     */
class RabateController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_rabate_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $page = $request->attributes->getInt('page');

        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Rabate');
        $query = $repo->getRabate();

        $paginator  = $this->get('knp_paginator');
        $list = $paginator->paginate(
            $query, /* query NOT result */
            $page/*page number*/,
            20/*limit per page*/
        );
        
        return array(
            'list' => $list,
        );
    }

    /**
     * @Route(
     *      "/add",
     *      name = "admin_rabate_add",
     * )
     * @Template()
     */
    public function addAction(Request $request)
    {
        $session = $this->get('session');
        $entity = new Rabate();    

        $form = $this->createForm(RabateType::class, $entity);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){      
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, Rabat dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_rabate_index'));
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_rabate_edit"
     * )
     * @Template("MiuzeAdminBundle:Rabate:add.html.twig")
     */
    public function editAction(Request $request)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Rabate');
        $entity = $repo->find($request->attributes->getInt('id'));
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( RabateType::class, $entity);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){  
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, Rabat zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_rabate_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_rabate_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Rabate');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Rabat został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_rabate_index'));
    }
}
