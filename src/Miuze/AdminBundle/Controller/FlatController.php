<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Miuze\AdminBundle\Entity\Flat;
use Miuze\AdminBundle\Form\Flat\FlatType;

/**
     * @Route(
     *      "/flat",
     * )
     */
class FlatController extends Controller
{

    /**
     * @Route(
     *      "/{page}",
     *      name="admin_flat_index",
     *     defaults={"page" = 1},
     *     requirements={"page": "\d+"}
     * )
     * @Template("@MiuzeAdmin/Flat/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $page = $request->attributes->getInt('page');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Flat');
        $query = $repo->getAdminFlats(array(
            'lang' => $request->getLocale()
        ));
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page ,
            230
        );
        
        
        return array(
            'pagination' => $pagination
        );        
    }
    
    /**
     * @Route(
     *      "/flat/page-{page}/id-{id}",
     *      name="admin_flat_flat",
     *      defaults = {"id" = 1,"page" = 1},
     *     requirements={"page": "\d+", "id": "\d+"}
     * )
     * @Template("@MiuzeAdmin/Flat/index.html.twig")
     */
    public function flatAction(Request $request)
    {
        $page = $request->attributes->getInt('page', 1);
        $cat = $request->attributes->getInt('id');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Flat');
        $query = $repo->getAdminFlats(array(
            'lang' => $request->getLocale(),
            'build' => $cat
        ));
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page ,
            30
        );
        
        return array(
            'pagination' => $pagination
        );        
    }
    
    
    
    
    /**
     * @Route(
     *      "/add/{id}",
     *      name="admin_flat_add",
     *      defaults = {"id" = 0}
     * )
     * @Template("@MiuzeAdmin/Flat/add.html.twig")
     */
    public function addAction(Request $request, $id)
    {           
        $session = $this->get('session');
        $flat = new Flat;
        
        if($id == 0){
            $session->getFlashBag()->add('warning', 'Brak parametru budynku');
            return $this->redirect($this->generateUrl('admin_build_index'));
        }else{
            $build = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Build')->findOneBy(array('id' => $id));
            $flat->setBuild($build);
        }
        
        $form = $this->createForm( FlatType::class, $flat);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $functions = $this->get('utils_service');
                $flat->setUrlTitle($functions->slugify($flat->getName()));
                $em->persist($flat);
                $em->flush();
                $session->getFlashBag()->add('success', 'Galeria dodana prawidłowo');
                if($id == 0){
                    return $this->redirect($this->generateUrl('admin_flat_index'));
                }else{                
                    return $this->redirect($this->generateUrl('admin_flat_flat', array('id' => $build->getId())));
                }
            }else{
                $session->getFlashBag()->add('Danger', 'Wystapił błąd w formularzu.');
            }
        }

        return array(
            'form' => $form->createView(),
        );        
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_flat_edit"
     * )
     * @Template("@MiuzeAdmin/Flat/add.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Flat');
        $entity = $repo->findOneBy(array('id' =>$id));

        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $flatForm = $this->createForm(FlatType::class, $entity);        
        if($request->isMethod('POST')){
            $flatForm->handleRequest($request);
            if($flatForm->isValid()){    
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getName()));
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_flat_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił błąd w formularzu.');
            }
        }        
        return array(
            'form' => $flatForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_flat_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction(Request $request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Flat');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Galeria usunieta prawidłowo');
        return $this->redirect($this->generateUrl('admin_flat_index'));
    }
    
    
    
}
