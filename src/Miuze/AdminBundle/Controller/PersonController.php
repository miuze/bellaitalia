<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Miuze\AdminBundle\Entity\Person;
use Miuze\AdminBundle\Form\Person\PersonType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
/**
     * @Route(
     *      "/person"
     * )
     */
class PersonController extends Controller
{
    
    /**
     * @Route(
     *      "/",
     *      name = "admin_person_index"
     * )
     * @Template("@MiuzeAdmin/Person/index.html.twig")
     */
    public function indexAction()
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Person');
        $personList = $repo->findAll();        
        return array(
            'personList' => $personList,
        );
    }
    
    /**
     * @Route(
     *      "/personcategorylist/{categoryId}/{page}",
     *      name="admin_person_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     * )
     * @Template("@MiuzeAdmin/Person/index.html.twig")
     */
    public function personListCategoryAction(Request $request)
    {
        $categoryId = $request->attributes->getInt('categoryId');
        $page = $request->attributes->getInt('page_number');
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Person');
            $personList = $repo->findBy(array('category' => $categoryId ));
            $paginator  = $this->get('knp_paginator');            
            $pagination = $paginator->paginate($personList, $page ,15);
            return array(
                'personList' => $personList,
                'paginator' => $pagination,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    
    /**
     * @Route(
     *      "/addperson/{categoryId}",
     *      name = "admin_person_add",
     *      defaults = {"categoryId" = 0}
     * )
     * @Template("@MiuzeAdmin/Person/add.html.twig")
     */
    public function addAction(Request $request)
    {
        $categoryId = $request->attributes->getInt('categoryId');
        $session = $this->get('session');
        $entity = new Person();
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PersonCategory')->findOneBy(array('id' => $categoryId));
        if ($cat) {
            $entity->setCategory($cat);
        }

        $form = $this->createForm( PersonType::class, $entity);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                //dump($pageForm); die;
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, osoba dadana prawidłowo!');
                return $this->redirect($this->generateUrl('admin_person_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
            }
        }        
        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_person_edit"
     * )
     * @Template("@MiuzeAdmin/Person/add.html.twig")
     */
    public function editAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Person');
        $entity = $repo->findOneBy(array('id' => $id));
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        
        $form = $this->createForm( PersonType::class, $entity);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();                
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, osoba została zapisana prawidłowo!');
                return $this->redirect($this->generateUrl('admin_person_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Coś poszło nie tak, sprawdź formularz!');
            }
        }        
        return [
            'entity' => $entity,
            'form' => $form->createView()
        ];
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_person_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Person');
        $row = $repo->findOneBy(array('id' => $id));
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Osoba została usunieta!');
        return $this->redirect($this->generateUrl('admin_person_index'));
    }
    
}
