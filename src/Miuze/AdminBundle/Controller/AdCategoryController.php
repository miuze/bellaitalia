<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Ad;
use Miuze\AdminBundle\Entity\AdCategory;
use Miuze\AdminBundle\Form\Ad\AdCategoryType;
use Miuze\AdminBundle\Form\Ad\SectionType;
/**
     * @Route(
     *      "/ad-category"
     * )
     */
class AdCategoryController extends Controller
{
           
    /**
     * @Route(
     *      "/",
     *      name = "admin_ad-category_index"
     * )
     * @Template("MiuzeAdmin/AdCategory/index.html.twig")
     */
    public function indexAction(Request $Request)
    {        
        $lang = $Request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:AdCategory');
        $list = $repo->getAllCategories(array('lang' => $lang));

        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_ad-category_add"
     * )
     * @Template("MiuzeAdmin/AdCategory/add.html.twig")
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $category = new AdCategory();   
                
        $form = $this->createForm( AdCategoryType::class, $category); 
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);            
            if($form->isValid()){     
                $lang = $Request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_ad-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_ad-category_edit"
     * )
     * @Template("MiuzeAdmin/AdCategory/add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:AdCategory');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( AdCategoryType::class, $entity);        
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);
            if($form->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, category zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_ad-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_ad-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:AdCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategorie usunieto prawidłowo');
        return $this->redirect($this->generateUrl('admin_ad-category_index'));
    }
    
}
