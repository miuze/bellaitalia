<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Registry;
use Miuze\AdminBundle\Entity\Download;
use Miuze\AdminBundle\Entity\DownloadCategory;
use Miuze\AdminBundle\Entity\DownloadFile;
use Miuze\AdminBundle\Form\Download\DownloadCategoryType;
use Miuze\AdminBundle\Form\Download\DownloadType;
use Miuze\AdminBundle\Form\Download\DownloadFileType;
/**
     * @Route(
     *      "/download-file"
     * )
     */
class DownloadFileController extends Controller
{
//        
//    /**
//     * @Route(
//     *      "/{download}/{page}",
//     *      name = "admin_download-file_index",
//     *      defaults={"page" = 1, "download" = 0},
//     *      requirements={"page": "\d+", "download": "\d+"}
//     * )
//     * @Template()
//     */
//    public function indexAction($page, $download)
//    {        
//        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadFile');
//        $list = $repo->findBy(array('download' => $download));
//        return array(
//            'list' => $list,
//        );
//    }
//    
//    
//    /**
//     * @Route(
//     *      "/add/{download}",
//     *      name = "admin_download-file_add",
//     *      defaults={"download" = 0}
//     * )
//     * @Template()
//     */
//    public function addAction(Request $Request, $download)
//    {
//        
//        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadFile');
//        $list = $repo->getFilesDownload(array('download' => $download));
//        $down = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Download')->findOneBy(array('id' => $download));
//        
//        $session = $this->get('session');
//        $entity = new DownloadFile();    
//        $entity->setDownload(array($down));
//        $form = $this->createForm(DownloadFileType::class, $entity); 
//        if($Request->isMethod('POST')){
//            $form->handleRequest($Request);            
//            if($form->isValid()){      
//                $em = $this->getDoctrine()->getManager();
//                $em->persist($entity);
//                $em->flush();
//                
//                // dodanie do rejestru
//                $em = $this->getDoctrine()->getManager();
//                $registry = new Registry();                              
//                $user = $this->get('security.token_storage')->getToken()->getUser();
//                $registry->setUser($user);
//                $registry->setType('Dodano plik.');
//                $entity->setFile(NULL);
//                $registry->setDownload($entity->getDownload()[0]);
//                $em->persist($registry);
//                $em->flush();
//                
//                $session->getFlashBag()->add('success', 'Gratulacje, Plik dodano prawidłowo');
//                return $this->redirect($this->generateUrl('admin_download-file_add', array('download' => $download)));                
//            }else{
//                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
//            }
//        }        
//        return array(
//            'list' => $list,
//            'form' => $form->createView(),
//        );
//    }
//    
//    
//    /**
//     * @Route(
//     *      "/delete/{download}/{id}",
//     *      name="admin_download-file_delete",
//     *      defaults = {"id" = 0}
//     * )
//     */
//    public function deleteAction($id, $download)
//    {
//        $session = $this->get('session');
//        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadFile');
//        $row = $repo->find($id);
//        if(NULL == $row ){
//            throw $this->createNotFoundException('Nie znaleziono');
//        }
//        $em = $this->getDoctrine()->getManager();
//        $em->remove($row);
//        $em->flush();
//        $session->getFlashBag()->add('success', 'Plik został usuniety prawidłowo.');
//        return $this->redirect($this->generateUrl('admin_download-file_add', array('download' => $download))); 
//    }
    
    /**
     * @Route(
     *      "/{categoryId}/{page}",
     *      name = "admin_download-file_index",
     *      defaults={"page" = 1, "categoryId" = 0},
     *      requirements={"page": "\d+", "categoryId": "\d+"}
     * )
     * @Template()
     */
    public function indexAction($page, $categoryId)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadFile');
        $list = $repo->findBy(array('categoryFile' => $categoryId));
        return array(
            'list' => $list,
        );
    }
    
    
    /**
     * @Route(
     *      "/add/{categoryId}",
     *      name = "admin_download-file_add",
     *      defaults={"categoryId" = 0}
     * )
     * @Template()
     */
    public function addAction(Request $Request, $categoryId)
    {
        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadFile');
        $list = $repo->findBy(array('categoryFile' => $categoryId));
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadCategory')->findOneBy(array('id' => $categoryId));
        
        $session = $this->get('session');
        $entity = new DownloadFile();    
        $entity->setCategoryFile($cat);
        $form = $this->createForm(DownloadFileType::class, $entity); 
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);            
            if($form->isValid()){  
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, Plik dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_download-file_add', array('categoryId' => $categoryId)));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'list' => $list,
            'form' => $form->createView(),
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{categoryId}/{id}",
     *      name="admin_download-file_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id, $categoryId)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadFile');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Plik został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_download-file_add', array('categoryId' => $categoryId))); 
    }
}
