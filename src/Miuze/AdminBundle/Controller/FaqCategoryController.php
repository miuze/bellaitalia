<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Faq;
use Miuze\AdminBundle\Entity\FaqCategory;
use Miuze\AdminBundle\Form\Faq\FaqCategoryType;
use Miuze\AdminBundle\Form\Faq\FaqType;
/**
     * @Route(
     *      "/faq-category"
     * )
     */
class FaqCategoryController extends Controller
{
           
    /**
     * @Route(
     *      "/",
     *      name = "admin_faq-category_index"
     * )
     * @Template("@MiuzeAdmin/FaqCategory/index.html.twig")
     */
    public function indexAction(Request $Request)
    {        
        $lang = $Request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:FaqCategory');
        $list = $repo->getAllCategories(array('lang' => $lang));
        
        
        $session = $this->get('session');
        $category = new FaqCategory();   
                
        $categoryForm = $this->createForm( FaqCategoryType::class, $category); 
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);            
            if($categoryForm->isValid()){          
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_faq-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        
        return array(
            'list' => $list,
            'form' => $categoryForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_faq-category_add"
     * )
     * @Template("@MiuzeAdmin/FaqCategory/add.html.twig")
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $category = new FaqCategory();   
                
        $categoryForm = $this->createForm( FaqCategoryType::class, $category); 
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);            
            if($categoryForm->isValid()){   
                $lang = $Request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_faq-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_faq-category_edit"
     * )
     * @Template("@MiuzeAdmin/FaqCategory/add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:FaqCategory');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $categoryForm = $this->createForm( FaqCategoryType::class, $entity);        
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);
            if($categoryForm->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_faq-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_faq-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:FaqCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategorie usunieto prawidłowo');
        return $this->redirect($this->generateUrl('admin_faq-category_index'));
    }
    
}
