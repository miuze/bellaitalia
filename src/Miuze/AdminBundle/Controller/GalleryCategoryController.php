<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\GalleryCategory;
use Miuze\AdminBundle\Form\Gallery\GalleryCategoryType;

/**
     * @Route(
     *      "/gallery-category",
     * )
     */
class GalleryCategoryController extends Controller
{
//    --------------------------------------------------------------------------Category-------------------------   
    /**
     * @Route(
     *      "/",
     *      name="admin_gallery-category_index",
     * )
     * @Template("@MiuzeAdmin/GalleryCategory/index.html.twig")
     */
    public function indexAction(Request $request)
    {   
        $lang = $request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:GalleryCategory');
        $galleryList = $repo->getAllCategories(array('lang' => $lang));
        return array(
            'categoryList' => $galleryList
        );        
    }
    
    
    
    
    /**
     * @Route(
     *      "/add",
     *      name="admin_gallery-category_add",
     * )
     * @Template("@MiuzeAdmin/GalleryCategory/add.html.twig")
     */
    public function addAction(Request $request)
    {   
        $session = $this->get('session');
        $category = new GalleryCategory();
        $form = $this->createForm( GalleryCategoryType::class, $category);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $lang = $request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Kategoria dodana prawidłowo');
                return $this->redirect($this->generateUrl('admin_gallery-category_index'));
            }else{
                $session->getFlashBag()->add('Danger', 'Wystapił błąd w formularzu.');
            }
        }

        return array(
            'form' => $form->createView(),
        );        
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_gallery-category_edit"
     * )
     * @Template("@MiuzeAdmin/GalleryCategory/add.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:GalleryCategory');
        $entity = $repo->findOneBy(array('id' =>$id));
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $galleryForm = $this->createForm(GalleryCategoryType::class, $entity);        
        if($request->isMethod('POST')){
            $galleryForm->handleRequest($request);
            if($galleryForm->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_gallery-category_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił błąd w formularzu.');
            }
        }        
        return array(
            'form' => $galleryForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_gallery-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:GalleryCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategoria usunieta prawidłowo');
        return $this->redirect($this->generateUrl('admin_gallery-category_index'));
    }

}
