<?php

namespace Miuze\AdminBundle\Controller;

use Miuze\AdminBundle\Entity\Order;
use Miuze\AdminBundle\Form\Order\OrderType;
use Miuze\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Ad;
use Miuze\AdminBundle\Entity\AdCategory;
use Miuze\AdminBundle\Entity\Registry;
use Miuze\AdminBundle\Form\Ad\RabateType;
use Miuze\AdminBundle\Form\Ad\ProviderType;


/**
     * @Route(
     *      "/order"
     * )
     */
class OrderController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_order_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction(Request $request)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order');
        $list = $repo->findBy(array(
                'seller' => $this->getUser()->getId()
            ), array(
                'createDate' => 'DESC'
            )
        );
        
        return array(
            'list' => $list,
        );
    }

    /**
     * @Route(
     *      "/user-{userId},page-{page}",
     *      name = "admin_order-user_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+", "userId": "\d+"}
     * )
     * @Template()
     */
    public function userAction(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Order');
        $list = $repo->findBy(array(), array('createDate' => 'DESC'));

        return array(
            'list' => $list,
        );
    }
    

    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_order_add",
     * )
     * @Template()
     */
    public function addAction(Request $request)
    {
        $session = $this->get('session');
        $entity = new Order();
        $form = $this->createForm(OrderType::class, $entity);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $entity->setSeller($this->getUser());
                $entity->setSession($session->getId());
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, Ogłoszenie dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_order_index'));
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }
    

}
