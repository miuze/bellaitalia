<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Realization;
use Miuze\AdminBundle\Entity\RealizationCategory;
use Miuze\AdminBundle\Entity\RealizationPhoto;
use Miuze\AdminBundle\Form\Realization\RealizationCategoryType;
use Miuze\AdminBundle\Form\Realization\RealizationType;
use Miuze\AdminBundle\Form\Realization\RealizationPhotoType;
/**
     * @Route(
     *      "/realization"
     * )
     */
class RealizationController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_realization_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction($page)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Realization');
        $newsList = $repo->findAll();
        $paginator  = $this->get('knp_paginator');            
        $pagination = $paginator->paginate($newsList, $page ,15);
        return array(
            'newsList' => $newsList,
            'paginator' => $pagination,
        );
    }
    
    /**
     * @Route(
     *      "/newscategorylist/{categoryId}/{page}",
     *      name="admin_realization_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     * )
     * @Template("MiuzeAdminBundle:Realization:index.html.twig")
     */
    public function realizationListCategoryAction($categoryId, $page)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Realization');
            $newsList = $repo->findBy(array('category' => $categoryId ));
            $paginator  = $this->get('knp_paginator');            
            $pagination = $paginator->paginate($newsList, $page ,15);
            return array(
                'newsList' => $newsList,
                'paginator' => $pagination,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add/{categoryId}",
     *      name = "admin_realization_add",
     *      defaults = {"categoryId" = 0}
     * )
     * @Template()
     */
    public function addAction(Request $Request, $categoryId)
    {
        $session = $this->get('session');
        $news = new Realization();    
                
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:RealizationCategory')->findOneBy(array('id' => $categoryId));
        if ($cat) {
           $news->setCategory($cat); 
        }
        
        $newsForm = $this->createForm(RealizationType::class, $news); 
        if($Request->isMethod('POST')){
            $newsForm->handleRequest($Request);            
            if($newsForm->isValid()){      
                
                $functions = $this->get('utils_service');
                $news->setSeoTitle($functions->slugify($news->getTitle()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($news);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, Realizacja dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_realization_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $newsForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_realization_edit"
     * )
     * @Template("MiuzeAdminBundle:Realization:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Realization');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $newsForm = $this->createForm( RealizationType::class, $entity);        
        if($Request->isMethod('POST')){
            $newsForm->handleRequest($Request);
            if($newsForm->isValid()){  
                
                $functions = $this->get('utils_service');
                $entity->setSeoTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager();                
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, realizacja zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_realization_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $newsForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_realization_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Realization');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'News został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_realization_index'));
    }
    
    /**
     * @Route(
     *      "/photo/add/{id}",
     *      name="admin_realization_addPhoto",
     *      defaults = {"id" = 0},
     *      requirements={"page": "\d+"}
     * )
     * @Template();
     */
    public function addPhotoAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Realization');
        $entity = $repo->findOneBy(array('id' => $id));
        
        $repo1 = $this->getDoctrine()->getRepository('MiuzeAdminBundle:RealizationPhoto');
        $list = $repo1->findBy(array('realization' => $id));
        
        $photo = new RealizationPhoto();
        $photo->setRealization($entity);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono realizacji');
        }       
        $photoForm = $this->createForm( RealizationPhotoType::class, $photo);        
        if($Request->isMethod('POST')){
            $photoForm->handleRequest($Request);
            if($photoForm->isValid()){  
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($photo);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, zdjęcie zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_realization_addPhoto', array('id' => $id)));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $photoForm->createView(),
            'list' => $list
        );
    }
    
    /**
     * @Route(
     *      "/photo/delete/{id},{realization}",
     *      name="admin_realization_deletephoto",
     *      defaults = {"id" = 0}
     * )
     */
    public function deletephotoAction($id, $realization)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:RealizationPhoto');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Zdjęcie zostało usuniete prawidłowo.');
        return $this->redirect($this->generateUrl('admin_realization_addPhoto', array('id' => $realization)));
    }
}
