<?php

namespace Miuze\AdminBundle\Controller;

use Miuze\AdminBundle\Form\Modules\ModuleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Module;
use Symfony\Component\Yaml\Yaml;


/**
 * @Route(
 *      "/modules"
 * )
 */
class ModulesController extends Controller
{
        
    /**
     * @Route(
     *      "/",
     *      name = "admin_modules_index",
     * )
     * @Template("@MiuzeAdmin/Modules/index.html.twig")
     */
    public function indexAction()
    {
        $file = __DIR__. '/../../../../app/config/modules.yml';
        $values = Yaml::parseFile($file);
        $modules = [];

        if(!empty($values)) {
            foreach ($values as $key => $item) {
                $entity = new Module();
                $entity->setName($key);
                $entity->setTitle($item['title']);
                $entity->setIcon($item['icon']);
                $entity->setEnabled($item["enabled"]);
                $entity->setLinks($item['links']);
                array_push($modules, $entity);
            }
        }

        return array(
            'list' => $modules,
        );
    }

    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_modules_edit",
     * )
     * @Template("@MiuzeAdmin/Modules/edit.html.twig")
     */
    public function editAction(Request $request)
    {
        $session = $this->get('session');
        $id = $request->attributes->get('id');
        $file = __DIR__. '/../../../../app/config/modules.yml';
        $values = Yaml::parseFile($file);
        $modules = [];


        if(!empty($values) && isset($values[$id])) {
            $entity = new Module();
            $entity->setLinks($values[$id]['links']);
            if($request->isMethod('GET')) {
                $entity->setName($id);
                $entity->setTitle($values[$id]['title']);
                $entity->setIcon($values[$id]['icon']);
                $entity->setEnabled($values[$id]["enabled"]);
            }
            $form = $this->createForm(ModuleType::class, $entity);
            if($request->isMethod('POST')){
                $form->handleRequest($request);

                $values[$entity->getName()] = array(
                    'icon' => $entity->getIcon(),
                    'enabled' => $entity->getEnabled(),
                    'title' => $entity->getTitle(),
                    'links' => $entity->getLinks(),
                );
                $yaml = Yaml::dump($values);
                file_put_contents($file, $yaml);
                $session->getFlashBag()->add('success', 'Zapisano moduł');
                return $this->redirect($this->generateUrl('admin_modules_index'));
            }
        }else{
            $session->getFlashBag()->add('warning', 'Nie znaleziono modułu');
            return $this->redirect($this->generateUrl('admin_modules_index'));
        }

        return array(
            'form' => $form->createView(),
        );
    }
}
