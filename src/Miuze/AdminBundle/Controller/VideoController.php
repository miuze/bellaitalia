<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Video;
use Miuze\AdminBundle\Entity\VideoCategory;
use Miuze\AdminBundle\Form\Video\VideoCategoryType;
use Miuze\AdminBundle\Form\Video\VideoType;
/**
     * @Route(
     *      "/video"
     * )
     */
class VideoController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_video_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction($page)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Video');
        $videoList = $repo->findBy(array(), array('createDate' => 'DESC'));
        return array(
            'videoList' => $videoList,
        );
    }
    
    /**
     * @Route(
     *      "/videocategorylist/{categoryId}/{page}",
     *      name="admin_video_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     * )
     * @Template("MiuzeAdminBundle:Video:index.html.twig")
     */
    public function videoListCategoryAction($categoryId, $page)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Video');
            $videoList = $repo->findBy(array('category' => $categoryId ),  array('createDate' => 'DESC'));
            $paginator  = $this->get('knp_paginator');            
            $pagination = $paginator->paginate($videoList, $page ,15);
            return array(
                'videoList' => $videoList,
                'paginator' => $pagination,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add/{categoryId}",
     *      name = "admin_video_add",
     *      defaults = {"categoryId" = 0}
     * )
     * @Template()
     */
    public function addAction(Request $Request, $categoryId)
    {
        $session = $this->get('session');
        $video = new Video();  
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:VideoCategory')->findOneBy(array('id' => $categoryId));
        if ($cat) {
           $video->setCategory($cat); 
        }
        
        $videoForm = $this->createForm(VideoType::class, $video); 
        if($Request->isMethod('POST')){
            $videoForm->handleRequest($Request);            
            if($videoForm->isValid()){      
                $functions = $this->get('utils_service');
                $youtube = $functions->youtubeConverter($video->getYoutube());
                $video->setYoutube($youtube);
                $video->setSeoTitle($functions->slugify($video->getTitle()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($video);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, Video dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_video_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $videoForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_video_edit"
     * )
     * @Template("MiuzeAdminBundle:Video:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Video');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $videoForm = $this->createForm( VideoType::class, $entity);        
        if($Request->isMethod('POST')){
            $videoForm->handleRequest($Request);
            if($videoForm->isValid()){  
                $functions = $this->get('utils_service');
                $youtube = $functions->youtubeConverter($entity->getYoutube());
                $entity->setYoutube($youtube);
                
                $entity->setSeoTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager();                
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, video zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_video_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $videoForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_video_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Video');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Video został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_video_index'));
    }
}
