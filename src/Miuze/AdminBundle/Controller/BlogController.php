<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Blog;
use Miuze\AdminBundle\Entity\BlogCategory;
use Miuze\AdminBundle\Form\Blog\BlogCategoryType;
use Miuze\AdminBundle\Form\Blog\BlogType;
/**
     * @Route(
     *      "/blog"
     * )
     */
class BlogController extends Controller
{
        
    /**
     * @Route(
     *      "/",
     *      name = "admin_blog_index"
     * )
     * @Template("@MiuzeAdmin/Blog/index.html.twig")
     */
    public function indexAction()
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Blog');
        $list = $repo->findAll(array(), array('date' => 'DESC'));
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/bloglistcategory/{id}",
     *      name="admin_blog_listCategory",
     *      defaults={"id": "0"},
     * )
     * @Template("@MiuzeAdmin/Blog/index.html.twig")
     */
    public function blogListCategoryAction($id)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Blog');
            $list = $repo->findBy(array('category' => $id), array('createDate' => 'DESC'));
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate($list, $id ,2);

            return array(
                'list' => $pagination,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add/{categoryId}",
     *      name = "admin_blog_add",
     *      defaults = {"categoryId" = 0}
     * )
     * @Template("@MiuzeAdmin/Blog/add.html.twig")
     */
    public function addAction(Request $request)
    {
        $session = $this->get('session');
        $blog = new Blog();
        $categoryId = $request->attributes->getInt('categoryId');
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:BlogCategory')->findOneBy(array('id' => $categoryId));
        if ($cat) {
            $blog->setCategory($cat);
        }

        $blogForm = $this->createForm( BlogType::class, $blog); 
        if($request->isMethod('POST')){
            $blogForm->handleRequest($request);
            if($blogForm->isValid()){          
                
                $functions = $this->get('utils_service');
                $blog->setUrlTitle($functions->slugify($blog->getTitle()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($blog);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, Wpis dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_blog_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $blogForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_blog_edit"
     * )
     * @Template("@MiuzeAdmin/Blog/add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Blog');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $blogForm = $this->createForm( BlogType::class, $entity);        
        if($Request->isMethod('POST')){
            $blogForm->handleRequest($Request);
            if($blogForm->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getTitle()));                
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, wpis zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_blog_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $blogForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_blog_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Blog');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Wpis został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_blog_index'));
    }
    
    
}
