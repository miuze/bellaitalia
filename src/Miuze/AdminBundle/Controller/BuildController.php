<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Build;
use Miuze\AdminBundle\Form\Flat\BuildType;

/**
     * @Route(
     *      "/build",
     * )
     */
class BuildController extends Controller
{
//    --------------------------------------------------------------------------Category-------------------------   
    /**
     * @Route(
     *      "/",
     *      name="admin_build_index",
     * )
     * @Template("@MiuzeAdmin/Build/index.html.twig")
     */
    public function indexAction(Request $request)
    {   
        $lang = $request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Build');
        $categoryList = $repo->getAdminAllCategories(array('lang' => $lang));

        return array(
            'categoryList' => $categoryList
        );        
    }
    
    
    
    
    /**
     * @Route(
     *      "/add/{parent}",
     *      name="admin_build_add",
     *      defaults={"parent"= 0}
     * )
     * @Template("@MiuzeAdmin/Build/add.html.twig")
     */
    public function addAction(Request $request)
    {   
        $session = $this->get('session');
        $category = new Build();
        $parent = $request->attributes->getInt('parent');
        if($parent != 0){
            $parentEntity = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Build')->getOneCategory(['id' => $parent]);
            if($parentEntity !== null){
                $category->setParent($parentEntity);
                $category->setPage($parentEntity->getPage());
            }
        }
        
        $form = $this->createForm( BuildType::class, $category);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $lang = $request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Kategoria dodana prawidłowo');
                return $this->redirect($this->generateUrl('admin_build_index'));
            }else{
                $session->getFlashBag()->add('Danger', 'Wystapił błąd w formularzu.');
            }
        }

        return array(
            'form' => $form->createView(),
        );        
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_build_edit"
     * )
     * @Template("@MiuzeAdmin/Build/add.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Build');
        $entity = $repo->findOneBy(array('id' =>$id));
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $productForm = $this->createForm(BuildType::class, $entity);        
        if($request->isMethod('POST')){
            $productForm->handleRequest($request);
            if($productForm->isValid()){
                $entity->preUpload();
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_build_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił błąd w formularzu.');
            }
        }        
        return array(
            'form' => $productForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_build_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Build');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategoria usunieta prawidłowo');
        return $this->redirect($this->generateUrl('admin_build_index'));
    }
    
        
    /**
     * @Route(
     *      "/sort",
     *      name="admin_build_sort",
     * )
     */
    public function sortAction(Request $request) {
        $session = $this->get('session');
        if ($request->isMethod('POST')) {
            $sort = $request->request->get('sort');
            $sort = explode('|', $sort);
            $categorylist = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Build')->findAll();
            foreach($sort as $key => $item){
                $attr = explode('-', $item);
                $category = $attr[0];
                $parent = $attr[1];
                foreach($categorylist as $thiscategory){
                    if($thiscategory->getId() == (int)$category){
                        if((int)$parent == 0){
                            $thiscategory->setParent(NULL);
                            $thiscategory->setSort($key);
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($thiscategory);
                            $em->flush();
                        }else{
                            foreach($categorylist as $thisparent){
                                if($thisparent->getId() == (int)$parent){
                                    $thiscategory->setParent($thisparent);
                                    $thiscategory->setSort($key);
                                    $em = $this->getDoctrine()->getManager();
                                    $em->persist($thiscategory);
                                    $em->flush();
                                }elseif($thisparent->getId() == 0){
                                    $thiscategory->setSort($key);
                                    $em = $this->getDoctrine()->getManager();
                                    $em->persist($thiscategory);
                                    $em->flush();
                                }
                            }
                        }
                    }
                }
            }
            $session->getFlashBag()->add('success', 'Kolejność została zaktualizowana');
            return $this->redirect($this->generateUrl('admin_build_index'));
        }else {
            $session->getFlashBag()->add('warning', 'Kolejność nie została zaktualizowana');
        }
    }
}
