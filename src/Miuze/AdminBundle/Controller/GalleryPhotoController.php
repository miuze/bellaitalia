<?php

namespace Miuze\AdminBundle\Controller;

use Miuze\AdminBundle\Entity\GalleryPhoto;
use Miuze\AdminBundle\Form\Gallery\GalleryPhotoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
     * @Route(
     *      "/gallery-photo",
     * )
     */
class GalleryPhotoController extends Controller
{
    
    /**
     * @Route(
     *      "/upload/{id}",
     *      name="admin_gallery-photo_upload",
     * )
     * @Template("@MiuzeAdmin/GalleryPhoto/upload.html.twig")
     */
    public function uploadAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        //list zdjęć
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:GalleryPhoto');
        $photoList = $repo->findBy(array('gallery' => $id));
        
        //pobieranie kategorii 
        $gellery = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Gallery')->findOneById($id);
        //tworzenie obiektu i ustawienie id
        $photo = new GalleryPhoto();
        if ($gellery) {
           $photo->setGallery($gellery); 
        }
        
        $form = $this->createForm( GalleryPhotoType::class, $photo, array(
            'attr' => array(
                'id' => 'upload'
            )
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($photo);
                $em->flush();
                $res = array(
                    "status" => "success",
                    "path" => $photo->getPath().$photo->getImage(),
                    "links" => array( 
                        "delete" => $this->generateUrl('admin_flat_photo_delete', array(
                            "category" =>  $id,
                            "id" => $photo->getId()
                        )),
                        "setImage" => $this->generateUrl('admin_flat-photo_set', array(
                            "id" => $photo->getId()
                        ))
                                
                    )
                );
                return new JsonResponse($res);
            }else{
                $err='';
                foreach ($form->getErrors(true) as $e){
                    $err .= $e->getMessage().', ';
                }
                $res = array(
                    "status" => "error",
                    "msg" => $err
                );
                return new JsonResponse($res);
            }
        }

        return array(
            'form' => $form->createView(),
            'photoList' => $photoList,
        );        
    }
    
    /**
     * @Route(
     *      "/delete/{id}/category/{category}",
     *      name="admin_gallery_photo_delete",
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $category = $request->attributes->getInt('category');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:GalleryPhoto');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono zdjęcia');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Gratulacje, zdjęcie usunieto prawidłowo.');
        return $this->redirect($this->generateUrl('admin_gallery-photo_upload', array('id' => $category)));
    }
    
    
    
    
    
    
    
    
    /**
     * @Route(
     *      "/import",
     *      name="admin_gallery-photo_import",
     * )
     * @Template()
     */
    public function importAction(Request $Request)
    {    
//        $id = 104;
//        $session = $this->get('session');
//        //list zdjęć
//        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:GalleryPhoto');
//        $photoList = $repo->findBy(array('gallery' => $id));
//        
//        //pobieranie kategorii 
//        $gellery = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Gallery')->findOneById($id);
//        //tworzenie obiektu i ustawienie id
//        
//        $dirviews = __DIR__.'/../../../../web/uploads/fotograf/'.$gellery->getCategory()->getId().'/'.$gellery->getId();
//        $path = '/uploads/fotograf/'.$gellery->getCategory()->getId().'/'.$gellery->getId().'/';
//        foreach(scandir($dirviews) as $file){
//            if($file != '.' && $file != '..'){
//                $name = strstr($file, '.', true);
//                $views[$name] = $file;
//            
//                $photo = new GalleryPhoto();
//                if ($gellery) {
//                   $photo->setGallery($gellery); 
//                }
//                $photo->setPath($path);
//                $photo->setImage($file);
////                dump($photo);die;
//                $em = $this->getDoctrine()->getEntityManager();
//                $em->persist($photo);
//                $em->flush();
//            }
//        }
        

        return array(           
        );        
    }
}