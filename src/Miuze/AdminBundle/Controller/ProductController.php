<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Miuze\AdminBundle\Entity\Product;
use Miuze\AdminBundle\Form\Product\ProductType;

/**
     * @Route(
     *      "/product",
     * )
     */
class ProductController extends Controller
{

    /**
     * @Route(
     *      "/{page}",
     *      name="admin_product_index",
     *     defaults={"page" = 1},
     *     requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $page = $request->attributes->getInt('page');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product');
        $query = $repo->getAdminProducts(array(
            'lang' => $request->getLocale()
        ));
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page ,
            230
        );
        
        
        return array(
            'pagination' => $pagination
        );        
    }
    
    /**
     * @Route(
     *      "/product/page-{page}/id-{id}",
     *      name="admin_product_product",
     *      defaults = {"id" = 1,"page" = 1},
     *     requirements={"page": "\d+", "id": "\d+"}
     * )
     * @Template("MiuzeAdminBundle:Product:index.html.twig")
     */
    public function productAction(Request $request)
    {
        $page = $request->attributes->getInt('page', 1);
        $cat = $request->attributes->getInt('id');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product');
        $query = $repo->getAdminProducts(array(
            'lang' => $request->getLocale(),
            'category' => $cat
        ));
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page ,
            30
        );
        
        return array(
            'pagination' => $pagination
        );        
    }
    
    
    
    
    /**
     * @Route(
     *      "/add/{id}",
     *      name="admin_product_add",
     *      defaults = {"id" = 0}
     * )
     * @Template()
     */
    public function addAction(Request $Request, $id)
    {           
        $session = $this->get('session');
        $product = new Product;
        
        if($id == 0){
            
        }else{
            $category = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductCategory')->findOneBy(array('id' => $id));
            $product->setCategory($category);
        }
        
        $form = $this->createForm( ProductType::class, $product);
        if ($Request->isMethod('POST')) {
            $form->handleRequest($Request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $functions = $this->get('utils_service');
                $product->setUrlTitle($functions->slugify($product->getName()));
                $em->persist($product);
                $em->flush();
                $session->getFlashBag()->add('success', 'Galeria dodana prawidłowo');
                if($id == 0){
                    return $this->redirect($this->generateUrl('admin_product_index'));
                }else{                
                    return $this->redirect($this->generateUrl('admin_product_product', array('id' => $category->getId())));
                }
            }else{
                $session->getFlashBag()->add('Danger', 'Wystapił błąd w formularzu.');
            }
        }

        return array(
            'form' => $form->createView(),
        );        
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_product_edit"
     * )
     * @Template("MiuzeAdminBundle:Product:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product');
        $entity = $repo->findOneBy(array('id' =>$id));

        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $productForm = $this->createForm(ProductType::class, $entity);        
        if($Request->isMethod('POST')){
            $productForm->handleRequest($Request);
            if($productForm->isValid()){    
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getName()));
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_product_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił błąd w formularzu.');
            }
        }        
        return array(
            'form' => $productForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_product_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction(Request $request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Galeria usunieta prawidłowo');
        return $this->redirect($this->generateUrl('admin_product_index'));
    }
    
    
    /**
     * @Route(
     *      "/hidepage/{id}",
     *      name="admin_product_hide",
     *      defaults = {"id" = 0}
     * )
     */
    public function hideAction($id, $value)
    {
        $session = $this->get('session');
        $page = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product')->findOneById($id);
        
        if(NULL == $page ){
            throw $this->createNotFoundException('Nie znaleziono takiej strony');
        }
        if($page->getShowMain() == 0){
            $page->setShowMain(1);
            $session->getFlashBag()->add('success', 'Produkt widoczno w sekcji');
        }else{
            $page->setShowMain(0);
            $session->getFlashBag()->add('success', 'Produkt ukryta w sekcji');
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($page);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_product_index'));
    }

    /**
     * @Route(
     *      "/isstock/{id}",
     *      name="admin_product_isStock",
     *      defaults = {"id" = 0}
     * )
     */
    public function isStockAction($id, $value)
    {
        $session = $this->get('session');
        $product = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Product')->findOneById($id);

        if($product->getIsStock() == 0){
            $product->setIsStock(1);
            $session->getFlashBag()->add('success', 'Produkt dostępny');
        }else{
            $product->setIsStock(0);
            $session->getFlashBag()->add('success', 'Produkt niedostępny');
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_product_index'));
    }
}
