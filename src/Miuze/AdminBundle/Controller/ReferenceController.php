<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Reference;
use Miuze\AdminBundle\Entity\ReferenceCategory;
use Miuze\AdminBundle\Form\Reference\ReferenceCategoryType;
use Miuze\AdminBundle\Form\Reference\ReferenceType;
/**
     * @Route(
     *      "/reference"
     * )
     */
class ReferenceController extends Controller
{
        
    /**
     * @Route(
     *      "/",
     *      name = "admin_reference_index"
     * )
     * @Template("@MiuzeAdmin/Reference/index.html.twig")
     */
    public function indexAction()
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Reference');
        $list = $repo->findAll(array(), array('date' => 'DESC'));
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/referencelistcategory/{id}",
     *      name="admin_reference_listCategory",
     *      defaults={"id": "0"},
     * )
     * @Template("@MiuzeAdmin/Reference/index.html.twig")
     */
    public function blogListCategoryAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Reference');
            $list = $repo->findBy(array('category' => $id), array('createDate' => 'DESC'));
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate($list, $id ,2);

            return array(
                'list' => $pagination,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add/{categoryId}",
     *      name = "admin_reference_add"
     * )
     * @Template("@MiuzeAdmin/Reference/add.html.twig")
     */
    public function addAction(Request $request)
    {
        $session = $this->get('session');
        $reference = new Reference();    
        $categoryId = $request->attributes->getInt('categoryId');
        if($categoryId !== null){
            $category = $this->getDoctrine()->getRepository('MiuzeAdminBundle:RealizationCategory')->findOneById($categoryId);
            if($category !== null){
                $reference->setCategory($category);
            }
        }
        $form = $this->createForm( ReferenceType::class, $reference); 
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){          
                
                $functions = $this->get('utils_service');
                $reference->setUrlTitle($functions->slugify($reference->getName()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($reference);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, Referencja dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_reference_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_reference_edit"
     * )
     * @Template("@MiuzeAdmin/Reference/add.html.twig")
     */
    public function editAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Reference');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( ReferenceType::class, $entity);        
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){     
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getName()));
                $em = $this->getDoctrine()->getManager();
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, referencje zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_reference_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_reference_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Reference');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Wpis został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_reference_index'));
    }
    
    
}
