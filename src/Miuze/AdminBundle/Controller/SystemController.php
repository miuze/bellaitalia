<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Output\BufferedOutput;

/**
     * @Route(
     *      "/system"
     * )
     */
class SystemController extends Controller
{

    /**
     * @Route(
     *      "/cache-clear/{env}",
     *      name = "admin_system_cache-clear",
     *      defaults = {"env" = "prod"}
     * )
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $session = $this->get('session');
        $env = $request->attributes->get('env');
        $path = __DIR__.'/../../../'.$this->getParameter('kernel.cache_dir');
        $fs = new Filesystem();
        $fs->remove($path);
        $session->getFlashBag()->add('success', 'Gratulacje, Cache w środowisku '. $env .' wyczyszczono prawidłowo');
        return $this->redirect($this->generateUrl('admin_pages_index'));
    }
    
    
    
}
