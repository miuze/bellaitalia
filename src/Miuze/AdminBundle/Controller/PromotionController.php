<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Promotion;
use Miuze\AdminBundle\Entity\PromotionCategory;
use Miuze\AdminBundle\Form\Promotion\PromotionCategoryType;
use Miuze\AdminBundle\Form\Promotion\PromotionType;
/**
     * @Route(
     *      "/promotion"
     * )
     */
class PromotionController extends Controller
{
        
    /**
     * @Route(
     *      "/",
     *      name = "admin_promotion_index"
     * )
     * @Template()
     */
    public function indexAction()
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Promotion');
        $list = $repo->findAll(array(), array('date' => 'DESC'));
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/bloglistcategory/{id}",
     *      name="admin_promotion_listCategory",
     *      defaults={"id": "0"},
     * )
     * @Template("MiuzeAdminBundle:Promotion:index.html.twig")
     */
    public function blogListCategoryAction($id)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Promotion');
            $list = $repo->findBy(array('category' => $id), array('createDate' => 'DESC'));

            return array(
                'list' => $list,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_promotion_add"
     * )
     * @Template()
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $promotion = new Promotion();    
                
        $promotionForm = $this->createForm( PromotionType::class, $promotion); 
        if($Request->isMethod('POST')){
            $promotionForm->handleRequest($Request);            
            if($promotionForm->isValid()){          
                
                $functions = $this->get('utils_service');
                $promotion->setUrlTitle($functions->slugify($promotion->getName()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($promotion);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, Promocje dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_promotion_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $promotionForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_promotion_edit"
     * )
     * @Template("MiuzeAdminBundle:Promotion:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Promotion');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $promotionForm = $this->createForm( PromotionType::class, $entity);        
        if($Request->isMethod('POST')){
            $promotionForm->handleRequest($Request);
            if($promotionForm->isValid()){     
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getName()));
                $em = $this->getDoctrine()->getManager();
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, promocje zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_promotion_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $promotionForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_promotion_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Promotion');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Wpis został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_promotion_index'));
    }
    
    
}
