<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('admin_dashboard_index'));
    }
    
    /**
     * @Route(
     *      "/lang/{lang}",
     *      defaults={"lang" = "pl"},
     *      name="admin_lang_lang"
     * )
     */
    public function langAction(Request $request, $lang)
    {
        $session = $this->get('session');
        if($session->has('_locale')){
             $session->set('_locale', $lang);
        }else{
            $session->set('_locale', $lang);
        }
        return $this->redirect($this->generateUrl('admin_pages_index'));
    }
}
