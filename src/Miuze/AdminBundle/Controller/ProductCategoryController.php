<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\ProductCategory;
use Miuze\AdminBundle\Form\Product\ProductCategoryType;

/**
     * @Route(
     *      "/product-category",
     * )
     */
class ProductCategoryController extends Controller
{
//    --------------------------------------------------------------------------Category-------------------------   
    /**
     * @Route(
     *      "/",
     *      name="admin_product-category_index",
     * )
     * @Template()
     */
    public function indexAction(Request $Request)
    {   
        $lang = $Request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductCategory');
        $categoryList = $repo->getAdminAllCategoriesToTree(array('lang' => $lang));

        $options = array(
            'decorate' => true,
            'level0Open'=> '<ol class="pageView sortable">',
            'level0Close' => '</ol>',
            'rootOpen' => function($node){
                return '<ol id="p'.$node["id"].'" class="pageView"  style="height: 0px; display: none;">';
            },
            'rootClose' => '</ol>',
            'childOpen' => function ($node) {
                return '<li id="e' . $node["id"] . '" class="sortli">';
            },
            'childClose' => '</li>',
            'nodeDecorator' => function($node) {
                !empty($node['__children']) ? $more = '<span class="fa fa-plus"></span>' : $more = '';
                return '<a href="/parentId/'. $node["id"].'" class="treeNode expand">'.
                            '<span class="cover"></span>'.
                            $more.
                        '</a>'.
                        '<article class="dd-handle item-list">'.
                            '<div class="col-xs-12 col-sm-6">'.
                            '<p class="title-item">'. $node["name"] . '</p>'.
                            '<a href="'. $this->generateUrl("admin_product-category_edit", array("id" => $node["id"])).'" title="Edytuj"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>'.
                            '<a href="'. $this->generateUrl("admin_product-category_delete", array("id" => $node["id"])).'" title="Usuń" class="confirm"><i class="fa fa-trash" aria-hidden="true"></i></a>'.
                            '<a href="'. $this->generateUrl("admin_product_product", array("id" => $node["id"])).'" title="Lista produktów"><i class="fa fa-list" aria-hidden="true"></i></a>'.
                            '<a href="'. $this->generateUrl("admin_product_add", array("id" => $node["id"])).'" title="Dodaj produkt"><i class="fa fa-plus"></i></a>'.
                            '</div>'.
                            '<div class="col-xs-12 col-sm-6">'.
                            '<a class="pull-right btn btn-info" href="'. $this->generateUrl("admin_product-category_add", array("parent" => $node["id"])).'" title="Dodaj podkategorię">Dodaj podkategorię</a>'.
                            '</div>'.
                        '</article>';
            }


        );
        $tree = $repo->buildTreeArray($categoryList);
        $managerTree = $this->get('tree');
        $htmlTree = $managerTree->buildTreeHtmlAdmin($tree, $options);


        return array(
            'categoryList' => $htmlTree
        );        
    }
    
    
    
    
    /**
     * @Route(
     *      "/add/{parent}",
     *      name="admin_product-category_add",
     *      defaults={"parent"= 0}
     * )
     * @Template()
     */
    public function addAction(Request $Request)
    {   
        $session = $this->get('session');
        $category = new ProductCategory();
        $parent = $Request->attributes->getInt('parent');
        if($parent != 0){
            $parentEntity = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductCategory')->getOneCategory(['id' => $parent]);
            if($parentEntity !== null){
                $category->setParent($parentEntity);
                $category->setPage($parentEntity->getPage());
            }
        }
        
        $form = $this->createForm( ProductCategoryType::class, $category);
        if ($Request->isMethod('POST')) {
            $form->handleRequest($Request);
            if ($form->isValid()) {
                $lang = $Request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Kategoria dodana prawidłowo');
                return $this->redirect($this->generateUrl('admin_product-category_index'));
            }else{
                $session->getFlashBag()->add('Danger', 'Wystapił błąd w formularzu.');
            }
        }

        return array(
            'form' => $form->createView(),
        );        
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_product-category_edit"
     * )
     * @Template("MiuzeAdminBundle:ProductCategory:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductCategory');
        $entity = $repo->findOneBy(array('id' =>$id));
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $productForm = $this->createForm(ProductCategoryType::class, $entity);        
        if($Request->isMethod('POST')){
            $productForm->handleRequest($Request);
            if($productForm->isValid()){     
                $entity->preUpload();
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_product-category_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił błąd w formularzu.');
            }
        }        
        return array(
            'form' => $productForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_product-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategoria usunieta prawidłowo');
        return $this->redirect($this->generateUrl('admin_product-category_index'));
    }
    
        
    /**
     * @Route(
     *      "/sort",
     *      name="admin_product-category_sort",
     * )
     */
    public function sortAction(Request $request) {
        $session = $this->get('session');
        if ($request->isMethod('POST')) {
            $sort = $request->request->get('sort');
            $sort = explode('|', $sort);
            $categorylist = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ProductCategory')->findAll();
            foreach($sort as $key => $item){
                $attr = explode('-', $item);
                $category = $attr[0];
                $parent = $attr[1];
                foreach($categorylist as $thiscategory){
                    if($thiscategory->getId() == (int)$category){
                        if((int)$parent == 0){
                            $thiscategory->setParent(NULL);
                            $thiscategory->setSort($key);
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($thiscategory);
                            $em->flush();
                        }else{
                            foreach($categorylist as $thisparent){
                                if($thisparent->getId() == (int)$parent){
                                    $thiscategory->setParent($thisparent);
                                    $thiscategory->setSort($key);
                                    $em = $this->getDoctrine()->getManager();
                                    $em->persist($thiscategory);
                                    $em->flush();
                                }elseif($thisparent->getId() == 0){
                                    $thiscategory->setSort($key);
                                    $em = $this->getDoctrine()->getManager();
                                    $em->persist($thiscategory);
                                    $em->flush();
                                }
                            }
                        }
                    }
                }
            }
            $session->getFlashBag()->add('success', 'Kolejność została zaktualizowana');
            return $this->redirect($this->generateUrl('admin_product-category_index'));
        }else {
            $session->getFlashBag()->add('warning', 'Kolejność nie została zaktualizowana');
        }
    }
}
