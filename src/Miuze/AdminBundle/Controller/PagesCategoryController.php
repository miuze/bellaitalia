<?php

namespace Miuze\AdminBundle\Controller;

use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Entity\Slug;
use Miuze\AdminBundle\Enum\SlugEnum;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use Miuze\AdminBundle\Entity\PageCategory;
use Miuze\AdminBundle\Form\Page\MenuType;

/**
 * @Route("/pages-category")
 */
class PagesCategoryController extends Controller
{
    /**
     * @Route("/list",
     *      name="admin_pagescategory_index",
     *      defaults = {"id" = 1}
     * )
     *  @Template("@MiuzeAdmin/PagesCategory/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $menuList = $this->getDoctrine()->getRepository(PageCategory::class)->findAll();
        return array(
            'manuList' => $menuList,
        );
    }
    
    /**
     * @Route(
     *      "/add/{id}",
     *      name="admin_pagescategory_add",
     *      defaults = {"id" = 0}
     * )
     * @Template("@MiuzeAdmin/PagesCategory/add.html.twig")
     */
    public function addAction(Request $Request, $id)
    {   
        $session = $this->get('session');
        if($id !== 0){
            $menu = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PageCategory')->findOneById($id);
            if(NULL == $menu ){
                throw $this->createNotFoundException('Nie znaleziono takiego menu');
            }
        }else{
            $menu = new PageCategory();
            $slug = new Slug();
            $slug->setName('Strona główna');
            $slug->setSlug(SlugEnum::PAGE);
            $mainPage = new Page();
            $mainPage->setSlug($slug);
            $mainPage->setType('index');
            $mainPage->setLayout('layout-index.html.twig');
            $menu->addPage($mainPage);
        }
        $form = $this->createForm(MenuType::class, $menu);
        if ($Request->isMethod('POST')) {
            $form->handleRequest($Request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($menu);
                $em->flush();

                $session->getFlashBag()->add('success', 'Menu dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_pages_index', ['menu' => $menu->getId()]));
            }else{
                $session->getFlashBag()->add('Danger', 'Wystapił błąd w formularzu.');
            }
        }

        return array(
            'form' => $form->createView(),
        );        
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_pagescategory_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $menu = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PageCategory')->findOneById($id);
        
        if(NULL == $menu ){
            throw $this->createNotFoundException('Nie znaleziono takiego menu');
        }
        $menu->setRemoved(1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($menu);
        $em->flush();
        $session->getFlashBag()->add('success', 'Menu usuniete prawidłowo');
        return $this->redirect($this->generateUrl('admin_pages_index'));
    }
    
    
}
