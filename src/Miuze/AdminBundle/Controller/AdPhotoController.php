<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Ad;
use Miuze\AdminBundle\Entity\AdPhoto;
use Miuze\AdminBundle\Form\Ad\AdCategoryType;
use Miuze\AdminBundle\Form\Ad\AdPhotoType;
use Miuze\AdminBundle\Form\Ad\SectionType;

/**
     * @Route(
     *      "/ad-photo",
     * )
     */
class AdPhotoController extends Controller
{
    
    /**
     * @Route(
     *      "/upload/{id}",
     *      name="admin_ad-photo_upload",
     * )
     * @Template()
     */
    public function uploadAction(Request $Request, $id)
    {    
        $session = $this->get('session');
        //list zdjęć
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:AdPhoto');
        $photoList = $repo->findBy(array('ad' => $id));
        
        //pobieranie kategorii 
        $ad = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Ad')->findOneById($id);
        //tworzenie obiektu i ustawienie id
        $photo = new AdPhoto();
        if ($ad) {
           $photo->setAd($ad); 
        }
        $form = $this->createForm(AdPhotoType::class, $photo);
        if ($Request->isMethod('POST')) {
            $form->handleRequest($Request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($photo);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, zdjęcie zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_ad-photo_upload', array('id' => $id)));
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił błąd w formularzu.');
            }
        }
        return array(
            'form' => $form->createView(),
            'photoList' => $photoList,
        );        
    }
    
    /**
     * @Route(
     *      "/delete/{id}/category/{category}",
     *      name="admin_ad_photo_delete",
     * )
     */
    public function deleteAction(Request $request, $id, $category)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:AdPhoto');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono zdjęcia');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Gratulacje, zdjęcie usunieto prawidłowo.');
        return $this->redirect($this->generateUrl('admin_ad-photo_upload', array('id' => $category)));
    }
    
}