<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Award;
use Miuze\AdminBundle\Entity\AwardCategory;
use Miuze\AdminBundle\Form\Award\AwardCategoryType;
use Miuze\AdminBundle\Form\Award\AwardType;
/**
     * @Route(
     *      "/award"
     * )
     */
class AwardController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_award_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction($page)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Award');
        $list = $repo->findBy(array(), array('createDate' => 'DESC'));
        
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/awardcategorylist/{categoryId}/{page}",
     *      name="admin_award_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     * )
     * @Template("MiuzeAdminBundle:Award:index.html.twig")
     */
    public function adListCategoryAction($categoryId, $page)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Award');
            $list = $repo->findBy(array('category' => $categoryId ),  array('createDate' => 'DESC'));
            
            return array(
                'list' => $list,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_award_add"
     * )
     * @Template()
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $entity = new Award();    
                
        $form = $this->createForm(AwardType::class, $entity); 
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);            
            if($form->isValid()){      
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, Nagroda dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_award_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_award_edit"
     * )
     * @Template("MiuzeAdminBundle:Award:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Award');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( AwardType::class, $entity);        
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);
            if($form->isValid()){  
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager();                
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, Nagroda zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_aword_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_aword_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Aword');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Nagroda został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_aword_index'));
    }
}
