<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Controller;

use Miuze\AdminBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Section;
use Miuze\AdminBundle\Form\Section\SectionType;


/**
 * @Route(
 *      "/section"
 * )
 */
class SectionController extends Controller
{
    /**
     * @Route(
     *      "/add/page-{id}",
     *      name = "admin_section_add",
     * )
     * @Template("@MiuzeAdmin/Section/add.html.twig")
     */
    public function addAction(Request $request)
    {
        $session = $this->get('session');
        $entity = new Section();
        $form = $this->createForm(SectionType::class, $entity);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $page = $this->getDoctrine()->getRepository(Page::class)->findOneById($request->attributes->getInt('id'));
                $entity->setPage($page);
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $session->getFlashBag()->add('success', 'Gratulacje, Sekcje dodano prawidłowo');
                return $this->redirect($this->generateUrl(
                    'admin_pages_add',
                    [
                        'menu' => $entity->getPage()->getCategory()->getId(),
                        'id' => $entity->getPage()->getId(),
                    ]
                ));
            } else {
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }
        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route(
     *      "/edit/section-{id}",
     *      name = "admin_section_edit"
     * )
     * @Template("@MiuzeAdmin/Section/add.html.twig")
     */
    public function editAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Section');
        $entity = $repo->find($id);

        if (NULL == $entity) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $form = $this->createForm(SectionType::class, $entity);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();

                $session->getFlashBag()->add('success', 'Gratulacje, Sekcje zapisano prawidłowo');
                return $this->redirect($this->generateUrl(
                    'admin_pages_add',
                    [
                        'menu' => $entity->getPage()->getCategory()->getId(),
                        'id' => $entity->getPage()->getId(),
                    ]
                ));

            } else {
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }
        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_section_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Section');
        $entity = $repo->findOneById($id);
        if (NULL == $entity) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $session->getFlashBag()->add('success', 'Sekcja została usunieta prawidłowo.');
        $redirectEdit = $this->generateUrl(
            'admin_pages_add',
            [
                'menu' => $entity->getPage()->getCategory()->getId(),
                'id' => $entity->getPage()->getId()
            ]
        );
        return $this->redirect($redirectEdit);
    }
}
