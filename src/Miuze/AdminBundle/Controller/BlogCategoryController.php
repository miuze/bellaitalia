<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Blog;
use Miuze\AdminBundle\Entity\BlogCategory;
use Miuze\AdminBundle\Form\Blog\BlogCategoryType;
use Miuze\AdminBundle\Form\Blog\BlogType;
/**
     * @Route(
     *      "/blog-category"
     * )
     */
class BlogCategoryController extends Controller
{
        
    
    /**
     * @Route(
     *      "/category",
     *      name = "admin_blog-category_index"
     * )
     * @Template("@MiuzeAdmin/BlogCategory/index.html.twig")
     */
    public function indexAction(Request $request)
    {        
        $lang = $request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:BlogCategory');
        $list = $repo->getAllCategories(array('lang' => $lang));
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_blog-category_add"
     * )
     * @Template("@MiuzeAdmin/BlogCategory/add.html.twig")
     */
    public function addAction(Request $request)
    {
        $session = $this->get('session');
        $category = new BlogCategory();   
                
        $categoryForm = $this->createForm( BlogCategoryType::class, $category); 
        if($request->isMethod('POST')){
            $categoryForm->handleRequest($request);
            if($categoryForm->isValid()){        
                $lang = $request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_blog-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_blog-category_edit"
     * )
     * @Template("@MiuzeAdmin/BlogCategory/add.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:BlogCategory');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $categoryForm = $this->createForm( BlogCategoryType::class, $entity);        
        if($request->isMethod('POST')){
            $categoryForm->handleRequest($request);
            if($categoryForm->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, category zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_category_blog'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_blog-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:BlogCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategorie usunieto prawidłowo');
        return $this->redirect($this->generateUrl('admin_blog-category_index'));
    }
}
