<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Place;
use Miuze\AdminBundle\Entity\PlaceCategory;
use Miuze\AdminBundle\Form\Place\PlaceCategoryType;
use Miuze\AdminBundle\Form\Place\PlaceType;
/**
     * @Route(
     *      "/place-category"
     * )
     */
class PlaceCategoryController extends Controller
{
           
    /**
     * @Route(
     *      "/",
     *      name = "admin_place-category_index"
     * )
     * @Template("@MiuzeAdmin/PlaceCategory/index.html.twig")
     */
    public function indexAction(Request $Request)
    {     
        $lang = $Request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PlaceCategory');
        $list = $repo->getAllCategories();
        
        $session = $this->get('session');
        $category = new PlaceCategory();   
                
        $categoryForm = $this->createForm( PlaceCategoryType::class, $category); 
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);            
            if($categoryForm->isValid()){          
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_place-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        
        return array(
            'list' => $list,
            'form' => $categoryForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_place-category_add"
     * )
     * @Template("@MiuzeAdmin/PlaceCategory/add.html.twig")
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $category = new PlaceCategory();   
                
        $categoryForm = $this->createForm( PlaceCategoryType::class, $category); 
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);            
            if($categoryForm->isValid()){          

                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_place-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_place-category_edit"
     * )
     * @Template("@MiuzeAdmin/PlaceCategory/add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PlaceCategory');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $categoryForm = $this->createForm( PlaceCategoryType::class, $entity);        
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);
            if($categoryForm->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_place-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_place-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PlaceCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategorie usunieto prawidłowo');
        return $this->redirect($this->generateUrl('admin_place-category_index'));
    }
    
}
