<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Client;
use Miuze\AdminBundle\Entity\ClientCategory;
use Miuze\AdminBundle\Form\Client\ClientCategoryType;
use Miuze\AdminBundle\Form\Client\ClientType;
/**
     * @Route(
     *      "/client-category"
     * )
     */
class ClientCategoryController extends Controller
{
    /**
     * @Route(
     *      "/category",
     *      name = "admin_client-category_index"
     * )
     * @Template()
     */
    public function indexAction()
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ClientCategory');
        $list = $repo->getAllCategories(array('lang' => $lang));
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_client-category_add"
     * )
     * @Template()
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $category = new ClientCategory();   
                
        $categoryForm = $this->createForm( ClientCategoryType::class, $category); 
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);            
            if($categoryForm->isValid()){ 
                $lang = $Request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_client-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_client-category_edit"
     * )
     * @Template("MiuzeAdminBundle:ClientCategory:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ClientCategory');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $categoryForm = $this->createForm( ClientCategoryType::class, $entity);        
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);
            if($categoryForm->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_client-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_client-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ClientCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategorie usunieto prawidłowo');
        return $this->redirect($this->generateUrl('admin_client-category_index'));
    }
}
