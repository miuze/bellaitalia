<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Download;
use Miuze\AdminBundle\Entity\DownloadCategory;
use Miuze\AdminBundle\Entity\DownloadFile;
use Miuze\AdminBundle\Form\Download\DownloadCategoryType;
use Miuze\AdminBundle\Form\Download\DownloadType;
use Miuze\AdminBundle\Form\Download\DownloadFileType;
/**
     * @Route(
     *      "/download-category"
     * )
     */
class DownloadCategoryController extends Controller
{
           
    /**
     * @Route(
     *      "/",
     *      name = "admin_download-category_index"
     * )
     * @Template()
     */
    public function indexAction(Request $Request)
    {       
        $lang = $Request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadCategory');
        $list = $repo->getAllCategories(array('lang' => $lang));
        
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_download-category_add"
     * )
     * @Template()
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $category = new DownloadCategory();   
                
        $categoryForm = $this->createForm( DownloadCategoryType::class, $category); 
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);            
            if($categoryForm->isValid()){
                $lang = $Request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_download-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_download-category_edit"
     * )
     * @Template("MiuzeAdminBundle:DownloadCategory:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadCategory');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $categoryForm = $this->createForm( DownloadCategoryType::class, $entity);        
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);
            if($categoryForm->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, category zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_download-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_download-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategorie usunieto prawidłowo');
        return $this->redirect($this->generateUrl('admin_download-category_index'));
    }
    
    
    /**
     * @Route(
     *      "/sort",
     *      name="admin_download-category_sort",
     * )
     */
    public function sortAction(Request $request) {
        
        if ($request->isMethod('POST')) {
            $session = $this->get('session');
            $sort = $request->request->get('sort');
            $sort = explode(',', $sort);
            $category = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadCategory')->findAll();
            $em = $this->getDoctrine()->getManager();
            foreach($sort as $key => $item){
                foreach($category as $cat){
                    if($cat->getId() == (int)$item){
                        $cat->setSort($key);
                        $em->persist($cat);
                        $em->flush();
                    }
                }
            }
            $session->getFlashBag()->add('success', 'Kolejność została zaktualizowana');
            return $this->redirect($this->generateUrl('admin_download-category_index'));
        }else {
            $session->getFlashBag()->add('warning', 'Kolejność nie została zaktualizowana');
        }
    }
    
}
