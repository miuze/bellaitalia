<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Faq;
use Miuze\AdminBundle\Entity\FaqCategory;
use Miuze\AdminBundle\Form\Faq\FaqCategoryType;
use Miuze\AdminBundle\Form\Faq\FaqType;
/**
     * @Route(
     *      "/faq"
     * )
     */
class FaqController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_faq_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template("@MiuzeAdmin/Faq/index.html.twig")
     */
    public function indexAction($page)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Faq');
        $newsList = $repo->findBy(array(), array('sort' => 'ASC'));
        $paginator  = $this->get('knp_paginator');            
        $pagination = $paginator->paginate($newsList, $page ,10);
        return array(
            'newsList' => $newsList,
            'paginator' => $pagination,
        );
    }
    
    /**
     * @Route(
     *      "/faqcategorylist/{categoryId}/{page}",
     *      name="admin_faq_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     * )
     * @Template("@MiuzeAdmin/Faq/index.html.twig")
     */
    public function faqListCategoryAction($categoryId, $page)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Faq');
            $newsList = $repo->findBy(array('category' => $categoryId ), array('sort' => 'ASC'));
            $paginator  = $this->get('knp_paginator');            
            $pagination = $paginator->paginate($newsList, $page ,10);
            return array(
                'newsList' => $newsList,
                'paginator' => $pagination,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add/{categoryId}",
     *      name = "admin_faq_add",
     *      defaults = {"categoryId" = 0}
     * )
     * @Template("@MiuzeAdmin/Faq/add.html.twig")
     */
    public function addAction(Request $Request, $categoryId)
    {
        $session = $this->get('session');
        $faq = new Faq();    
                
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:FaqCategory')->findOneBy(array('id' => $categoryId));
        if ($cat) {
           $faq->setCategory($cat); 
        }
        
        $form = $this->createForm( FaqType::class, $faq);
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);            
            if($form->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->persist($faq);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, Faq dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_faq_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_faq_edit"
     * )
     * @Template("@MiuzeAdmin/Faq/add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Faq');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $newsForm = $this->createForm( FaqType::class, $entity);        
        if($Request->isMethod('POST')){
            $newsForm->handleRequest($Request);
            if($newsForm->isValid()){     
                $em = $this->getDoctrine()->getManager();                                
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, faq zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_faq_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $newsForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_faq_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Faq');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'News został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_faq_index'));
    }
    
    
    /**
     * @Route(
     *      "/sort",
     *      name="admin_faq_sort",
     * )
     */
    public function sortAction(Request $request) {
        if ($request->isMethod('POST')) {
            $session = $this->get('session');
            $sort = $request->request->get('sort');
            $sort = explode(',', $sort);
            $items = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Faq')->findAll();
            $em = $this->getDoctrine()->getManager();
            foreach($sort as $key => $value){
                foreach($items as $item){
                    if($item->getId() == (int)$value){
                        $item->setSort($key);
                        $em->persist($item);
                        $em->flush();
                    }
                }
            }
            $session->getFlashBag()->add('success', 'Kolejność została zaktualizowana');
            return $this->redirect($this->generateUrl('admin_faq_index'));
        }else {
            $session->getFlashBag()->add('warning', 'Kolejność nie została zaktualizowana');
        }
    }
}
