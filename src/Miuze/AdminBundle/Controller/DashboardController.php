<?php

namespace Miuze\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/",
     *      name="admin_dashboard_index",
     * )
     *  @Template("@MiuzeAdmin/Dashboard/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        return [];
    }
}
