<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Miuze\AdminBundle\Entity\Place;
use Miuze\AdminBundle\Form\Place\PlaceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
/**
     * @Route(
     *      "/place"
     * )
     */
class PlaceController extends Controller
{
    
    /**
     * @Route(
     *      "/",
     *      name = "admin_place_index"
     * )
     * @Template("@MiuzeAdmin/Place/index.html.twig")
     */
    public function indexAction()
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Place');
        $placeList = $repo->findAll();        
        return array(
            'placeList' => $placeList,
        );
    }
    
    /**
     * @Route(
     *      "/placecategorylist/{categoryId}/{page}",
     *      name="admin_place_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     * )
     * @Template("@MiuzeAdmin/Place/index.html.twig")
     */
    public function placeListCategoryAction($categoryId, $page)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Place');
            $placeList = $repo->findBy(array('category' => $categoryId ));
            $paginator  = $this->get('knp_paginator');            
            $pagination = $paginator->paginate($placeList, $page ,15);
            return array(
                'placeList' => $placeList,
                'paginator' => $pagination,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    
    /**
     * @Route(
     *      "/addplace/{categoryId}",
     *      name = "admin_place_add",
     *      defaults = {"categoryId" = 0}
     * )
     * @Template("@MiuzeAdmin/Place/add.html.twig")
     */
    public function addAction(Request $Request, $categoryId)
    {
        $session = $this->get('session');
        $place = new Place();    
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PlaceCategory')->findOneBy(array('id' => $categoryId));
        if ($cat) {
           $place->setCategory($cat); 
        }
        
        $placeForm = $this->createForm( PlaceType::class, $place); 
        if($Request->isMethod('POST')){
            $placeForm->handleRequest($Request);            
            if($placeForm->isValid()){          
                //dump($pageForm); die;
                $em = $this->getDoctrine()->getManager();
                $em->persist($place);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, placówka dadana prawidłowo!');
                return $this->redirect($this->generateUrl('admin_place_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
            }
        }        
        return array(
            'form' => $placeForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_place_edit"
     * )
     * @Template("@MiuzeAdmin/Place/index.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Place');
        $entity = $repo->findOneBy(array('id' => $id));
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        
        $placeForm = $this->createForm( PlaceType::class, $entity);        
        if($Request->isMethod('POST')){
            $placeForm->handleRequest($Request);
            if($placeForm->isValid()){     
                $em = $this->getDoctrine()->getManager();                
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, placówka została zapisana prawidłowo!');
                return $this->redirect($this->generateUrl('admin_place_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Coś poszło nie tak, sprawdź formularz!');
            }
        }        
        return array(
            'form' => $placeForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_place_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Place');
        $row = $repo->findOneBy(array('id' => $id));
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Placówka została usunieta!');
        return $this->redirect($this->generateUrl('admin_place_index'));
    }
    
}
