<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Ad;
use Miuze\AdminBundle\Entity\AdCategory;
use Miuze\AdminBundle\Entity\Registry;
use Miuze\AdminBundle\Form\Ad\AdCategoryType;
use Miuze\AdminBundle\Form\Ad\SectionType;


/**
     * @Route(
     *      "/ad"
     * )
     */
class AdController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_ad_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template("MiuzeAdmin/Ad/index.html.twig")
     */
    public function indexAction($page)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Ad');
        $list = $repo->findBy(array(), array('createDate' => 'DESC'));
        
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/adcategorylist/{categoryId}/{page}",
     *      name="admin_ad_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     * )
     * @Template("MiuzeAdmin/Ad/index.html.twig")
     */
    public function adListCategoryAction($categoryId, $page)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Ad');
            $list = $repo->findBy(array('category' => $categoryId ),  array('createDate' => 'DESC'));
            
            return array(
                'list' => $list,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add/{categoryId}",
     *      name = "admin_ad_add",
     *      defaults = {"categoryId" = 0}
     * )
     * @Template("MiuzeAdmin/Ad/add.html.twig")
     */
    public function addAction(Request $Request, $categoryId)
    {
        $session = $this->get('session');
        $entity = new Ad();    
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:AdCategory')->findOneBy(array('id' => $categoryId));
        if ($cat) {
           $entity->setCategory($cat); 
        }
        
        $form = $this->createForm(SectionType::class, $entity); 
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);            
            if($form->isValid()){      
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, Ogłoszenie dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_ad_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_ad_edit"
     * )
     * @Template("MiuzeAdmin/Ad/add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Ad');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( SectionType::class, $entity);        
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);
            if($form->isValid()){  
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager();                
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, ogłoszenie zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_ad_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_ad_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Ad');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Ogłoszenie został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_ad_index'));
    }
}
