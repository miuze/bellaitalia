<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Banner;
use Miuze\AdminBundle\Entity\BannerCategory;
use Miuze\AdminBundle\Form\Banner\BannerCategoryType;
use Miuze\AdminBundle\Form\Banner\BannerType;
/**
     * @Route(
     *      "/banner"
     * )
     */
class BannerController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_banner_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template("@MiuzeAdmin/Banner/index.html.twig")
     */
    public function indexAction($page)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Banner');
        $list = $repo->findBy(array(), array('createDate' => 'ASC'));
        
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/bannercategorylist/{categoryId}/{page}",
     *      name="admin_banner_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     * )
     * @Template("@MiuzeAdmin/Banner/index.html.twig")
     */
    public function adListCategoryAction($categoryId, $page)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Banner');
            $list = $repo->findBy(array('category' => $categoryId ),  array('sort' => 'ASC'));
            
            return array(
                'list' => $list,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add/{categoryId}",
     *      name = "admin_banner_add",
     *      defaults = {"categoryId" = 0}
     * )
     * @Template("@MiuzeAdmin/Banner/add.html.twig")
     */
    public function addAction(Request $Request, $categoryId)
    {
        $session = $this->get('session');
        $entity = new Banner();    
        
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:AdCategory')->findOneBy(array('id' => $categoryId));
        if ($cat) {
           $entity->setCategory($cat); 
        }        
        
        $form = $this->createForm(BannerType::class, $entity); 
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);            
            if($form->isValid()){      
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, Baner dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_banner_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_banner_edit"
     * )
     * @Template("@MiuzeAdmin/Banner/add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Banner');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( BannerType::class, $entity);        
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);
            if($form->isValid()){  
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager();                
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, baner zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_banner_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_banner_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Banner');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Baner został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_banner_index'));
    }
    
    /**
     * @Route(
     *      "/sort",
     *      name="admin_banner_sort",
     * )
     */
    public function sortAction(Request $request) {
        $session = $this->get('session');
        if ($request->isMethod('POST')) {
            $sort = $request->request->get('sort');
            $sort = explode(',', $sort);
            $items = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Banner')->findAll();
            $em = $this->getDoctrine()->getManager();
            foreach($sort as $key => $value){
                foreach($items as $item){
                    if($item->getId() == (int)$value){
                        $item->setSort($key);
                        $em->persist($item);
                        $em->flush();
                    }
                }
            }
            $session->getFlashBag()->add('success', 'Kolejność została zaktualizowana');
            return $this->redirect($this->generateUrl('admin_banner-category_index'));
        }else {
            $session->getFlashBag()->add('warning', 'Kolejność nie została zaktualizowana');
        }
    }
}
