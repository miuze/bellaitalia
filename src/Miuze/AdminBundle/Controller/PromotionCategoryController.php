<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Promotion;
use Miuze\AdminBundle\Entity\PromotionCategory;
use Miuze\AdminBundle\Form\Promotion\PromotionCategoryType;
use Miuze\AdminBundle\Form\Promotion\PromotionType;
/**
     * @Route(
     *      "promotion-category"
     * )
     */
class PromotionCategoryController extends Controller
{
        
    
    /**
     * @Route(
     *      "/category",
     *      name = "admin_promotion-category_index"
     * )
     * @Template()
     */
    public function indexAction()
    {        
        $lang = $Request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PromotionCategory');
        $list = $repo->getAllCategories(array('lang' => $lang));
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_promotion-category_add"
     * )
     * @Template()
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $category = new PromotionCategory();   
                
        $categoryForm = $this->createForm( PromotionCategoryType::class, $category); 
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);            
            if($categoryForm->isValid()){     
                $lang = $Request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_promotion-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_promotion-category_edit"
     * )
     * @Template("MiuzeAdminBundle:PromotionCategory:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PromotionCategory');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $categoryForm = $this->createForm( PromotionCategoryType::class, $entity);        
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);
            if($categoryForm->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, category zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_promotion-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_promotion-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:PromotionCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategorie usunieto prawidłowo');
        return $this->redirect($this->generateUrl('admin_promotion-category_index'));
    }
}
