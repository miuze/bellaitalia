<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Gallery;
use Miuze\AdminBundle\Form\Gallery\GalleryType;

/**
     * @Route(
     *      "/gallery",
     * )
     */
class GalleryController extends Controller
{

    /**
     * @Route(
     *      "/",
     *      name="admin_gallery_index",
     * )
     * @Template("@MiuzeAdmin/Gallery/index.html.twig")
     */
    public function indexAction()
    {   
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Gallery');
        $galleryList = $repo->findAll();
        
        
        return array(
            'galleryList' => $galleryList
        );        
    }
    
    /**
     * @Route(
     *      "/gallery/{id}",
     *      name="admin_gallery_gallery",
     *      defaults = {"id" = 0}
     * )
     * @Template("@MiuzeAdmin/Gallery/index.html.twig")
     */
    public function galleryAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Gallery');
        $galleryList = $repo->findBy(array('category' => $id));
        
        
        return array(
            'galleryList' => $galleryList
        );        
    }
    
    
    
    
    /**
     * @Route(
     *      "/add/{id}",
     *      name="admin_gallery_add",
     *      defaults = {"id" = 0}
     * )
     * @Template("@MiuzeAdmin/Gallery/add.html.twig")
     */
    public function addAction(Request $request)
    {           
        $session = $this->get('session');
        $gallery = new Gallery;
        $id = $request->attributes->getInt('id');
        if($id == 0){
            
        }else{
            $category = $this->getDoctrine()->getRepository('MiuzeAdminBundle:GalleryCategory')->findOneBy(array('id' => $id));
            $gallery->setCategory($category);
        }
        
        $form = $this->createForm( GalleryType::class, $gallery);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                
                $functions = $this->get('utils_service');
                $gallery->seturlTitle($functions->slugify($gallery->getName()));
                
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($gallery);
                $em->flush();
                $session->getFlashBag()->add('success', 'Galeria dodana prawidłowo');
                if($id == 0){
                    return $this->redirect($this->generateUrl('admin_gallery_index'));
                }else{                
                    return $this->redirect($this->generateUrl('admin_gallery_gallery', array('id' => $category->getId())));
                }
            }else{
                $session->getFlashBag()->add('Danger', 'Wystapił błąd w formularzu.');
            }
        }

        return array(
            'form' => $form->createView(),
        );        
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_gallery_edit"
     * )
     * @Template("@MiuzeAdmin/Gallery/add.html.twig")
     */
    public function editAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Gallery');
        $entity = $repo->findOneBy(array('id' =>$id));
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $galleryForm = $this->createForm(GalleryType::class, $entity);        
        if($request->isMethod('POST')){
            $galleryForm->handleRequest($request);
            if($galleryForm->isValid()){  
                
                $functions = $this->get('utils_service');
                $entity->seturlTitle($functions->slugify($entity->getName()));
                
                $em = $this->getDoctrine()->getManager();
                $entity->preUpload();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_gallery_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił błąd w formularzu.');
            }
        }        
        return array(
            'form' => $galleryForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_gallery_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Gallery');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Galeria usunieta prawidłowo');
        return $this->redirect($this->generateUrl('admin_gallery_index'));
    }
}
