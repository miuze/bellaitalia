<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Download;
use Miuze\AdminBundle\Entity\DownloadCategory;
use Miuze\AdminBundle\Entity\Registry;
use Miuze\AdminBundle\Form\Download\DownloadCategoryType;
use Miuze\AdminBundle\Form\Download\DownloadType;
/**
     * @Route(
     *      "/download"
     * )
     */
class DownloadController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_download_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction($page)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Download');
        $list = $repo->findBy(array(), array('createDate' => 'DESC'));
        $paginator  = $this->get('knp_paginator');            
        $pagination = $paginator->paginate($list, $page ,15);
        return array(
            'list' => $list,
            'paginator' => $pagination,
        );
    }
    
    /**
     * @Route(
     *      "/downloadcategorylist/{categoryId}/{page}",
     *      name="admin_download_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     *      requirements={"page": "\d+", "categoryId": "\d+"}
     * )
     * @Template("MiuzeAdminBundle:Download:index.html.twig")
     */
    public function downloadListCategoryAction($categoryId, $page)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Download');
            $list = $repo->findBy(array('category' => $categoryId ),  array('createDate' => 'DESC'));
            $paginator  = $this->get('knp_paginator');            
            $pagination = $paginator->paginate($list, $page ,15);
            return array(
                'list' => $list,
                'paginator' => $pagination,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add/{categoryId}",
     *      name = "admin_download_add",
     *      defaults = {"categoryId" = 0}
     * )
     * @Template()
     */
    public function addAction(Request $Request, $categoryId)
    {
        $session = $this->get('session');
        $entity = new Download(); 
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:DownloadCategory')->findOneBy(array('id' => $categoryId));
        if ($cat) {
           $entity->setCategory($cat); 
        }
        
        $form = $this->createForm(DownloadType::class, $entity); 
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);            
            if($form->isValid()){      
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                //               zapis w rejestrze zmian
                $registry = new Registry();
                $registry->setDownload($entity);
                $user = $this->get('security.token_storage')->getToken()->getUser();
                $registry->setUser($user);
                $registry->setType('Dodano treść.');
                $em->persist($registry);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, Opis plików dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_download_listcategory', array('categoryId' => $categoryId)));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_download_edit"
     * )
     * @Template("MiuzeAdminBundle:Download:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Download');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( DownloadType::class, $entity);        
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);
            if($form->isValid()){  
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager(); 
                $em->persist($entity);
                $em->flush();
                
                //               zapis w rejestrze zmian
                $registry = new Registry();
                $registry->setDownload($entity);
                $registry->setType('Edytowano treść.');
                $user = $this->get('security.token_storage')->getToken()->getUser();
                $registry->setUser($user);
                $em->persist($registry);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, opis plików zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_download_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_download_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Download');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Opis plików został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_download_index'));
    }
}
