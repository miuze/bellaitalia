<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Miuze\AdminBundle\Controller;


use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Entity\Top;
use Miuze\AdminBundle\Form\Top\TopType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
/**
 * Description of TopController
 *
 * @author Kamil
 */
/**
     * @Route(
     *      "/top")
     */
class TopController extends Controller{
        
    
    
    /**
     * @Route(
     *      "/add/{page}",
     *      name = "admin_top_add",
     *      defaults = {"page" = 0}
     * )
     * @Template("@MiuzeAdmin/Top/add.html.twig")
     */
    public function addAction(Request $Request, $page)
    {
        $session = $this->get('session');        
        $tops = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Top')->findAll();  
        $top = new Top();
        
        $topForm = $this->createForm( TopType::class, $top); 
        if($Request->isMethod('POST')){
            $topForm->handleRequest($Request);            
            if($topForm->isValid()){          
                $em = $this->getDoctrine()->getManager();
                $em->persist($top);
                $em->flush();
                
                $session->getFlashBag()->add('success', 'Gratulacje, top dadany prawidłowo!');
                return $this->redirect($this->generateUrl('admin_top_add'));                
            }else{
                $session->getFlashBag()->add('warning', 'Coś poszło nie tak przy walidacji sprawdź formularz');
            }
        }        
        if($page == 0){
            return array(
                'form' => $topForm->createView(),
                'tops' => $tops,
                );
        }else{
            $page = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Page')->findOneBy(array('id' => $page));
            return array(
                'form' => $topForm->createView(),
                'tops' => $tops,
                'page'  => $page
            );
        }
    }
    
    /**
     * @Route(
     *      "/bind/{top}/{page}",
     *      name = "admin_top_bind",
     *      defaults = {"id" = 0}
     * )
     */
    public function bindAction(Request $Request, $top, $page)
    {
        $session = $this->get('session');
        $em = $this->getDoctrine()->getManager();
        $repo1 = $em->getRepository('MiuzeAdminBundle:Page');
        $repo2 = $em->getRepository('MiuzeAdminBundle:Top');
        $page = $repo1->findOneBy(array('id' => $page));        
        $tops = $repo2->findOneBy(array('id' => $top)); 
        if(NULL == $page || NULL == $tops){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $page->setTop($tops);
        $em->persist($page);
        $em->flush();
                
        $session->getFlashBag()->add('success', 'Gratulacje, top dadany prawidłowo!');
        return $this->redirect($this->generateUrl('admin_pages_index'));
    }
    
    /**
     * @Route(
     *      "/del/{id}",
     *      name="admin_top_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Top');
        $row = $repo->findOneById($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        
        $session->getFlashBag()->add('success', 'Gratulacje, top został usuniety.');
        return $this->redirect($this->generateUrl('admin_top_add'));
    }
}
