<?php

namespace Miuze\AdminBundle\Controller;

use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Entity\Section;
use Miuze\AdminBundle\Form\Page\PageType;
use Miuze\AdminBundle\Manager\PageManager;
use Miuze\AdminBundle\Manager\PageSortManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/pages")
 */
class PagesController extends Controller
{
    /**
     * @Route("/list/{menu}",
     *      name="admin_pages_index",
     * )
     * @Template("@MiuzeAdmin/Pages/index.html.twig")
     */
    public function indexAction(Request $request, $menu)
    {
        $menuId = $request->attributes->getInt('menu');
        $repo = $this->getDoctrine()->getRepository(Page::class);

        $pages = $repo->getAdminAllCategoriesToTree($menuId);
        if (!empty($pages)) {
            $options = array(
                'decorate' => true,
                'level0Open' => '<ol class="pageView sortable">',
                'level0Close' => '</ol>',
                'rootOpen' => function ($node) {
                    return '<ol id="p' . $node["id"] . '" class="pageView"  style="height: 0px; display: none;">';
                },
                'rootClose' => '</ol>',
                'childOpen' => function ($node) {
                    return '<li id="e' . $node["id"] . '" class="sortli">';
                },
                'childClose' => '</li>',
                'nodeDecorator' => function ($node) {
                    if ($node['visable'] === false) {
                        $iconHide = '<i class="fa fa-eye-slash" aria-hidden="true" style="color: red;"></i>';
                        $hideTitle = 'Pokaż ';
                    } else {
                        $iconHide = '<i class="fa fa-eye" aria-hidden="true" style="color: green"></i>';
                        $hideTitle = 'Ukryj ';
                    }
                    if ($node['active'] === false) {
                        $iconDisable = '<i class="fa fa-power-off" aria-hidden="true" style="color: red"></i>';
                        $disableTitle = 'Włącz ';
                    } else {
                        $iconDisable = '<i class="fa fa-power-off" aria-hidden="true" style="color: green"></i>';
                        $disableTitle = 'Wyłącz ';
                    }
                    !empty($node['__children']) ? $more = '<span class="fa fa-plus"></span>' : $more = '';
                    return '<a href="/parentId/' . $node["id"] . '" class="treeNode expand">' .
                        '<span class="cover"></span>' .
                        $more .
                        '</a>' .
                        '<article class="dd-handle item-list">' .
                        '<div class="col-xs-12 col-sm-6">' .
                        '<p class="title-item">' . $node["slug"]['name'] . '</p>' .
                        '<a href="' . $this->generateUrl("admin_pages_add", array("menu" => $node["category"]["id"], "id" => $node["id"])) . '" title="Edytuj"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>' .
                        '<a href="' . $this->generateUrl("admin_top_add", array("page" => $node["id"])) . '" title="Dodaj zdjecie" ><i class="fa fa-camera" aria-hidden="true"></i></a>' .
                        '<a href="' . $this->generateUrl("admin_pages_visable", array("id" => $node["id"])) . '" title="' . $hideTitle . ' w menu">' . $iconHide . '</a>' .
                        '<a href="' . $this->generateUrl("admin_pages_disable", array("id" => $node["id"])) . '" title="' . $disableTitle . ' w menu">' . $iconDisable . '</a>' .
                        '<a href="' . $this->generateUrl("admin_pages_delete", array("id" => $node["id"])) . '" title="Usuń" class="confirm"><i class="fa fa-trash" aria-hidden="true"></i></a>' .
                        '</div>' .
                        '<div class="col-xs-12 col-sm-6">' .
                        '<a class="pull-right btn btn-info" href="' . $this->generateUrl("admin_pages_add", array("menu" => $node["category"]["id"], "parent" => $node["id"])) . '" title="Dodaj podstronę">Dodaj podstronę</a>' .
                        '</div>' .
                        '</article>';
                }


            );
            $tree = $repo->buildTreeArray($pages);
            $managerTree = $this->get('tree');
            $htmlTree = $managerTree->buildTreeHtmlAdmin($tree, $options);
        } else {
            $htmlTree = null;
        }
        return [
            'pageTree' => $htmlTree,
        ];
    }

    /**
     * @Route(
     *      "/add/menu-{menu}/id-{id}/parnet-{parent}",
     *      name="admin_pages_add",
     *      defaults = {"parent" = 0, "id"=0}
     * )
     * @Template("@MiuzeAdmin/Pages/add.html.twig")
     */
    public function addAction(Request $request)
    {
        $pageManager = $this->get(PageManager::class);
        $page = $pageManager->getPage();
        $form = $this->createForm(PageType::class, $page);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $redirectList = $pageManager->persist($page);
                $redirectEdit = $this->generateUrl(
                    'admin_pages_add',
                    [
                        'menu' => $page->getCategory()->getId(),
                        'id' => $page->getId()
                    ]
                );
                $url = $form->get('saveAndEdit')->isClicked() ? $redirectEdit : $redirectList;
                return $this->redirect($url);
            } else {
                $pageManager->setMessage('Wystapił błąd w formularzu.', 'danger');
            }
        }

        $response = [
            'form' => $form->createView(),
            'page' => $page,
        ];

        if (!is_null($page->getId())) {
            $response = array_merge(
                $response,
                [
                    'isEdit' => true,
                    'sections' => $this->getDoctrine()
                        ->getRepository(Section::class)
                        ->findByPage($page->getId())
                ]
            );
        }

        return $response;
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_pages_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $pageManager = $this->get(PageManager::class);
        return $this->redirect($pageManager->remove());
    }

    /**
     * @Route(
     *      "/disable/{id}",
     *      name="admin_pages_disable",
     *      defaults = {"id" = 0}
     * )
     */
    public function disableAction($id)
    {
        $pageManager = $this->get(PageManager::class);
        return $this->redirect($pageManager->disable());
    }

    /**
     * @Route(
     *      "/hide/{id}",
     *      name="admin_pages_visable",
     *      defaults = {"id" = 0}
     * )
     */
    public function visableAction(Request $request)
    {
        $pageManager = $this->get(PageManager::class);
        return $this->redirect($pageManager->hideMenu());
    }

    /**
     * @Route(
     *      "/sort",
     *      name="admin_pages_sort",
     * )
     */
    public function sortAction(Request $request)
    {
        $pageSortManager = $this->get(PageSortManager::class);
        return $pageSortManager->sort();
    }


}
