<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Banner;
use Miuze\AdminBundle\Entity\BannerCategory;
use Miuze\AdminBundle\Form\Banner\BannerCategoryType;
use Miuze\AdminBundle\Form\Banner\BannerType;
/**
     * @Route(
     *      "/banner-category"
     * )
     */
class BannerCategoryController extends Controller
{
           
    /**
     * @Route(
     *      "/",
     *      name = "admin_banner-category_index"
     * )
     *  @Template("@MiuzeAdmin/BannerCategory/index.html.twig")
     */
    public function indexAction(Request $Request)
    {       
        $lang = $Request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:BannerCategory');
        $list = $repo->getAllCategories(array('lang' => $lang));
        
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_banner-category_add"
     * )
     *  @Template("@MiuzeAdmin/BannerCategory/add.html.twig")
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $category = new BannerCategory();   
                
        $form = $this->createForm( BannerCategoryType::class, $category); 
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);            
            if($form->isValid()){    
                $lang = $Request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_banner-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_banner-category_edit"
     * )
     * @Template("@MiuzeAdmin/BannerCategory/add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:BannerCategory');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( BannerCategoryType::class, $entity);        
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);
            if($form->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, category zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_banner-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_banner-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:BannerCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategorie usunieto prawidłowo');
        return $this->redirect($this->generateUrl('admin_banner-category_index'));
    }
    
}
