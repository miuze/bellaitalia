<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Miuze\AdminBundle\Entity\Newsletter;
use Miuze\AdminBundle\Form\Newsletter\NewsletterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Miuze\AdminBundle\Mailer\NewsletterMailer;
use Miuze\AdminBundle\Form\Offer\OfferTemplateType;
use Miuze\AdminBundle\Entity\Offer;

/**
     * @Route(
     *      "/offer"
     * )
     */
class OfferController extends Controller
{
    protected $miuzeMailer;

    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_offer_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction(Request $request)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Offer');
        $query = $repo->getAllQuery();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->attributes->getInt('page') ,
            30
        );
        return array(
            'pagination' => $pagination,
        );
    }
    


    
    /**
     * @Route(
     *      "/create",
     *      name = "admin_offer_create",
     * )
     * @Template()
     */
    public function createAction(Request $request, $send)
    {
        $session = $this->get('session');
        $offer = new Offer();
        
        $form = $this->createForm( OfferTemplateType::class, $offer);
        if($request->isMethod('POST')){
            $form->handleRequest($request);            
            if($form->isValid()){
                
                $em =$this->getDoctrine()->getManager();
                $em->persist($offer);
                $em->flush();
                return $this->redirect($this->generateUrl('admin_offer_details', array(
                    'id' => $offer->getId()
                )));
              
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_offer_edit",
     * )
     * @Template()
     */
    public function editAction(Request $request)
    {
        $session = $this->get('session');
        $offer = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Offer')->getDetails(array('id' => $request->attributes->getInt('id')));
        if($offer === null){
            $session->getFlashBag()->add('warning', 'Nie istnieje taka oferta.');
            return $this->redirect($this->generateUrl('admin_offer_index'));
        }
        $form = $this->createForm( OfferTemplateType::class, $offer);
        if($request->isMethod('POST')){
            $form->handleRequest($request);            
            if($form->isValid()){
                $promarray = array();
                $it = 0;
                if(null === $offer->getEmail()){
                    if(null !== $offer->getSubscriber()[0]){
                        $offer->setEmail($offer->getSubscriber()[0]->getEmail());
                    }else{
                        $session->getFlashBag()->add('danger', 'Brak adresu email.');
                        return array(
                            'form' => $form->createView(),
                        );
                    }
                }
                $em =$this->getDoctrine()->getManager();
                $em->persist($offer);
                $em->flush();
              
            }else{
                $session->getFlashBag()->add('danger', 'Wystapił problem sprawdź formularz.');
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route(
     *      "/details/{id}",
     *      name = "admin_offer_details"
     * )
     * @Template()
     */
    public function detailsAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Offer');
        $offer = $repo->findOneById($id);
        $htmlBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:offer.html.twig', array(
            'offer' => $offer
        ));
        return array(
            'htmlBody' => $htmlBody,
        );
    }
    
    /**
     * @Route(
     *      "/send/{id}",
     *      name = "admin_offer_send"
     * )
     * @Template()
     */
    public function sendAction(Request $request)
    {
        $session = $this->get('session');
        $id = $request->attributes->getInt('id');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Offer');
        $offer = $repo->findOneById($id);
        $htmlBody = $this->renderView('MiuzeAdminBundle:EmailTemplate:offer.html.twig', array(
            'offer' => $offer
        ));
        $mailer = $this->get('admin_offer_sender');
        $title = 'ST-'.$offer->getId();
        $mailer->send($title, $htmlBody, $offer->getEmail());
        $session->getFlashBag()->add('success', 'Oferta wysłana.');
//        return $this->redirect($this->generateUrl('admin_offer_index'));
        return array();
    }


    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_offer_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction(Request $request)
    {
        $session = $this->get('session');
        $id = $request->attributes->getInt('id');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Offer');
        $entity = $repo->findOneBy(array('id' => $id));
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $session->getFlashBag()->add('success', 'Oferta została usunięta!');
        return $this->redirect($this->generateUrl('admin_offer_index'));
    }
    

}
