<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Client;
use Miuze\AdminBundle\Entity\ClientCategory;
use Miuze\AdminBundle\Form\Client\ClientCategoryType;
use Miuze\AdminBundle\Form\Client\ClientType;
/**
     * @Route(
     *      "/client"
     * )
     */
class ClientController extends Controller
{
        
    /**
     * @Route(
     *      "/",
     *      name = "admin_client_index"
     * )
     * @Template()
     */
    public function indexAction()
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Client');
        $list = $repo->findAll(array(), array('date' => 'DESC'));
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/clientlistcategory/{id}",
     *      name="admin_client_listCategory",
     *      defaults={"id": "0"},
     * )
     * @Template("MiuzeAdminBundle:Client:index.html.twig")
     */
    public function clientListCategoryAction($id)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Client');
            $list = $repo->findBy(array('category' => $id), array('createDate' => 'DESC'));
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate($list, $id ,2);

            return array(
                'list' => $pagination,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_client_add"
     * )
     * @Template()
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $client = new Client();    
                
        $form = $this->createForm( ClientType::class, $client); 
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);            
            if($form->isValid()){          
                
                $functions = $this->get('utils_service');
                $client->setUrlTitle($functions->slugify($client->getName()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, Clienta dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_client_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_client_edit"
     * )
     * @Template("MiuzeAdminBundle:Client:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Client');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $form = $this->createForm( ClientType::class, $entity);        
        if($Request->isMethod('POST')){
            $form->handleRequest($Request);
            if($form->isValid()){     
                
                $functions = $this->get('utils_service');
                $entity->setUrlTitle($functions->slugify($entity->getName()));
                $em = $this->getDoctrine()->getManager();
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, klienta zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_client_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $form->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_client_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Client');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Wpis został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_client_index'));
    }
    
    
}
