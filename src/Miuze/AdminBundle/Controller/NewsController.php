<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\News;
use Miuze\AdminBundle\Entity\NewsCategory;
use Miuze\AdminBundle\Form\News\NewsCategoryType;
use Miuze\AdminBundle\Form\News\NewsType;
/**
     * @Route(
     *      "/news"
     * )
     */
class NewsController extends Controller
{
        
    /**
     * @Route(
     *      "/{page}",
     *      name = "admin_news_index",
     *      defaults={"page" = 1},
     *      requirements={"page": "\d+"}
     * )
     * @Template()
     */
    public function indexAction($page)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:News');
        $newsList = $repo->findBy(array(), array('createDate' => 'DESC'));
        $paginator  = $this->get('knp_paginator');            
        $pagination = $paginator->paginate($newsList, $page ,15);
        return array(
            'newsList' => $newsList,
            'paginator' => $pagination,
        );
    }
    
    /**
     * @Route(
     *      "/newscategorylist/{categoryId}/{page}",
     *      name="admin_news_listcategory",
     *      defaults={"categoryId": "0", "page" = 1},
     * )
     * @Template("MiuzeAdminBundle:News:index.html.twig")
     */
    public function newsListCategoryAction($categoryId, $page)
    {
        
        try{
            $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:News');
            $newsList = $repo->findBy(array('category' => $categoryId ),  array('createDate' => 'DESC'));
            $paginator  = $this->get('knp_paginator');            
            $pagination = $paginator->paginate($newsList, $page ,15);
            return array(
                'newsList' => $newsList,
                'paginator' => $pagination,
            );
        } catch (Exception $ex) {
            throw $this->createNotFoundException('Nie znaleziono');
        }
        
    }
    
    /**
     * @Route(
     *      "/add/{categoryId}",
     *      name = "admin_news_add",
     *      defaults = {"categoryId" = 0}
     * )
     * @Template()
     */
    public function addAction(Request $Request, $categoryId)
    {
        $session = $this->get('session');
        $news = new News();  
        $cat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:NewsCategory')->findOneBy(array('id' => $categoryId));
        if ($cat) {
           $news->setCategory($cat); 
        }
        
        $newsForm = $this->createForm(NewsType::class, $news); 
        if($Request->isMethod('POST')){
            $newsForm->handleRequest($Request);            
            if($newsForm->isValid()){      
                
                $functions = $this->get('utils_service');
                $news->setSeoTitle($functions->slugify($news->getTitle()));
                $em = $this->getDoctrine()->getManager();
                $em->persist($news);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, News dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_news_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $newsForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_news_edit"
     * )
     * @Template("MiuzeAdminBundle:News:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:News');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $newsForm = $this->createForm( NewsType::class, $entity);        
        if($Request->isMethod('POST')){
            $newsForm->handleRequest($Request);
            if($newsForm->isValid()){  
                
                $functions = $this->get('utils_service');
                $entity->setSeoTitle($functions->slugify($entity->getTitle()));
                $em = $this->getDoctrine()->getManager();                
                $entity->preUpload();
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, news zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_news_index'));
                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $newsForm->createView()
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_news_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:News');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'News został usuniety prawidłowo.');
        return $this->redirect($this->generateUrl('admin_news_index'));
    }
}
