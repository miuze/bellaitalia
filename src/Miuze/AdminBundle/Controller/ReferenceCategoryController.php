<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Reference;
use Miuze\AdminBundle\Entity\ReferenceCategory;
use Miuze\AdminBundle\Form\Reference\ReferenceCategoryType;
use Miuze\AdminBundle\Form\Reference\ReferenceType;
/**
     * @Route(
     *      "reference-category"
     * )
     */
class ReferenceCategoryController extends Controller
{
        
    
    /**
     * @Route(
     *      "/category",
     *      name = "admin_reference-category_index"
     * )
     * @Template("@MiuzeAdmin/ReferenceCategory/index.html.twig")
     */
    public function indexAction(Request $request)
    {        
        $lang = $request->getLocale();
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ReferenceCategory');
        $list = $repo->getAllCategories(array('lang' => $lang));
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_reference-category_add"
     * )
     * @Template("@MiuzeAdmin/ReferenceCategory/add.html.twig")
     */
    public function addAction(Request $request)
    {
        $session = $this->get('session');
        $category = new ReferenceCategory();   
                
        $categoryForm = $this->createForm( ReferenceCategoryType::class, $category); 
        if($request->isMethod('POST')){
            $categoryForm->handleRequest($request);
            if($categoryForm->isValid()){     
                $lang = $request->getLocale();
                $lg = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Lang')->findOneBy(array('lang' => $lang));
                $category->setLang($lg);
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_reference-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_reference-category_edit"
     * )
     * @Template("@MiuzeAdmin/ReferenceCategory/add.html.twig")
     */
    public function editAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ReferenceCategory');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $categoryForm = $this->createForm( ReferenceCategoryType::class, $entity);        
        if($request->isMethod('POST')){
            $categoryForm->handleRequest($request);
            if($categoryForm->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, category zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_reference-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_reference-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:ReferenceCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategorie usunieto prawidłowo');
        return $this->redirect($this->generateUrl('admin_reference-category_index'));
    }
}
