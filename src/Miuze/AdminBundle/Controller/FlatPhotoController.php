<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Miuze\AdminBundle\Entity\Flat;
use Miuze\AdminBundle\Entity\FlatPhoto;
use Miuze\AdminBundle\Form\Flat\BuildType;
use Miuze\AdminBundle\Form\Flat\FlatPhotoType;
use Miuze\AdminBundle\Form\Flat\FlatType;

/**
     * @Route(
     *      "/flat-photo",
     * )
     */
class FlatPhotoController extends Controller
{
    
    /**
     * @Route(
     *      "/upload/{id}",
     *      name="admin_flat-photo_upload",
     * )
     * @Template("@MiuzeAdmin/FlatPhoto/upload.html.twig")
     */
    public function uploadAction(Request $request)
    {    
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        //list zdjęć
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:FlatPhoto');
        $photoList = $repo->findBy(array('flat' => $id));
        
        //pobieranie kategorii 
        $flat = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Flat')->findOneById($id);
        //tworzenie obiektu i ustawienie id
        $photo = new FlatPhoto();
        if ($flat) {
           $photo->setFlat($flat); 
        }
        
        $form = $this->createForm( FlatPhotoType::class, $photo, array(
            'attr' => array(
                'id' => 'upload'
            )
        ));
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($photo);
                $em->flush();
                $res = array(
                    "status" => "success",
                    "path" => $photo->getPath().$photo->getImage(),
                    "links" => array( 
                        "delete" => $this->generateUrl('admin_flat_photo_delete', array(
                            "category" =>  $id,
                            "id" => $photo->getId()
                        )),
                        "setImage" => $this->generateUrl('admin_flat-photo_set', array(
                            "id" => $photo->getId()
                        ))
                                
                    )
                );
                return new JsonResponse($res);
            }else{
                $err='';
                foreach ($form->getErrors(true) as $e){
                    $err .= $e->getMessage().', ';
                }
                $res = array(
                    "status" => "error",
                    "msg" => $err
                );
                return new JsonResponse($res);
            }
        }

        return array(
            'form' => $form->createView(),
            'photoList' => $photoList,
        );        
    }
    
    /**
     * @Route(
     *      "/delete/{id}/category/{category}",
     *      name="admin_flat_photo_delete",
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $category = $request->attributes->getInt('category');
        $session = $this->get('session');

        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:FlatPhoto');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono zdjęcia');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Gratulacje, zdjęcie usunieto prawidłowo.');
        return $this->redirect($this->generateUrl('admin_flat-photo_upload', array('id' => $category)));
    }
    
    
    
    /**
     * @Route(
     *      "/setimage/{id}",
     *      name="admin_flat-photo_set",
     * )
     * @Template()
     */
    public function setAction(Request $request)
    {    
        $id = $request->attributes->getInt('id');
        $session = $this->get('session');
        //list zdjęć
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:FlatPhoto');
        $photo = $repo->findOneById($id);
        $repo1 = $this->getDoctrine()->getRepository('MiuzeAdminBundle:Flat');
        $flat = $repo1->findOneById($photo->getFlat()->getId());

        $flat->setImage($photo->getImage());
        $flat->setPath($photo->getPath());
        $em = $this->getDoctrine()->getManager();
        $em->persist($flat);
        $em->flush();
        
        $session->getFlashBag()->add('success', 'Gratulacje, zdjęcie wyróżniające przypisano prawidłowo.');
        return $this->redirect($this->generateUrl('admin_flat-photo_upload', array('id' => $flat->getId())));
    }
}