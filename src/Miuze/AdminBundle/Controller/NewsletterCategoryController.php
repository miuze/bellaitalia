<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Miuze\AdminBundle\Entity\Newsletter;
use Miuze\AdminBundle\Entity\NewsletterCategory;
use Miuze\AdminBundle\Form\Newsletter\NewsletterCategoryType;
use Miuze\AdminBundle\Form\Newsletter\NewsletterType;
/**
     * @Route(
     *      "/newsletter-category"
     * )
     */
class NewsletterCategoryController extends Controller
{
           
    /**
     * @Route(
     *      "/",
     *      name = "admin_newsletter-category_index"
     * )
     * @Template()
     */
    public function indexAction(Request $Request)
    {        
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:NewsletterCategory');
        $list = $repo->findAll();
                
        return array(
            'list' => $list,
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name = "admin_newsletter-category_add"
     * )
     * @Template()
     */
    public function addAction(Request $Request)
    {
        $session = $this->get('session');
        $category = new NewsLetterCategory();   
                
        $categoryForm = $this->createForm( NewsletterCategoryType::class, $category); 
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);            
            if($categoryForm->isValid()){          
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię dodano prawidłowo');
                return $this->redirect($this->generateUrl('admin_newsletter-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView(),
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name = "admin_newsletter-category_edit"
     * )
     * @Template("MiuzeAdminBundle:NewsletterCategory:add.html.twig")
     */
    public function editAction(Request $Request, $id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:NewsletterCategory');
        $entity = $repo->find($id);
        
        if(NULL == $entity ){
            throw $this->createNotFoundException('Nie znaleziono');
        }       
        $categoryForm = $this->createForm( NewsletterCategoryType::class, $entity);        
        if($Request->isMethod('POST')){
            $categoryForm->handleRequest($Request);
            if($categoryForm->isValid()){     
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $session->getFlashBag()->add('success', 'Gratulacje, kategorię zapisano prawidłowo');
                return $this->redirect($this->generateUrl('admin_newsletter-category_index'));                
            }else{
                $session->getFlashBag()->add('danger', 'Wystąpił błąd, sprawdź formularz');
            }
        }        
        return array(
            'form' => $categoryForm->createView()
        );
    }
    
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="admin_newsletter-category_delete",
     *      defaults = {"id" = 0}
     * )
     */
    public function deleteAction($id)
    {
        $session = $this->get('session');
        $repo = $this->getDoctrine()->getRepository('MiuzeAdminBundle:NewsletterCategory');
        $row = $repo->find($id);
        if(NULL == $row ){
            throw $this->createNotFoundException('Nie znaleziono');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($row);
        $em->flush();
        $session->getFlashBag()->add('success', 'Kategorie usunieto prawidłowo');
        return $this->redirect($this->generateUrl('admin_newsletter-category_index'));
    }
    
}
