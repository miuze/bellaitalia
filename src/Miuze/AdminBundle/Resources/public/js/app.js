var _animationSpeed = 10;

$(document).ready(function() {
    expandAndDisableButton();
    cscroller();
    accordion();
    ckeditorRender();
    showmsg();
    $(".selected").delay(3000).trigger('click');
    confirmButton();
    date();
    submitsort();
    lang();
    color();
    sortables();
    _bindExpand($('.main'));
    $('.sortable').nestedSortable({
        handle: 'article',
        items: 'li',
        toleranceElement: '> article',
        distance: 10,
        placeholder: 'sortPlaceholder',
        helper: 'clone',
        cursor: 'resize',
        start: function () {
            $('.cover').parent().removeClass('last');
        },
        stop: function () {
            _initLast();
            _recalculateCover();
        },
        update: function (event, ui) {
            $(ui.item).parent('ol').addClass('pageView');
            updateNodeTree(ui);
            _initLast();
            _recalculateCover();
        }
    });
});
function expandAndDisableButton() {
    if ($('.dd').length) {
        let rootLink = $('.dd li').first().find('a').first();
        setTimeout(function () {
            $(rootLink[0]).trigger('click');
            $(rootLink[0]).remove();
        }, 500)

    }
}
function updateNodeTree(element){
    var page = {id: null, parentId: null, next: null, prev: null};
    var parent = $(element.item[0]).parent().parent();
    page.id = $(element.item[0]).attr('id').split('e')[1];
    var nextPage = $(element.item[0]).next();
    var prevPage = $(element.item[0]).prev();
    if(nextPage.length > 0){
        page.next = nextPage.attr('id').split('e')[1];
    }
    if(prevPage.length > 0){
        page.prev = prevPage.attr('id').split('e')[1];
    }
    if(!parent.hasClass('dd')){
        page.parentId = parent.attr('id').split('e')[1];
    }
    console.log(page);
    $.ajax({
        method: 'post',
        url: $('.dd').data('sort-url'),
        dataType : 'json',
        data: page,
        // contentType: "application/json",
    }).success(function(response) {
        if(response.status === 'danger'){
            alert('Operacja niedzowolona')
            // location.reload();
        }
        renderMessage(response);
    }).error(function(){
        alert('Operacja nieddozwolona');
        // location.reload();
    });
}
function sortables(){
    if($('.sortables').length){
        $('.sortables').sortable({
            update: function (event, ui) {
                var order = $(this).sortable('toArray').toString();
                $('#sort').val(order);
            }
        });
    } ;
}
function lang(){
   $('.lang').on('change', function(){
        var l = $(this).find(':selected').data('t');        
        window.location=l
    }); 
}
function submitsort(){
    $('.sortdata').on('click', function(e){
        e.preventDefault();
        $('#formSort').submit();
    });
}

function _initLast() {
    $('.cover').each(function () {
        $(this).parent().removeClass('last');
    });
    $('ol.pageView', $('#content')).each(function () {
        $(this).find('li:last-child').children('a.treeNode').addClass('last');
    });
}
function _recalculateCover() {
    $('.cover').each(function () {
        h = $(this).parents('li').first().height();
        h = h - 29;
        $(this).css({
            height: h
        });
    });
}
function _bindExpand(scope) {
    $('.expand', scope).on('click', function (e) {
        e.preventDefault();
        var elem = $(this);
            ol = elem.siblings('ol');
            h = ol.attr('old-height');

            ol.removeAttr('style').css({
                height: 0
            });

            ol.animate({
                height: h
            }, _animationSpeed).promise().done(function () {
                //$(this).css('display', 'block');
                $(this).removeAttr('style');
                _initLast();
                _recalculateCover();

                _bindCollapse(elem);
                el = elem.removeClass('expand').addClass('collapse');
                el.children('span[class^=fa]').removeClass('fa-plus').addClass('fa-minus');
            });
//        }
    });
}
function _bindCollapse(element) {
    element.off('click');
    element.on('click', function (e) {
        e.preventDefault();
        $('.cover').removeAttr('style').css({
            display: 'none'
        });
        element.siblings('ol').attr('old-height', element.siblings('ol').first().height());
        element.siblings('ol').animate({
            height: 0
        }, _animationSpeed).promise().done(function () {
            $(this).css('display', 'none');
            $('.cover').removeAttr('style');
            element.off(e);

            element.children('span[class^=fa]').removeClass('fa-minus').addClass('fa-plus');
            _recalculateCover();
            elem = element.removeClass('collapse').addClass('expand');
            _bindExpand(elem.parent());
        });
    });
}


function date(){
    if($('.datepicker').length){
        $( ".datepicker" ).datepicker({ 
            dateFormat: 'dd-mm-yy' 
        });
    }
}
function confirmButton() {
    if($('.confirm').length){        
        $('.confirm').on('click', function(e){
            var option = confirm("Potwierdź usunięcie");
            if(option != true){
                e.preventDefault();
            }     
        });
    }
}


function accordion(){
    if($('#accordion-sidebar')){
        $('#accordion-sidebar').accordion({
//            firstChildExpand: true,
            multiExpand: false,
            speed: 'fast', 
            dropDownIcon: "&#9660"
        });
    }
}
function ckeditorRender(){
    $('.render-editor').on('click', function(e){
        e.preventDefault()
        var selector = $(this).data('toggle');

        tinyMCE.init({
            lang: "pl",
            // General options
            content_css : "/bundles/miuzepage/css/style.css",
            mode : "textareas",
            convert_urls : true,
            entity_encoding : "raw",
            plugins : "advlist autolink link image lists charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking table contextmenu directionality emoticons paste textcolor responsivefilemanager code internallinking insertspecial",
            // Theme options
            toolbar1 : "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | outdent indent | styleselect",
            toolbar2 : "| responsivefilemanager | link unlink anchor internallinking insertspecial | image media | print preview code | bullist numlist | forecolor backcolor",
            image_advtab : true,
            selector : selector,
            theme : "modern",
            width : "98%",
            height: "300",
            language : "pl",
            apply_source_formatting : true,
            relative_urls : false,
            remove_script_host : true,
            convert_fonts_to_spans : true,
            font_size_classes : "font1,fontSize2,fontSize3,fontSize4,fontSize5,fontSize6,fontSize7",
            paste_create_paragraphs : 0,
            paste_create_linebreaks : 0,
            paste_use_dialog : 1,
            paste_auto_cleanup_on_paste : 1,
            paste_convert_middot_lists : 0,
            paste_unindented_list_class : "unindentedList",
            paste_convert_headers_to_strong : 1,
            style_formats_merge : 1,
            external_filemanager_path:"/bundles/miuzeadmin/js/filemanager/",
            filemanager_title:"Responsive Filemanager" ,
            verify_html: false,
            external_plugins: { "filemanager" : "/bundles/miuzeadmin/js/filemanager/plugin.min.js"}
            // setup: function(editor) {
            //     console.log(editor);
            //     function toTimeHtml(date) {
            //         return '<time datetime="' + date.toString() + '">' + date.toDateString() + '</time>';
            //     }
            //
            //     function insertDate() {
            //         var html = toTimeHtml(new Date());
            //         editor.insertContent(html);
            //     }
            //
            //     editor.addButton('currentdate', {
            //         icon: 'insertdatetime',
            //         //image: 'http://p.yusukekamiyamane.com/icons/search/fugue/icons/calendar-blue.png',
            //         tooltip: "Insert Current Date",
            //         onclick: insertDate
            //     });
            // }

        });
        $(this).remove();
    });
}

function renderMessage(data){
    $('#wrapper').append(
        '<div class="site-message alert">' +
        '<div class="alert alert-' + data.status + '">' +
        data.msg +
        '</div>' +
        '</div>'
    );
    showmsg();
}
function showmsg(){
    if($('.site-message').length){        
        $('.site-message').addClass('show-msg');
        setTimeout(function () {
            $('.site-message').removeClass('show-msg');
        }, 4000);        
    }
}
function cscroller(){
    $('.side-nav').mCustomScrollbar({
        theme:"light-3"
    });
}
function color(){
    if($('#product_color_color').length){
        $('#product_color_color').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
		$(el).val('#'+hex);
		$(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                    $(this).ColorPickerSetColor(this.value);
            }
        })
        .bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
        });
    }
}