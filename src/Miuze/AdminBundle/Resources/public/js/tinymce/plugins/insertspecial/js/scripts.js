$(document).ready(function() {
    $('#insert').click(function() {
        parse();
    });

    $('#content').change(function() {
        var ajdi = $(this).val();
        ajdi = '#' + ajdi;
        $('div#settingsAll > div').hide();
        $(ajdi).show();
    });

    $('#facebook #boxType').change(function() {
        $('#facebook .fbHide').hide();
        id = $(this).val();
        id = '.' + id;

        $(id).show();
    });

    //createNewsOptions($('#news'));    
    createGalleryOptions($('.checkboxes'));
    editCurrent(top.tinymce.activeEditor.selection.getContent({
        format: 'text'
    }));
});

function parse() {
    var elementA = $('#content').val();
    var elemaId = '#' + elementA;

    var aOpts = {};
    var galleryArr = [];
    var galIt = 0;
    var aId = '';
    var elType = '';
    var nhtml = '';
    var cbName = '';

    var elel = $('div:visible', $(elemaId));
    $('input[type=text], input[type=checkbox]:checked, select', elel).each(function(index, element) {
        elType = element.tagName;
        aId = $(this).attr('id');
        idid = $(element, elel).attr('id');

        switch (elType) {
            case 'INPUT':
                var subtype = $(this).attr('type');
                if (subtype == 'checkbox' && $(this).hasClass('galleries')) {
                    cbName = $(this).attr('name');
                    cbName = cbName.split('[]')[0];
                    galleryArr.push($(this).val());
                    break;
                } else if (subtype == 'checkbox') {
                    aOpts[aId] = ($(this).val() == 'on' ? 1 : 0);
                    break;
                }
            case 'SELECT':
            case 'DEFAULT':
                aOpts[aId] = $(this).val();
                break;
        }
    });

    if (galleryArr.length > 0) {
        aOpts[cbName] = {};
        aOpts[cbName] = galleryArr;
    }

    var serializedOpts = '';
    if (Object.size(aOpts)) {
        serializedOpts = PHPSerializer.serialize(aOpts);
    }

    nhtml = '{#' + elementA + '' + (serializedOpts.length > 0 ? '|' + serializedOpts : '') + '#}';

    top.tinymce.activeEditor.execCommand('mceInsertContent', false, nhtml);

    top.tinymce.activeEditor.windowManager.close();
    return false;
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
};

function editCurrent(node) {
    if (node.length > 0) {
        reg = /{#(.*)#}/;
        suboptionsReg = /\[([0-9:]*)\]/;
        data = node.split(reg)[1];

        hasOptions = null;
        hasOptions = data.indexOf('|');

        if (hasOptions > 0) {
            module = data.split('|');
            mod = module[0];

            aoptions = module[1];

            aoptions = PHPSerializer.unserialize(aoptions);

            for (var key in aoptions) {
                elem = '#' + key;

                switch (mod) {
                    case 'gallery':
                        if (key == 'galleries') {
                            gals = aoptions[key];

                            var c = 0;
                            for (var bi in gals) {
                                c += 1;
                            }

                            for (z = 0; z < c; z++) {
                                $('.checkboxes').find('input[value=' + gals[z] + ']').attr('checked', 'checked');
                            }
                        }
                        break;
                    case 'facebook':
                        if (key == 'boxType') {
                            a = '.' + aoptions[key];
                            $(a).show();
                        }
                        break;
                    default:
                        break;
                }

                if ($(elem).length > 0) {
                    if (!($(elem).is('[type=checkbox]')))
                        $(elem).val(aoptions[key]);
                    else if (aoptions[key] == 1)
                        $(elem).attr('checked', 'checked');
                }
            }
        } else {
            mod = data;
        }

        $('#content').val(mod);
        mod = '#' + mod;
        if ($(mod).length > 0)
            $(mod).show();

        return true;
    } else
        return false;
}

function createNewsOptions(elem) {
    news = top.tinymce.settings.widgetData.news;
    newsPages = top.tinymce.settings.widgetData.newsPages;
    newsOptions = '';
    pagesSize = newsPages.length;
    if (news !== undefined) {
        newsSize = news.length;
        newsOptions = '<div><label for="newsCategory">Wybierz kategorie:</label><div class="clearline"></div>' +
                '<select name="newsCategory" id="newsCategory">';
        newsOptions += '<option value="all">Wszystkie</option>';
        for (i = 0; i < newsSize; i++) {
            newsOptions += '<option value="' + news[i].id + '">' + news[i].title + '</option>';
        }
        newsOptions += '';
        newsOptions += '</select></div>';

        newsOptions += '<div class="clearline"></div>';
    }
    newsOptions += '<div><label for="newsPages">Wybierz stronę kontekstu newsów:</label><div class="clearline"></div>' +
            '<select name="newsPages" id="newsPages">';
    newsOptions += '<option value="all">Wszystkie</option>';

    for (var i in newsPages) {
        if (newsPages[i].ptype_action == 'news')
            newsOptions += '<option value="' + newsPages[i].id + '">' + newsPages[i].title + '</option>';
    }
    newsOptions += '';
    newsOptions += '</select></div>';

    elem.prepend(newsOptions);
}

function createGalleryOptions(elem) {
    galleries = top.tinymce.settings.widgetData.galleryList;
    galleriesSize = galleries.length;

    var content = '<p>Wybierz galerie do wyświetlenia</p>';
    for (i = 0; i < galleriesSize; i++) {
        content += '<input type="checkbox" name="galleries[]" value="' + galleries[i]['albumId'] + '" class="galleries"><span>' + galleries[i]['title'] + '</span>';
    }

    elem.prepend(content);
}