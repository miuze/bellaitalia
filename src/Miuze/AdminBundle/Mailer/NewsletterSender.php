<?php

namespace Miuze\AdminBundle\Mailer;

use Miuze\AdminBundle\Entity\Newsletter;
use Symfony\Component\DependencyInjection\Container;

class NewsletterSender {
    
    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;
    
    private $container;
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    function __construct(ContainerInterface $container, \Swift_Mailer $swiftMailer, \Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
        $this->doctrine = $doctrine;
        $this->swiftMailer = $swiftMailer;
        $this->container = $container;
    }
    
    public function send($email, $title, $htmlBody) {
        $message = \Swift_Message::newInstance()
                    ->setSubject($title)
                    ->setFrom($this->container->getParameter('mailmaster'), $this->container->getParameter('company_name'))
                    ->setTo($email->getEmail(), $email->getEmail())
                    ->setBody($htmlBody, 'text/html');
        $this->swiftMailer->send($message);
    }

}
