<?php

namespace Miuze\AdminBundle\Mailer;

use Miuze\AdminBundle\Entity\Newsletter;
use Symfony\Component\DependencyInjection\ContainerInterface;


class NewsletterMailer {
    
    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;
    
    private $container;
    
    
    function __construct(ContainerInterface $container, \Swift_Mailer $swiftMailer) {
        $this->swiftMailer = $swiftMailer;
        $this->container = $container;
    }
    
    public function send(Newsletter $newsletter, $title, $htmlBody) {
        $message = \Swift_Message::newInstance()
            ->setSubject($title)
            ->setFrom($this->container->getParameter('mailmaster'), $this->container->getParameter('company_name'))
            ->setTo($newsletter->getEmail(), 'Newsletter')
            ->setBody($htmlBody, 'text/html');
        $this->swiftMailer->send($message);
    }

}
