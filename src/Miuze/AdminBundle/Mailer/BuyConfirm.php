<?php

namespace Miuze\AdminBundle\Mailer;

use Miuze\AdminBundle\Entity\Buyer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BuyConfirm {
    
    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;
    
    private $container;
    
    function __construct(ContainerInterface $container, \Swift_Mailer $swiftMailer) {
        $this->swiftMailer = $swiftMailer;
        $this->container = $container;
    }
    
    public function send(Buyer $buyer, $title, $htmlBody) {
        $message = \Swift_Message::newInstance()
                        ->setSubject($title)
                        ->setFrom($this->container->getParameter('mailmaster'), $this->container->getParameter('company_name'))
                        ->setTo($buyer->getEmail(), $buyer->getFirstName().' '.$buyer->getLastName())
                        ->setBody($htmlBody, 'text/html');
        
        $this->swiftMailer->send($message);
    }
}
