<?php

namespace Miuze\AdminBundle\Mailer;

use Symfony\Component\DependencyInjection\ContainerInterface;

class OfferMailer {
    
    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;
    
    private $container;
    
    function __construct(ContainerInterface $container, \Swift_Mailer $swiftMailer) {
        $this->swiftMailer = $swiftMailer;
        $this->container = $container;
    }
    
    public function send($title, $htmlBody, $email) {
        $message = \Swift_Message::newInstance()
                        ->setSubject($title)
                        ->setFrom($this->container->getParameter('mailmaster'))
                        ->setTo($email)
                        ->setBody($htmlBody, 'text/html');  
        
        $this->swiftMailer->send($message);
    }
}
