<?php

namespace Miuze\AdminBundle\Mailer;

use Miuze\AdminBundle\Entity\Contact;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContactMailer {
    
    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;
    
    private $container;
    
    function __construct(ContainerInterface $container, \Swift_Mailer $swiftMailer) {
        $this->swiftMailer = $swiftMailer;
        $this->container = $container;
    }
    
    public function send(Contact $contact, $title, $htmlBody) {
        $message = \Swift_Message::newInstance()
                        ->setSubject($title)
                        ->setFrom($this->container->getParameter('mailmaster'), $this->container->getParameter('company_name'))
                        ->setTo($contact->getEmail(), $this->container->getParameter('company_name'))
                        ->setBody($htmlBody, 'text/html');
        
        $this->swiftMailer->send($message);
    }
    
    public function sendAdmin(Contact $contact, $title, $htmlBody) {
        $message = \Swift_Message::newInstance()
                        ->setSubject($title)
                        ->setFrom($this->container->getParameter('mailmaster'), $this->container->getParameter('company_name'))
                        ->setTo($this->container->getParameter('contact_email'), 'Wiadomość ze strony')
                        ->setBody($htmlBody, 'text/html');
        
        $this->swiftMailer->send($message);
    }

}
