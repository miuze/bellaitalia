<?php

namespace Miuze\AdminBundle\Mailer;

use Miuze\AdminBundle\Entity\Cv;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobMailer {
    
    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;
    
    private $container;
    
    function __construct(ContainerInterface $container, \Swift_Mailer $swiftMailer) {
        $this->swiftMailer = $swiftMailer;
        $this->container = $container;
    }
    
    public function send(Cv $cv, $title, $htmlBody) {
        $message = \Swift_Message::newInstance()
                        ->setSubject($title)
                        ->setFrom($this->container->getParameter('mailmaster'))
                        ->setTo($cv->getEmail())
                        ->setBody($htmlBody, 'text/html');
        $this->swiftMailer->send($message);
    }
    
    public function sendAdmin(Cv $cv, $title, $htmlBody) {
        $message = \Swift_Message::newInstance()
                        ->setSubject($title)
                        ->setFrom($this->container->getParameter('mailmaster'))
                        ->setTo($this->container->getParameter('contact_email'))
                        ->setBody($htmlBody, 'text/html')
                        ->attach(\Swift_Attachment::fromPath($cv->getRootFile()));
        
        $this->swiftMailer->send($message);
    }

}
