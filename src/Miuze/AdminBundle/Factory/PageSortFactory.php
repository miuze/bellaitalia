<?php


namespace Miuze\AdminBundle\Factory;


use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Model\PageSortDTO;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageSortFactory
{
    private $repository;
    private $request;
    public function __construct(
        Registry $doctrine,
        RequestStack $requestStack
    ) {
        $this->repository = $doctrine->getRepository(Page::class);
        $this->request = $requestStack->getCurrentRequest();
    }

    public function create(): PageSortDTO
    {
        $pageId = $this->request->get("id");
        $parentId = $this->request->get('parentId');
        $nextId = $this->request->get('next');
        $prevId = $this->request->get('prev');
        $pageSortDTO = new PageSortDTO();

        $page = $this->repository->findOneById($pageId);

        if (is_null($page)) {
            throw new NotFoundHttpException(\sprintf('Nie znaleziono storny o id: %s', $pageId));
        }

        $pageSortDTO->setPage($page);

        if (!is_null($parentId)) {
            $page = $this->repository->findOneById($parentId);
            if (!is_null($page)) {
                $pageSortDTO->setParent($page);
            }
        }

        if (!is_null($nextId)) {
            $page = $this->repository->findOneById($nextId);
            if (!is_null($page)) {
                $pageSortDTO->setNext($page);
            }
        }

        if (!is_null($prevId)) {
            $page = $this->repository->findOneById($prevId);
            if (!is_null($page)) {
                $pageSortDTO->setPrevent($page);
            }
        }

        return $pageSortDTO;
    }
}
