<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Enum;

class SlugEnum
{
    public const PAGE = 'page';
    public const NEWS = 'news';
    public const BLOG = 'blog';

}