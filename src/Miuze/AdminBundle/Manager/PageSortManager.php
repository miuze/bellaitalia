<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Factory\PageSortFactory;
use Miuze\AdminBundle\Repository\PageRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

class PageSortManager
{
    /**
     * @var PageRepository
     */
    private $repository;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager|object
     */
    private $em;

    /**
     * @var PageSortFactory
     */
    private $pageSortFactory;

    public function __construct(
        Registry $doctrine,
        PageSortFactory $pageSortFactory
    ) {
        $this->repository = $doctrine->getRepository(Page::class);
        $this->em = $doctrine->getManager();
        $this->pageSortFactory = $pageSortFactory;
    }

    public function sort(): JsonResponse
    {
        try {
            $page = $this->pageSortFactory->create();
        } catch (\Exception $e) {
            return $this->jsonView($e->getMessage());
        }

        if (!is_null($page->getPrevent())) {
            $this->repository->persistAsNextSiblingOf($page->getPage(), $page->getPrevent());
            $this->em->flush();

            return $this->jsonView(\sprintf('Poprawnie zapisano stronę za %s', $page->getPrevent()->getSlug()->getName()));
        } else {
            $this->repository->persistAsFirstChildOf($page->getPage(), $page->getParent());
            $this->em->flush();

            return $this->jsonView(\sprintf('Poprawnie zapisano podstronę jak dziecko %s', $page->getParent()->getSlug()->getName()));
        }

        return $this->jsonView('Kolejność niezaktualizowana', 'danger');
    }

    private function jsonView($msg, $status = 'success'): JsonResponse
    {
        return new JsonResponse([
            'msg' => $msg,
            'status' => $status
        ]);
    }
}