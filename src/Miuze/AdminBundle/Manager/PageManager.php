<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\AdminBundle\Creator\SitemapCreator;
use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Entity\PageCategory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class PageManager
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager|object
     */
    private $em;

    /**
     * @var SitemapCreator
     */
    private $sitemapCreator;

    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(
        Registry $doctrine,
        RequestStack $requestStack,
        SessionInterface $session,
        SitemapCreator $sitemapCreator,
        RouterInterface $router
    ) {
        $this->doctrine = $doctrine;
        $this->request = $requestStack->getCurrentRequest();
        $this->session = $session;
        $this->em = $doctrine->getManager();
        $this->sitemapCreator = $sitemapCreator;
        $this->router = $router;
    }

    public function persist(Page $page): string
    {
        $this->em->persist($page);
        $parentId = $this->request->attributes->getInt('parent');

        if(0 !== $parentId){
            $repo = $this->doctrine->getRepository(Page::class);
            $parent = $repo->findOneById($parentId);

            if(is_null($parent)){
                throw new NotFoundHttpException('Nie znaleziono rodzica.');
            }

            $repo->persistAsFirstChildOf($page, $parent);
        }
        $this->em->flush();
        $this->setMessage('Strona zapisana prawidłowo');
        $this->sitemapCreator->create($page);
        return $this->router->generate('admin_pages_index', ['menu' => $page->getCategory()->getId()]);
    }

    public function remove(): string
    {
        $id = $this->request->attributes->getInt('id');
        $repo = $this->doctrine->getRepository(Page::class);
        $page = $this->getPage();
        $menu = $page->getCategory()->getId();
        $repo->removeFromTree($page);
        $this->em->flush();
        $this->setMessage('Usunięto poprawnie');

        return $this->router->generate('admin_pages_index', ['menu' => $menu]);
    }

    public function disable(): string
    {
        $page = $this->getPage();

        if($page->getActive() === 0){
            $page->setActive(1);
            $msg = 'włączono';
        }else{
            $page->setActive(0);
            $msg = 'wyłączono';
        }

        $this->em->persist($page);
        $this->em->flush();
        $this->setMessage('Strona %s prawidłowo', $msg);

        return $this->router->generate('admin_pages_index', ['menu' => $page->getCategory()->getId()]);
    }

    public function hideMenu(): string
    {
        $page = $this->getPage();

        if($page->getVisable() === false){
            $page->setVisable(true);
            $msg = 'widoczna';
        }else{
            $page->setVisable(false);
            $msg = 'ukryta';
        }

        $this->em->persist($page);
        $this->em->flush();
        $this->setMessage('Strona %s w menu', $msg);

        return $this->router->generate('admin_pages_index', ['menu' => $page->getCategory()->getId()]);
    }

    public function getPage(): Page
    {
        $pageId = $this->request->attributes->getInt('id');

        if ($pageId !== 0) {
            $repo = $this->doctrine->getRepository(Page::class);
            $page = $repo->findOneById($pageId);
            if(NULL == $page ){
                throw $this->createNotFoundException('Nie znaleziono takiej strony');
            }
        } else {
            $page = new Page();
            $page->setCategory($this->getMenu());
        }

        return $page;
    }

    public function getFrontPage(?string $slug): Page
    {
        $repo = $this->doctrine->getRepository(Page::class);
        $page = $repo->getPageBySlug($slug);

        if(NULL == $page ){
            throw new NotFoundHttpException('Nie znaleziono takiej strony');
        }

        return $page;
    }

    public function getMenu(): PageCategory
    {
        $menuId = $this->request->attributes->getInt('menu');
        $menu = $this->doctrine->getRepository(PageCategory::class)->findOneById($menuId);

        if(is_null($menu)){
            throw new NotFoundHttpException('Nie znaleziono menu!');
        }

        return $menu;
    }

    public function setMessage(string $message, string $type = 'success')
    {
        $this->session->getFlashBag()->add($type, $message);
    }

}