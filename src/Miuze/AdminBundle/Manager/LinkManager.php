<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\AdminBundle\Entity\Page;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class LinkManager
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    public function __construct(
        UrlGeneratorInterface $router
    ) {
        $this->router = $router;
    }

    public function generateUri(Page $page): string
    {
        if($page->getLink() !== null){
            $url = $page->getLink();
        }elseif($page->getType() == 'index'){
            $url = $this->router->generate($page->getType(), [], UrlGeneratorInterface::ABSOLUTE_URL);
        }else{
            $url = $this->router->generate('subpage', ['slug' => $page->getSlug()->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        return $url;
    }

}