<?php


namespace Miuze\AdminBundle\Model;


use Miuze\AdminBundle\Entity\Page;

class PageSortDTO
{
    /**
     * @var Page|null
     */
    private $prevent;

    /**
     * @var Page|null
     */
    private $next;

    /**
     * @var Page|null
     */
    private $parent;

    /**
     * @var Page|null
     */
    private $page;

    /**
     * @return Page|null
     */
    public function getPrevent(): ?Page
    {
        return $this->prevent;
    }

    /**
     * @param Page|null $prevent
     */
    public function setPrevent(?Page $prevent): void
    {
        $this->prevent = $prevent;
    }

    /**
     * @return Page|null
     */
    public function getNext(): ?Page
    {
        return $this->next;
    }

    /**
     * @param Page|null $next
     */
    public function setNext(?Page $next): void
    {
        $this->next = $next;
    }

    /**
     * @return Page|null
     */
    public function getParent(): ?Page
    {
        return $this->parent;
    }

    /**
     * @param Page|null $parent
     */
    public function setParent(?Page $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return Page|null
     */
    public function getPage(): ?Page
    {
        return $this->page;
    }

    /**
     * @param Page|null $page
     */
    public function setPage(?Page $page): void
    {
        $this->page = $page;
    }
}
