<?php

namespace Miuze\AdminBundle\Form\Award;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityRepository;

class AwardType extends AbstractType{
    public function getName(){
        return 'award_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('category', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:AwardCategory',
                'label' => 'Kategoria',
                'choice_label' => 'title',
            ))   
            ->add('title', TextType::class, array(
                'label' => 'Tytuł',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę nagrody',
                ),
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'Treść opisu nagrody',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
                
            ))
            ->add('lead', TextareaType::class, array(
                'label' => 'Zajawka',
                'attr' => array(
                    'id' => 'ckeditor1',
                ),
            ))                
            ->add('createDate', DateType::class, array(
                'label' => 'Data utworzenia',
                'widget' => 'single_text', 
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'readonly' => true,
                    'class' => 'datepicker',
                )
            ))
            ->add('startDate', DateType::class, array(
                'label' => 'Data publikacji',
                'widget' => 'single_text', 
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'readonly' => true,
                    'class' => 'datepicker',
                )
            ))
            ->add('endDate', DateType::class, array(
                'label' => 'Data zakończenia',
                'widget' => 'single_text', 
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                )
            ))
            ->add('file', FileType::class, array(
                'label' => 'Zdjęcie wyróżniające',
                'data_class' => NULL
            ))
                        
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Award'
        ));
    }
}
