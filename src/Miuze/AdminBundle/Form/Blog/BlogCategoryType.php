<?php

namespace Miuze\AdminBundle\Form\Blog;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class BlogCategoryType extends AbstractType{
    public function getName(){
        return 'blog_cat_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('page', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Page',
                'label' => 'Strony',
                'multiple' => true,
                'choice_label' => 'title',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.type = :blog')
                        ->setParameter('blog', 'blog');
                    return $res;
                },
            )) 
                
            ->add('title', TextType::class, array(
                'label' => 'Tytuł',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę kategorii',
                ),
            ))  
                
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));            
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\BlogCategory'
        ));
    }
}
