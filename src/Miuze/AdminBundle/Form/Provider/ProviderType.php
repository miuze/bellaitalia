<?php

namespace Miuze\AdminBundle\Form\Provider;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityRepository;

class ProviderType extends AbstractType{
    public function getName(){
        return 'provider_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa dostawcy',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę',
                ),
            ))
            ->add('email', EmailType::class, array(
                'label' => 'E-mail do zamówień',
                'attr' => array(
                    'placeholder' => 'Podaj e-mail',
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Provider'
        ));
    }
}
