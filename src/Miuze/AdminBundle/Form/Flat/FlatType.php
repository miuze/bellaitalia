<?php

namespace Miuze\AdminBundle\Form\Flat;

use Miuze\AdminBundle\Entity\Build;
use Miuze\AdminBundle\Repository\BuildRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class FlatType extends AbstractType{
    public function getName(){
        return 'flat_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa produktu',
                'attr' => array(
                    'placeholder' => 'Nazwa produktu',
                ),
            ))
            ->add('area', NumberType::class, array(
                'label' => 'Ilość metrów kw',
                'attr' => array(
                    'placeholder' => 'Podaj ilość metrów',
                ),
            ))
            ->add('bathroom', NumberType::class, array(
                'label' => 'Ilość łazienek',
                'attr' => array(
                    'placeholder' => 'Podaj ilość łazienek',
                ),
            ))
            ->add('room', NumberType::class, array(
                'label' => 'Ilość sypialni',
                'attr' => array(
                    'placeholder' => 'Podaj ilość sypialni',
                ),
            ))
            ->add('garage', NumberType::class, array(
                'label' => 'Ilość garaży (miejsc)',
                'attr' => array(
                    'placeholder' => 'Podaj ilość miejsc',
                ),
            ))
            ->add('cords', TextType::class, array(
                'label' => 'Poligony nawigacynjne',
                'attr' => array(
                    'placeholder' => 'Poligony nawigacynjne',
                ),
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'Pełny opis',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
            ))  
            ->add('lead', TextareaType::class, array(
                'label' => 'Zajawka w sliderze',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
            ))
            ->add('isStock', ChoiceType::class, array(
                'label' => 'Dostępny',
                'choices'  => array(
                    'Tak' => TRUE,
                    'Nie' => FALSE,
                )
            ))
            ->add('price', MoneyType::class, array(
                'label' => 'Cena podstawowa w zł np. (12,99)',
                'attr' => array(
                    'placeholder' => 'Cena podstawowa netto',
                ),
                'currency' => 'PLN'
            ))
            ->add('build', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Build',
                'label' => 'Budynek',
                'multiple' => false,
                'expanded' => false,
                'choice_label' => 'name',
            ))
                
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Flat',
            'validation_groups' => array('add'),
        ));
    }
}
