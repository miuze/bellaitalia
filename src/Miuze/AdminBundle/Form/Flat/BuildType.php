<?php

namespace Miuze\AdminBundle\Form\Flat;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BuildType extends AbstractType{
    public function getName(){
        return 'build_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa budynku',
                'attr' => array(
                    'placeholder' => 'Nazwa budynku',
                ),
            ))
            ->add('address', TextType::class, array(
                'label' => 'Adres budynku',
                'attr' => array(
                    'placeholder' => 'Podaj adres budynku',
                ),
            ))
            ->add('lat', NumberType::class, array(
                'label' => 'Szerokość geograficzna',
                'attr' => array(
                    'placeholder' => 'Podaj szerokość geograficzną',
                ),
            ))
            ->add('lng', NumberType::class, array(
                'label' => 'Długość geograficzna',
                'attr' => array(
                    'placeholder' => 'Podaj długość geograficzną',
                ),
            ))
            ->add('autoGeo', CheckboxType::class, array(
                'label' => 'Auto geolokalizacja',
            ))
            
            ->add('page', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Page',
                'label' => 'Strony',
                'multiple' => true,
                'choice_label' => 'title',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.type = :flat OR p.type = :search')
                        ->setParameter('search', 'search')
                        ->setParameter('flat', 'flat');
                    return $res;
                },
            ))
            ->add('file', FileType::class, array(
                'label' => 'Zdjęcie',
                'data_class' => NULL
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Build'
        ));
    }
}
