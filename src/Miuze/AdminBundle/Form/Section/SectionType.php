<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Form\Section;

use Miuze\AdminBundle\Entity\Section;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class SectionType extends AbstractType
{
    public function getName(): string
    {
        return 'section_form';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('content', TextareaType::class, [
                'label' => 'Treść sekcji',
                'attr' => [
                    'rows' => 20,
                ]

            ])
            ->add('file', FileType::class, [
                'label' => 'Zdjęcie wyróżniające',
                'data_class' => NULL
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz',
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Section::class
        ));
    }
}
