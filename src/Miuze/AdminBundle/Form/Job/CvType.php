<?php

namespace Miuze\AdminBundle\Form\Job;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints as Assert;

class CvType extends AbstractType{
    public function getName(){
        return 'cv_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder            
            ->add('firstName', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Imię',
                ),
            ))
            ->add('lastName', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Nazwisko',
                ),
            ))
            ->add('phone', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Telefon kontaktowy',
                ),
            ))
            ->add('email', EmailType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Adres e-mail',
                ),
            ))
            ->add('content', TextareaType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Treść wiadomości'
                    
                ),
                
            ))
            ->add('file', FileType::class, array(
                'label' => 'Zestawienie materiałów',
                'data_class' => NULL
            ))           
            ->add('submit', SubmitType::class, array(
                'label' => 'Wyślij',
                'attr' => array(
                    'class'=> 'btn btn-msg'
                )
            ));            
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Cv'
        ));
    }
}
