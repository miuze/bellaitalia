<?php

namespace Miuze\AdminBundle\Form\Newsletter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class NewsletterType extends AbstractType{
    public function getName(){
        return 'newsletter_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('email', EmailType::class, array(
                'label' => 'Twój email',
                'attr' => array(
                    'placeholder' => 'Twój email',
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-defaul button'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Newsletter'
        ));
    }
}
