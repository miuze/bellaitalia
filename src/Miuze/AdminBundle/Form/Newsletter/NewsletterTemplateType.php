<?php

namespace Miuze\AdminBundle\Form\Newsletter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class NewsletterTemplateType extends AbstractType{
    public function getName(){
        return 'newsletter_template_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Tytuł wiadomości',
                'attr' => array(
                    'placeholder' => 'Tytuł wiadomości',
                ),
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'Treść wiadomości',
                'attr' => array(
                    'placeholder' => 'Treść wiadomości',
                ),
            ))
            ->add('news', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:News',
                'label' => 'Aktualności (dadaj więcej z "ctrl + click")',
                'multiple' => true,
                'choice_label' => 'title',
                'attr' => array(
                    'style' => 'height: 400px;'
                )
            ))   
            
            ->add('userCategory', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:NewsletterCategory',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('c')
                        ->select('c', 'n')
                        ->leftJoin('c.newsletter', 'n')
                        ->where('n.active = 1');
                    return $res;
                    },
                'label' => 'Grupy odbiorców (dadaj więcej z "ctrl + click")',
                'multiple' => true,
                'choice_label' => 'title',
                'attr' => array(
                    'style' => 'height: 400px;'
                )
            ))   
                
            ->add('submit', SubmitType::class, array(
                'label' => 'Podgląd',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\NewsletterTemplate'
        ));
    }
}
