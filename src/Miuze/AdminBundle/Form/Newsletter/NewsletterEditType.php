<?php

namespace Miuze\AdminBundle\Form\Newsletter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class NewsletterEditType extends AbstractType{
    public function getName(){
        return 'newsletter_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('email', EmailType::class, array(
                'label' => 'Twój email',
                'attr' => array(
                    'placeholder' => 'Twój email',
                ),
            ))
            ->add('category', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:NewsletterCategory',
                'label' => 'Kategoria',
                'multiple' => true,
                'choice_label' => 'title',
            )) 
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Newsletter'
        ));
    }
}
