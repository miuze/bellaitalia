<?php

namespace Miuze\AdminBundle\Form\Rabate;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class RabateType extends AbstractType{
    public function getName(){
        return 'rabate_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            
            ->add('rate', NumberType::class, array(
                'label' => 'Ilosć rabatu',
                'attr' => array(
                    'placeholder' => 'Podaj wartość rabatu w %',
                ),
            ))   
            
            ->add('user', EntityType::class, array(
                'class' => 'MiuzeUserBundle:User',
                'label' => 'Klienci',
                'multiple' => false,
                'choice_label' => function($item){
                    return $item->getFirstName(). ' '.$item->getLastName(). ' | '.$item->getEmail();
                },
            ))
            ->add('productCategory', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:ProductCategory',
                'label' => 'Kategorie',
                'multiple' => false,
                'choice_label' => function($tree){
                    $pre = '|';
                    for($i = 0; $tree->getLvl() > $i; $i++ ){
                        $pre .= '- ';
                    }
                    return $pre.$tree->getName();
                },
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('c')
                        ->select('c')
                        ->orderBy('c.root, c.sort, c.lft', 'ASC');
                    return $res;
                },
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));            
    }
    
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Rabate'
        ));
    }
}
