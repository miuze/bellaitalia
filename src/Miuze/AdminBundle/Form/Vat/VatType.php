<?php

namespace Miuze\AdminBundle\Form\Vat;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityRepository;

class VatType extends AbstractType{
    public function getName(){
        return 'vat_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('rate', NumberType::class, array(
                'label' => 'Stawka Vat',
                'attr' => array(
                    'placeholder' => 'Wpisz stawkę VAT',
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Vat'
        ));
    }
}
