<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Form\Place;

use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Entity\PlaceCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PlaceCategoryType extends AbstractType
{
    public function getName(): string
    {
        return 'place_cat_form';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Nazwa',
                'attr' => [
                    'placeholder' => 'Podaj nazwę kategorii',
                ],
            ])
            
            ->add('page', EntityType::class, [
                'class' => 'MiuzeAdminBundle:Page',
                'label' => 'Strony',
                'multiple' => true,
                'choice_label' => function(Page $page)
                {
                    return $page->getSlug()->getName();
                },
            ])
                
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz',
                'attr' => [
                    'class'=> 'btn btn-success',
                ],
            ]);
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PlaceCategory::class
        ]);
    }
}
