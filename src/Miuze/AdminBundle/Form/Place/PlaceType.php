<?php

namespace Miuze\AdminBundle\Form\Place;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;

class PlaceType extends AbstractType{
    public function getName(){
        return 'place_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('category', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:PlaceCategory',
                'label' => 'Kategoria',
                'choice_label' => 'title',
            ))   
            ->add('name', TextType::class, array(
                'label' => 'Nazwa',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę',
                ),
            ))
            ->add('address', TextType::class, array(
                'label' => 'Adres',
                'attr' => array(
                    'placeholder' => 'Podaj adres (44-230 Stanowice ul.Morcina 27)',
                ),
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Telefon',
                'attr' => array(
                    'placeholder' => 'Podaj telefon',
                ),
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'placeholder' => 'Podaj adres email',
                ),
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Opis',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
                
            ))
            ->add('file', FileType::class, array(
                'label' => 'Zdjęcie',
                'data_class' => NULL
            ))
                        
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Place'
        ));
    }
}
