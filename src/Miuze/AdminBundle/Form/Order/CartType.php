<?php

namespace Miuze\AdminBundle\Form\Order;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityRepository;

class CartType extends AbstractType{

    private $em;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder

            ->add('product', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Product',
                'label' => 'Produkt',
                'attr' => array(
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Wybierz produkt',
                    'tabindex' => '2'
                ),
                'choice_label' => function($product){
                    return $product->getCategory()->getName(). ' | ' .$product->getName(). ' | '. $product->getPrice().'netto';
//                    $path = $this->getChild($tree);
//                    return $path;
                },
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('p')
                        ->select('p, c')
                        ->leftJoin('p.category', 'c')
                        ->orderBy('c.root, c.sort, c.lft', 'ASC');
                    return $res;
                },
            ))
            ->add('quantity', NumberType::class, array(
                'label' => 'Ilość',
                'attr'=> array(
                    'value' => 1,
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Cart',
        ));
    }

    public function getName(){
        return 'order_cart_form';
    }

    protected function getChild($item){
        $paths = $this->em->getRepository('MiuzeAdminBundle:ProductCategory')->getPath($item->getCategory());
        $breadcrumbs = '';
        foreach ($paths as $node){
            $breadcrumbs .= $node->getName().' -> ';
        }
        $breadcrumbs.= $item->getName();
        return $breadcrumbs;
    }
}
