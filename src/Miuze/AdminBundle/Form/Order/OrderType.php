<?php

namespace Miuze\AdminBundle\Form\Order;


use Miuze\AdminBundle\Form\Order\CartType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityRepository;

class OrderType extends AbstractType{
    public function getName(){
        return 'order_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('carts', CollectionType::class, array(
                'entry_type' => CartType::class,
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'entry_options'  => array(
                    'attr' => array(
                        'class' => 'cart-box'
                    )
                ),
                
            ))
            ->add('buyer', EntityType::class, array(
                'class' => 'MiuzeUserBundle:User',
                'label' => 'Klienci',
                'attr' => array(
                    'class' => 'chosen-select'
                ),
                'choice_label' => function($user){
                    return $user->getFirstName(). ' ' .$user->getLastName(). ' '. $user->getEmail();
                },
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('u')
                        ->select('u')
                        ->orderBy('u.lastName', 'ASC');
                    return $res;
                },
            ))
            ->add('travel', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Travel',
                'label' => 'Sposób dostawy',
                'attr' => array(
                    'class' => 'chosen-select'
                ),
                'choice_label' => function($travel){
                    return $travel->getName(). ' - ' .$travel->getPrice();
                },
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('t')
                        ->select('t');
                    return $res;
                },
            ))
            ->add('payment', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Payment',
                'label' => 'Sposób płatności',
                'choice_label' => 'name',
            ))

            ->add('submit', SubmitType::class, array(
                'label' => 'Podsumowanie',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Order'
        ));
    }
}
