<?php

namespace Miuze\AdminBundle\Form\Reference;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;

class ReferenceType extends AbstractType{
    public function getName(){
        return 'reference_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('category', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:ReferenceCategory',
                'label' => 'Kategoria',
                'choice_label' => 'title',
            ))  
            ->add('name', TextType::class, array(
                'label' => 'Nazwa firmy',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę',
                ),
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'Treść pełna',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
                
            ))
            ->add('lead', TextareaType::class, array(
                'label' => 'Zajawka',
                'attr' => array(
                    'id' => 'ckeditor1',
                ),
            ))                
            ->add('file', FileType::class, array(
                'label' => 'Logo',
                'data_class' => NULL
            ))
                        
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Reference'
        ));
    }
}
