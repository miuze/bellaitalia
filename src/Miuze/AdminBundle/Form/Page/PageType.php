<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Form\Page;

use Miuze\AdminBundle\Entity\Page;
use Miuze\AdminBundle\Form\Slug\SlugType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Translation\TranslatorInterface;

class PageType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getName()
    {
        return 'page_form';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//      moduły
        $modules = ['Strona główna' => 'index'];
        $dirviews = __DIR__ . '/../../../PageBundle/Controller/';
        foreach (scandir($dirviews) as $file) {
            if ($file !== '.' && $file !== '..' && $file !== 'BaseController.php') {
                $ctrlName = str_replace("Controller.php", "", $file);
                $name = $this->translator->trans($ctrlName, [], 'MiuzeAdminBundle');
                $route = strtolower($ctrlName);
                $modules[$name] = $route;
            }
        }

//      wyszukiwanie widoków
        $dirviews = __DIR__ . '/../../../PageBundle/Resources/views/Custom/';
        $views = ['Standardowy' => null];
        foreach (scandir($dirviews) as $file) {
            if ($file != '.' && $file != '..') {
                $name = strstr($file, '.', true);
                $views[$name] = $file;
            }
        }

//      wyszukiwanie layoutów
        $dirlayouts = __DIR__ . '/../../../PageBundle/Resources/views/layouts/';
        $layouts = [];
        foreach (scandir($dirlayouts) as $file) {
            if ($file != '.' && $file != '..') {
                $name = strstr($file, '.', true);
                $layouts[$name] = $file;
            }
        }
        ksort($modules);
        ksort($views);
        ksort($layouts);
        $builder
            ->add('slug', SlugType::class, [
                'label' => false,
            ])
            ->add('pageid', TextType::class, [
                'label' => 'ID',
                'attr' => array(
                    'placeholder' => 'ID',
                ),
            ])
            ->add('subtitle', TextType::class, [
                'label' => 'Podtytuł',
                'attr' => array(
                    'placeholder' => 'Podaj podtytuł strony',
                ),
            ])
            ->add('lead', TextareaType::class, [
                'label' => 'Dodatkowa treść strony',
                'attr' => array(
                    'id' => 'ckeditor',
                ),

            ])
            ->add('class', TextType::class, [
                'label' => 'Klasy css',
            ])
            ->add('visable', ChoiceType::class, [
                'label' => 'Widoczność w menu',
                'choices' => array(
                    'Tak' => TRUE,
                    'Nie' => FALSE,
                )
            ])
            ->add('active', ChoiceType::class, [
                'label' => 'Aktywność',
                'choices' => array(
                    'Tak' => TRUE,
                    'Nie' => FALSE,
                )
            ])
            ->add('link', TextType::class, [
                'label' => 'Link',
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Typ strony',
                'choices' => $modules
            ])
            ->add('layout', ChoiceType::class, [
                'label' => 'Szablon',
                'choices' => $layouts,
            ])
            ->add('view', ChoiceType::class, [
                'label' => 'Widok',
                'choices' => $views,
            ])
            ->add('seoDescription', TextType::class, [
                'label' => 'Seo opis',
            ])
            ->add('submit', SubmitType::class, [
                    'label' => 'Zapisz',
                    'attr' => [
                        'class' => 'btn btn-success'
                    ]
            ])
            ->add('saveAndEdit', SubmitType::class, ['label' => 'Zapisz i edytuj dalej']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
        ]);
    }
}
