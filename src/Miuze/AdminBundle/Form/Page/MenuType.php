<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Form\Page;

use Miuze\AdminBundle\Entity\Lang;
use Miuze\AdminBundle\Entity\PageCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MenuType extends AbstractType{
    public function getName(){
        return 'page_category_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Nazwa menu',
                'attr' => [
                    'placeholder' => 'Podaj nazwę',
                ],
            ])
            ->add('lang', EntityType::class, [
                'class' => Lang::class,
                'label' => 'Język',
                'choice_label' => function($item){
                    return $item->getLang();
                },
            ])
            ->add('removed', ChoiceType::class, [
                'label' => 'Pokazanie na stronie',
                'choices'  => [
                    'Tak' => TRUE,
                    'Nie' => FALSE,
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz',
                'attr' => [
                    'class'=> 'btn btn-success'
                ]
            ]);
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            'data_class' => PageCategory::class
        ]);
    }
}
