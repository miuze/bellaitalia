<?php

namespace Miuze\AdminBundle\Form\Translate;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class TranslateAddType extends AbstractType{
    public function getName(){
        return 'translateAdd_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('lang', TextType::class, array(
                'label' => 'symbol języka',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Lang'
        ));
    }
}
