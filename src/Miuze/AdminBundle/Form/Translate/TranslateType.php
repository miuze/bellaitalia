<?php

namespace Miuze\AdminBundle\Form\Translate;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class TranslateType extends AbstractType{
    public function getName(){
        return 'translate_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('content', TextareaType::class, array(
                'label' => 'Treść tłumaczenia',
                'attr' => array(
                    'class' => 'no-wysiwyg',
                    'style' => 'min-height: 700px;'
                )
                
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Translate'
        ));
    }
}
