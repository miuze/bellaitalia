<?php

namespace Miuze\AdminBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProductCategoryType extends AbstractType{
    public function getName(){
        return 'product_category_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa kategorii',
                'attr' => array(
                    'placeholder' => 'Nazwa kategorii',
                ),
            ))
            ->add('defaultRabate', NumberType::class, array(
                'label' => 'Ilosć rabatu',
                'attr' => array(
                    'placeholder' => 'Podaj wartość rabatu w %',
                ),
            ))
            ->add('page', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Page',
                'label' => 'Strony',
                'multiple' => true,
                'choice_label' => 'title',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.type = :product')
                        ->setParameter('product', 'product');
                    return $res;
                },
            ))
            ->add('parent', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:ProductCategory',
                'label' => 'Rodzic',
                'choice_label' => function($tree){
                    $pre = '|';
                    for($i = 0; $tree->getLvl() > $i; $i++ ){
                        $pre .= '- ';
                    }
                    return $pre.$tree->getName();
                },
                'empty_data' => null,
                'placeholder' => 'Brak',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('c')
                        ->select('c')
                        ->orderBy('c.root, c.lft, c.lvl, c.sort', 'ASC');
                    return $res;
                },
            ))
            ->add('file', FileType::class, array(
                'label' => 'Zdjęcie',
                'data_class' => NULL
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\ProductCategory'
        ));
    }
}
