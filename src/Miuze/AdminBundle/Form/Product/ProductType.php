<?php

namespace Miuze\AdminBundle\Form\Product;

use Miuze\AdminBundle\Entity\ProductCategory;
use Miuze\AdminBundle\Repository\ProductCategoryRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class ProductType extends AbstractType{
    public function getName(){
        return 'product_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('provider', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Provider',
                'label' => 'Dostawca',
                'choice_label' => 'name',
                'empty_data' => null,
                'placeholder' => 'Brak',
            ))
            ->add('vat', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Vat',
                'label' => 'Stawka VAT',
                'choice_label' => function($item){
                    return $item->getRate(). '%';
                },
                'empty_data' => null,
                'placeholder' => 'Ustaw VAT',
            ))
            ->add('name', TextType::class, array(
                'label' => 'Nazwa produktu',
                'attr' => array(
                    'placeholder' => 'Nazwa produktu',
                ),
            ))
            ->add('code', TextType::class, array(
                'label' => 'Symbol produktu',
                'attr' => array(
                    'placeholder' => 'Symbol produktu',
                ),
            ))
            ->add('ean', TextType::class, array(
                'label' => 'Kod ean',
                'attr' => array(
                    'placeholder' => 'Kod ean',
                ),
            ))
            ->add('tags', TextType::class, array(
                'label' => 'Tagi',
                'attr' => array(
                    'placeholder' => 'Tagi produktu',
                ),
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'Pełny opis',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
            ))  
            ->add('lead', TextareaType::class, array(
                'label' => 'Zajawka w sliderze',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
            ))
            ->add('details', TextareaType::class, array(
                'label' => 'Parametry techniczne',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
            ))
            ->add('isStock', ChoiceType::class, array(
                'label' => 'Dostępny',
//                'expanded' => true,                
                'choices'  => array(
                    'Tak' => TRUE,
                    'Nie' => FALSE,
                )
            ))
            ->add('priceOnLogin', ChoiceType::class, array(
                'label' => 'Cena tylko po zalogowaniu',
//                'expanded' => true,                
                'choices'  => array(
                    'Tak' => TRUE,
                    'Nie' => FALSE,
                )
            ))
            ->add('price', MoneyType::class, array(
                'label' => 'Cena podstawowa w zł np. (12,99)',
                'attr' => array(
                    'placeholder' => 'Cena podstawowa netto',
                ),
                'currency' => 'PLN'
            ))
            ->add('promoPrice', MoneyType::class, array(
                'label' => 'Cena promocyjna w zł np. (12,99)',
                'attr' => array(
                    'placeholder' => 'Cena podstawowa',
                ),
                'currency' => 'PLN'
            ))
            ->add('category', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:ProductCategory',
                'label' => 'Kategoria',
                'multiple' => false,
                'expanded' => false,
                'choice_label' => function($tree){
                    $pre = '|';
                    for($i = 0; $tree->getLvl() > $i; $i++ ){
                        $pre .= '- ';
                    }
                    return $pre.$tree->getName();
                },
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('c')
                        ->select('c')
                        ->orderBy('c.root, c.sort, c.lft', 'ASC');
                    return $res;
                },
            ))
                
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Product',
            'validation_groups' => array('add'),
        ));
    }
}
