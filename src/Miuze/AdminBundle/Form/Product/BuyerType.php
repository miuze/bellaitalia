<?php

namespace Miuze\AdminBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class ClientType extends AbstractType{
    public function getName(){
        return 'buyer_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('firstName', TextType::class, array(
                'label' => 'Imię',
                'attr' => array(
                    'placeholder' => 'Imię',
                ),
            ))
            ->add('lastName', TextType::class, array(
                'label' => 'Nazwisko',
                'attr' => array(
                    'id' => 'blindHeight',
                    'placeholder' => 'Nazwisko',
                ),
            ))
            ->add('city', TextType::class, array(
                'label' => 'Miejscowość',
                'attr' => array(
                    'placeholder' => 'Miejscowość',
                ),
            ))
            ->add('postCode', TextType::class, array(
                'label' => 'Kod pocztowy',
                'attr' => array(
                    'placeholder' => 'Kod pocztowy',
                ),
            ))
            ->add('street', TextType::class, array(
                'label' => 'Ulica',
                'attr' => array(
                    'placeholder' => 'Ulica',
                ),
            ))
            ->add('homeNumber', TextType::class, array(
                'label' => 'Numer domu',
                'attr' => array(
                    'placeholder' => 'Numer domu',
                ),
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'placeholder' => 'Email',
                ),
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Telefon',
                'attr' => array(
                    'placeholder' => 'Telefon',
                ),
            ))
            ->add('travel', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Travel',
                'label' => 'Sposób dostawy',
                'choice_label' => function ($travel) {
                    return $travel->getName(). ' - '.$travel->getPrice().' zł' ;
                }
            ))                   
            ->add('submit', SubmitType::class, array(
                'label' => 'Przejdź do podsumowania',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\UserBundle\Entity\User'
        ));
    }
}
