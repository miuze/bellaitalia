<?php

namespace Miuze\AdminBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;

class ProductSearchType extends AbstractType{
    public function getName(){
        return 'productSearch_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                
            ->add('name', TextType::class, array(
                'label' => FALSE,
                'attr' => array(
                    'placeholder' => 'Szukaj w tytułach',
                ),
            ))
            ->add('content', TextType::class, array(
                'label' => FALSE,
                'attr' => array(
                    'placeholder' => 'Szukaj w opisie',
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Szukaj',
                'attr' => array(
                    'class'=> 'btn-0'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Product',
            'validation_groups' => array('search'),
        ));
    }
}
