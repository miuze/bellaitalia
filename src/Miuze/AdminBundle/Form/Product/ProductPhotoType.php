<?php

namespace Miuze\AdminBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class ProductPhotoType extends AbstractType{
    public function getName(){
        return 'product_photo_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
//            ->add('product', EntityType::class, array(
//                'class' => 'MiuzeAdminBundle:Product',
//                'label' => 'Produkt',
//                'choice_label' => 'name',
//            ))   
            ->add('file', FileType::class, array(
                    'label' => 'Zdjęcie',
                    'attr' => array(
                        'multiple' => true,
                        'data_class' => NULL
                    )
                ));
//            ->add('submit', SubmitType::class, array(
//                'label' => 'Dodaj',
//                'attr' => array(
//                    'class'=> 'btn btn-info'
//                )
//            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\ProductPhoto'
        ));
    }
}
