<?php

namespace Miuze\AdminBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class CartType extends AbstractType{
    public function getName(){
        return 'cart_form';
    }
    
    private $options = array();
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
//        dump($options);die;
        $builder
            ->add('quantity', NumberType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Ilość sztuk',
                ),
            ))
            ->add('price', HiddenType::class, array(
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj do koszyka',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Cart',
            'productId'     => 0
        ));
    }
    
//    public function __construct(array $options = array())
//    {
//        $resolver = new OptionsResolver();
//        $resolver->setDefaults(array(
//            'productId'     => 0,
//        ));
//
//        $this->options = $resolver->resolve($options);
//    }
}
