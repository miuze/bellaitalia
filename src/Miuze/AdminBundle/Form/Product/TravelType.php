<?php

namespace Miuze\AdminBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class TravelType extends AbstractType{
    public function getName(){
        return 'travel_form';
    }
    
    private $options = array();
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('travel', EntityType::class, array(
                'label' => 'Typ dostawy',
                'class' => 'MiuzeAdminBundle:Travel',
                'choice_label' => function($travel){
                    return $travel->getName().' - '.$travel->getPrice();
                },
            ))
            ->add('message', TextareaType::class, array(
                'label' => 'Wiadomość do sprzedawcy',
                'attr' => array(
                    'placeholder' => 'Wpisz wiadomość',
                    'rows' => 5
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Przejdź do podsumowania',
                'attr' => array(
                    'class'=> 'btn-0'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Order'
        ));
    }
}
