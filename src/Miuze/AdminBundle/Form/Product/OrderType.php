<?php

namespace Miuze\AdminBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class OrderType extends AbstractType{
    public function getName(){
        return 'order_form';
    }
    
    private $options = array();
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
//        dump($options);die;
        $builder
            
            ->add('name', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Imię i nazwisko'
                )
            ))
            ->add('address', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'ulica, kod, miejscowość'
                )
            ))
            ->add('phone', NumberType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Telefon'
                )
            ))
            ->add('products', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Product',
                'label' => 'Produkty',
                'choice_label' => 'name', 
                'empty_data' => null,
                'multiple' => true,
                'placeholder' => 'Wybierz produkty',
                'attr' => array(
                    'class' => 'multiple',
                )
            ))
            ->add('message', TextareaType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Wiadomość dla sprzedającego'
                )
            )) 
            ->add('submit', SubmitType::class, array(
                'label' => 'Wyślij zamówienie',
                'attr' => array(
                    'class'=> 'btn btn-send'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Orders',
        ));
    }
}
