<?php

namespace Miuze\AdminBundle\Form\Ad;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class AdPhotoType extends AbstractType{
    public function getName(){
        return 'ad_photo_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('ad', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Ad',
                'label' => 'Ogłoszenie',
                'choice_label' => 'title',
            ))   
            ->add('name', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Podaj nazwę pliku',
                ),
            ))
            ->add('file', FileType::class, array(
                    'label' => 'Plik'
                ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\AdPhoto'
        ));
    }
}
