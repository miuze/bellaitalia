<?php

namespace Miuze\AdminBundle\Form\Ad;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityRepository;

class AdType extends AbstractType{
    public function getName(){
        return 'ad_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('category', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:AdCategory',
                'label' => 'Kategoria',
                'choice_label' => 'title',
            ))   
            ->add('title', TextType::class, array(
                'label' => 'Tytuł',
                'attr' => array(
                    'placeholder' => 'Podaj tytuł',
                ),
            ))
            ->add('times', TextType::class, array(
                'label' => 'Czas od - do',
                'attr' => array(
                    'placeholder' => 'Podaj termin wyjazdu od - do',
                ),
            ))
            ->add('place', TextType::class, array(
                'label' => 'Miejsce',
                'attr' => array(
                    'placeholder' => 'Podaj miejsce',
                ),
            ))
            ->add('price', TextType::class, array(
                'label' => 'Cena',
                'attr' => array(
                    'placeholder' => 'Podaj cene i informacje do niej',
                ),
            ))
            ->add('organizer', TextType::class, array(
                'label' => 'Organizator',
                'attr' => array(
                    'placeholder' => 'Podaj cene i informacje do niej',
                ),
            ))
            ->add('old', TextType::class, array(
                'label' => 'Wiek uczestników',
                'attr' => array(
                    'placeholder' => 'Podaj zakres wieku',
                ),
            ))
            ->add('priceHave', TextareaType::class, array(
                'label' => 'W cenie jest',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
                
            ))
            ->add('priceNotHave', TextareaType::class, array(
                'label' => 'W cenie nie jest',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
                
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'Treść ogłoszenia',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
                
            ))
            ->add('lead', TextareaType::class, array(
                'label' => 'Krótki opis oferty',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
                
            ))
            ->add('attributes', TextType::class, array(
                'label' => 'Charakterystyka wyjazdu',
                'attr' => array(
                    'placeholder' => 'Charakterystyka obozu',
                ),
            ))                
            ->add('createDate', DateType::class, array(
                'label' => 'Data utworzenia',
                'widget' => 'single_text', 
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'readonly' => true,
                    'class' => 'datepicker',
                )
            ))
            ->add('startDate', DateType::class, array(
                'label' => 'Data publikacji',
                'widget' => 'single_text', 
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'readonly' => true,
                    'class' => 'datepicker',
                )
            ))
            ->add('endDate', DateType::class, array(
                'label' => 'Data zakończenia',
                'widget' => 'single_text', 
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                )
            ))
            ->add('file', FileType::class, array(
                'label' => 'Zdjęcie wyróżniające',
                'data_class' => NULL
            ))
            ->add('disable', ChoiceType::class, array(
                'label' => 'Aktywność',
                'expanded' => true,                
                'choices'  => array(
                    'W trakcie' => FALSE,
                    'Zakończono' => TRUE,
                )
            ))
                        
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Ad'
        ));
    }
}
