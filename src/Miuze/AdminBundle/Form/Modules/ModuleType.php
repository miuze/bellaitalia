<?php
/**
 * Created by PhpStorm.
 * User: Kamil
 * Date: 2018-02-05
 * Time: 19:39
 */

namespace Miuze\AdminBundle\Form\Modules;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;

class ModuleType extends AbstractType{

    public function getName(){
        return 'modul_edit_form';
    }

    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa modułu',
                'attr' => array(
                    'placeholder' => 'Nazwa modułu',
                    'readonly' => true
                ),
            ))
            ->add('title', TextType::class, array(
                'label' => 'Wyświetlana nazwa',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę do wyświetlenia',
                ),
            ))
            ->add('icon', TextType::class, array(
                'label' => 'Ikona FontaAwesome',
                'attr' => array(
                    'placeholder' => 'Podaj klasę fa fa-*',
                ),
            ))
            ->add('enabled', ChoiceType::class, array(
                'label' => 'Stan modułu',
                'choices' => array(
                    'Wyłączony' => false,
                    'Włączony' => true
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-info pull-right'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Module',
        ));
    }
}