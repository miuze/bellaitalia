<?php

namespace Miuze\AdminBundle\Form\Cart;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class PaymentType extends AbstractType{
    public function getName(){
        return 'payment_form';
    }
    
    private $options = array();
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa dostawy',
                'attr' => array(
                    'placeholder' => 'Wpisz nazwę',
                ),
            ))
            ->add('type', ChoiceType::class, array(
                'label' => 'Tryb działania',
                'attr' => array(
                    'placeholder' => 'Wpisz nazwę',
                ),
                'choices' => array(
                    'Online' => 'online',
                    'Przelew' => 'transfer',
                    'Stacjonarnie' => 'station'
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj',
                'attr' => array(
                    'class'=> 'btn-success'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Payment'
        ));
    }
}
