<?php

namespace Miuze\AdminBundle\Form\Cart;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;

class CartType extends AbstractType{
    public function getName(){
        return 'cart_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('quantity', NumberType::class, array(
                'label' => 'Ilość',
            ))

                        
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj do koszyka',
                'attr' => array(
                    'class'=> 'btn-0'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Cart'
        ));
    }
}
