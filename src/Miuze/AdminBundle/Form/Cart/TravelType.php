<?php

namespace Miuze\AdminBundle\Form\Cart;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class TravelType extends AbstractType{
    public function getName(){
        return 'travel_admin_form';
    }
    
    private $options = array();
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa usługi transportu',
                'attr' => array(
                    'placeholder' => 'Wpisz nazwę',
                ),
            ))
            ->add('price', MoneyType::class, array(
                'label' => 'Cena brutto w zł np. (12,99)',
                'attr' => array(
                    'placeholder' => 'Cena brutto',
                ),
                'currency' => 'PLN'
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj',
                'attr' => array(
                    'class'=> 'btn-success'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Travel'
        ));
    }
}
