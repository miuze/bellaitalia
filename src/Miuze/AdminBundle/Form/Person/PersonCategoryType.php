<?php

namespace Miuze\AdminBundle\Form\Person;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class PersonCategoryType extends AbstractType{
    public function getName(){
        return 'person_cat_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            
            ->add('title', TextType::class, array(
                'label' => 'Nazwa',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę kategorii',
                ),
            ))   
            
            ->add('page', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Page',
                'label' => 'Strony',
                'multiple' => true,
                'choice_label' => function($entity)
                {
                    return $entity->getSlug()->getName();
                },
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('p')
                        ->select('p');
                    return $res;
                },
            ))   
                
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));            
    }
    
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\PersonCategory'
        ));
    }
}
