<?php

namespace Miuze\AdminBundle\Form\Person;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;

class PersonType extends AbstractType{
    public function getName(){
        return 'person_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('category', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:PersonCategory',
                'label' => 'Kategoria',
                'choice_label' => 'title',
            ))   
            ->add('firstname', TextType::class, array(
                'label' => 'Imię',
                'attr' => array(
                    'placeholder' => 'Podaj imię',
                ),
            ))
            ->add('lastname', TextType::class, array(
                'label' => 'Podaj nazwisko',
                'attr' => array(
                    'placeholder' => 'Podaj nazwisko',
                ),
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Telefon',
                'attr' => array(
                    'placeholder' => 'Podaj telefon',
                ),
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'placeholder' => 'Podaj adres email',
                ),
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Opis',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
                
            ))
            ->add('position', TextType::class, array(
                'label' => 'Stanowisko',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę stanowiska'
                )
            ))
            
            ->add('place', TextType::class, array(
                'label' => 'Miasto',
                'attr' => array(
                    'id' => 'ckeditor',
                    'placeholder' => 'Podaj miasto'
                ),
                
            ))
            
            ->add('file', FileType::class, array(
                'label' => 'Zdjęcie',
                'data_class' => NULL
            ))
                        
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Person'
        ));
    }
}
