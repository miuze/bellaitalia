<?php

namespace Miuze\AdminBundle\Form\Save;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Validator\Constraints as Assert;

class SaveType extends AbstractType{
    public function getName(){
        return 'save_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder   
            ->add('startDate', DateType::class, array(
                'label' => 'Przybycie',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'label_attr' => array(
                    'class' => 'form-label-outside'
                ),
                'attr' => array(
                    'readonly' => true,
                    'placeholder' => 'Data przybycia',
                    'class' => 'form-input form-control-has-validation form-control-last-child',
                    'data-time-picker' => "date",
                    'style' => "font-size: 20px; -webkit-text-fill-color: #989898"
                )
            )) 
            ->add('endDate', DateType::class, array(
                'label' => 'Opuszczenie',
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'label_attr' => array(
                    'class' => 'form-label-outside'
                ),
                'attr' => array(
                    'readonly' => true,
                    'placeholder' => 'Data opuszczenia',
                    'class' => 'form-input form-control-has-validation form-control-last-child',
                    'data-time-picker' => "date",
                    'style' => "font-size: 20px; -webkit-text-fill-color: #989898"
                )
            )) 
            ->add('email', EmailType::class, array(
                'label' => 'Adres e-mail',
                'label_attr' => array(
                    'class' => 'form-label-outside'
                ),
                'attr' => array(
                    'placeholder' => 'Adres email',
                    'class' => 'form-input form-control-has-validation form-control-last-child',
                    'style' => "font-size: 20px; -webkit-text-fill-color: #989898"
                ),
            ))                
            ->add('submit', SubmitType::class, array(
                'label' => 'Wyślij zapytanie',
                'attr' => array(
                    'class'=> 'button button-primary'
                )
            ));            
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Save'
        ));
    }
}
