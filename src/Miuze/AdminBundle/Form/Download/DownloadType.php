<?php

namespace Miuze\AdminBundle\Form\Download;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityRepository;

class DownloadType extends AbstractType{
    public function getName(){
        return 'download_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('category', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:DownloadCategory',
                'label' => 'Kategoria',
                'choice_label' => 'title',
            ))   
            ->add('title', TextType::class, array(
                'label' => 'Tytuł',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę strony',
                ),
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'Opis',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
                
            ))
                        
            ->add('createDate', DateType::class, array(
                'label' => 'Data utworzenia',
                'widget' => 'single_text', 
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'readonly' => true,
                )
            ))
            ->add('startDate', DateType::class, array(
                'label' => 'Data publikacji',
                'widget' => 'single_text', 
                'format' => 'dd-MM-yyyy',
                'attr' => array(                    
                    'class' => 'datepicker',
                )
                
            ))
            ->add('endDate', DateType::class, array(
                'label' => 'Data zakończenia',
                'widget' => 'single_text', 
                'format' => 'dd-MM-yyyy',
                'attr' => array(                    
                    'class' => 'datepicker',
                )
            ))
            ->add('updateDate', DateType::class, array(
                'label' => 'Data aktualizacji',
                'widget' => 'single_text', 
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'readonly' => true,
                )
            ))
                   
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Download'
        ));
    }
}
