<?php

namespace Miuze\AdminBundle\Form\Download;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityRepository;

class DownloadFileType extends AbstractType{
    public function getName(){
        return 'download_file_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
//            ->add('download', EntityType::class, array(
//                'class' => 'MiuzeAdminBundle:Download',
//                'label' => 'Download',
//                'choice_label' => 'title',
//                "multiple" => true
//            )) 
            ->add('title', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Tytuł pliku'
                )
            ))
            ->add('file', FileType::class, array(
                'label' => 'Plik',
                'data_class' => NULL
            ))
                
                        
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\DownloadFile'
        ));
    }
}
