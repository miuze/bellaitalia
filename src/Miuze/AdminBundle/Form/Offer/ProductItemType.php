<?php

namespace Miuze\AdminBundle\Form\Offer;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;

class ProductItemType extends AbstractType{
    public function getName(){
        return 'product_item_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('product', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Product',
                'label' => 'Produkty',
                'choice_label' => 'name',
                'choice_attr' => function($product) {
                    return ['data-price' => $product->calcNettoNoRabate()];
                },
                'attr' => array(
                    'class' => 'chosen-select',
                    'tabindex' => '2',
                ),
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.isStock = 1');
                    return $res;
                },

            ))

            ->add('quantity', NumberType::class, array(
                'label' => 'Ilość',
            ))
            ->add('price', MoneyType::class, array(
                'label' => 'Cena podstawowa w zł np. (12,99)',
                'attr' => array(
                    'placeholder' => 'Cena podstawowa',
                ),
                'currency' => 'PLN'
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\ProductOffer'
        ));
    }
}
