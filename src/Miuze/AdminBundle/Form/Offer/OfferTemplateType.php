<?php

namespace Miuze\AdminBundle\Form\Offer;

use Miuze\AdminBundle\Form\Offer\ProductItemType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class OfferTemplateType extends AbstractType{
    public function getName(){
        return 'newsletter_template_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            
            ->add('content', TextareaType::class, array(
                'label' => 'Treść wiadomości',
                'attr' => array(
                    'placeholder' => 'Treść wiadomości',
                ),
            ))
            ->add('productItems', CollectionType::class, array(
                'entry_type' => ProductItemType::class,
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'entry_options'  => array(
                    'attr' => array(
                        'class' => 'cart-box'
                    )
                ),

            ))
            ->add('email', TextType::class, array(
                'label' => 'Adres odbiorcy',
                'attr' => array(
                    'placeholder' => 'Adres email odbiorcy',
                ),
            ))
            ->add('subscriber', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Newsletter',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('n')
                        ->select('n')
                        ->where('n.active = 1');
                    return $res;
                    },
                'label' => 'Wybierz odbiorce',
                'choice_label' => 'email',
                'attr' => array(
                    'class' => 'chosen-select'
                ),
                'placeholder'   =>'Wybież istniejący email',
                'empty_data'  => null,
            ))   
                
            ->add('submit', SubmitType::class, array(
                'label' => 'Podgląd',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Offer'
        ));
    }
}
