<?php

namespace Miuze\AdminBundle\Form\Top;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints as Assert;

class TopType extends AbstractType{
    public function getName(){
        return 'top_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder  
            ->add('file', FileType::class, array(
                    'label' => 'Dodaj zdjęcie'
                ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Top'
        ));
    }
}
