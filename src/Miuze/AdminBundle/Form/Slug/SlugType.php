<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Form\Slug;

use Miuze\AdminBundle\Entity\Slug;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SlugType extends AbstractType
{
    public function getName(){
        return 'slug_form';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'Tytuł',
                    'attr' => [
                        'placeholder' => 'Podaj tytuł',
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            'data_class' => Slug::class
        ]);
    }
}
