<?php

namespace Miuze\AdminBundle\Form\System;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints as Assert;

class SystemType extends AbstractType{
    public function getName(){
        return 'system_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('domain', TextType::class, array(
                'label' => 'Domena',
                'attr' => array(
                    'placeholder' => 'Podaj domenę (http://example.com)',
                ),
            ))
            ->add('companyName', TextType::class, array(
                'label' => 'Nazwa firmy',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę firmy',
                ),
            ))
            ->add('title', TextType::class, array(
                'label' => 'Tytuł',
                'attr' => array(
                    'placeholder' => 'Podaj tytuł strony',
                ),
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Opis strony',
                'attr' => array(
                    'placeholder' => 'Podaj opis około 80 znaków',
                    'class' => 'no-wysiwyg',
                    'style' => 'border-radius:0px; height: 300px'
                ),
            ))
            ->add('mailMaster', TextType::class, array(
                'label' => 'Konto email do wysyłania e-mail',
                'attr' => array(
                    'placeholder' => 'Podaj e-mail',
                ),
            ))
            ->add('emailContact', TextType::class, array(
                'label' => 'Email do kontaktu',
                'attr' => array(
                    'placeholder' => 'Podaj e-mail',
                ),
            ))
            ->add('file', FileType::class, array(
                'label' => 'Logo',
                'data_class' => NULL
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\System'
        ));
    }
}
