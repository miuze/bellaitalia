<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Form\Contact;

use Karser\Recaptcha3Bundle\Form\Recaptcha3Type;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3;
use Miuze\AdminBundle\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType
{
    public function getName(): string
    {
        return 'contact_form';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        
        $builder            
            ->add('title', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Tytuł*',
                ],
            ])
            ->add('content', TextareaType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Treść wiadomości*',
                ],
                
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Adres email*',
                ],
            ])
            ->add('captcha', Recaptcha3Type::class, [
                'constraints' => new Recaptcha3(),
//                'action_name' => 'homepage',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Wyślij',
                'attr' => [
                    'class'=> 'submit-btn-1 mb-0'
                ]
            ]);
    }
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Contact::class,
            'validation_groups' => array('contact'),
        ));
    }
}
