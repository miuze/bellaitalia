<?php

namespace Miuze\AdminBundle\Form\Faq;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityRepository;

class FaqType extends AbstractType{
    public function getName(){
        return 'faq_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('category', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:FaqCategory',
                'label' => 'Kategoria',
                'choice_label' => 'title',
            ))   
            ->add('title', TextType::class, array(
                'label' => 'Tytuł',
                'attr' => array(
                    'placeholder' => 'Podaj tytuł',
                ),
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'Treść',
                'attr' => array(
                    'id' => 'ckeditor',
                ),
            ))
            ->add('lead', TextareaType::class, array(
                'label' => 'Krótka treść',
            ))
            ->add('file', FileType::class, array(
                'label' => 'Plik',
                'data_class' => NULL
            ))
                        
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));         
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Faq'
        ));
    }
}
