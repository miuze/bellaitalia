<?php
/**
 * Created by PhpStorm.
 * User: Kamil
 * Date: 2018-02-05
 * Time: 19:40
 */

namespace Miuze\AdminBundle\Form\Paramiter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;

class ParamiterType extends AbstractType{

    public function getName(){
        return 'paramiter_subform_form';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder

            ->add('name', TextType::class, array(
                'label' => 'Nazwa',
                'attr' => array(
                    'placeholder' => 'Podaj nazwę parametru',
                    'readonly' => $options['read']
                ),
            ))
            ->add('value', TextType::class, array(
                'label' => 'Wartość',
                'attr' => array(
                    'placeholder' => 'Podaj wartość parametru',
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-info pull-right'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Paramiter',
            'read' => false
        ));
    }
}