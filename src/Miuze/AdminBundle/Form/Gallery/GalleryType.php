<?php

namespace Miuze\AdminBundle\Form\Gallery;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints as Assert;

class GalleryType extends AbstractType{
    public function getName(){
        return 'gallery_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa galerii',
                'attr' => array(
                    'placeholder' => 'Nazwa galerii',
                ),
            ))
                
            ->add('category', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:GalleryCategory',
                'label' => 'Kategoria',
                'choice_label' => 'name',
            ))  
            ->add('file', FileType::class, array(
                'label' => 'Zdjęcie wyróżniające',
                'data_class' => NULL
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Gallery'
        ));
    }
}
