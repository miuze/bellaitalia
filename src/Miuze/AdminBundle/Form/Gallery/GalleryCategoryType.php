<?php

namespace Miuze\AdminBundle\Form\Gallery;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class GalleryCategoryType extends AbstractType{
    public function getName(){
        return 'gallery_category_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa kategorii',
                'attr' => array(
                    'placeholder' => 'Nazwa kategorii',
                ),
            ))
            ->add('page', EntityType::class, array(
                'class' => 'MiuzeAdminBundle:Page',
                'label' => 'Strony',
                'multiple' => true,
                'choice_label' => 'title',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                    $res = $repository->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.type = :gallery')
                        ->setParameter('gallery', 'gallery');
                    return $res;
                },
            ))   
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\GalleryCategory'
        ));
    }
}
