<?php

namespace Miuze\AdminBundle\Form\Lang\Lang;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class LangType extends AbstractType{
    public function getName(){
        return 'lang_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('lang', TextType::class, array(
                'label' => FALSE,
                'attr' => array(
                    'placeholder' => 'Wprowadź kod języka "PL"',
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj',
                'attr' => array(
                    'class'=> 'btn btn-info'
                )
            ));
    }
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\AdminBundle\Entity\Lang'
        ));
    }
}
