<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Twig\Model;

use Twig\Environment;

interface TwigExtensionInterface
{
    public function getOne(Environment $environment, int $id, string $template);

    public function getForPage(Environment $environment, int $pageId, string $template);
}