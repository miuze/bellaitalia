<?php

namespace Miuze\AdminBundle\Twig\Extension;

use Miuze\AdminBundle\Entity\Contact;
use Miuze\AdminBundle\Form\Contact\FastContactType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment;

class ContactExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;
    
    function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function getName(){
        return 'miuze_contact_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction(
                'render_fast_contact',
                [$this, 'fastContact'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
        );
    }
    
    public function fastContact(Environment $environment, $msg = '', $template = 'form.html.twig')
    {
        $contact = new Contact();
        $contact->setContent($msg);
        $form = $this->container->get('form.factory')->create( FastContactType::class, $contact, [
            "method" => "post",
            "action" => $this->container->get('router')->generate("contact-ajax"),
            "attr" => [
                "id" => "fastContact"
            ]
        ]);
        return $environment->render('@MiuzePage/Contact/partials/'.$template, [
            'form' => $form->createView()
        ]);
    }

}
