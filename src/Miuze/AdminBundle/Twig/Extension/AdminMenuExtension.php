<?php

namespace Miuze\AdminBundle\Twig\Extension;

use Miuze\AdminBundle\Entity\Module;
use Symfony\Component\Yaml\Yaml;

/**
 * Description of PageExtension
 *
 * @author User
 */
class AdminMenuExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_admin_menu_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('get_admin_menu', array($this, 'adminMenu'), array('is_safe' => array('html'))),
        );
    }

    public function adminMenu(){
        $file = __DIR__. '/../../../../../app/config/modules.yml';
        $values = Yaml::parseFile($file);
        $menu = [];

        if(!empty($values)) {
            foreach ($values as $key => $item) {
                if ($item['enabled'] == 1) {
                    $entity = new Module();
                    $entity->setName($key);
                    $entity->setTitle($item['title']);
                    $entity->setIcon($item['icon']);
                    $entity->setLinks($item['links']);
                    array_push($menu, $entity);
                }
            }
        }
        return $menu;

    }
    
}
