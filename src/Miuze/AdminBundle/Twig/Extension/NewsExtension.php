<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class NewsExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
        $this->doctrine = $doctrine;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_news_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('get_news', array($this, 'news')),
            new \Twig_SimpleFunction('slider', array($this, 'slider'))
        );
    }

    public function news($limit = 10){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:NewsCategory');
        $news = $repo->getNews(array('limit' => $limit));
        
        return $news;
    }
    
    public function slider($limit = 10){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:NewsCategory');
        $news = $repo->getSlider(array('limit' => $limit));
        
        return $news;
    }
    
}
