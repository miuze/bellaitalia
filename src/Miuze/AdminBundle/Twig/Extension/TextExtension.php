<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class TextExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
        $this->doctrine = $doctrine;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_text_extension';
    }
    
    public function getFilters(){
        return array(
            new \Twig_SimpleFilter('text_limiter', array($this, 'textLimiter'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('get_ext', array($this, 'getExt'), array('is_safe' => array('html'))),
        );
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('remove_hash', array($this, 'removeHash'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('toInt', array($this, 'toInt'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('explode_strineg', array($this, 'explodeStrineg'), array('is_safe' => array('html'))),
        );
    }
    
    public function toInt($string){
        $string = (int)$string;
        return $string;
    }

    public function textLimiter($text, $limit = 20){
        
        $explode = explode(' ', $text);
        $string = '';

        $dots = '...';
        if (count($explode) <= $limit) {
            $dots = '';
        }
        for ($i = 0; $i < $limit; $i++) {
            $string .= $explode[$i] . " ";
        }
        return $text;

    }
    
    public function getExt($file){
        
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        return $ext;
    }
    
    public function removeHash($text){
        $text = explode(' ', $text);
        $r = str_replace('#', '', $text[0]);
        
        return $r;
    }
    
    public function explodeStrineg($text){
        $text = explode(',', $text);
        $ar = array();
        foreach($text as $v){
            array_push($ar, $v);
        }
        return $ar;
    }
    
}
