<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Twig\Extension;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\AdminBundle\Twig\Model\TwigExtensionInterface;
use Twig\Extension\AbstractExtension;
use Twig\Environment;
use Twig\TwigFunction;

class PersonExtension extends AbstractExtension implements TwigExtensionInterface
{
    /**
     * @var Registry
     */
    private $doctrine;
    
    function __construct(Registry $doctrine) {
        $this->doctrine = $doctrine;
    }

    public function getName(): string
    {
        return 'miuze_person_extension';
    }

    public function getFunctions(){
        return [
            new TwigFunction(
                'render_person',
                [$this, 'getOne'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
            new TwigFunction(
                'render_persons',
                [$this, 'getForPage'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
        ];
    }

    public function getOne(Environment $environment, int $id, string $template)
    {
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Person');
        $person = $repo->findOneById($id);

        return $environment->render('@MiuzePage/Person/partials/'.$template, array('person' => $person));
    }

    public function getForPage(Environment $environment, int $pageId, string $template)
    {
        // TODO: Implement getForPage() method.
    }
}
