<?php

namespace Miuze\AdminBundle\Twig\Extension;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Description of PageExtension
 *
 * @author User
 */
class SettingsExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    private $session;

    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine, Session $session) {
        $this->doctrine = $doctrine;
        $this->session = $session;
    }
    
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_settings_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('styles', array($this, 'styles'), array('is_safe' => array('html'))),
        );
    }

    public function styles(){
        
        $style = $this->session->get('monostyle');
       
        return $style;
    }
    
}
