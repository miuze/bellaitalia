<?php

namespace Miuze\AdminBundle\Twig\Extension;

use Miuze\AdminBundle\Entity\Newsletter;
use Miuze\AdminBundle\Form\Newsletter\NewsletterType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment;

class NewsletterExtension extends \Twig_Extension{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Newsletter
     */
    private $newsletter;

    function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->newsletter = new Newsletter();
    }

    public function getName(){
        return 'miuze_newsletter_extension';
    }
    
    public function getFunctions(){
        return [
            new \Twig_SimpleFunction(
                'render_newsletter',
                [$this, 'newsletter'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
        ];
    }
    
    public function newsletter(Environment $environment){
        $form = $this->container->get('form.factory')->create( NewsletterType::class, $this->newsletter, [
            "method" => "post",
            "action" => $this->container->get('router')->generate("add_subscriber")
        ]);
        return $environment->render('@MiuzePage/Newsletter/form.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
