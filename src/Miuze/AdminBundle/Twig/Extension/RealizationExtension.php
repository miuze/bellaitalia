<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class RealizationExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
        $this->doctrine = $doctrine;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_realization_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('get_realization', array($this, 'getRealization')),
        );
    }

    public function getRealization($limit = 10){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Realization');
        $news = $repo->getRealization(array('limit' => $limit));
        
        return $news;
    }
    
   
    
}
