<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class BlogExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;

    private $container;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine, $container) {
        $this->doctrine = $doctrine;
        $this->container = $container;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_blog_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('blog_split_tag', array($this, 'blogSplitTag')),
            new \Twig_SimpleFunction('blog_get_last_post', array($this, 'getLastPost'), array('is_safe' => array('html'))),
        );
    }

    public function blogSplitTag($tags){
        $tagsArr = explode(',',$tags);
        $tagsArray = [];
        foreach($tagsArr as $item){
            $tagsArray[str_replace(' ', '-', strtolower(trim($item)))] = strtolower(trim($item));
        }
        return $tagsArray;
    }

    public function getLastPost($page, $limit = 5, $template = 'sidebar_post.html.twig' ){
        if(!is_object($page)){
            $page = $this->doctrine->getRepository('MiuzeAdminBundle:Page')->findOneById($page);
        }
        $posts = $this->doctrine->getRepository('MiuzeAdminBundle:Blog')->getLastPost(array(
            'page' => $page->getId(),
            'limit' => $limit,
        ));
        if($page == null || empty($posts)){
            return null;
        }
        return $this->environment->render('@MiuzePage/Blog/partials/'.$template, array(
            'page' => $page,
            'posts' => $posts
        ));
    }
    
}
