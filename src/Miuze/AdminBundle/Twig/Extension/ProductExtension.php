<?php

namespace Miuze\AdminBundle\Twig\Extension;

use Symfony\Component\HttpFoundation\RequestStack;
use Miuze\AdminBundle\Service\Tree;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Miuze\AdminBundle\Form\Product\ProductSearchType;
use Miuze\AdminBundle\Entity\Product;

/**
 * Description of PageExtension
 *
 * @author User
 */
class ProductExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    private $request;

    private $tree;

    private $router;

    private $container;

    private $session;
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine, RequestStack $requestStack, Tree $tree, $router, $container, Session $session) {
        $this->doctrine = $doctrine;
        $this->request = $requestStack;
        $this->tree = $tree;
        $this->router = $router;
        $this->container = $container;
        $this->session = $session;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_product_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('get_main_product', array($this, 'getMainProduct')),
            new \Twig_SimpleFunction('get_last_product', array($this, 'getLastProduct')),
            new \Twig_SimpleFunction('get_last_product_cat', array($this, 'getCategoryProduct')),
            new \Twig_SimpleFunction('get_product_cat', array($this, 'getMainCategoryProduct')),
            new \Twig_SimpleFunction('get_product_cat_menu', array($this, 'getProductCatMenu')),
            new \Twig_SimpleFunction('formSearch', array($this, 'formSearch'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('get_breadcrumbs_product', array($this, 'getBreadcrumbsProduct'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('get_last_add_product', array($this, 'getLastAddProduct')),
            new \Twig_SimpleFunction('renderProduct', array($this, 'renderProduct'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('renderProductItem', array($this, 'renderProductItem'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('get_most_popular_product', array($this, 'getMostPopularProduct')),
            new \Twig_SimpleFunction('get_last_view_product', array($this, 'getLastViewProduct')),
            new \Twig_SimpleFunction('get_bestseller_product', array($this, 'getBestSellerProduct')),


        );
    }

    public function getLastProduct($limit = 10){
        $lang = $this->request->getMasterRequest()->getLocale();
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Product');
        $items = $repo->getProduct(array('limit' => $limit, 'lang' => $lang));
        
        return $items;
    }
    
    public function getCategoryProduct($cat = 1, $limit = 3){
        $lang = $this->request->getMasterRequest()->getLocale();
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Product');
        $items = $repo->getProduct(array('cat' => $cat, 'limit' => $limit, 'lang' => $lang));
        
        return $items;
    }
    
    public function getMainProduct($limit = 6){
        $lang = $this->request->getMasterRequest()->getLocale();
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Product');
        $items = $repo->getMainProduct(array('limit' => $limit, 'lang' => $lang));
        
        return $items;
    }
    
    public function getMainCategoryProduct($limit = 8){
        $lang = $this->request->getMasterRequest()->getLocale();
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:ProductCategory');
        $items = $repo->getMainCategory(array('limit' => $limit, 'lang' => $lang));
        
        return $items;
    }

    public function getProductCatMenu(){
        $lang = $this->request->getMasterRequest()->getLocale();
        $pageid = $this->request->getMasterRequest()->attributes->get('pageid');
        $title = $this->request->getMasterRequest()->attributes->get('title');
        $cat = $this->request->getMasterRequest()->attributes->getInt('cat');
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:ProductCategory');
        $categoryList = $repo->getLeftCategoryMenu(array('lang' => $lang, 'page' => $pageid));

        $id = $repo->findOneById($cat);
        if($id !== null) {
            $list = $repo->getOnlyChildren(array(
                'parentId' => $id->getId()
            ));
            if(empty($list) && $id->getParent() !== null) {
                $categoryList = $repo->getOnlyChildren(array(
                    'parentId' => $id->getParent()->getId()
                ));
            }else{
                $categoryList =$list;
            }
        }else{
            $categoryList = $repo->getLeftCategoryMenu(array('lang' => $lang, 'page' => $pageid));
        }


        return $categoryList;
    }


    public function getBreadcrumbsProduct(){

        $catId = $this->request->getMasterRequest()->attributes->getInt('cat');
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:ProductCategory');
        $node = $repo->findOneById($catId);
        if(!empty($node)) {
            $breadcrumbs = $repo->getPath($node);
        }else{
            $breadcrumbs = [];
        }

        return $breadcrumbs;
    }


    public function formSearch(){
        $product = new Product();
        $form = $this->container->get('form.factory')->create( ProductSearchType::class, $product, [
            "method" => "get",
            "action" => $this->container->get('router')->generate("product_search")
        ]);
        return $this->environment->render('MiuzePageBundle:Product:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function getLastAddProduct(){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Product');
        $products = $repo->getLastAddProduct($this->request->getMasterRequest()->getLocale());
        return $products;
    }

    public function renderProduct(Product $product, $class = ''){
        return $this->environment->render('MiuzePageBundle:Product:partials/product.html.twig', array(
            'product' => $product,
            'class' => $class
        ));

    }
    public function renderProductItem(Product $product, $class = '', $page){
        return $this->environment->render('MiuzePageBundle:Product:partials/productItem.html.twig', array(
            'product' => $product,
            'class' => $class,
            'page' => $page
        ));

    }

    public function getMostPopularProduct(){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Product');
        $products = $repo->getMorePopularProduct($this->request->getMasterRequest()->getLocale());
        return $products;
    }

    public function getLastViewProduct(){
        if($this->session->has('lastViewProduct')){
            $products = $this->session->get('lastViewProduct');
            if(!empty($products)){
                return $products;
            }
        }else{
            return [];
        }
    }

    public function getBestSellerProduct(){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Product');
        $products = $repo->getBestSellerProduct($this->request->getMasterRequest()->getLocale());
        return $products;
    }

   
    
}
