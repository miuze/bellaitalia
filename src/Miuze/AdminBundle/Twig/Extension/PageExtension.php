<?php

namespace Miuze\AdminBundle\Twig\Extension;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment;

/**
 * Description of PageExtension
 *
 * @author User
 */
class PageExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    private $request;
    
    private $container;
    
    private $router;

    private $tree;
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine, RequestStack $requestStack, $router, ContainerInterface $container, $tree) {
        $this->doctrine = $doctrine;
        $this->request = $requestStack;
        $this->router = $router;
        $this->container = $container;
        $this->tree = $tree;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_page_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('cat_menu', array($this, 'catMenu'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('menu_top', array($this, 'menuTop'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('menu_mobile', array($this, 'menuMobile'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction(
                'lang',
                [$this, 'lang'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
            new \Twig_SimpleFunction('get_lang', array($this, 'langPrint'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('get_page', array($this, 'getPage'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('get_Url', array($this, 'getUrl')),
            new \Twig_SimpleFunction('gUrlId', array($this, 'gUrlId')),
            new \Twig_SimpleFunction('get_Param', array($this, 'getParam'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('get_page_breadcrumbs', array($this, 'breadcrumbs')),
            
            // fukcje do modułów            
            new \Twig_SimpleFunction('getClients', array($this, 'getClients')),
        );
    }
    
    public function catMenu(){
        $catMenu = $this->doctrine->getRepository('MiuzeAdminBundle:PageCategory')->findAll();

        return $catMenu;
    }

    public function menuTop(){
        $lang = $this->request->getMasterRequest()->getLocale();

        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Page');
        $pages = $repo->getPageToTree(array(
            'lang' => $lang,
        ));
        if(!empty($pages)) {
            $options = array(
                'decorate' => true,
                'level0Open' => '<ul class="main-menu text-center">',
                'level0Close' => '</ul>',
                'rootOpen' => function ($node) {
                    return '<ul class="drop-menu">';
                },
                'rootClose' => '</ul>',
                'childOpen' => function ($node) {
                    return '<li>';
                },
                'childClose' => '</li>',
                'nodeDecorator' => function ($node) {
                    if($node['link'] == null){
                        if($node['type'] == 'index'){
                            $link = $this->router->generate('index');
                        }else{
                            $link = $this->router->generate('subpage', array('slug' => $node['slug']['slug']));
                        }
                    }else{
                        $link = $node['link'];
                    }
                    return '<a href="'.$link.'">' .$node['slug']['name'].'</a>';
                }


            );
            $tree = $repo->buildTreeArray($pages);
            $htmlTree = $this->tree->buildTreeHtml($tree, $options);
        }else{
            $htmlTree = null;
        }
//        dump($htmlTree);die;
        return $htmlTree;
    }

    public function menuMobile($menuId){
        $lang = $this->request->getMasterRequest()->getLocale();

        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Page');
        $pages = $repo->getPageToTree(array(
            'lang' => $lang,
            'menuId' => $menuId
        ));
        if(!empty($pages)) {
            $options = array(
                'decorate' => true,
                'level0Open' => '<ul>',
                'level0Close' => '</ul>',
                'rootOpen' => function ($node) {
                    return '<ul>';
                },
                'rootClose' => '</ul>',
                'childOpen' => function ($node) {
                    return '<li>';
                },
                'childClose' => '</li>',
                'nodeDecorator' => function ($node) {
                    if($node['link'] == null){
                        if($node['type'] == 'index'){
                            $link = $this->router->generate('index');
                        }else{
                            $link = $this->router->generate('subpage', ['slug' => $node['slug']['slug']]);
                        }
                    }else{
                        $link = $node['link'];
                    }
                    return '<a href="'.$link.'">' .$node['slug']['name'].'</a>';
                }


            );
            $tree = $repo->buildTreeArray($pages);
            $htmlTree = $this->tree->buildTreeHtmlAdmin($tree, $options);
        }else{
            $htmlTree = null;
        }

        return $htmlTree;
    }
    

    public function getPage($id){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Page');
        $page = $repo->findOneById($id);
        
        return $page;
    }
    
    public function lang(Environment $environment){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Lang');
        $langs = $repo->findAll();
        
        return $environment->render('@MiuzeAdmin/Pages/lang.html.twig', array(
            'langs' => $langs
        ));
    }
    
    public function langPrint(){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Lang');
        $langs = $repo->findAll();
        
        return $langs;
    }
    
    public function getUrl($page){
        if($page->getLink() !== null){
            $url =  $page->getLink();
        }elseif($page->getType() == 'index'){
            $url = $this->router->generate('index');
        }else{
            $url = $this->router->generate($page->getType(), ['pageid' => $page->getId(), 'title' => $page->getSeoUrl()]);
        }
        return $url;
    }
    
    public function gUrlId($id){
        $page = $this->doctrine->getRepository('MiuzeAdminBundle:Page')->findOneById($id);
        if($page->getLink() !== null){
            $url =  $page->getLink();
        }elseif($page->getType() == 'index'){
            $url = $this->router->generate('index');
        }else{
            $url = $this->router->generate($page->getType(), ['pageid' => $page->getId(), 'title' => $page->getSeoUrl()]);
        }
        return $url;
    }
    
    public function getParam($name){
        $param = $this->container->getParameter($name);
        return $param;
    }
    
    public function breadcrumbs($page){
        if($page->getId() !== null){
            $breadcrumbs = $this->doctrine->getRepository('MiuzeAdminBundle:Page')->getPath($page);
        }else{
            $breadcrumbs = null;
        }
        
        return $breadcrumbs;
    }
}
