<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class VideoExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
        $this->doctrine = $doctrine;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_video_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('get_video', array($this, 'getVideo'), array('is_safe' => array('html'))),
        );
    }

    public function getVideo($limit = 3){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Video');
        $v = $repo->getVideo(array(
            'limit' => $limit
        ));
        return $v;
    }
    
}
