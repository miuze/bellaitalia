<?php

namespace Miuze\AdminBundle\Twig\Extension;

use Miuze\AdminBundle\Entity\Save;
use Miuze\AdminBundle\Form\Save\SaveType;

use Miuze\AdminBundle\Entity\Contact;
use Miuze\AdminBundle\Form\Contact\ContactType;
/**
 * Description of PageExtension
 *
 * @author User
 */
class FormExtension extends \Twig_Extension{
    
    private $container;
    
    private $save;
    
    private $contact;
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct($container) {
        $this->container = $container;
        $this->save = new Save();
        $this->contact = new Contact();
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_form_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('form_save', array($this, 'save'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('form_contact', array($this, 'contact'), array('is_safe' => array('html'))),
        );
    }
    
    public function save(){
        $form = $this->container->get('form.factory')->create( SaveType::class, $this->save, [
            "method" => "post",
            "action" => $this->container->get('router')->generate("save-ajax")
        ]);
        return $this->environment->render('MiuzePageBundle:Save:form.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    public function contact(){
        $form = $this->container->get('form.factory')->create( ContactType::class, $this->contact, [
            "method" => "post",
            "action" => $this->container->get('router')->generate("contact-ajax"),
            "attr" => array(
                "id" => "contactForm",
                "class" => "contact-form"
            )
        ]);
        return $this->environment->render('MiuzePageBundle:Contact:ajax-form.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
