<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class PlaceExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
        $this->doctrine = $doctrine;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_place_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('get_places', array($this, 'getPlaces')),
            new \Twig_SimpleFunction('get_place', array($this, 'getPlace')),
            new \Twig_SimpleFunction('get_latlng', array($this, 'getLatLng')),
        );
    }
    
    public function getPlace($id){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Place');
        $v = $repo->findOneById($id);
        
        return $v;
    }
    
    public function getPlaces($cat, $limit = 3){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Place');
        $v = $repo->getPlace(array(
            'cat' => $cat,
            'limit' => $limit
        ));
        
        return $v;
    }
    
    public function getLatLng($cat, $limit = 3){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Place');
        $v = $repo->getLatLng(array(
            'cat' => $cat,
            'limit' => $limit
        ));
        
        return $v;
    }
    
}
