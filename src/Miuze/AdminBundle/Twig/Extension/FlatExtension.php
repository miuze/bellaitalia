<?php

namespace Miuze\AdminBundle\Twig\Extension;

use Miuze\AdminBundle\Entity\Flat;
use Miuze\AdminBundle\Form\Flat\SearchType;
/**
 * Description of PageExtension
 *
 * @author User
 */
class FlatExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    private $container;

    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine, $container) {
        $this->doctrine = $doctrine;
        $this->container = $container;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_flat_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('get_flats', array($this, 'getFlats')),
            new \Twig_SimpleFunction('render_flat_search_form', array($this, 'renderFlatSearchForm'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('render_flat_item', array($this, 'renderFlatItem'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('flat_get_last_flats', array($this, 'getLastFlats'), array('is_safe' => array('html'))),
        );
    }

    
    public function getFlats($limit = 2){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Flat');
        $photo = $repo->getFlats(array('limit' => $limit));
        
        return $photo;
    }

    public function renderFlatSearchForm($page, $template = 'form-fullwidth.html.twig'){
        if(!is_object($page)){
            $page = $this->doctrine->getRepository('MiuzeAdminBundle:Page')->findOneById($page);
        }
        if (is_null($page)) {
            return;
        }
        $form = $this->container->get('form.factory')->create( SearchType::class, new Flat() , [
            "method" => "get",
            "action" => $this->container->get('router')->generate("search", ['slug' => $page->getSlug()->getSlug()])
        ]);
        return $this->environment->render('@MiuzePage/Flat/partials/'.$template, array(
            'form' => $form->createView()
        ));
    }

    public function renderFlatItem($page, $flat, $template = 'flatItem.html.twig'){
        if(!is_object($page)){
            $page = $this->doctrine->getRepository('MiuzeAdminBundle:Page')->findOneById($page);
        }
        if($page == null){
            return '';
        }
        return $this->environment->render('@MiuzePage/Flat/partials/'.$template, array(
            'page' => $page,
            'flat' => $flat
        ));
    }


    public function getLastFlats($page, $limit = 9, $template = 'flat.html.twig'){
        if(!is_object($page)){
            $page = $this->doctrine->getRepository('MiuzeAdminBundle:Page')->findOneById($page);
        }

        $flats = $this->doctrine->getRepository('MiuzeAdminBundle:Flat')->getLastFlats(array(
            'page' => $page->getId(),
            'limit' => $limit,
        ));
        if($page == null || empty($flats)){
            return null;
        }
        return $this->environment->render('@MiuzePage/Flat/partials/'.$template, array(
            'page' => $page,
            'flats' => $flats
        ));
    }

}
