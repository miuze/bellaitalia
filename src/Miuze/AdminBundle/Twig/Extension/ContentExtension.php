<?php

declare(strict_types=1);

namespace Miuze\AdminBundle\Twig\Extension;

use Twig\Environment;

class ContentExtension extends \Twig_Extension
{
    public function getName(): string
    {
        return 'miuze_content_extension';
    }

    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction(
                'render_content',
                [$this, 'render'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
        );
    }
    
    public function render(Environment $environment, ?string $string){

        if (is_null($string)) {
            return '';
        }

        preg_match('/{{\ [a-z\_]*\(([1-9, \,]*)\) }}/', $string, $matches);
        if (empty($matches)) {
            return $string;
        }

        foreach ($matches as $item) {
            $result = 'Test';
//            render('@MiuzePage/common/twig/content.html.twig', ['function' => $item ]);
            $string = str_replace($item, $result, $string);
        }

        return $string;
    }
}
