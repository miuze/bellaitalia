<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class GalleryExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
        $this->doctrine = $doctrine;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_gallery_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('get_gallery', array($this, 'gallery')),
            new \Twig_SimpleFunction('get_photo', array($this, 'getPhoto')),
        );
    }

    public function gallery($limit = 10){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Gallery');
        $news = $repo->getGallery(array('limit' => $limit));
        
        return $news;
    }
    
    public function getPhoto($limit = 10){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:GalleryPhoto');
        $photo = $repo->getPhoto(array('limit' => $limit));
        
        return $photo;
    }
    
   
    
}
