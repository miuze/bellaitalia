<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class BannerExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
        $this->doctrine = $doctrine;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_banner_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('banners', array($this, 'banners'), array('is_safe' => array('html'))),
        );
    }

    public function banners($cat){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:BannerCategory');
        $banners = $repo->getBanners(array('cat' => $cat));
        
        return $banners;
    }
    
}
