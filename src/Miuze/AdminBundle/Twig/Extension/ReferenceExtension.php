<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class ReferenceExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;

    private $container;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine, $container) {
        $this->doctrine = $doctrine;
        $this->container = $container;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_reference_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('reference_render_reference', array($this, 'renderSection'), array('is_safe' => array('html'))),
        );
    }


    public function renderSection($page = 1, $limit = 5, $template = 'section.html.twig' ){
        if(!is_object($page)){
            $page = $this->doctrine->getRepository('MiuzeAdminBundle:Page')->findOneById($page);
        }
        $references = $this->doctrine->getRepository('MiuzeAdminBundle:Reference')->getLastReferences(array(
            'limit' => $limit,
        ));
        if($page == null || empty($references)){
            return null;
        }
        return $this->environment->render('@MiuzePage/Reference/partials/'.$template, array(
            'references' => $references
        ));
    }
    
}
