<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class FaqExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
        $this->doctrine = $doctrine;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_faq_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('get_faq', array($this, 'getFaq'), array('is_safe' => array('html'))),
        );
    }

    public function getFaq($cat, $limit = 3){
        $repo = $this->doctrine->getRepository('MiuzeAdminBundle:Faq');
        $v = $repo->getFaq(array(
            'cat' => $cat,
            'limit' => $limit
        ));
        
        return $v;
    }
    
}
