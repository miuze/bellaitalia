<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class CartExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $session;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;
    
    function __construct(\Symfony\Component\HttpFoundation\Session\Session $session) {
        $this->session = $session;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_cart_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('get_cart', array($this, 'cart'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('get_cartLength', array($this, 'cartLength'), array('is_safe' => array('html'))),
        );
    }

    public function cart(){
        if($this->session->has('cart')){
            $cart = $this->session->get('cart');
        }else{
            $cart = array();
        }
        
        return $this->environment->render('MiuzePageBundle:common:widget/cart.html.twig', array(
            'cart' => $cart
        ));
    }
    
    public function cartLength(){
        if($this->session->has('cart')){
            $cart = $this->session->get('cart');
        }else{
            $cart = array();
        }
        
        $length = count($cart);
        return $length;
    }
    
}
