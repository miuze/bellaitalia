<?php

namespace Miuze\AdminBundle\Twig\Extension;

/**
 * Description of PageExtension
 *
 * @author User
 */
class SectionExtension extends \Twig_Extension{
    
    /**
     *
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;
    
    /**
     *
     * @var \Twig\Environment
     */
    private $environment;

    private $container;
    
    function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine, $container) {
        $this->doctrine = $doctrine;
        $this->container = $container;
    }
    public function initRuntime(\Twig_Environment $environment){
        $this->environment = $environment;
    }

    public function getName(){
        return 'miuze_section_extension';
    }
    
    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('section_render_section', array($this, 'renderSection'), array('is_safe' => array('html'))),
        );
    }


    public function renderSection($id = 1){
        $section = $this->doctrine->getRepository('MiuzeAdminBundle:Section')->findOneBy(array(
            'id' => $id,
        ));
        if($section === null){
            return null;
        }
        return $section->getContent();
    }
    
}
