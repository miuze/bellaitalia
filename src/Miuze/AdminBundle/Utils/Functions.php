<?php

namespace Miuze\AdminBundle\Utils;

class Functions {
        
    static function no_pl($tekst) {
        $tabela = Array(
            //WIN
            "\xb9" => "a", "\xa5" => "A", "\xe6" => "c", "\xc6" => "C",
            "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
            "\xf3" => "o", "\xd3" => "O", "\x9c" => "s", "\x8c" => "S",
            "\x9f" => "z", "\xaf" => "Z", "\xbf" => "z", "\xac" => "Z",
            "\xf1" => "n", "\xd1" => "N",
            //UTF
            "\xc4\x85" => "a", "\xc4\x84" => "A", "\xc4\x87" => "c", "\xc4\x86" => "C",
            "\xc4\x99" => "e", "\xc4\x98" => "E", "\xc5\x82" => "l", "\xc5\x81" => "L",
            "\xc3\xb3" => "o", "\xc3\x93" => "O", "\xc5\x9b" => "s", "\xc5\x9a" => "S",
            "\xc5\xbc" => "z", "\xc5\xbb" => "Z", "\xc5\xba" => "z", "\xc5\xb9" => "Z",
            "\xc5\x84" => "n", "\xc5\x83" => "N",
            //ISO
            "\xb1" => "a", "\xa1" => "A", "\xe6" => "c", "\xc6" => "C",
            "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
            "\xf3" => "o", "\xd3" => "O", "\xb6" => "s", "\xa6" => "S",
            "\xbc" => "z", "\xac" => "Z", "\xbf" => "z", "\xaf" => "Z",
            "\xf1" => "n", "\xd1" => "N");

        return strtr($tekst, $tabela);
    }

    function slugify($string) {
        $string = self::no_pl($string);
        $string = trim($string);
        $string = strtolower($string);
        $string = preg_replace('/[^0-9a-z\-]+/', '-', $string);
        $string = preg_replace('/[\-]+/', '-', $string);
        $string = trim($string, '-');
        return $string;
    }

    function slugifyFile($string) {
        $string = self::no_pl($string);
        $string = trim($string);
        $string = strtolower($string);
        $string = preg_replace('/[^0-9a-z\-\.]+/', '-', $string);
        $string = preg_replace('/[\-]+/', '+', $string);
        $string = trim($string, '-');
        return $string;
    }
    
    static function WordLimiter($text, $limit = 20) {
        $explode = explode(' ', $text);
        $string = '';
        $explode = array_slice($explode,0,$limit);
        $dots = '...';
        if (count($explode) <= $limit) {
            $dots = '';
        }
        foreach ($explode as $value) {
            $string .= $value . " ";
        }

        return $string . $dots;
    }
    
    function youtubeConverter($string){
        $ytarray= explode("/", $string);
        $ytendstring=end($ytarray);
        $ytendarray=explode("?v=", $ytendstring);
        $ytendstring=end($ytendarray);
        $ytendarray=explode("&", $ytendstring);
        return 'http://www.youtube.com/embed/'.$ytendarray[0].'';
    }
    
    static function getGeo($address){

        $address = str_replace(" ", "+", $address);
        try {
            $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $address . "&sensor=false");
            $json = json_decode($json);
            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
        } catch (\Exception $e) {
            $lat = '';
            $long = '';
        }
        return array('lat' => $lat, 'long' => $long);

    }
}
